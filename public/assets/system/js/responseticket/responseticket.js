 var  statusaprove = "" ;
 var  idcategory   = "" ;

$(document).ready(function() {



  $(document).on("click",".bt-response" ,function(){  


         var id       = $(this).data('value');
         statusaprove = $(this).data('status');
         idcategory   = $(this).data('category');
         idticket     = id;
         var profile  = $("#profile").val();
        
         response(id);
 
          var url=window.location.href; //reuse edit
          url = url.replace('#custom-tabs-response',''  );
          url = url.replace('ticket','teamcategory/find'  );


          datateamcategory = {
            'idteam'     : $("#pteamid").val(),
            'idcategory' : idcategory,
          };

 
          if(profile == "MANAGER" || profile == "ADMIN" ){


               $.ajax({ //task-356 03/11/2020
                        cache:false,
                        type: 'POST',
                        url: url,
                        data: {data:datateamcategory},
                        dataType: "json",
                        success: function(data) {


                          statusaprove =statusaprove.replace('""','');

                          if(data.custom.count > 0 && data.success){

                              //task 212 -  André - 11/08/2020 
                             if(statusaprove == "Aguardando Aprovação" || statusaprove == "Reprovado" ){
                                 $(".bt-add-response").addClass('d-none');
                                 $('.select-status').attr('disabled', 'disabled');
                             }else{
                                $(".bt-status").addClass('d-none');
                                $(".bt-add-response").removeClass('d-none');
                                $('.select-status').removeAttr('disabled');
                             }   

                         }else if(data.custom.count == 0 && data.success == false  ){ //task-356
                              
                              //task 212 -  André - 11/08/2020 
                             if(statusaprove == "Aguardando Aprovação" || statusaprove == "Reprovado" ){

                                  var tr     = $(this),
                                  show       = tr.data('show'),
                                  hide       = tr.data('hide');

                                  $.jAlert({

                                  'title':'Oppssss!',
                                  'content':'Gerente/Atendente sem permissão para aprovar, times diferentes da categoria do ticket',
                                      'theme': 'yellow',
                                      'showAnimation' : show,
                                      'hideAnimation' : hide,
                                      'btns': { 'text': 'Fechar' },       
                                            'onOpen': function(alert){

                                                window.setTimeout(function(){
                                                 $(".bt-status").addClass('d-none');
                                                 $('.select-status').attr('disabled', 'disabled');                                            
                                                    alert.closeAlert();
                                                }, 3000);
                                            }
                                   });

                             }else{
                                $(".bt-status").addClass('d-none');
                                $(".bt-status").removeClass('d-none');
                                $(".bt-add-response").removeClass('d-none');
                             }  
                         }else{

                            console.log(data , "Erro ao carregar Aprovado/reprovado" )
                         }

                        }  
                    });

             }else{

                $(".bt-status").addClass('d-none');

             }



   
  

         if (typeof(team) === 'undefined') {
          $("#div-response").text('');
          $('#response-address').text('')
          //response(id);
         }

    })

$('#message-response-add').on('keydown',function(){


    if ($(".select-status").val() == "")
       $(".bt-add-response").prop('disabled', false);
    else
       $(".bt-add-response").prop('disabled', true);


})

$(".select-status").on('change',function(){



      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000
      });

      var status = $(this).val();
      
      var tr     = $(this),
      show       = tr.data('show'),
      hide       = tr.data('hide');



      //validate Fields
      if($('#message-response-add').val() == "" &&   $(".select-status").val() == "Finalizado" ){
          //first param name field error and second parameter name BOX message 
          validateFields('message-response-add','Resposta');
          $(".bt-add-response").prop('disabled', true);
          $(".select-status").val("");
          return;      
      }

            //validate Fields
      if($('#message-response-add').val() == "" && $(".select-status").val() == "Concluido"  ){
          //first param name field error and second parameter name BOX message 
          validateFields('message-response-add','Resposta');
          $(".bt-add-response").prop('disabled', true);
          $(".select-status").val("");
          return;      
      }

    if(statusticket=="Encerrado" || statusticket=="Cancelado"  || statusticket=="Finalizado" )
    {

        $.jAlert({

        'title':'Oppssss!',
        'content':'Esse tícket já teve uma alteração como '+ statusticket +' não sendo mais possível trocar seu status! favor contactar o admin do sistema. ',
            'theme': 'yellow',
            'showAnimation' : show,
            'hideAnimation' : hide,
            'btns': { 'text': 'Fechar' },       
                  'onOpen': function(alert){
                      window.setTimeout(function(){
                          alert.closeAlert();
                      }, 5000);
                  }
         });


        return;

    }



      //confirm yes no
            $.jAlert({

              'title':'Alterar Status?',
              'type':'confirm',
              'content':'Deseja alterar o status do ticket? , essa ação só poderá ser desfeita pelo admin do sistema!',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {

                   //alter status ticket
                   data = {
                          'status'  : status,
                          'id'      : idticket,
                   };

                   updateStatus(data,idticket);
                   //end status  

                   //message ok
                    Toast.fire({
                      type: 'success',
                      title: 'Status '+ status + ' Alterado!'
                    })


                    //enable lock fields
                    $("#inputfile-response").prop("disabled",true);
                    $("#message-response-add").prop("disabled",true);
                    $(".bt-add-response").prop("disabled",true);

                    //add msg time line only upon
                    var url=window.location.href; //reuse edit
                    url = url.replace('#custom-tabs-response',''  );
                    url = url.replace('ticket','ticketresponse/add'  );                    

                    if(status=="Suspenso" ){
                        
                        var data = {
                            'message'  : 'Esse ticket teve o status modificado para '+ status + '  podendo dar continuidade para o mesmo!',
                            'idticket' : idticket,
                            'email'    : email,
                            'idteam'   : teamid,

                        };

                        var i;

 
                           $.ajax({
                                    cache:false,
                                    type: 'POST',
                                    url: url,
                                    data: {data:  data},
                                    dataType: "json",
                                    success: function(data) {


                                      if(data.success){
                                         Toast.fire({
                                            type: 'success',
                                            title: data.message
                                         })

                                             response(idticket); //update message view


                                      }else{
                                         Toast.fire({
                                            type: 'DANGER',
                                            title: data.message
                                         })
                                      }
                                    }  
                                 });
                         //end msg                      
                    }else{


                        var data = {
                            'message'  : 'Esse ticket teve o status modificado para '+ status + ' somente o admin poderá destravar esse ticket informe o id ao admin! <br><br> <b>Resposta do Usuário: '+ $('#message-response-add').val()+'</b>',
                            'idticket' : idticket,
                            'email'    : email,
                            'idteam'   : teamid,
                        };

                        console.log(data);


                       $.ajax({
                                cache:false,
                                type: 'POST',
                                url: url,
                                data: {data:  data},
                                dataType: "json",
                                success: function(data) {


                                  if(data.success){
                                     Toast.fire({
                                        type: 'success',
                                        title: data.message
                                     })

                                         response(idticket); //update message view
 

                                  }else{
                                     Toast.fire({
                                        type: 'DANGER',
                                        title: data.message
                                     })
                                  }
                                }  
                             });
                         //end msg    

                    }  
                                           window.setTimeout(function(){
                                             location.reload(); 
                                          }, 1000);   
                    return;
              },


            }); 
            
            $(".select-status").val("");
            $(".bt-add-response").prop('disabled', false);


    })





//---------------------------------------------------------------edit customer mysql  
  $(document).on("click",".bt-aprove-ticket,.bt-reprove-ticket" ,function(){  


            var status = $(this).data('status');      


            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000
            });
        
            if(status == 1)
              var content = "Deseja Realmente Aprovar?";
            else if(status == 0 )
              var content = "Deseja Realmente Reprovar?";


            if(status == 0 && $("#category-obs-add").val() == "" ){
                //first param name field error and second parameter name BOX message 
                validateFields('category-obs-add','Observação');
                return;      
            }else{
              $("#message-response-add").val("Aprovado");
            }

            obs = $("#category-obs-add").val();

            var tr  = $(this),
              show    = tr.data('show'),
              hide    = tr.data('hide');

            //confirm yes no
            $.jAlert({

              'title':'Aprovar/Reprovar?',
              'type':'confirm',
              'content':content,
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {


                    if(status ==1 ){
                       //alter status ticket
                       var data = {
                              'status'     : 'Aprovado',
                              'id'         : idticket,
                              'email'      : email,
                              'idcategory' : categoryid

                       };


                      var data_add = {
                          'message'  : "Aprovado Motivo :Aprovado pelo GESTOR!",
                          'idticket' : idticket,
                          'email'    : email,
                          'idteam'   : teamid,
                          

                      };


                    }else if(status == 0){

                         var data = {
                              'status'     : 'Reprovado',
                              'id'         : idticket,
                              'email'      : email, //task 304
                              'idcategory' : categoryid, // task 304
                              'obs'        : obs
                       };


                      var data_add = {
                          'message'  : "Reprovado Motivo :" + $("#category-obs-add").val(),
                          'idticket' : idticket,
                          'email'    : email,
                          'idteam'   : teamid,
                          
                      };


                    }   

                      updateStatus(data,idticket); 


                      var url=window.location.href; //reuse edit
                      url = url.replace('#custom-tabs-response',''  );
                      url = url.replace('ticket','ticketresponse/add'  );


                     $.ajax({
                              cache:false,
                              type: 'POST',
                              url: url,
                              data: {data:  data_add},
                              dataType: "json",
                              success: function(data) {


                                if(data.success){
                                   var lastid   = data.custom.lastid;

                                   Toast.fire({
                                      type: 'success',
                                      title: data.message
                                   })

                                     

                                     response(idticket); //update message view


                                   window.setTimeout(function(){
                                       location.reload();
                                   }, 1000);    

                               }else{
                                   Toast.fire({
                                      type: 'error',
                                      title: data.message
                                   })
                                }
                              }  
                          });
         
              },

            });



  })  



//name file upload responseticket
$('#inputfile-response').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                fileName = fileName.replace("C:\\fakepath\\" ,"");
                //replace the "Choose a file" label
                $(this).next('.custom-file-label-response').html("Arquivo Selecionado: "+fileName);

})

//clear duplicate label ticketresponse
$("#tab-branchoffice").on('click',function(){

  $('#response-address').text('');

})



  $(".bt-add-response").on('click',function(){

    //validate Fields
     /* if($('#message-response-add').val() == "" ){
          //first param name field error and second parameter name BOX message 
          validateFields('message-response-add','Resposta Chamado');
          return;      
      }*/

      //clear div
      //$("#div-response").text('');
      //$('#response-address').text('')
      var data_add = {
          'message'  : $('#message-response-add').val(),
          'idticket' : idticket,
          'email'    : email,
          'idteam'   : teamid,

      };

      //get prfile log in 
      var profile = $("#profile").val();
      
      //upload response
      var file = [];
      file.push($('#inputfile-response').get(0).files[0]);

      var url=window.location.href; //reuse edit
      url = url.replace('#custom-tabs-response',''  );
      url = url.replace('ticket','ticketresponse/add'  );

      var tr  = $(this),
      show    = tr.data('show'),
      hide    = tr.data('hide');

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000
      });

      //confirm yes no
            $.jAlert({

              'title':'Responder Chamado?',
              'type':'confirm',
              'content':'Deseja responder o ticket?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {

                 $.ajax({
                          cache:false,
                          type: 'POST',
                          url: url,
                          data: {data:  data_add},
                          dataType: "json",
                          success: function(data) {


                            if(data.success){
                               var lastid   = data.custom.lastid;

                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                               //if($('#message-response-add').val() != "" )


                                //SOLICITANTE not response your ticket only other profiles
                                if($('#profile').val() == "REQUESTER" ){ //Trello 227 - André - 31/07

                                  //alter status ticket
                                   data = {
                                        'status'  : 'Aguardando Atendimento',
                                        'id'      : idticket,
                                        
                                   };

                                }else{

                                   //alter status ticket
                                   data = {
                                        'status'  : 'Em Andamento',
                                        'id'      : idticket,
                                        
                                   };

                                }

                                  

                                 $('#response-address').text('');

                               if(idticket != '') 
                                 updateStatus(data,idticket);
                                 //end status  


                               if (typeof file[0] !== 'undefined' ) // if file exists
                                  upload(file,"",lastid);
                                 

                                 response(idticket); //update message view


                               $('#message-response-add').val('');


                               

                               window.setTimeout(function(){
                                   location.reload();
                               }, 1000);   

                           }else{
                               Toast.fire({
                                  type: 'error',
                                  title: data.message
                               })
                            }
                          }  
                      });
              },
            });          


  })



});


function response(id){

    viewticket(id);


    //clear div
    //$("#div-response").text('');
    //$('#response-address').text('')

    var url=window.location.href; //reuse edit
    url = url.replace('#custom-tabs-response',''  );
    url = url.replace('ticket','ticketresponse'  );
    //clear update
    
    //console.log(url,"chegou");
    //return;

     $.ajax({
                    cache:false,
                    type: 'GET',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {
                        


                      if(data.success){


                                  var timestamp = '';
                                  var formattedDate = '';
                                  var formattedTime = '';
                                  var lastdate='';
                                  var j=0;

                                   //created = created;
                                   created = moment(created).format('DD/MM/YYYY HH:m a ');
                                   

                                  $('#date-response').text('Abertura do Chamado: '+created);


                                 viewticket(id);                                  

                                 if(typeof(team) === 'undefined'){


                                    team     = '';
                                    priority = '';
                                    phone    = '';
                                    email    = '';
                                    message  = ''; 
                                    created  = '';

                                    //clear div
                                    //$("#div-response").text('');
                                    //$('#response-address').text('')
                                    return;
                                        /* $('#response-address').append(
                                            '<br>'+
                                                  'Erro ao carregar os dados na conexão,clique em listar chamados  e  entre novamente!'      
                                                  
                                        );   
                                       */ 

                                 }else{



                                      if(createdresponse==null){
                                         createdresponse  = 'Ainda não houve resposta';
                                      }else{
                                        createdresponse = createdresponse;
                                         createdresponse = moment(createdresponse).format('DD/MM/YYYY HH:m a ');

                                      }

                                      if(id!=idticket)
                                        response(id)

                                      //console.log($('#response-address').val())

                                      $('#response-address').append(
                                            '<br>'+
                                                  'Id: ' + idticket  +
                                                  '<br>' +                                            
                                                  'Time: '  + team  +
                                                  '<br>' +
                                                  'Prioridade: ' + priority +
                                                  '<br>' +
                                                  'Status:<a style="color:red"> ' + statusticket +'</a>'+
                                                  '<br>' +
                                                  'Últ. Data Resposta:<a style="color:red"> ' + createdresponse +'</a>'+      
                                                  '<br>' +                                              
                                                  'Telefone: ' + phone +
                                                  '<br>' +
                                                  'Email:' + email+
                                                  '<br>' +
                                                  'Problema:' + message 
                                        );                                  
                                }





                                  //clear div
                                  $("#div-response").text('')

                                  var fileupload = '' ;          
                                  var image = '';
                                  var html="";
                                 //var fileimages =[];

              

                                  var url=window.location.href; //reuse edit
                                  url = url.replace('#custom-tabs-response',''  );
                                  url = url.replace('ticket','ticket/liststicket');


                                  //message ticket
                                   $.ajax({
                                    cache:false,
                                    type: 'GET',
                                    url: url,
                                    data: {id:  id},
                                    dataType: "json",
                                    success: function(data) {

                                               timestamp = data.custom.query[0].created;
                                               formattedDate = moment(timestamp).format('DD/MM/YYYY');
                                               formattedTime = moment(timestamp).format('HH:mm a');

                                             
                                                //console.log(data.custom.query[0].message);

                                                    $("#div-response").append(

                                                        '<div class="timeline">'+

                                                          '<div class="time-label">'+
                                                            '<span class="bg-red">'+formattedDate+'</span>'+
                                                         ' </div>'+

                                                          '<div id="first-div'+j+'"">'+
                                                            '<i class="fas fa-user bg-green"></i>'+
                                                            '<div class="timeline-item" style="width:800px" >'+
                                                              '<span class="time"><i class="fas fa-clock"></i>'+formattedTime+'</span>'+
                                                              '<h3 class="timeline-header no-border"><a href="#">' + (data.custom.query[0].userresponse ? data.custom.query[0].userresponse.replace(",", "<br>") : data.custom.query[0].name) + '</a> '+data.custom.query[0].message+'</h3>'+
                                                            '</div>'+
                                                         ' </div>'+
                     
                                                         
                                                    '</div>'); 



                                                    //get images
                                                    $.each(data['custom']['filepath']['query'], function (k, value) {

                                                        timestampfile = value.created;
                                                        formattedDateFile = moment(timestampfile).format('DD/MM/YYYY');
                                                        formattedTimeFile = moment(timestampfile).format('HH:mm:ss a ');

                                                        html = html + '<a href='+value.path+'   target="_blank">&nbsp;&nbsp<img src='+value.path+' title='+formattedTimeFile+' width="100px" heigth="100px" ></a>';
           

                                                    });

                                                      //explode images
                                                      if(html != ""){
                                                           $("#first-div"+j).append(
                                                              '<div class="timeline">'+

                                                                '<div>'+
                                                                  '<i class="fas fa-user bg-green fa-camera bg-purple"></i>'+
                                                                  '<div class="timeline-item" style="width:800px">'+
                                                                    //'<span class="time"><i class="fas fa-clock"></i>'+formattedTimeFile+'</span>'+
                                                                    '<h3 class="timeline-header no-border"><a href="#">'+data.custom.query[0].name+'</a> Upload de Imagens</h3>'+
                                                                '<div class="timeline-body">'+
                                                                  html+
                                                                '</div>'+                                             
                                                                 '</div>'+
                                                                '</div>'+
                                                               
                                                          '</div>');   
                                                      }

                                        

                                
                          }

                         });   
                        


                        lastdate = "";

                        //message response ticket
                        $.each(data['custom']['query'], function (i, item) {


                                     timestamp = item.created;
                                     formattedDate = moment(timestamp).format('DD/MM/YYYY');
                                     formattedTime = moment(timestamp).format('HH:mm A');



                                     
                                      if(lastdate != formattedDate )   
                                      {   

                                         j++;
                                          $("#div-response").append(

                                              '<div class="timeline">'+

                                                '<div class="time-label">'+
                                                  '<span class="bg-red">'+formattedDate+'</span>'+
                                               ' </div>'+

                                                '<div id="first-div'+j+'"">'+
                                                  '<i class="fas fa-user bg-green"></i>'+
                                                  '<div class="timeline-item" style="width:800px" >'+
                                                    '<span class="time"><i class="fas fa-clock"></i>'+formattedTime+'</span>'+
                                                    '<h3 class="timeline-header no-border"><a href="#">' + (item.userresponse ? item.userresponse.replace(",", "<br>") : item.name) + '</a> '+item.message+'</h3>'+
                                                  '</div>'+
                                               ' </div>'+
           
                                               
                                          '</div>'); 
                                    }else{

                                        if(j>=1 && lastdate == formattedDate ){

                                          $("#first-div"+j).append(
                                              '<div class="timeline">'+

                                                '<div>'+
                                                  '<i class="fas fa-user bg-green"></i>'+
                                                  '<div class="timeline-item" style="width:800px">'+
                                                    '<span class="time"><i class="fas fa-clock"></i>'+formattedTime+'</span>'+
                                                    '<h3 class="timeline-header no-border"><a href="#">' + (item.userresponse ? item.userresponse.replace(",", "<br>") : item.name) + '</a> '+item.message+'</h3>'+
                                                  '</div>'+
                                                '</div>'+
                                               
                                          '</div>');        
                                        }

                                    }

                                    lastdate = formattedDate
                        });                          
                           
                              //task 212 -  André - 11/08/2020 
                             if(statusaprove == "Aguardando Aprovação" )
                               $(".bt-status").removeClass("d-none");
                             else
                               $(".bt-status").addClass("d-none");
                                 

                                 
                             $("#tab-response-ticket").tab('show'); 
                      }
                        


                    }
                });

}



function viewticket(id){

      $('#response-address').text('');  
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000
      });
  
    var url=window.location.href+'/view';
    url = url.replace('#custom-tabs-response',''  );

     $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {


                      if(data.success){


                         console.log(data['custom']['query'][0].id,"--",id)

                            idticket        = data['custom']['query'][0].id;
                            categoryid      = data['custom']['query'][0].idcategory;
                            phone           = data['custom']['query'][0].phone;
                            email           = data['custom']['query'][0].email;
                            priority        = data['custom']['query'][0].nametype;
                            message         = data['custom']['query'][0].description;
                            team            = data['custom']['query'][0].name;
                            teamid          = data['custom']['query'][0].idteam;
                            statusticket    = data['custom']['query'][0].status;
                            created         = data['custom']['query'][0].created;  
                            createdresponse = data['custom']['query'][0].createdresponse;  
                        


                     

                        if(statusticket=="Encerrado" || statusticket=="Cancelado" || statusticket=="Finalizado" )
                        {
                          $("#inputfile-response").prop("disabled",true);
                          $("#message-response-add").prop("disabled",true);
                          $(".bt-add-response").prop("disabled",true);
                        }else{
                          $("#inputfile-response").prop("disabled",false);
                          $("#message-response-add").prop("disabled",false);
                          $(".bt-add-response").prop("disabled",false);                          
                        }  

                     }else{

                       Toast.fire({
                         type: 'DANGER',
                         title: data.message
                       }) 

                     }
                     

                    }
                });

}


function updateStatus(data,id){
      
     const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000
      });

     var statusupdate  = data.status ; 
     var createdupdate;


      var url=window.location.href+'/updateStatus'; //reuse edit
      url = url.replace('#custom-tabs-response',''  );
     

      /*var data = {
            'status'  : statusupdate,
            'id'      : idticket,
       };*/


       $.ajax({
                cache:true,
                type: 'POST',
                url: url,
                data: {data:  data},
                dataType: "json",
                success: function(data) {

                  if(data.success){
                  
                    var dateupdate = new Date();
                      var day = dateupdate.getDate();
                      var month = dateupdate.getMonth() + 1;
                      var year = dateupdate.getFullYear();
                      var hour = dateupdate.getHours();
                      var minutes = dateupdate.getMinutes();
                      var seconds = dateupdate.getSeconds();

                      viewticket(id);
                      createdupdate = day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds;

                                //clear div
                    //$("#div-response").text('');
                    //$('#response-address').text('');


                    $('#response-address').append(
                                '<br>'+
                                      'Id: '      + idticket  +
                                      '<br>' +
                                      'Time: '      + team  +
                                      '<br>' +
                                      'Prioridade: ' + priority +
                                      '<br>' +
                                      'Status:<a style="color:red"> ' + statusupdate +'</a>'+
                                      '<br>' +
                                      'Última Resposta:<a style="color:red"> ' + createdupdate +'</a>'+                                      
                                      '<br>' +                                              
                                      'Telefone: ' + phone +
                                      '<br>' +
                                      'Email:' + email+
                                      '<br>' +
                                      'Problema:' + message 
                     );                                  
                                 

                  }else{
                    
                     Toast.fire({
                        type: 'DANGER',
                        title: data.message
                     })
                  }
                }  
            });

       
}

