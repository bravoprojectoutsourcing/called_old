$(document).ready(function() {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1000
    });

    $(document).on('click', '.edit-team', function() {
        var id=$(this).data('value');
        var name = $('#time'+id).text();
        $('#nameTeam-edit').val(name);
        $("#id-modal-edit").val(id);        
    });

    $('#btn-edit').on('click',function(){

        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });   
    
        var url=window.location.href+'/edit';    
        //validate Fields
        if($('#nameTeam-edit').val() == "" ){
            //first param name field error and second parameter name BOX message 
            validateFields('nameTeam-edit','nome do time');
            return;      
        }    
    
        var data = {    
            'id'          : $('#id-modal-edit').val(),
            'name'        : $('#nameTeam-edit').val()
        };       
    
        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data},
            dataType: "json",
            success: function(data) {   
                if(data.success && data.redirect == ""){
                    Toast.fire({
                        type: 'success',
                        title: data.message
                    })    
                    $('#nameTeam-edit').val('');

                    window.setTimeout(function(){
                        location.reload();
                    }, 1000);
    
                }else{    
                    if(data.redirect=='1'){
                        Toast.fire({
                            type: 'warning',
                            title: data.message
                        })
                    }else{
                        Toast.fire({
                            type: 'danger',
                            title: data.message
                        })
                    }                      
                }
            }
        });
        return false;   
    });

    $(document).on("click",".bt-del" ,function(){

        var id=$(this).data('value');           

        var url=window.location.href+'/del';

        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');

        //confirm yes no
        $.jAlert({
            'title':'Excluir?',
            'type':'confirm',
            'content':'Deseja realmente excluir ?',
            'theme': 'blue',
            'showAnimation' : show,
            'hideAnimation' : hide,
            'confirmBtnText': "Sim, com Certeza!",
            'denyBtnText': 'Não, Prefiro Não!',
            'onConfirm': function() {
                $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {
                        if(data.success){
                            Toast.fire({
                              type: 'success',
                              title: data.message
                            })
                            
                            window.setTimeout(function(){
                                location.reload();
                            }, 3000);
                        }else{
                           Toast.fire({
                              type: 'DANGER',
                              title: data.message
                           })
                        }
                    }  
                });
            },
        });          
    });

    $(document).on("click",".addTeamUser" ,function(){
        var team = $('#notHave').val();
        var user = $('#idUserTeam').val();
        var url=window.location.href+'/adduserteam';
        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        var data = {
            'team' : team,
            'user' : user
        };

        if(data.team==null){
            validateFields('notHave','time');
            return
        }
        
        if($('#notHave').val() == "" ){
            //first param name field error and second parameter name BOX message 
            validateFields(data.team,'time');
            return;      
        } 

        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data},
            dataType: "json",
            beforeSend: function(){
                $('body').css('cursor', 'progress')
            },
            success: function(data) {
                $('body').css('cursor', 'auto')
                getUserTeam(user)
            }
        });

    });

    $(document).on("click",".delUserTeam" ,function(){
        var team = $(this).data('value');
        var user = $('#idUserTeam').val();
        var url=window.location.href+'/deluserteam';
        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        var data = {
            'team' : team,
            'user' : user
        }; 

        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data},
            dataType: "json",
            beforeSend: function(){
                $('body').css('cursor', 'progress')
            },
            success: function(data) {
                $('body').css('cursor', 'auto')
                getUserTeam(user)
            }
        });

    });

    $(document).on("click",".addUserTeam" ,function(){
        var id=$(this).data('value');
        getUserTeam(id);
        
    });

    $('#btn-add').on('click',function(){

        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        var url=window.location.href+'/add';    
        //validate Fields
        if($('#name-add').val() == "" ){
            //first param name field error and second parameter name BOX message 
            validateFields('name-add','nome do time');
            return;      
        }    
        
        var data_add = {
            'name':$('#name-add').val()
        };               
    
        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data_add},
            dataType: "json",
            success: function(data) {    
                if(data.success && data.redirect == ""){   
                    Toast.fire({
                        type: 'success',
                        title: data.message
                    })    
    
                    $('#name-add').val('');


                    window.setTimeout(function(){
                        location.reload();
                    }, 1000);

                }else{   
                    if(data.redirect=='1'){
                        Toast.fire({
                        type: 'warning',
                        title: data.message
                        })
                    }else{
                        Toast.fire({
                        type: 'danger',
                        title: data.message
                        })
                    }                                
                }
            }
        });
        
        return false; 
    
      });

});

function getUserTeam(id){
    var url=window.location.href+'/userteam';
    $('#tabelaModal').html('');
    $('#notHave').html('');

        $.ajax({
            cache:false,
            type: 'GET',
            url: url,
            data: {id:  id},
            dataType: "json",
            beforeSend: function(){
                $('body').css('cursor', 'progress')
            },
            success: function(data) {
                $('body').css('cursor', 'auto')
                $('#idUserTeam').val(id);
                if(data.isntHave.length > 0){
                    $.each( data.isntHave, function( key, value ) {
                        $('#notHave').append('\
                            <option value="'+value.id+'">'+value.name+'</option>\
                        ');
                    });
                }else{
                    $('#notHave').append('\
                            <option disabled >Sem times a adicionar</option>\
                        ');
                }
                
                if(data.have.length > 0){
                    $.each( data.have, function( key, value ) {
                        $('#tabelaModal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td class="d-none">'+value.id+'</td>\
                            <td >'+value.name+'</td>\
                            <td style="text-align: center" colspan="2">\
                                <button type="button" class="btn btn-danger btn-sm delUserTeam" data-value="'+value.id+'"><i class="fas fa-trash"></i></button>\
                            </td>\
                        </tr>\
                        ');
                    });
                }else{
                    $('#tabelaModal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td colspan="2">Não há times cadastrados para usuário</td>\
                        </tr>\
                        ');
                }
                
            }
        });
}
