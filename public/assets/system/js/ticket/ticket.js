﻿var team;
var teamid;
var priority
var phone;
var email;
var message; 
var statusticket;
var createdresponse;//last date  && hour response
var created;
var idfirst;//id image first ticket
var idticket;
var categoryid;
var session = $("#sessionstatustime").val(); 
var secondtable; 
var firsttable;
var total=0;

 //vars datatable
 var rows;
 var namecategorynormal = "";
 var status;
 var colorstatus;
 var nameburdenurgency;
 var colornameburdenurgency;
 var colorpriority;
 var resttime;
 var colorresttime;
 var slastatus;
 var colorsla;
 var button;
 var colorbutton;
 var userresponse;
 
 var rowst2="";
 var namecategorynormalt2 = "";
 var statust2;
 var colorstatust2;
 var nameburdenurgencyt2;
 var colornameburdenurgencyt2;
 var colorpriorityt2;
 var resttimet2;
 var colorresttimet2;
 var slastatust2;
 var colorslat2;
 var buttont2;
 var colorbuttont2;
 var userresponse2;

//TASK-331 AJAX GET VALUE IF PIER CLICKED
if(session!="")
  getSLA(session); //load SLA GRAPH PIER
else
  getSLA(session); //load SLA

//load var firsttable datatable
tbl1();

//show progress bar task-355
$(".div-pb").removeClass('d-none');
$("#msg-pb").removeClass('d-none');
$("#msg-pb").text("0/"+total+" registros carregando...");
//progress bar


$(document).ready(function() {

      var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000
      });


    $(".select-company").select2();


    $(".select-branchoffice").select2();
    $(".select-company").select2({ width: '100%'});

    $(".select-departament").select2();
    $(".select-departament").select2({ width: '100%'});


    $(".select-category").select2();
    $(".select-category").select2({ width: '100%'});


    $(".select-typecategory").select2();
    $(".select-typecategory").select2({ width: '100%'});

    $(".select-severity").select2();
    $(".select-severity").select2({ width: '100%'});


    $(".select-team").select2();
    $(".select-team").select2({ width: '100%'});


    $(".select-priority").select2();
    $(".select-priority").select2({ width: '100%'});
//-------------------------------------------------------------------------select

  var lastid         = "";
  var pathfile       = "";
  var originalfile   = "";
  
  //tsk 212
  var statuscategory = "";

  $(".select-category").on('change',function(){

    statuscategory = $(this).find(':selected').data('value');
   
  })

  $('.bt-add').on('click',function(){


      //validate Fields
      if($('#phonecontact-add').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('phonecontact-add','Telefone');
          return;      
      }

      //validate Fields
      if($('#email-add').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('email-add','Email');
          return;      
      }

            //validate Fields
      if($('.select-company').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-company','Empresa');
          return;      
      }

      //validate Fields
      if($('.select-branchoffice').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-branchoffice','Estabelecimento');
          return;      
      }


      //validate Fields
      if($('.select-departament').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-departament','Departamento');
          return;      
      }


      //validate Fields
      if($('.select-category').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-category','Categoria');
          return;      
      }


      //validate Fields
      if($('.select-severity').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-severity','Criticidade');
          return;      
      }

      //validate Fields
      if($('.select-priority').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-priority','Prioridade');
          return;      
      }


      //validate Fields
      if($('.select-typecategory').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-typecategory','Tipo Categoria');
          return;      
      }

      //validate Fields
      if($('.select-team').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-team','Time');
          return;      
      }

            //validate Fields
      if($('#description-add').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('description-add','Descrição');
          return;      
      }


      //validate Fields
      if($('#message-add').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('message-add','Chamado');
          return;      
      }
      
     

      if(statuscategory == 0) //Trello 212 - André - 31/07
          status = "Aguardando Atendimento";
      else
          status = "Aguardando Aprovação";
      

        

      var data_add = {
          'phone'         : $('#phonecontact-add').val(),
          'email'         : $('#email-add').val(),
          'idbranchoffice': $('.select-branchoffice').val(),
          'idcompany'     : $('.select-company').val(),
          'iddepartament' : $('.select-departament').val(),
          'idcategory'    : $('.select-category').val(),
          'idseverity'    : $('.select-severity').val(),
          'idpriority'    : $('.select-priority').val(),
          'idtypecategory': $('.select-typecategory').val(),
          'idteam'        : $('.select-team').val(),
          'description'   : $('#description-add').val(),
          'message'       : $('#message-add').val(),
          'status'        : status //first status open ticket
          
      };

     var url = window.location.href+'/add';


      var tr  = $(this),
      show    = tr.data('show'),
      hide    = tr.data('hide');

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000
      });

      //confirm yes no
            $.jAlert({

              'title':'Abrir Chamado?',
              'type':'confirm',
              'content':'Deseja abrir um chamado?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {

                 $.ajax({
                          cache:false,
                          type: 'POST',
                          url: url,
                          data: {data:data_add},
                          dataType: "json",
                          success: function(data) {
                      
                            //upload file
                            lastid   = data.custom.lastid;
                            idticket = data.custom.lastid;
                            var file = [];
                            file.push($('#inputfile').get(0).files[0]);
                            //end upload


                            if(data.success){

                               if (typeof file[0] !== 'undefined' ) // if file exists
                                 upload(file,lastid,""); 
                               
                               
                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                                
                                 window.setTimeout(function(){
                                  location.reload();
                                 }, 3000);
                                
                            }else{
                               Toast.fire({
                                  type: 'DANGER',
                                  title: data.message
                               })
                            }
                          }  
                      });
              },
            });          

  })


  $( ".select-company" ).change(function() {


      //get id state
      var id = $(this).find(':selected').attr('value');
      var url = window.location.href;
      url = url.replace('/ticket','/branchoffice/listbranchofficecompany');

    if(id==null){
      $('.select-branchoffice')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'POST',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                       $('.select-branchoffice')
                      .find('option')
                      .remove()
                      .end();
                              //console.log(data['custom']['query'],"sss") 
                      $.each(data['custom']['query'], function (i, item) {
                        
                        $('.select-branchoffice').append($('<option>', { 
                            value: item.id,
                            text : item.fantasy_name 
                        }));
                       
                    });
                     
                } 
      });       
                
    }

  });

  
  $(".edit-mail , .edit-phone").on("click",function(){

      var id = $(this).data('value');
      var url = window.location.href;
      url = url.replace('/ticket','/user/editview');

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });

        $.ajax({
                cache:false,
                type: 'POST',
                url: url,
                data: {id: id ,phone:  $("#phonecontact-add").val(),email:  $("#email-add").val()},
                dataType: "json",
                success: function(data) {

                    if(data.success){
                              
                              Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                    }else{
                               Toast.fire({
                                  type: 'DANGER',
                                  title: data.message
                               })
                    }

                } 
        }); 
             

  })


  $( ".select-severity" ).change(function() {


      //get id state
      var id = $(this).find(':selected').attr('value');
      var url = window.location.href;
      url = url.replace('/ticket','/priority/listpriorityseverity');

    if(id==null){
      $('.select-priority')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'POST',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                       $('.select-priority')
                      .find('option')
                      .remove()
                      .end();

                      $.each(data['custom']['query'], function (i, item) {
                        
                        $('.select-priority').append($('<option>', { 
                            value: item.idpriority,
                            text : item.nametype 
                        }));
                        
                    });


                } 
      });       
                
    }

  }); 


//---------------------------------------------------------------edit
$( ".select-company-edit" ).change(function() {


      //get id state
      var id = $(this).find(':selected').attr('value');
      var url = window.location.href;
      url = url.replace('/ticket','/branchoffice/listbranchofficecompany');

    if(id==null){
      $('.select-branchoffice-edit')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'POST',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                       $('.select-branchoffice-edit')
                      .find('option')
                      .remove()
                      .end();
                              //console.log(data['custom']['query'],"sss") 
                      $.each(data['custom']['query'], function (i, item) {
                        
                        $('.select-branchoffice-edit').append($('<option>', { 
                            value: item.id,
                            text : item.name 
                        }));
                       
                    });
                     
                } 
      });       
                
    }

  });


  $( ".select-severity-edit" ).change(function() {


      //get id state
      var id = $(this).find(':selected').attr('value');
      var url = window.location.href;
      url = url.replace('/ticket','/priority/listpriorityseverity');

    if(id==null){
      $('.select-priority')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'POST',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                       $('.select-priority-edit')
                      .find('option')
                      .remove()
                      .end();

                      $.each(data['custom']['query'], function (i, item) {
                        
                        $('.select-priority-edit').append($('<option>', { 
                            value: item.idpriority,
                            text : item.nametype 
                        }));
                        
                    });


                } 
      });       
                
    }


  }); 


//---------------------------------------------------------------edit customer mysql  
  $(document).on("click",".edit-ticket" ,function(){  

    var id     = $(this).data('value');
    var status = $(this).data('category');
    var url=window.location.href+'/view';

     $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {


                      //console.log(data);
                      //return;

                        $("#id-modal-edit").val(id);
                        $("#phonecontact-edit").val(data['custom']['query'][0].phone);
                        $("#email-edit").val(data['custom']['query'][0].email);
                        $('.select-branchoffice-edit').append($('<option>', { 
                            value: data['custom']['query'][0].idbranchoffice,
                            text : data['custom']['query'][0].namebranchoffice 
                        }));
                        $('.select-priority-edit').append($('<option>', { 
                            value: data['custom']['query'][0].idpriority,
                            text : data['custom']['query'][0].nametype 
                        })); 
                             
                        $('.select-team-edit').append($('<option>', { 
                            value: data['custom']['query'][0].idteam,
                            text : data['custom']['query'][0].name 
                        }));        


                        $('.select-company-edit').append($('<option>', { 
                            value: data['custom']['query'][0].idcompany,
                            text : data['custom']['query'][0].companyname 
                        }));        
                        

                        $(".select-company-edit").val(data['custom']['query'][0].idcompany);
                        $(".select-branchoffice-edit").val(data['custom']['query'][0].idbranchoffice);
                        $(".select-severity-edit").val(data['custom']['query'][0].idseverity);

                        if(status==""){
                          $(".select-category-edit").val(data['custom']['query'][0].idcategory);
                        }else{
                          $(".select-category-edit").val(data['custom']['query'][0].idcategory);
                          $(".select-category-edit").prop("disabled",true);
                        }

                        $(".select-team-edit").val(data['custom']['query'][0].idteam);
                        $(".select-typecategory-edit").val(data['custom']['query'][0].idtypecategory);
                        
                        $('.select-departament-edit').text('');
                        $('.select-departament-edit').val('');
                        $('.select-departament-edit').append($('<option>', { 
                            value: data['custom']['query'][0].iddepartament,
                            text : data['custom']['query'][0].namedepartament 
                        }));       

                        $('.select-departament-edit:selected').val();

                        console.log(data['custom']['query'][0].iddepartament,data['custom']['query'][0].namedepartament) 
                                                
                        //$(".select-departament-edit").val(data['custom']['query'][0].iddepartament);

                        $("#description-edit").val(data['custom']['query'][0].description);
                        $("#message-edit").val(data['custom']['query'][0].message);
                    }
                });
  });

//task 337 - event tab hide button click
$('#bt-before-page').on('click',function(){
  $( "#link-tab-ticket" ).trigger( "click" );
})


//------------------------------------add data mysql
  $('#btn-edit').on('click',function(){

    //token json POST
    $.ajaxSetup({
        headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
    });


          var url=window.location.href+'/edit';

      //validate Fields
      if($('#phonecontact-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('phonecontact-add','Telefone');
          return;      
      }

      //validate Fields
      if($('#email-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('email-add','Email');
          return;      
      }

            //validate Fields
      if($('.select-company-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-company','Empresa');
          return;      
      }

      //validate Fields
      if($('.select-branchoffice-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-branchoffice','Estabelecimento');
          return;      
      }


      //validate Fields
      if($('.select-departament-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-departament','Departamento');
          return;      
      }


      //validate Fields
      if($('.select-category-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-category','Categoria');
          return;      
      }


      //validate Fields
      if($('.select-severity-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-severity','Criticidade');
          return;      
      }

      //validate Fields
      if($('.select-priority-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-priority','Prioridade');
          return;      
      }


      //validate Fields
      if($('.select-typecategory-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('select-typecategory','Tipo Categoria');
          return;      
      }


            //validate Fields
      if($('#description-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('description-add','Descrição');
          return;      
      }


      //validate Fields
      if($('#message-edit').val() == "" ){
          //first param name field error && second parameter name BOX message 
          validateFields('message-add','Chamado');
          return;      
      }



          var data = {
            'id'            : $('#id-modal-edit').val(),
            'phone'         : $('#phonecontact-edit').val(),
            'email'         : $('#email-edit').val(),
            'idbranchoffice': $('.select-branchoffice-edit').val(),
            'idcompany'     : $('.select-company-edit').val(),
            'iddepartament' : $('.select-departament-edit').val(),
            'idcategory'    : $('.select-category-edit').val(),
            'idseverity'    : $('.select-severity-edit').val(),
            'idpriority'    : $('.select-priority-edit').val(),
            'idteam'        : $('.select-team-edit').val(),
            'idtypecategory': $('.select-typecategory-edit').val(),
            'description'   : $('#description-edit').val(),
            'message'       : $('#message-edit').val(),
         };


       $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,                   
                    data: {data:  data },
                    dataType: "json",
                    success: function(data) {
                   

                        if(data.success && data.redirect == ""){
                            

                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })
                                  $('#id-modal-edit').val('');
                                  $('#phonecontact-edit').val('');
                                  $('#email-edit').val('');
                                  $('.select-branchoffice-edit').val('');
                                  $('.select-company-edit').val('');
                                  $('.select-departament-edit').val('');
                                  $('.select-category-edit').val('');
                                  $('.select-severity-edit').val('');
                                  $('.select-priority-edit').val('');
                                  $('.select-team-edit').val('');
                                  $('.select-typecategory-edit').val('');
                                  $('#description-edit').val('');
                                  $('#message-edit').val('');



                                    window.setTimeout(function(){
                                         location.reload();
                                     }, 1000);

                        }else{

                            if(data.redirect=='1'){
                               Toast.fire({
                                  type: 'warning',
                                  title: data.message
                               })
                            }else{
                               Toast.fire({
                                  type: 'danger',
                                  title: data.message
                               })
                            }                      
          }
              }
         });
                return false;     

  });

// delete ticket (button disabled)
$(document).on("click",".bt-del" ,function(){

            var id=$(this).data('value');           

            var tabactive=$('.nav-tabs li a.active')[0]["id"];
          

            var url = window.location.href+'/del';
            
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000
            });


            var tr  = $(this),
              show    = tr.data('show'),
              hide    = tr.data('hide');

            //confirm yes no
            $.jAlert({

              'title':'Excluir?',
              'type':'confirm',
              'content':'Deseja realmente excluir ?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {

                 $.ajax({
                          cache:false,
                          type: 'POST',
                          url: url,
                          data: {id:  id},
                          dataType: "json",
                          success: function(data) {


                            if(data.success){
                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                                 window.setTimeout(function(){
                                    location.reload();
                                 }, 3000);
                            }else{
                               Toast.fire({
                                  type: 'DANGER',
                                  title: data.message
                               })
                            }
                          }  
                      });
              },
            });          
  });


  

});

//name file upload
$('#inputfile').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                fileName = fileName.replace("C:\\fakepath\\" ,"");
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
})

//START  SLA CALCULATE, SESSION = GRAPH PIER CLICKED 
function getSLA(session){

  var url=window.location.href+'/getsla';

         $.ajax({
                  cache:false,
                  type: 'POST',
                  url: url,
                  dataType: "json",
                  success: function(data) {
                      
                      
                   
                    //**************************progress bar
                    total = data.count;
                    var pb = -1;
                    var count = 1;
                    var porc = (total*10)/100;
                    $("#msg-pb").text("0/"+total+" registros carregados...")
                    //**************************progress bar
                    var flag=0;

                    if(data.success){

      
                      $.each(data["query"], function (i, item) {
                        
                        //****************************************************progress bar***************************************
                            pb++;

                            $("#msg-pb").text(i+"/"+total+" registros carregando...")

                            if(pb > porc){
                              $(".pgb").css({"width":10*count+"%"});
                              pb=0;
                              count++;
                            }


                              if(count > 7  || count>9  ){
                                flag=1;
                                $(".pgb").css({"width":"100%"});
                                $("#msg-pb").text(i+"/"+total+" registros carregado.")

                                 window.setTimeout(function(){
                                    $(".div-pb").fadeOut("slow");
                                    $("#msg-pb").fadeOut("slow");
                                 }, 3000);

                              }
                              
                        
                        //****************************************************end progress bar**********************************   
                           
       
                            //**************************************************TABLE 1****************************************************************************

                            if(item.status != 'Finalizado' && item.status != 'Concluido' && item.status != 'Cancelado' && item.status != 'Reprovado'  ){

                                  //graph selected CRITICAL
                                  if(session!="" && session=="CRITICAL" && item.DIFERENCETIME==1 &&  item.STATUSTIME!='') //graph pier selected
                                    list1(item);                                    
                                  else if(session!="" && session=="OK" && item.DIFERENCETIME==0 &&  item.STATUSTIME!='' ) //graph pier selected
                                    list1(item);
                                  else if(session=="" ) //graph pier selected  //graph pier not selected
                                    list1(item); //load list em andamento and aguardando aprovacao

                                    
                              }
                              
                              //******************************************************END TABLE1*********************************************************************************************** 

                            //**************************************************TABLE 2****************************************************************************


                            if(item.status == 'Finalizado' || item.status == 'Concluido' || item.status == 'Cancelado' || item.status == 'Reprovado'  ){

                                  //graph selected CRITICAL
                                  if(session!="" && session=="CRITICAL" && item.DIFERENCETIME==1 || item.slacalculate == 1 ) //graph pier selected
                                    list2(item);
                                  else if(session!="" && session=="OK" && item.DIFERENCETIME==0 || item.slacalculate == 0 ) //graph pier selected
                                    list2(item);
                                  else if(session=="")  //graph pier not selected
                                    list2(item); //load list em andamento and aguardando aprovacao  

                              }
                              
                              //******************************************************END TABLE1*********************************************************************************************** 

                               namecategorynormal     ="";
                               status                 ="";
                               colorstatus            ="";
                               nameburdenurgency      ="";
                               colornameburdenurgency ="";
                               colorpriority          ="";
                               resttime               ="";
                               colorresttime          ="";
                               slastatus              ="";
                               colorsla               ="";



                               namecategorynormalt2     ="";
                               statust2                 ="";
                               colorstatust2            ="";
                               nameburdenurgencyt2      ="";
                               colornameburdenurgencyt2 ="";
                               colorpriorityt2          ="";
                               resttimet2               ="";
                               colorresttimet2          ="";
                               slastatust2              ="";
                               colorslat2               ="";                           

                           
                          });


                          if(flag ==0  ){
                               
                                $(".pgb").css({"width":"100%"});
                                
                                 window.setTimeout(function(){
                                    $(".div-pb").fadeOut("slow");
                                    $("#msg-pb").fadeOut("slow");
                                 }, 3000);

                              }

                          firsttable.destroy()
                          $("#tl1").html(rows);
                          $("#tl2").html(rowst2);
                          //$.getScript("assets/system/js/global.js"); //INCLUD JS INTO JS CALL FUNCTIONS GLOBAL
                          tbl1(); // change datatable
                          $(".chk-status").removeAttr("disabled");


                        }else{

                          $(".chk-status").removeAttr("disabled");
                        }
                  }  
              });


}

//***********************************************************************************TABLE SLA1*******************************************************************************

function list1(item){

  //MOUNT TABLE SLA

      if(item.namecategorynormal != null || item.namecategorynormal != "" )
        namecategorynormal = item.namecategorynormal



      if(item.status == 'Aguardando Atendimento'  ){
        status       = item.status;
        colorstatus  = "blue";
      }else if(item.status == 'Em Andamento'){
        status       = item.status;
        colorstatus  = "#FF4500";
      }else if(item.status == 'Suspenso' || item.status == 'Pausado' || item.status == 'Aguardando Aprovação' || item.status == 'Aguardando Solicitante'){
         status      = item.status;
         colorstatus = "red";
      }



      if(item.priorityid != null){
         nameburdenurgency      = item.nameburdenurgency;
         colornameburdenurgency = "red";
      }else if(item.priorityid == null){
         nameburdenurgency = "X";
         colornameburdenurgency     = "red";
      }
          
    if(typeof item.DIFERENCETIME ==="undefined" &&  item.priorityid == "" || item.priorityid == null){
        resttime      = "X";
        colorresttime = "red";                              
    }else if(typeof item.DIFERENCETIME ==="undefined" ){ 
        resttime      = "Ainda no Prazo!";
        colorresttime = "#FF4500";                              
    }else{  
      if(item.DIFERENCETIME == 0 && item.STATUSTIME !='' ){
        resttime      = "Ainda no Prazo!";
        colorresttime = "#FF4500";
      }else if(item.DIFERENCETIME==1 &&  item.STATUSTIME!=''){
        resttime      = "SLA Estourado!";
        colorresttime = "#FF4500";                          
      }else if(item.DIFERENCETIME == 2 && item.status == 'Aguardando Aprovação' || item.status == 'Aguardando Solicitante' || item.status  == 'Suspenso' || item.status == 'Pausado' && item.STATUSTIME!=''){
        resttime      = "Pausado";
        colorresttime = "#FF4500";                          
      }else if(item.DIFERENCETIME == 3 ) {  
        resttime      = "X";
        colorresttime = "#FF4500";
      }else if( item.STATUSTIME == "" && item.priorityid == "" && item.priorityid == null){
        resttime      = "X";
        colorresttime = "red";                             
      }else if(item.STATUSTIME == "" && item.priorityid != "" || item.priorityid != null) {
        resttime      = "X";
        colorresttime = "red";                                
      }                          
    }  

      if(item.userresponse==null || item.userresponse=="")
        userresponse = ""
      else
        userresponse = item.userresponse;

   

      if(typeof item.DIFERENCETIME ==="undefined" && item.priorityid == "" || item.priorityid == null){ 
          slastatus ="<td style='font-size: 11px;font-weight: bold;color: red;text-align: center '>Módulo SLA não cadastrado/não existente!</td>";
      }else if(typeof item.DIFERENCETIME ==="undefined" ){ 
          slastatus ="<td style='font-size: 11px;font-weight: bold;color: red;text-align: center '>X</td>";
      }else{ 
              if(item.DIFERENCETIME == 0 && item.status != "Aguardando Solicitante" && item.status != "Aguardando Aprovação"  && item.status != "Pausado" && item.status != "Suspenso")
                 slastatus = "<td style='font-size: 11px;font-weight: bold;color: blue ;text-align: center;'> <svg class='bi bi-emoji-sunglasses' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z'/><path d='M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z'/></svg></td>";
            
              if(item.DIFERENCETIME == 1 &&  item.status != "Aguardando Solicitante" &&  item.status != "Aguardando Aprovação" &&    item.status != "Pausado" &&  item.status != "Suspenso" )
                     slastatus ="<td style='font-size: 11px;font-weight: bold;color: red ;text-align: center;'><svg class='bi bi-emoji-dizzy' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M9.146 5.146a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708.708l-.647.646.647.646a.5.5 0 0 1-.708.708l-.646-.647-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708zm-5 0a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 1 1 .708.708l-.647.646.647.646a.5.5 0 1 1-.708.708L5.5 7.207l-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708z'/><path d='M10 11a2 2 0 1 1-4 0 2 2 0 0 1 4 0z'/></svg></td>"
           
              if(item.DIFERENCETIME == 2  &&  item.status == "Aguardando Solicitante" ||  item.status == "Aguardando Aprovação"  ||  item.status == "Pausado" ||  item.status == "Suspenso" )
                 slastatus ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>"
                                             

              if(item.status == 'Finalizado' && item.STATUSTIME!= '')
                 slastatus ="<td style='font-size: 11px;font-weight: bold;color: green ;text-align: center;'><svg class='bi bi-emoji-laughing' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z'/><path d='M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>"
          

              if(item.DIFERENCETIME==2 && item.STATUSTIME!= '')
                 slastatus ="<td style='font-size: 11px;font-weight: bold;color: green ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-alarm' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z'/></svg></td>"


              if(item.DIFERENCETIME==3 && item.STATUSTIME!= ''  )
                 slastatus ="<td style='font-size: 11px;font-weight: bold;color: #FF8C00 ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></td>"
      }



              button = "<td style='text-align: center' colspan='2'><div class='btn-group'><div  class='col-sm-4'>";
                      if($("#profile").val() == 'REQUESTER'  ||  $("#profile").val() == 'CLERK' && item.status == "Aguardando Aprovação" ){
                 button += "<button type='button' class='btn btn-default btn-sm' disabled ><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status != "Aguardando Aprovação"){
                  button += "<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK' && item.status == "Aguardando Aprovação"){
                  button +="<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() == 'REQUESTER' || $("#profile").val() == 'CLERK' &&  item.status != "Aguardando Aprovação" ){
                  button +="<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }
         
              button +="</div>";
              

              button += "<div  class='col-sm-4'>";
                      if($("#profile").val() == 'REQUESTER'  || $("#profile").val() == 'CLERK' && item.status == "Aguardando Aprovação" ){
                  button += "<button type='button' class='btn btn-success btn-sm' disabled ><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status != "Aguardando Aprovação"){
                  button += "<button type='button' class='btn btn-success btn-sm bt-response  '   data-status='"+item.status+"' data-team='"+item.idteam+"' data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status == "Aguardando Aprovação"){
                  button +="<button type='button' class='btn btn-success btn-sm bt-response '   data-status='"+item.status+"' data-team='"+item.idteam+"' data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() == 'REQUESTER' || $("#profile").val() == 'CLERK' &&  item.status != "Aguardando Aprovação" ){
                  button +="<button type='button' class='btn btn-success btn-sm bt-response'   data-status='"+item.status+"' data-team='"+item.idteam+"'  data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }
              button +="</div>";

              
              button +="</div>";
              button +="</td>";                                      

  

            rows = rows + "<tr >";
            rows = rows + "<th title='Criado Em: "+item.created+" /n Email: "+item.email+"'>"+item.id+"</th>";
            rows = rows + "<th style='font-size: 11px;font-weight: bold;'>"+item.companyname+"</th>";
            rows = rows + "<th style='font-size: 11px;font-weight: bold;'>"+item.description+"</th>"
            rows = rows + "<th style='font-size: 11px;font-weight: bold;'>"+namecategorynormal+"</th>"
            rows = rows + "<th style='font-size: 11px;font-weight: bold;' title='Data da última resposta: "+item.createdresponse+"'>"+userresponse+"</th>"
            rows = rows + "<td style='font-size: 11px;font-weight: bold;color: red '>"+item.nameteam+"</td>"
            rows = rows + "<th style='font-size: 11px;font-weight: bold;color:"+colorstatus+"'>"+status+"</th>"
            rows = rows + "<th style='font-size: 11px;font-weight: bold;color:"+colornameburdenurgency+";text-align: center'>"+nameburdenurgency+"</th>"
            rows = rows + "<th style='font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center' ><p style='font-size: 9.5px;font-weight: bold'>"+resttime+" </p></th>"
            rows = rows + slastatus;
            rows = rows + button;
            rows = rows + "</tr>";


}
//*********************************************************************END TABLE SLA1*************************************************************************************




//*********************************************************************TABLE SLA2******************************************************************************

function list2(item){

//MOUNT TABLE SLA FINALIZADOS ... 
        if(item.namecategorynormal != null || item.namecategorynormal != "" )
          namecategorynormalt2 = item.namecategorynormal


        if(item.status == 'Finalizado') {
         statust2        = item.status;
         colorstatust2   = "blue";
        }else if(item.status == 'Reprovado'  ){
          statust2       = item.status;
          colorstatust2  = "red";
        }else if(item.status == 'Concluido'){
          statust2       = item.status;
          colorstatust2  = "#FF4500"; 
        }else if(item.status == 'Cancelado'){
          statust2       = item.status;
          colorstatust2  = "red"; 
        }

  
          //console.log(item.id,"llll",item.nameburdenurgency);

        if(item.priorityid != null){
           nameburdenurgencyt2      = item.nameburdenurgency;
           colornameburdenurgency2 = "red";
        }else if(item.priorityid == null){
           nameburdenurgencyt2 = "X";
           colornameburdenurgency2     = "red";
        }else if(item.priorityid == ""){
           nameburdenurgencyt2 = "X";
           colornameburdenurgency2     = "red";
        }

        if(typeof item.DIFERENCETIME ==="undefined") {
          if(item.slacalculate == 0 ){ //SLA TICKET CALCULATE
              resttimet2      = "Concluido no Prazo!";
              colorresttimet2 = "green";                              
          }else if(item.slacalculate == 1 ){ 
              resttimet2      = "Concluído,porém, fora do prazo!";
              colorresttimet2 = "red";                              
          }else if(item.slacalculate == 3 ) {  
              resttimet2      = "X";
              colorresttimet2 = "#FF4500";
          }else if(item.slacalculate == 5 ) {  
              resttimet2      = "Sem nenhuma data de Finalização!";
              colorresttimet2 = "#FF4500";                                
          }else if( item.slastatustime != ""){
              resttimet2      = "X";
              colorresttimet2 = "red";                             
          }else if(item.slacalculate == "" ) {
              resttimet2      = "X";
              colorresttimet2 = "red";                                
          }                                                  
        }else{   
          if(item.DIFERENCETIME == 0 &&  item.priorityid != "" || item.priorityid != null){
              resttimet2      = "Concluido no Prazo!";
              colorresttimet2 = "green";                              
          }else if(item.DIFERENCETIME == 1 ){ 
              resttimet2      = "Concluído,porém, fora do prazo!";
              colorresttimet2 = "red";                              
          }else if(item.DIFERENCETIME == 3 ) {  
              resttimet2      = "X";
              colorresttimet2 = "#FF4500";
          }else if(item.DIFERENCETIME == 5 ) {  
              resttimet2      = "Sem nenhuma data de Finalização!";
              colorresttimet2 = "#FF4500";                                
          }else if( item.STATUSTIME == "" && item.priorityid == "" && item.priorityid == null){
              resttimet2      = "Xq";
              colorresttimet2 = "red";                             
          }else if(item.STATUSTIME == "" && item.priorityid != "" || item.priorityid != null) {
              resttimet2      = "Xer";
              colorresttimet2 = "red";                                
          }                          
        }

     


      if(item.userresponse==null )
        userresponse2 = ""
      else
        userresponse2 = item.userresponse;



        if(typeof item.DIFERENCETIME ==="undefined" && item.priorityid == "" || item.priorityid == null){ 
            slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: red;text-align: center '>Módulo SLA não cadastrado/não existente!</td>";
        }else if(item.slacalculate == 0  ){
            slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: green ;text-align: center;'> <svg class='bi bi-emoji-laughing' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z'/><path d='M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>";
        }else if(item.slacalculate == 1  ){
                   slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>";
        }else if(item.slacalculate == 3  ){
                   slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>";
        }else if(item.slacalculate == 5  ){
                   slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>";

        }else{ 
                if(item.DIFERENCETIME == 0 )
                   slastatust2 = "<td style='font-size: 11px;font-weight: bold;color: blue ;text-align: center;'> <svg class='bi bi-emoji-sunglasses' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z'/><path d='M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z'/></svg></td>";
              
                if(item.DIFERENCETIME == 1  )
                       slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: red ;text-align: center;'><svg class='bi bi-emoji-dizzy' width='32' height='32' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M9.146 5.146a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708.708l-.647.646.647.646a.5.5 0 0 1-.708.708l-.646-.647-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708zm-5 0a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 1 1 .708.708l-.647.646.647.646a.5.5 0 1 1-.708.708L5.5 7.207l-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708z'/><path d='M10 11a2 2 0 1 1-4 0 2 2 0 0 1 4 0z'/></svg></td>"
             
                if(item.DIFERENCETIME == 3 )
                   slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>"


                if(item.DIFERENCETIME == 5 )
                   slastatust2 ="<td style='font-size: 11px;font-weight: bold;color: orange ;text-align: center;'><svg width='32' height='32' viewBox='0 0 16 16' class='bi bi-emoji-frown' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z'/><path fill-rule='evenodd' d='M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z'/><path d='M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z'/></svg></td>"
                                               
        }



              buttont2 = "<td style='text-align: center' colspan='2'><div class='btn-group'><div  class='col-sm-4'>";
                      if($("#profile").val() == 'REQUESTER'  ||  $("#profile").val() == 'CLERK' && item.status == "Aguardando Aprovação" ){
                 buttont2 += "<button type='button' class='btn btn-default btn-sm' disabled ><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status != "Aguardando Aprovação"){
                  buttont2 += "<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK' && item.status == "Aguardando Aprovação"){
                  buttont2 +="<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }else if($("#profile").val() == 'REQUESTER' || $("#profile").val() == 'CLERK' &&  item.status != "Aguardando Aprovação" ){
                  buttont2 +="<button type='button' class='btn btn-default btn-sm  edit-ticket'  data-toggle='modal' data-target='#modal-edit' data-value="+item.id+" data-category=''><i class='fas fa-edit'></i></button>";
                      }
         
              buttont2 +="</div>";
              

              buttont2 += "<div  class='col-sm-4'>";
                      if($("#profile").val() == 'REQUESTER'  || $("#profile").val() == 'CLERK' && item.status == "Aguardando Aprovação" ){
                  buttont2 += "<button type='button' class='btn btn-success btn-sm' disabled ><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status != "Aguardando Aprovação"){
                  buttont2 += "<button type='button' class='btn btn-success btn-sm bt-response  '   data-status='"+item.status+"' data-team='"+item.idteam+"' data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() != 'REQUESTER' && $("#profile").val() != 'CLERK'  && item.status == "Aguardando Aprovação"){
                  buttont2 +="<button type='button' class='btn btn-success btn-sm bt-response '   data-status='"+item.status+"' data-team='"+item.idteam+"' data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }else if($("#profile").val() == 'REQUESTER' || $("#profile").val() == 'CLERK' &&  item.status != "Aguardando Aprovação" ){
                  buttont2 +="<button type='button' class='btn btn-success btn-sm bt-response'   data-status='"+item.status+"' data-team='"+item.idteam+"'  data-value="+item.id+" data-category='"+item.idcategory+"'><i class='fas fa-question-circle'></i></button>";
                      }
              buttont2 +="</div>";

              
              buttont2 +="</div>";
              buttont2 +="</td>";                                                     


              rowst2 = rowst2 + "<tr >";
              rowst2 = rowst2 + "<th title='Criado Em: "+item.created+" /n Email: "+item.email+"'>"+item.id+"</th>";
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;'>"+item.companyname+"</th>";
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;'>"+item.description+"</th>"
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;'>"+namecategorynormalt2+"</th>"
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;' title='Data da última resposta: "+item.createdresponse+"'>"+userresponse2+"</th>"
              rowst2 = rowst2 + "<td style='font-size: 11px;font-weight: bold;color: red '>"+item.nameteam+"</td>"
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;color:"+colorstatust2+"'>"+statust2+"</th>"
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;color:"+ colornameburdenurgency2 +";text-align: center'>"+nameburdenurgencyt2+"</th>"
              rowst2 = rowst2 + "<th style='font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center' ><p style='font-size: 9.5px;font-weight: bold'>"+resttimet2+" </p></th>"
              rowst2 = rowst2 + slastatust2;
              rowst2 = rowst2 + buttont2;
              rowst2 = rowst2 + "</tr>";

}

//*****************************************************************TABLE SLA2**********************************************************************************************

  //MOUNT PAGINATION DATATABLE
 function tbl1(){
  

    firsttable =   $('table.list-table').DataTable({
        "dom": 'Bfrtip',
        "buttons": [ 'excel',
                     'csv',
                     {
                        extend: 'pdf',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                      } 
        ],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "info": true,
        "autoWidth": false,
        "displayLength": 20,
        "order": [],
            "columnDefs": [ {
                'targets': [1], /* column index [0,1,2,3]*/
                'orderable': true, /* true or false */
            }],
        "language": {
          "sEmptyTable": "Carregando SLA...",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".ssss",
          //"sLengthMenu": 5,
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "<i class='fas fa-search'></i>",

        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        } 


      });
 }


//CLICK
$("#chk-filter").on("click",function(){

    if($(this).is(":checked") == true){
      
      firsttable.destroy();
      secondtable = $('table.list-table2').DataTable({
        "dom": 'Bfrtip',
        "buttons": [ 'excel',
                     'csv',
                     {
                        extend: 'pdf',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                      } 
        ],        
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "info": true,
          "autoWidth": false,
          "displayLength": 20,
          "order": [],
              "columnDefs": [ {
                  'targets': [1], /* column index [0,1,2,3]*/
                  'orderable': true, /* true or false */
              }],
          "language": {
            "sEmptyTable": "Carregando SLA ...",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".ssss",
            //"sLengthMenu": 5,
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "<i class='fas fa-search'></i>",

          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          } 


        });

        $(".list-table").addClass('d-none');
        $(".list-table2").removeClass('d-none');

     
    }else{
    
      secondtable.destroy();

      firsttable = $('table.list-table').DataTable({
        "dom": 'Bfrtip',
        "buttons": [ 'excel',
                     'csv',
                     {
                        extend: 'pdf',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                      } 
        ],          
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "info": true,
          "autoWidth": false,
          "displayLength": 20,
          "order": [],
              "columnDefs": [ {
                  'targets': [1], /* column index [0,1,2,3]*/
                  'orderable': true, /* true or false */
              }],
          "language": {
            "sEmptyTable": "Carregando SLA...",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".ssss",
            //"sLengthMenu": 5,
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "<i class='fas fa-search'></i>",

          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          } 


        });

        $(".list-table").removeClass('d-none');
        $(".list-table2").addClass('d-none');




    }  

})