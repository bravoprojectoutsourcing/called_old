

$(document).ready(function() {


  $(".bt-status").on('click',function(){

      var status = $(this).data("value");  

      url = window.location.href+'dashboard/menuboxteam';
      url = url.replace('dashboarddashboard','dashboard');

      url = url.replace('#','');

       $(".icon-list").text('');


   // console.log(url);
   // return;

      $.ajax({
                  cache:false,
                  type: 'POST',
                  url: url,
                  data: {data: status},
                  dataType: "json",
                  success: function(data) {

                    //console.log(data);
                    //return;

                    $(".icon-list").append("<a style='cursor:pointer'><i class='fa fa-bullseye' style='color:red'></i><span style='color:white;font-size:12px'>"+status+"</span></a>")

                    if(data.count == 0)
                      $(".icon-list").append("<a style='cursor:pointer'><i class='fa fa-fw fa-star-o' style='font-size:15px'></i><span>Sem Registro!</span></a>")


                      

                      $.each(data['query'], function (k, value) {
                         $(".icon-list").append("<a  style='cursor:pointer' class='bt-viewticket' data-value = "+value.idteam+" data-status = "+status.replace(' ','%')+"  ><i class='fa fa-check' style='font-size:15px;color:green'></i><span style='color:white;font-size:12px'>"+value.name+' (' + value.total + ")</span></a>")
                      });

              
                  }  
          });


  })


 $(document).on("click",".bt-viewticket" ,function(){  

      //save session idteam sessionidteambox
      var idteam = $(this).data('value');
      var status = $(this).data('status');


      url = window.location.href+'ticket/addSessionTicket';
      url = url.replace('#','');
      url = url.replace('dashboard','');

      $.ajax({
                  cache:false,
                  type: 'POST',
                  url: url,
                  data: {data: idteam,status:status},
                  dataType: "json",
                  success: function(data) {

                     url = window.location.href+'ticket';
                     url = url.replace('#','');
                     url = url.replace('dashboard','');



                     window.location.href = url;

                  }  
          });      

  })


});




 //change functions box and graphs
 box();
 graph();
 piergraphsla();


  //refresh 5s boxes
    setInterval(function(){      
         box();
        }, 20000);

  //refresh 10 graph  
    setInterval(function(){                                                       
               graph();
        }, 20000);

  //refresh 10 graph  
    setInterval(function(){                                                       
               piergraphsla();
        }, 60000);

 $('#song3').allow = "autoplay";
 $('#song3').autoplay();   

 function box(){

	//---------------BOX gadgets closeds,open,inprogress 
	url = window.location.href+'dashboard/ticket';
	url = url.replace('dashboarddashboard','dashboard');

	 $.ajax({
	          cache:false,
	          type: 'GET',
	          url: url,
	          dataType: "json",
	          success: function(data) {

              //console.log(data);
              //return;

	            if(data.boxclosed.success)				
					        $("#idboxclosed").text(data.boxclosed.custom["count"]);              
	            else
	              	$("#idboxclosed").text("0");

	            if(data.boxopen1.success)				
					        $("#idboxopen1").text(data.boxopen1.custom["count"]);              
	            else
	              	$("#idboxopen1").text("0");
	            

	            if(data.boxopen2.success)				
					        $("#idboxopen2").text(data.boxopen2.custom["count"]);              
	            else
	              	$("#idboxopen2").text("0");
	            


	            if(data.boxcancel.success)				
					        $("#idboxcancel").text(data.boxcancel.custom["count"]);              
	            else
	              	$("#idboxcancel").text("0");


	            if(data.boxconcluded.success)				
					        $("#idboxconcluded").text(data.boxconcluded.custom["count"]);              
	            else
	              	$("#idboxconcluded").text("0");


	            if(data.boxwait.success)				
					        $("#idboxwait").text(data.boxwait.custom["count"]);              
	            else
	              	$("#idboxwait").text("0");              

              if(data.boxonapproval.success)        
                  $("#idboxonapproval").text(data.boxonapproval.custom["count"]);              
              else
                  $("#idboxonapproval").text("0");    


              if(data.boxdisapproved.success)        
                  $("#idboxdisapproved").text(data.boxdisapproved.custom["count"]);              
              else
                  $("#idboxdisapproved").text("0");    

	      
	          }  
	  });

}	 

  function piergraphsla(){

     //SLA DONUT - TASK-324
     url = window.location.href+'/listsla';
     url = url.replace('/dashboard/','/listsla');
     url = url.replace('/listslalistsla','/listsla');
     


     

      $.ajax({
                  cache:false,
                  type: 'POST',
                  url: url,
                  dataType: "json",
                  success: function(data) {

                    //critical
                    var countcritical = 0;
                    
                    //em andamento
                    var countattention = 0;

                    //OK
                    var countok = 0;


                    $.each(data['query'], function (i, item) {



                        if(item.nameburdenurgency != null && item.STATUSTIME =='CRITICAL' && item.status != 'Suspenso' && item.status != 'Pausado' &&  item.status != 'Aguardando Aprovação' && item.status != 'Reprovado' )
                          countcritical = countcritical +1; 

                        

                        if(item.nameburdenurgency != null && item.STATUSTIME =='ATTENTION' && item.status != 'Suspenso' && item.status != 'Pausado' && item.status != 'Aguardando Solicitante'  && item.status != 'Aguardando Aprovação' && item.status != 'Reprovado'  )
                          countattention = countattention + 1; 

                        

                        if(item.nameburdenurgency != null && item.STATUSTIME =='OK' && item.status != 'Suspenso' && item.status != 'Pausado' && item.status != 'Aguardando Solicitante' && item.status != 'Aguardando Aprovação' && item.status != 'Reprovado'  )
                          countok = countok + 1;

                        


                    })
                        $(".iconchange").hide();
                        //-------------
                        //- DONUT CHART -
                        //-------------
                        // Get context with jQuery - using jQuery's .get() method.
                        var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
                        var donutData        = {
                          labels: [
                              'Crítico ('+countcritical+')', 
                              'Atenção ('+countattention+')',
                              'Ok ('+countok+')', 

                          ],
                          datasets: [
                            {
                              data: [countcritical,countattention,countok],
                              backgroundColor : ['#800000', '#f39c12', '#00a65a'],
                            }
                          ]
                        }
                        var donutOptions     = {
                          maintainAspectRatio : false,
                          responsive : true,
                        }
                        //Create pie or douhnut chart
                        // You can switch between pie and douhnut using the method below.
                        var donutChart = new Chart(donutChartCanvas, {
                          type: 'doughnut',
                          data: donutData,
                          options: {
                            donutOptions,
                            onClick:function(evt, elements){

                                    //get content value donut
                                    //var statustime = donutChart.data.datasets[0].data[elements[0]._index];

                                    if(elements[0]._index==2)
                                      statustime="OK";
                                    else if(elements[0]._index==0)
                                      statustime="CRITICAL";
                                    else if(elements[0]._index==1)
                                      statustime="ATTENTION";

                                    url = window.location.href+'ticket/addSessionTicketGraphPier';
                                    url = url.replace('#','');
                                    url = url.replace('dashboard','');

                                    $.ajax({
                                                cache:false,
                                                type: 'POST',
                                                url: url,
                                                data: {statustime:statustime},
                                                dataType: "json",
                                                success: function(data) {

                                                   url = window.location.href+'ticket';
                                                   url = url.replace('#','');
                                                   url = url.replace('dashboard','');

                                                   window.location.href = url;
                                                   

                                          }  
                                        });  



                            },
                          }
                        })

                  }  
          }); 


  }

	//--------------------GRAPH

	function graph(){
		
		url = window.location.href+'dashboard/graph';
		url = url.replace('dashboarddashboard','dashboard');


		 $.ajax({
		          cache:false,
		          type: 'GET',
		          url: url,
		          dataType: "json",
		          success: function(data) {


		          	var t1  = "";
		          	var t2  = "";
		          	var t3  = "";
		          	var t4  = "";
		          	var t5  = "";
		          	var t6  = "";
		          	var t7  = "";
		          	var t8  = "";
		          	var t9  = "";
		          	var t10 = "";
		          	var t11 = "";
		          	var t12 = "";
		          	var t13 = "";
		          	var t14 = "";
		          	var t15 = "";
		          	var t16 = "";
		          	var t17 = "";
		          	var t18 = "";
		          	var t19 = "";
		          	var t20 = "";
		          	var t21 = "";
		          	var t22 = "";
		          	var t23 = "";
		          	var t24 = "";
		          	var t25 = "";
		          	var t26 = "";
		          	var t27 = "";
		          	var t28 = "";
		          	var t29 = "";
		          	var t30 = "";
		          	var t31 = "";
		          	var t32 = "";
		          	var t33 = "";
		          	var t34 = "";
		          	var t35 = "";
		          	var t36 = "";
		          	var t37 = "";
		          	var t38 = "";
		          	var t39 = "";
		          	var t40 = "";
		          	var t41 = "";
		          	var t42 = "";
		          	var t43 = "";
		          	var t44 = "";
		          	var t45 = "";
		          	var t46 = "";
		          	var t47 = "";
		          	var t48 = "";
      					var t49 = "";
      					var t50 = "";
      					var t51 = "";
      					var t52 = "";
      					var t53 = "";
      					var t54 = "";
      					var t55 = "";
      					var t56 = "";
      					var t57 = "";
      					var t58 = "";
      					var t59 = "";
      					var t60 = "";
      					var t61 = "";
      					var t62 = "";
      					var t63 = "";
      					var t64 = "";
      					var t65 = "";
      					var t66 = "";
      					var t67 = "";
      					var t68 = "";
      					var t69 = "";
      					var t70 = "";
      					var t71 = "";
      					var t72 = "";		          	
 

                      $.each(data['custom']['query'], function (i, item) {

                         //console.log(item);
                         //return;

						            	//EM ANDAMENTO                      

                      		if(item.status == "Em Andamento" && item.monthnumber == 1)
                      			t1 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 2)
                      			t2 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 3)
                      			t3 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 4)
                      			t4 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 5)
                      			t5 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 6)
                      			t6 = item.total;
                      		


                      		if(item.status == "Em Andamento" && item.monthnumber == 7)
                      			t7 = item.total;
                      		
                      		
                      		if(item.status == "Em Andamento" && item.monthnumber == 8)
                      			t8 = item.total;
                      		
                      		
                      		if(item.status == "Em Andamento" && item.monthnumber == 9)
                      			t9 = item.total;
                      		

                      		if(item.status == "Em Andamento" && item.monthnumber == 10)
                      			t10 = item.total;
                      		


                      		if(item.status == "Em Andamento" && item.monthnumber == 11)
                      			t11 = item.total;
                      		


                      		if(item.status == "Em Andamento" && item.monthnumber == 12)
                      			t12 = item.total;
                      		


                      		//AGUARDANDO ATENDIMENTO
                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 1)
                      			t13 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 2)
                      			t14 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 3)
                      			t15 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 4)
                      			t16 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 5)
                      			t17 = item.total;
                      		


                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 6)
                      			t18 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 7)
                      			t19 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 8)
                      			t20 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 9)
                      			t21 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 10)
                      			t22 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 11)
                      			t23 = item.total;
                      		

                      		if(item.status == "Aguardando Atendimento" && item.monthnumber == 12)
                      			t24 = item.total;
                      		

                      		//FINALIZADO
                      		if(item.status == "Finalizado" && item.monthnumber == 1)
                      			t25 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 2)
                      			t26 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 3)
                      			t27 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 4)
                      			t28 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 5)
                      			t29 = item.total;
                      		


                      		if(item.status == "Finalizado" && item.monthnumber == 6)
                      			t30 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 7)
                      			t31 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 8)
                      			t32 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 9)
                      			t33 = item.total;
                      		
                      		if(item.status == "Finalizado" && item.monthnumber == 10)
                      			t34 = item.total;
                      		

                      		if(item.status == "Finalizado" && item.monthnumber == 11)
                      			t35 = item.total;
                      		


                      		if(item.status == "Finalizado" && item.monthnumber == 12)
                      			t36 = item.total;
                      		

                      		//CANCELADO
                      		if(item.status == "Cancelado" && item.monthnumber == 1)
                      			t37 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 2)
                      			t38 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 3)
                      			t39 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 4)
                      			t40 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 5)
                      			t41 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 6)
                      			t42 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 7)
                      			t43 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 8)
                      			t44 = item.total;
                      		
                      		if(item.status == "Cancelado" && item.monthnumber == 9)
                      			t45 = item.total;
                      		
                      		if(item.status == "Cancelado" && item.monthnumber == 10)
                      			t46 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 11)
                      			t47 = item.total;
                      		

                      		if(item.status == "Cancelado" && item.monthnumber == 12)
                      			t48 = item.total;
                      		

                      		//AGUARDANDO SOLICITANTE
                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 1)
                      			t49 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 2)
                      			t50 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 3)
                      			t51 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 4)
                      			t52 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 5)
                      			t53 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 6)
                      			t54 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 7)
                      			t55 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 8)
                      			t56 = item.total;
                      		
                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 9)
                      			t57 = item.total;
                      		
                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 10)
                      			t58 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 11)
                      			t59 = item.total;
                      		

                      		if(item.status == "Aguardando Solicitante" && item.monthnumber == 12)
                      			t60 = item.total;                      


                      		//CONCLUIDO
                      		if(item.status == "Concluido" && item.monthnumber == 1)
                      			t61 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 2)
                      			t62 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 3)
                      			t63 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 4)
                      			t64 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 5)
                      			t65 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 6)
                      			t66 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 7)
                      			t67 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 8)
                      			t68 = item.total;
                      		
                      		if(item.status == "Concluido" && item.monthnumber == 9)
                      			t69 = item.total;
                      		
                      		if(item.status == "Concluido" && item.monthnumber == 10)
                      			t70 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 11)
                      			t71 = item.total;
                      		

                      		if(item.status == "Concluido" && item.monthnumber == 12)
                      			t72 = item.total;  


                      });


//-------------------------------------------------------------------------------------------------------BAR GRAPH



							var ticksStyle = {
							    fontColor: '#495057',
							    fontStyle: 'bold'
							  }

							  var mode      = 'index'
							  var intersect = true

							  var $salesChart = document.getElementById('revenue-barchart-canvas').getContext('2d');
							  var salesChart  = new Chart($salesChart, {
							    type   : 'bar',
							    data   : {
							      labels  : ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro','Dezembro'],
							      datasets: [
								      {
								        label               : 'Chamados Em Andamento',
								        backgroundColor     : '#ffc107',
								        borderColor         : '#ffc107',
								        pointRadius          : true,
								        pointColor          : '#ffc107',
								        pointStrokeColor    : 'rgba(255,193,7)',
								        pointHighlightFill  : '#ffc107',
								        pointHighlightStroke: 'rgba(255,193,7)',
								        data                : [t1, t2, t3, t4, t5, t6, t7,t8,t9,t10,t11,t12]
								      },
								      {
								        label               : 'Aguardando Atendimento',
								        backgroundColor     : '#007bff',
								        borderColor         : '#007bff',
								        pointRadius         : true,
								        pointColor          : 'rgba(0,128,0)',
								        pointStrokeColor    : '#EB1414',
								        pointHighlightFill  : '#fff',
								        pointHighlightStroke: 'rgba(0,128,0)',
								        data                : [t13, t14, t15, t16, t17, t18, t19,t20,t21,t22,t23,t24]
								      },

								      {
								        label               : 'Finalizados',
								        backgroundColor     : 'rgba(0,128,0)',
								        borderColor         : 'rgba(0,128,0)',
								        pointRadius         : true,
								        pointColor          : 'rgba(0,128,0)',
								        pointStrokeColor    : '#EB1414',
								        pointHighlightFill  : '#fff',
								        pointHighlightStroke: 'rgba(0,128,0)',
								        data                : [t25, t26, t27, t28, t29, t30, t31,t32,t33,t34,t35,t36]
								      },
								      {
								        label               : 'Cancelados',
								        backgroundColor     : '#dc3545',
								        borderColor         : '#dc3545',
								        pointRadius         : true,
								        pointColor          : 'rgba(0,255,0)',
								        pointStrokeColor    : '#EB1414',
								        pointHighlightFill  : '#fff',
								        pointHighlightStroke: 'rgba(0,255,0)',
								        data                : [t37, t38, t39, t40, t41, t42, t43,t44,t45,t46,t47,t48]
								      },
								      {
								        label               : 'Aguardando Solicitante',
								        backgroundColor     : '#d6d8d9',
								        borderColor         : '#d6d8d9',
								        pointRadius         : true,
								        pointColor          : 'rgba(0,255,0)',
								        pointStrokeColor    : '#EB1414',
								        pointHighlightFill  : '#fff',
								        pointHighlightStroke: 'rgba(0,255,0)',
								        data                : [t49, t50, t51, t52, t53, t54, t55,t56,t57,t58,t59,t60]
								      },
								      {
								        label               : 'Concluido',
								        backgroundColor     : '#bee5eb',
								        borderColor         : '#bee5eb',
								        pointRadius         : true,
								        pointColor          : 'rgba(0,255,0)',
								        pointStrokeColor    : '#EB1414',
								        pointHighlightFill  : '#fff',
								        pointHighlightStroke: 'rgba(0,255,0)',
								        data                : [t61, t62, t63, t64, t65, t66, t67,t68,t69,t70,t71,t72]
								      },								      								      

							      ]
							    },
							    options: {
							      maintainAspectRatio: true,
							      tooltips           : {
							        mode     : mode,
							        intersect: intersect
							      },
							      hover              : {
							        mode     : mode,
							        intersect: intersect
							      },
							      legend             : {
							        display: true
							      },
							      scales             : {
							        yAxes: [{
							          // display: false,
							          gridLines: {
							            display      : true,
							            lineWidth    : '4px',
							            color        : 'rgba(0, 0, 0, .2)',
							            zeroLineColor: 'transparent'
							          },
							          ticks    : $.extend({
							            beginAtZero: true,

							            // Include a dollar sign in the ticks
							            callback: function (value, index, values) {
							              if (value >= 1000) {
							                value /= 1000
							                value += 'k'
							              }
							              return  value
							            }
							          }, ticksStyle)
							        }],
							        xAxes: [{
							          display  : true,
							          gridLines: {
							            display: true
							          },
							          ticks    : ticksStyle
							        }]
							      }
							    }
							  })
		      
		          }  
		  });


	}





						/* Chart.js Charts */
						  // Sales chart
	
						  /*
						  var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d');
						  //$('#revenue-chart').get(0).getContext('2d');

						  var salesChartData = {
						    labels  : ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro','Dezembro'],
						    datasets: [
						      {
						        label               : 'Chamados Em Andamento',
						        backgroundColor     : '#ffc107',
						        borderColor         : '#ffc107',
						        pointRadius          : true,
						        pointColor          : '#ffc107',
						        pointStrokeColor    : 'rgba(255,193,7)',
						        pointHighlightFill  : '#ffc107',
						        pointHighlightStroke: 'rgba(255,193,7)',
						        data                : [t1, t2, t3, t4, t5, t6, t7,t8,t9,t10,t11,t12]
						      },
						      {
						        label               : 'Aguardando Atendimento',
						        backgroundColor     : '#007bff',
						        borderColor         : '#007bff',
						        pointRadius         : true,
						        pointColor          : 'rgba(0,128,0)',
						        pointStrokeColor    : '#EB1414',
						        pointHighlightFill  : '#fff',
						        pointHighlightStroke: 'rgba(0,128,0)',
						        data                : [t13, t14, t15, t16, t17, t18, t19,t20,t21,t22,t23,t24]
						      },

						      {
						        label               : 'Finalizados',
						        backgroundColor     : 'rgba(0,128,0)',
						        borderColor         : 'rgba(0,128,0)',
						        pointRadius         : true,
						        pointColor          : 'rgba(0,128,0)',
						        pointStrokeColor    : '#EB1414',
						        pointHighlightFill  : '#fff',
						        pointHighlightStroke: 'rgba(0,128,0)',
						        data                : [t25, t26, t27, t28, t29, t30, t31,t32,t33,t34,t35,t36]
						      },
						      {
						        label               : 'Cancelados',
						        backgroundColor     : '#dc3545',
						        borderColor         : '#dc3545',
						        pointRadius         : true,
						        pointColor          : 'rgba(0,255,0)',
						        pointStrokeColor    : '#EB1414',
						        pointHighlightFill  : '#fff',
						        pointHighlightStroke: 'rgba(0,255,0)',
						        data                : [t37, t38, t39, t40, t41, t42, t43,t44,t45,t46,t47,t48]
						      }

						    ]
						  }

						  var salesChartOptions = {
						    maintainAspectRatio : true,
						    responsive : true,
						    legend: {
						      display: true
						    },
						    scales: {
						      xAxes: [{
						        gridLines : {
						          display : true,
						        }
						      }],
						      yAxes: [{
						        gridLines : {
						          display : true,
						        }
						      }]
						    }
						  }

						  // This will get the first returned node in the jQuery collection.
						  var salesChart = new Chart(salesChartCanvas, { 
						      type: 'line', 
						      data: salesChartData, 
						      options: salesChartOptions
						    }
						  )
					*/