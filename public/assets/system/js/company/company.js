var cpfcnpjvalid ="";

$(document).ready(function() {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1000
    });



    var cityname;
    var idcompany="";

    $(".select-state").select2();
    $(".select-city").select2({ width: '100%'});


    $(".select-state-edit").select2();
    $(".select-city-edit").select2({ width: '100%'});

    //bo - branch office - estabelecimentos
    $(".select-state-bo").select2();
    $(".select-city-bo").select2({ width: '100%'});    

    
  $('#cpfcnpj-add').blur(function() {

      cityname="";

      var cpfcnpj = $('#cpfcnpj-add').val();

      cpfcnpj = cpfcnpj.replace('/' ,'');
      cpfcnpj = cpfcnpj.replace('.' ,'');
      cpfcnpj = cpfcnpj.replace('-' ,'');
      cpfcnpj = cpfcnpj.replace('.' ,'');


      verifyCNPJ(cpfcnpj);
  })


  $('#cpfcnpj-edit').blur(function() {

      cityname="";

      var cpfcnpj = $('#cpfcnpj-edit').val();

      cpfcnpj = cpfcnpj.replace('/' ,'');
      cpfcnpj = cpfcnpj.replace('.' ,'');
      cpfcnpj = cpfcnpj.replace('-' ,'');
      cpfcnpj = cpfcnpj.replace('.' ,'');

        //Verify post
      if (cpfcnpj != "") {

        $.getJSON("//www.receitaws.com.br/v1/cnpj/"+ cpfcnpj +"/?callback=?", function(data) {
        
                  if (!("erro" in data)) {

                                  //Atualiza os campos com os valores da consulta.
                                  $("#name-edit").val(data.nome);
                                  $("#address-edit").val(data.logradouro);
                                  $("#neighborhood-edit").val(data.bairro);
                                  $("#number-edit").val(data.numero);
                                  $("#email-edit").val(data.email);
                                  $("#neigborhood-edit").val(data.bairro);
                                  $("#cep-edit").val(data.cep.replace(/\D/g, ''));
                                  $("#compl-edit").val(data.complemento);
                                  $("#phone-edit").val(data.telefone);

                                 
                                  //select state
                  $(".select-state-edit").val(data.uf).trigger('change');
                  $(".select-city-edit").val(data.municipio).trigger('change');

  
                  }else {
                        //file global.js CEP pesquisado não foi encontrado.
                      message($(this),'CNPJ Não encontrado na receita , continue o cadastro manual','yellow')    
                               
                  }
                             
               });

            }    
  })

  $(document).on("click",".btn-list-branchoffice" ,function(){    

    //$(".tbody-list-branchoffice").remove();

      //token json POST
      $.ajaxSetup({
          headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
      });

      id        = $(this).data("value");
      idcompany = id; //var global

      //branchoffice  = estabelecimentos
      
      var url = window.location.href;
      url = url.replace('/company','/branchoffice/listbranchofficecompany');

      //---------------------------initialize AJAX GET
       $.ajax({
                      cache:false,
                      type: 'POST',
                      url: url,
                      data: {id: id},
                      dataType: "json",
                      success: function(data) {
                                //console.log(data);
                                //return;
                                  var rows='';


                                  //clear table and remont
                                  $(".list-datatable-bo").remove();
                                  $(".tbody-list-branchoffice").remove();
                                  $("#div-null").remove();


                                  //mount datatable
                                 rows = rows + $(".div-bo").append('<table  class="table table-responsive-sm table-bordered table-striped table-sm list-datatable-bo " >'+
                                    '<thead>'+
                                      '<tr>'+
                                        '<th>CNPJ</th>'+
                                        '<th>Estabelecimento</th>'+
                                        '<th>Endereço</th>'+
                                        '<th>Criado em</th>'+
                                        '<th style="text-align:center">Ação</th>'+
                                      '</tr>'+
                                      '</thead>'+
                                      '<tbody class="tbody-list-branchoffice">'+
                                      '</tbody>'+
                                   '</table>');
                           
                           if(data.success){
                                  //destroy datatable remont
                                  var table = $('.list-datatable-bo').DataTable();
                                  table.destroy();
                                  

                              $.each(data["custom"]["query"], function (i, item) {
                              
                                  var timestamp = item.created;
                                  var formattedDate = moment(timestamp).format('DD/MM/YYYY h:mm:ss ');
                                  

                                  rows = rows + "<tr >";
                                    rows = rows + "<td class='tdid"+i+"'>"+item.cpfcnpj+"</td>";
                                    rows = rows + "<td >"+item.name+"</td>";
                                    rows = rows + "<td >"+item.address+"</td>";
                                    rows = rows + "<td >"+formattedDate +"</td>";
                                    rows = rows + '<td ><div class="btn-group"> <div  class="col-sm-4">';
                                    rows = rows + '</div><div  class="col-sm-4">';
                                    rows = rows + '<button type="button" class="btn btn-default btn-sm  edit-company-branchoffice"  data-toggle="modal" data-target="#modal-edit-branchoffice" data-value="'+item.id+'">';
                                    rows = rows + '<i class="fas fa-edit"></i></button>';
                                    rows = rows + '</div><div  class="col-sm-1">';
                                    rows = rows + '<button type="button" class="btn btn-danger btn-sm bt-del" data-value="'+item.id+'" >';
                                    rows = rows + '<i class="fas fa-trash"></i></button></div></div></td>';
                                  rows = rows + "</tr>";

                                    i=i+1;                    
                              });
                              
                              //-----------------------------------show table
                              $(".tbody-list-branchoffice").html(rows);
                              

                               $('.list-datatable-bo').DataTable({
                                  "paging": true,
                                  "lengthChange": true,
                                  "searching": true,
                                  "ordering": true,
                                  "info": true,
                                  "autoWidth": false,
                                  "displayLength": 5,
                                        "order": [],
                                  "columnDefs": [ {
                                      'targets': [1], /* column index [0,1,2,3]*/
                                      'orderable': true, /* true or false */
                                  }],
                                  "language": {
                                    "sEmptyTable": "Nenhum registro encontrado",
                                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                    "sInfoPostFix": "",
                                    "sInfoThousands": ".até",
                                    //"sLengthMenu": 5,
                                    "sLoadingRecords": "Carregando...",
                                    "sProcessing": "Processando...",
                                    "sZeroRecords": "Nenhum registro encontrado",
                                    "sSearch": "<i class='fas fa-search'></i>",

                                  },
                                  "oAria": {
                                      "sSortAscending": ": Ordenar colunas de forma ascendente",
                                      "sSortDescending": ": Ordenar colunas de forma descendente"
                                  } 

                                });

                               $("#tab-branchoffice").tab('show');
                               return;

                          }else{
                                  rows = rows + "<tr >";
                                    rows = rows + "<td colspan='5' style='text-align:center'>Nenhum Registro Encontrado</td>";
                                  rows = rows + "</tr>";
                                  

                              //-----------------------------------show table
                              $(".tbody-list-branchoffice").html(rows);
                              $("#tab-branchoffice").tab('show');
                              return;

                          } 

                       

                      }
                  }); 
  })

  $("#tab-company").on('click' , function(){

      idcompany="";
      //destroy datatable remont
      var table = $('.list-datatable-bo').DataTable();
      table.destroy();
      
      //clear table and remont
      //$(".list-datatable-bo").remove();
      $(".tbody-list-branchoffice").remove();
       $("#div-null").remove();

  })

  $('#bt-open-modal-add').on('click',function(){
    
    var tabactive = $('.nav-tabs li a.active')[0]["id"];    

    if(tabactive=="tab-company"){
      //add company
      $("#title-modal-add").text("Adicionar Empresa");
  
    }else if(tabactive=="tab-branchoffice"){

      if(idcompany==""){

              message($(this),'Escolha uma empresa antes de prosseguir com o cadastro','yellow');
              $("#title-modal-add").text("Adicionar Estabelecimento");      
      }else{
              $("#title-modal-add").text("Adicionar Estabelecimento");
      }
    }

  })

  //------------------------------------add data mysql

$(document).on("click","#btn-add" ,function(){

    //token json POST
    $.ajaxSetup({
        headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
    });


      var tabactive=$('.nav-tabs li a.active')[0]["id"];
    

      if(tabactive=="tab-company"){  //add company
          var url=window.location.href+'/add';
      }else if(tabactive=="tab-branchoffice"){ //add branchoffice/estabelecimentos
          var url = window.location.href;
          url = url.replace('/company','/branchoffice/add');
      }  


         var cpfcnpj = $('#cpfcnpj-add').val();

          if(cpfcnpj.length<14){

            message($(this),'O CNPJ precisa de 14 Dígitos','red');
            $('#cpfcnpj-add').val(null);
            return;

          }


          //validate Fields
          if(cpfcnpj == "" ){
               //first param name field error and second parameter name BOX message 
               validateFields('cpfcnpj-add','CNPJ');
               return;
          }


          //validate Fields
          if($('#name-add').val() == "" ){
              //first param name field error and second parameter name BOX message 
              validateFields('name-add','nome da empresa');
              return;      
          }
      

          if($('.select-state').val() == "null"){

              validateFields('select-state','estado');
              return;  
            
          }


          //validate Fields
          if($('#fantasy-name-add').val() == "" ){
              //first param name field error and second parameter name BOX message 
              validateFields('fantasy-name-add','nome fantasia');
              return;      
          }


             //case tab company active insert this
             if(tabactive=="tab-company"){

                var data_add = {
                          'name'        : $('#name-add').val(),
                          'address'     : $('#address-add').val(),
                          'neighborhood': $('#neighborhood-add').val(),
                          'email'       : $('#email-add').val(),
                          'cep'         : $('#cep-add').val(),
                          'number'      : $('#number-add').val(),
                          'compl'       : $('#compl-add').val(),
                          'phone'       : $('#phone-add').val(),
                          'state'       : $('.select-state').val(),
                          'city'        : $('.select-city').val(),
                          'cpfcnpj'     : $('#cpfcnpj-add').val(),
                          'fantasy_name': $('#fantasy-name-add').val(),
               };


             }else if(tabactive=="tab-branchoffice"){


                    if(idcompany==""){
                        message($(this),'Escolha uma empresa antes de prosseguir com o cadastro','yellow');
                        return;
                    }

                var data_add = {
                          'idcompany'   : idcompany, //diference
                          'name'        : $('#name-add').val(),
                          'address'     : $('#address-add').val(),
                          'neighborhood': $('#neighborhood-add').val(),
                          'email'       : $('#email-add').val(),
                          'cep'         : $('#cep-add').val(),
                          'number'      : $('#number-add').val(),
                          'compl'       : $('#compl-add').val(),
                          'phone'       : $('#phone-add').val(),
                          'state'       : $('.select-state').val(),
                          'city'        : $('.select-city').val(),
                          'cpfcnpj'     : $('#cpfcnpj-add').val(),
                          'fantasy_name': $('#fantasy-name-add').val(),
               };


             }  
            
             verifyCNPJ(cpfcnpj);

             if(cpfcnpjvalid == 0 ){

                  message($(this),'CNPJ Inválido','red')
                  cpfcnpjvalid ="";      
                  return;

             }



             $.ajax({
                          cache:false,
                          type: 'POST',
                          url: url,                   
                          data: {data:  data_add},
                          dataType: "json",
                          success: function(data) {

                              if(data.success && data.redirect == ""){
                                  

                                     Toast.fire({
                                        type: 'success',
                                        title: data.message
                                     })


                                          $('#name-add').val('');
                                          $('#address-add').val('');
                                          $('#neighborhood-add').val('');
                                          $('#email-add').val('');
                                          $('#cep-add').val('');
                                          $('#number-add').val('');
                                          $('#compl-add').val('');
                                          //$('#select-type').val('');
                                          //$('#cell-add').val('');
                                          $('#phone-add').val('');
                                          $('.select-state').val('');
                                          $('.select-city').val('');
                                          $('#cpfcnpj-add').val('');
                                          $('#fantasy-name-add').val('');


                                          window.setTimeout(function(){
                                               location.reload();
                                           }, 1000);

                              }else{


                                  if(data.redirect=='1'){
                                     Toast.fire({
                                        type: 'warning',
                                        title: data.message
                                     })
                                  }else{
                                     Toast.fire({
                                        type: 'danger',
                                        title: data.message
                                     })
                                  }
                            
                }
                    }
               });
                      return false; 

  });


//------------------------------------add data mysql


$(document).on("click","#btn-edit" ,function(){

    //token json POST
    $.ajaxSetup({
        headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
    });


          var url=window.location.href+'/edit';

         var cpfcnpj = $('#cpfcnpj-edit').val();

          //validate Fields
          if(cpfcnpj == "" ){
               //first param name field error and second parameter name BOX message 
               validateFields('cpfcnpj-edit','CNPJ');
               return;
          }


          //validate Fields
          if($('#name-edit').val() == "" ){
              //first param name field error and second parameter name BOX message 
              validateFields('name-edit','nome da empresa');
              return;      
          }


                  var data = {

                            'id'          : $('#id-modal-edit').val(),
                            'name'        : $('#name-edit').val(),
                            'address'     : $('#address-edit').val(),
                            'neighborhood': $('#neighborhood-edit').val(),
                            'email'       : $('#email-edit').val(),
                            'cep'         : $('#cep-edit').val(),
                            'number'      : $('#number-edit').val(),
                            'compl'       : $('#compl-edit').val(),
                            'phone'       : $('#phone-edit').val(),
                            'state'       : $('.select-state-edit').val(),
                            'city'        : $('.select-city-edit').val(),
                            'cpfcnpj'     : $('#cpfcnpj-edit').val(),
                            'fantasy_name': $('#fantasy-name-edit').val(),
                 };
   

       $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,                   
                    data: {data:  data},
                    dataType: "json",
                    success: function(data) {

                   

                        if(data.success && data.redirect == ""){
                            

                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                                    $('#name-edit').val('');
                                    $('#address-edit').val('');
                                    $('#neighborhood-edit').val('');
                                    $('#email-edit').val('');
                                    $('#cep-edit').val('');
                                    $('#number-edit').val('');
                                    $('#compl-edit').val('');
                                    //$('#select-type').val('');
                                    //$('#cell-add').val('');
                                    $('#phone-edit').val('');
                                    $('.select-state-edit').val('');
                                    $('.select-city-dit').val('');
                                    $('#cpfcnpj-edit').val('');
                                    $('#fantasy-name-edit').val('');

                                    window.setTimeout(function(){
                                         location.reload();
                                     }, 1000);

                        }else{

                            if(data.redirect=='1'){
                               Toast.fire({
                                  type: 'warning',
                                  title: data.message
                               })
                            }else{
                               Toast.fire({
                                  type: 'danger',
                                  title: data.message
                               })
                            }                      
          }
              }
         });
                return false;     

  });

//------------------------------------bt edit branch office - estabelecimentos

$(document).on("click","#btn-edit-bo" ,function(){
  
          //token json POST
          $.ajaxSetup({
              headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
          });


          var url = window.location.href;
          url = url.replace('/company','/branchoffice/edit');

          var cpfcnpj = $('#cpfcnpj-edit-bo').val();


          //validate Fields
          if(cpfcnpj == "" ){
               //first param name field error and second parameter name BOX message 
               validateFields('cpfcnpj-edit','CNPJ');
               return;
          }


          //validate Fields
          if($('#name-edit-bo').val() == "" ){
              //first param name field error and second parameter name BOX message 
              validateFields('name-edit','nome da empresa');
              return;      
          }

          //validate Fields
          if($('#fantasy-name-edit-bo').val() == "" ){
              //first param name field error and second parameter name BOX message 
              validateFields('fantasy-name-edit-bo','nome fantasia');
              return;      
          }

          if(idcompany==""){

                  message($(this),'Escolha uma empresa antes de prosseguir com o cadastro','yellow');
                  return;
          }
       
          var data = {

                    'idcompany'   : idcompany, //diference
                    'id'          : $('#id-modal-edit-bo').val(),
                    'name'        : $('#name-edit-bo').val(),
                    'address'     : $('#address-edit-bo').val(),
                    'neighborhood': $('#neighborhood-edit-bo').val(),
                    'email'       : $('#email-edit-bo').val(),
                    'cep'         : $('#cep-edit-bo').val(),
                    'number'      : $('#number-edit-bo').val(),
                    'compl'       : $('#compl-edit-bo').val(),
                    'phone'       : $('#phone-edit-bo').val(),
                    'state'       : $('.select-state-edit-bo').val(),
                    'city'        : $('.select-city-edit-bo').val(),
                    'cpfcnpj'     : $('#cpfcnpj-edit-bo').val(),
                    'fantasy_name': $('#fantasy-name-edit-bo').val(),
         };               


       $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,                   
                    data: {data:  data},
                    dataType: "json",
                    success: function(data) {

                   

                        if(data.success && data.redirect == ""){
                            

                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                                    $('#name-edit-bo').val('');
                                    $('#address-edit-bo').val('');
                                    $('#neighborhood-edit-bo').val('');
                                    $('#email-edit-bo').val('');
                                    $('#cep-edit-bo').val('');
                                    $('#number-edit-bo').val('');
                                    $('#compl-edit-bo').val('');
                                    //$('#select-type').val('');
                                    //$('#cell-add').val('');
                                    $('#phone-edit-bo').val('');
                                    $('.select-state-edit-bo').val('');
                                    $('.select-city-edit-bo').val('');
                                    $('#cpfcnpj-edit-bo').val('');
                                    $('#fantasy-name-edit-bo').val('');


                                    window.setTimeout(function(){
                                         location.reload();
                                     }, 1000);

                        }else{


                            if(data.redirect=='1'){
                               Toast.fire({
                                  type: 'warning',
                                  title: data.message
                               })
                            }else{
                               Toast.fire({
                                  type: 'danger',
                                  title: data.message
                               })
                            }                      
          }
              }
         });
                return false;     
  });

  //-------------------------------------------------------------------------select

  $( ".select-state" ).change(function() {


    //get id state
    var id = $(this).find(':selected').attr('data-value');
    var url = window.location.href;
    url = url.replace('/company','/listcity');

    if(id==null){
      $('.select-city')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'GET',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                       $('.select-city')
                      .find('option')
                      .remove()
                      .end();

  
                      
                      $('.select-city').append($('<option>', { 
                          value: cityname,
                          text : cityname 
                      }));         
                              
                      $.each(data, function (i, item) {
                        $('.select-city').append($('<option>', { 
                            value: item.name,
                            text : item.name 
                        }));
                        
                    });

                      cityname="";
                } 
      });       
                
    }

  });

  //-------------------------------------------------------------------------select

  $( ".select-state-edit" ).change(function() {

    //get id state
    var id = $(this).find(':selected').attr('data-value');
    var url = window.location.href;
    url = url.replace('/company','/listcity');

    if(id==null){
      $('.select-city-edit')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'GET',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                        $('.select-city-edit')
                        .find('option')
                        .remove()
                        .end();
                        

                        $('.select-city-edit').append($('<option>', { 
                            value: cityname,
                            text : cityname 
                        }));       
                                
                        $.each(data, function (i, item) {
                          $('.select-city-edit').append($('<option>', { 
                              value: item.name,
                              text : item.name 
                          }));
                          
                      });
                  
                } 
            }); 
    }
  });

  $( ".select-state-edit-bo" ).change(function() {

    //get id state
    var id = $(this).find(':selected').attr('data-value');
    var url = window.location.href;
    url = url.replace('/company','/listcity');

    if(id==null){
      $('.select-city-edit-bo')
          .find('option')
          .remove()
          .end()
          .append('<option value="whatever">Selecione...</option>')
          .val('whatever');
      
    }else{
       $.ajax({
                cache:false,
                type: 'GET',
                url: url,
                data: {id:  id},
                dataType: "json",
                success: function(data) {

                        $('.select-city-edit-bo')
                        .find('option')
                        .remove()
                        .end();
                        

                        $('.select-city-edit-bo').append($('<option>', { 
                            value: cityname,
                            text : cityname 
                        }));       
                                
                        $.each(data, function (i, item) {
                          $('.select-city-edit-bo').append($('<option>', { 
                              value: item.name,
                              text : item.name 
                          }));
                          
                      });
                  
                } 
            }); 
    }

  });


$(document).on("click",".bt-del" ,function(){

            var id=$(this).data('value');           

            var tabactive=$('.nav-tabs li a.active')[0]["id"];
          

            if(tabactive=="tab-company"){  //add company
                var url=window.location.href+'/del';
            }else if(tabactive=="tab-branchoffice"){ //add branchoffice/estabelecimentos
                var url = window.location.href;
                url = url.replace('/company','/branchoffice/del');
            }  


            var tr  = $(this),
              show    = tr.data('show'),
              hide    = tr.data('hide');

            //confirm yes no
            $.jAlert({

              'title':'Excluir?',
              'type':'confirm',
              'content':'Deseja realmente excluir ?',
                  'theme': 'blue',
                  'showAnimation' : show,
                  'hideAnimation' : hide,
                  'confirmBtnText': "Sim, com Certeza!",
                  'denyBtnText': 'Não, Prefiro Não!',
              'onConfirm': function() {

                 $.ajax({
                          cache:false,
                          type: 'POST',
                          url: url,
                          data: {id:  id},
                          dataType: "json",
                          success: function(data) {


                            if(data.success){
                               Toast.fire({
                                  type: 'success',
                                  title: data.message
                               })

                                 window.setTimeout(function(){
                                    location.reload();
                                 }, 3000);
                            }else{
                               Toast.fire({
                                  type: 'DANGER',
                                  title: data.message
                               })
                            }
                          }  
                      });
              },
            });          
});

//---------------------------------------------------------------edit customer mysql  

  $(document).on('click', '.edit-company',function(){

    cityname="";
    var id=$(this).data('value');
    var url=window.location.href+'/view';

     $.ajax({
                    cache:false,
                    type: 'GET',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {



                        $("#id-modal-edit").val(data['custom']['query'][0].id);
                        $("#name-edit").val(data['custom']['query'][0].name);
                        $("#address-edit").val(data['custom']['query'][0].address);
                        $("#cpfcnpj-edit").val(data['custom']['query'][0].cpfcnpj);
                        $("#email-edit").val(data['custom']['query'][0].email);
                        $("#cep-edit").val(data['custom']['query'][0].cep);
                        $("#neighborhood-edit").val(data['custom']['query'][0].neighborhood);
                        $("#number-edit").val(data['custom']['query'][0].number);
                        $("#compl-edit").val(data['custom']['query'][0].compl);
                        $("#phone-edit").val(data['custom']['query'][0].phone);
                        $("#fantasy-name-edit").val(data['custom']['query'][0].fantasy_name);
                        cityname = data['custom']['query'][0].city ;
                        $(".select-state-edit").val(data['custom']['query'][0].state).trigger('change'); 

                    }
                });
  });

 $(document).on('click', '.edit-company-branchoffice', function() {

    cityname="";
    var id=$(this).data('value');    
    var url = window.location.href;
    url = url.replace('/company','/branchoffice/view');

     $.ajax({
                    cache:false,
                    type: 'GET',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {


                        $("#id-modal-edit-bo").val(data['custom']['query'][0].id);
                        $("#name-edit-bo").val(data['custom']['query'][0].name);
                        $("#address-edit-bo").val(data['custom']['query'][0].address);
                        $("#cpfcnpj-edit-bo").val(data['custom']['query'][0].cpfcnpj);
                        $("#email-edit-bo").val(data['custom']['query'][0].email);
                        $("#cep-edit-bo").val(data['custom']['query'][0].cep);
                        $("#neighborhood-edit-bo").val(data['custom']['query'][0].neighborhood);
                        $("#number-edit-bo").val(data['custom']['query'][0].number);
                        $("#compl-edit-bo").val(data['custom']['query'][0].compl);
                        $("#phone-edit-bo").val(data['custom']['query'][0].phone);
                        $("#fantasy-name-edit-bo").val(data['custom']['query'][0].fantasy_name);
                        cityname = data['custom']['query'][0].city ;
                        $(".select-state-edit-bo").val(data['custom']['query'][0].state).trigger('change');

                    }
                });

  });


});

function verifyCNPJ(cpfcnpj){
    cpfcnpjvalid ="";
   //Verify cnpj
      if (cpfcnpj != "") {

        var cnpjvalid = isCNPJValid(cpfcnpj);



        if(!cnpjvalid){
          message($(this),'CNPJ Inválido','red')
          cpfcnpjvalid =0;      
          return ;
        }
        
        $.getJSON("//www.receitaws.com.br/v1/cnpj/"+ cpfcnpj +"/?callback=?", function(data) {
        
                        if (!("erro" in data)) {

                                        //Atualiza os campos com os valores da consulta.
                                        $("#name-add").val(data.nome);
                                        $("#address-add").val(data.logradouro);
                                        $("#neighborhood-add").val(data.bairro);
                                        $("#number-add").val(data.numero);
                                        $("#email-add").val(data.email);
                                        $("#neigborhood-add").val(data.bairro);
                                        $("#cep-add").val(data.cep);
                                        $("#compl-add").val(data.complemento);
                                        $("#phone-add").val(data.telefone);

                                       
                                        //select state
                        cityname = data.municipio;                


                              
                      $('.select-state').append($('<option>', { 
                            value: data.uf,
                            text : data.uf 
                      }));
                        

                      $('.select-city').append($('<option>', { 
                          value: data.municipio,
                          text : data.municipio 
                      }));  

                      console.log(cityname);

                       $(".select-state").val(data.uf).trigger('change');
                     

                      var url = window.location.href;
                      url = url.replace('/company','/listcitystate');


                     $.ajax({
                              cache:false,
                              type: 'GET',
                              url: url,
                              data: {state:  data.uf},
                              dataType: "json",
                              success: function(data) {

                                if(data != ""){

                                       var url = window.location.href;
                                       url = url.replace('/company','/listcity');

                                       $.ajax({
                                                cache:false,
                                                type: 'GET',
                                                url: url,
                                                data: {id:  data},
                                                dataType: "json",
                                                success: function(data) {

                                                       $('.select-city')
                                                      .find('option')
                                                      .remove()
                                                      .end();

                                  
                                                      
                                                      $('.select-city').append($('<option>', { 
                                                          value: cityname,
                                                          text : cityname 
                                                      }));         
                                                              
                                                      $.each(data, function (i, item) {
                                                        $('.select-city').append($('<option>', { 
                                                            value: item.name,
                                                            text : item.name 
                                                        }));
                                                        
                                                    });

                                                      cityname="";
                                                } 
                                      });                                    

                                }else{

                                }
                              }  
                          });                      


            
                    }else {
                         //CEP pesquisado não foi encontrado.
                         message($(this),'CNPJ Não encontrado na receita , continue o cadastro manual','yellow')      
                    }
                             
               });
              cpfcnpjvalid = 1; 
            }    


}