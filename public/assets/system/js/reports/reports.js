// $(document).on("ready", handler), deprecated as of jQuery 1.8 and removed in jQuery 3.0
// Shorthand for $(document).ready()
$(function () {
	$('#phone').mask('(ZZ) 0ZZZZZZZZ', { placeholder: "(__) _________", translation: { 'Z': { pattern: /[0-9]/, optional: true }} });
});

$("#selectCompany").select2();
$("#selectBranchOffice").select2();
$("#selectDepartament").select2();
$("#selectCategory").select2();
$("#selectSeverity").select2();
$("#selectPriority").select2();
$("#selectTypeCategory").select2();
$("#selectTeam").select2();
$("#selectStatus").select2();

$('#selectStatus').select2().change(function () {
	var status = [];
	// get Array IdStatus
	$.each($("#selectStatus option:selected"), function () {
		status.push($(this).val());
	});
	var id = "'"+status.join("','")+"'";
	$('input[name = arrSelectStatus]').val(id);
});

$('#selectTeam').select2().change(function () {
	var teams = [];
	// get Array IdTeams
	$.each($("#selectTeam option:selected"), function () {
		teams.push($(this).val());
	});
	var id = teams.join(",");
	$('input[name = arrSelectTeam]').val(id);
});

$('#selectTypeCategory').select2().change(function () {
	var typesCategory = [];
	// get Array IdTypesCategory
	$.each($("#selectTypeCategory option:selected"), function () {
		typesCategory.push($(this).val());
	});
	var id = typesCategory.join(",");
	$('input[name = arrSelectTypeCategory]').val(id);
});

$('#selectPriority').select2().change(function () {
	var priorities = [];
	// get Array IdPriorities
	$.each($("#selectPriority option:selected"), function () {
		priorities.push($(this).val());
	});
	var id = priorities.join(",");
	$('input[name = arrSelectPriority]').val(id);
});

$("#selectSeverity").select2().change(function () {
	var severities = [];
	// get Array of IdSeverities
	$.each($("#selectSeverity option:selected"), function () {
		severities.push($(this).val());
	});

	var id = severities.join(",");
	var url = window.location.href;
	url = url.replace('/reports', '/reports/listPriorities');
	$('input[name = arrSelectSeverity]').val(id);

	if (id == null) {
		$('#selectPriority')
			.find('option')
			.remove()
			.end()
			.append('<option value="" disabled>-- Selecione uma ou mais Prioridades --</option>')
			.val('');
	} else {
		$.ajax({
			cache: false,
			type: 'POST',
			url: url,
			data: { id: id },
			dataType: "json",
			success: function (data) {
				$('#selectPriority')
					.find('option')
					.remove()
					.end();
				$.each(data['custom']['query'], function (i, item) {
					$('#selectPriority').append($('<option>', {
						value: item.idpriority,
						text: item.nametype
					}));
				});
			}
		});
	}
}); 

$('#selectCategory').select2().change(function () {
	var categories = [];
	// get Array IdCategories
	$.each($("#selectCategory option:selected"), function () {
		categories.push($(this).val());
	});
	var id = categories.join(",");
	$('input[name = arrSelectCategory]').val(id);
});

$('#selectDepartament').select2().change(function () {
	var departaments = [];
	// get Array IdDepartaments
	$.each($("#selectDepartament option:selected"), function () {
		departaments.push($(this).val());
	});
	var id = departaments.join(",");
	$('input[name = arrSelectDepartament]').val(id);
});

$('#selectBranchOffice').select2().change(function () {
	var branchoffices = [];
	// get Array IdBranchoffices
	$.each($("#selectBranchOffice option:selected"), function () {
		branchoffices.push($(this).val());
	});
	var id = branchoffices.join(",");
	$('input[name = arrSelectBranchOffice]').val(id);
});

$('#selectCompany').select2().change(function () {
	var companies = [];
	// get Array IdCompanies
	$.each($("#selectCompany option:selected"), function () {
		companies.push($(this).val());
	});
	
	var id = companies.join(",");
	var url = window.location.href;
	url = url.replace('/reports', '/reports/listBranchOffices');
	$('input[name = arrSelectCompany]').val(id);
	
	if (id == null) {
		$('#selectBranchOffice')
			.find('option')
			.remove()
			.end()
			.append('<option value="" disabled>-- Selecione um ou mais Estabelecimentos --</option>')
			.val('');

	} else {
		$.ajax({
			cache: false,
			type: 'POST',
			url: url,
			data: { id: id },
			dataType: "json",
			success: function (data) {
				$('#selectBranchOffice')
					.find('option')
					.remove()
					.end();
				// console.log(data['custom']['query'],"sss") 
				$.each(data['custom']['query'], function (i, item) {
					$('#selectBranchOffice').append($('<option>', {
						value: item.id,
						text: item.fantasy_name
					}));
				});
			}
		});
	}
});