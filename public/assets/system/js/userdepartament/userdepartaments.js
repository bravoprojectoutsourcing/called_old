$(document).ready(function() {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1000
    });


    $(document).on("click",".btn-adduserdepartament" ,function(){
        
         $('#tabledepartamentmodal').text('');
        var id = $(this).data('value');
        $("#iduserdepartament-modal").val(id);
        getUserdepartament(id);

    })


    $(document).on("click",".bt-del-userdepartament" ,function(){

        var id=$(this).data('value');           

        var url=window.location.href+'/del';
        url = url.replace("departaments","userdepartament");


        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');

        //confirm yes no
        $.jAlert({
            'title':'Excluir?',
            'type':'confirm',
            'content':'Deseja realmente excluir ?',
            'theme': 'blue',
            'showAnimation' : show,
            'hideAnimation' : hide,
            'confirmBtnText': "Sim, com Certeza!",
            'denyBtnText': 'Não, Prefiro Não!',
            'onConfirm': function() {
                $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {
                        if(data.success){
                            Toast.fire({
                              type: 'success',
                              title: data.message
                            })
                            
                            window.setTimeout(function(){
                                location.reload();
                            }, 3000);
                        }else{
                           Toast.fire({
                              type: 'DANGER',
                              title: data.message
                           })
                        }
                    }  
                });
            },
        });          
    });


 $(document).on("click",".adddepartamentuser" ,function(){
        var iddepartament = $('#select-userdepartament-modal').val();
        var iduser = $('#iduserdepartament').val();

        var url=window.location.href+'/add';
        
        url = url.replace("departaments","userdepartament");

        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        var data = {
            'iddepartament' : iddepartament,
            'iduser' : iduser
        };



        if(data.iddepartament==null){
            validateFields('select-user-departament-modal','departamento');
            return
        }
        
        if($('#select-departament-modal').val() == "" ){
            //first param name field error and second parameter name BOX message 
            validateFields(data.iddepartament,'departamento');
            return;      
        } 

        $('#tabledepartamentmodal').text('');

        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data},
            dataType: "json",
            beforeSend: function(){
                $('body').css('cursor', 'progress')
            },
            success: function(data) {
                $('body').css('cursor', 'auto');
                getUserdepartament(iduser);
            }
        });

    });

})    



function getUserdepartament(id){


    var url=window.location.href;


    url = url.replace("departaments","userdepartament");


   // console.log(url);return;    
    $('#tableuserdepartamentmodal').html('');
    //$('#notHave').html('');

        $.ajax({
            cache:false,
            type: 'GET',
            url: url,
            data: {id:  id},
            dataType: "json",

            success: function(data) {

                //console.log(data["list"]);
                //return;

                $('body').css('cursor', 'auto')
                $('#iduserdepartament').val(id);


               /* if(data["list"]["custom"]["count"] > 0){
                    $.each( data["list"]["custom"]["query"], function( key, value ) {
                        $('#select-userdepartament-modal').append('\
                            <option value="'+value.id+'">'+value.namedepartament+'</option>\
                        ');
                    });
                }else{
                    $('#select-userdepartament-modal').append('\
                            <option disabled >Sem times a adicionar</option>\
                        ');
                }*/
                
                if(data["list"]["custom"]["count"] > 0){
                    $.each( data["list"]["custom"]["query"], function( key, value ) {
                        $('#tabledepartamentmodal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td class="d-none">'+value.id+'</td>\
                            <td >'+value.namedepartament+'</td>\
                            <td style="text-align: center" colspan="2">\
                                <button type="button" class="btn btn-danger btn-sm bt-del-userdepartament" data-value="'+value.id+'"><i class="fas fa-trash"></i></button>\
                            </td>\
                        </tr>\
                        ');
                    });
                }else{
                    $('#tabledepartamentmodal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td colspan="2">Não há times cadastrados para usuário</td>\
                        </tr>\
                        ');
                }
                
            }
        });
     }  