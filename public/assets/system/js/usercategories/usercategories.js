$(document).ready(function() {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1000
    });


    $(document).on("click",".btn-addusercategory" ,function(){

         $('#tablecategorymodal').text('');
        var id = $(this).data('value');
        $("#idusercategory-modal").val(id);
        getUserCategory(id);

    })


    $(document).on("click",".bt-del-usercategory" ,function(){

        var id=$(this).data('value');           

        var url=window.location.href+'/del';
        url = url.replace("categories","usercategory");


        var tr  = $(this),
          show    = tr.data('show'),
          hide    = tr.data('hide');

        //confirm yes no
        $.jAlert({
            'title':'Excluir?',
            'type':'confirm',
            'content':'Deseja realmente excluir ?',
            'theme': 'blue',
            'showAnimation' : show,
            'hideAnimation' : hide,
            'confirmBtnText': "Sim, com Certeza!",
            'denyBtnText': 'Não, Prefiro Não!',
            'onConfirm': function() {
                $.ajax({
                    cache:false,
                    type: 'POST',
                    url: url,
                    data: {id:  id},
                    dataType: "json",
                    success: function(data) {
                        if(data.success){
                            Toast.fire({
                              type: 'success',
                              title: data.message
                            })
                            
                            window.setTimeout(function(){
                                location.reload();
                            }, 3000);
                        }else{
                           Toast.fire({
                              type: 'DANGER',
                              title: data.message
                           })
                        }
                    }  
                });
            },
        });          
    });


 $(document).on("click",".addcategoryuser" ,function(){
        var idcategory = $('#select-usercategory-modal').val();
        var iduser = $('#idusercategory').val();

        var url=window.location.href+'/add';
        
        url = url.replace("categories","usercategory");

        //token json POST
        $.ajaxSetup({
            headers:{'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        var data = {
            'idcategory' : idcategory,
            'iduser' : iduser
        };



        if(data.idcategory==null){
            validateFields('select-user-category-modal','categoria');
            return
        }
        
        if($('#select-category-modal').val() == "" ){
            //first param name field error and second parameter name BOX message 
            validateFields(data.idcategory,'categoria');
            return;      
        } 

        $('#tablecategorymodal').text('');

        $.ajax({
            cache:false,
            type: 'POST',
            url: url,                   
            data: {data:  data},
            dataType: "json",
            beforeSend: function(){
                $('body').css('cursor', 'progress')
            },
            success: function(data) {
                $('body').css('cursor', 'auto');
                getUserCategory(iduser);
            }
        });

    });

})    



function getUserCategory(id){


    var url=window.location.href;


    url = url.replace("categories","usercategory");


   // console.log(url);return;    
    $('#tableusercategorymodal').html('');
    //$('#notHave').html('');

        $.ajax({
            cache:false,
            type: 'GET',
            url: url,
            data: {id:  id},
            dataType: "json",

            success: function(data) {

                //console.log(data["list"]);
                //return;

                $('body').css('cursor', 'auto')
                $('#idusercategory').val(id);


               /* if(data["list"]["custom"]["count"] > 0){
                    $.each( data["list"]["custom"]["query"], function( key, value ) {
                        $('#select-usercategory-modal').append('\
                            <option value="'+value.id+'">'+value.namecategory+'</option>\
                        ');
                    });
                }else{
                    $('#select-usercategory-modal').append('\
                            <option disabled >Sem times a adicionar</option>\
                        ');
                }*/
                
                if(data["list"]["custom"]["count"] > 0){
                    $.each( data["list"]["custom"]["query"], function( key, value ) {
                        $('#tablecategorymodal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td class="d-none">'+value.id+'</td>\
                            <td >'+value.namecategory+'</td>\
                            <td style="text-align: center" colspan="2">\
                                <button type="button" class="btn btn-danger btn-sm bt-del-usercategory" data-value="'+value.id+'"><i class="fas fa-trash"></i></button>\
                            </td>\
                        </tr>\
                        ');
                    });
                }else{
                    $('#tablecategorymodal').append('\
                        <tr style="position: relative;max-height: -5px">\
                            <td colspan="2">Não há times cadastrados para usuário</td>\
                        </tr>\
                        ');
                }
                
            }
        });
     }  