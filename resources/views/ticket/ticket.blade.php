  <!-- Content Wrapper. Contains page content -->

  <input type="hidden" id="path" value="<?php echo URL::to('/upload/') ?> ">
  <input type="hidden" id="profile" value="<?php echo session('resp')["custom"][0]['nameprofile'] ?> ">
  <input type="hidden" id="pteamid" value="<?php echo session('resp')["custom"][0]['teamid'] ?> ">
  <input type="hidden" id="sessionstatustime" value="<?php echo $resp['sessionstatustime'] ?>">


  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row" style="height:  20px;width: 100%;margin-top: 40px;" >
          <div class="col-sm-6">
            <h2>Chamados</h2>
          </div>

          <div class="col-sm-7" style="margin-left: -10%">
            <ol class="breadcrumb ">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Chamados</li>

            </ol>
          </div>



        </div>
      </div><!-- /.container-fluid -->
    </section>

    <style type="text/css">

      .form-inline{
          display:block;
      }

       .dataTables_filter{
        position: relative ;
      }

      .dataTables_length{
        position: relative;
      }

      .select2-selection__rendered {
          line-height: 16px !important;
      }

      .label-text{
        margin-left: 0;
        position: relative;
        top: 10px;
        font-size: 15px;
      }

     #tab-company{
       /*background-color: #848484;*/
       background-image: linear-gradient(to right, rgba(255,0,0,0), rgba(255,0,0,1));

     }

     .card-col{
      background-color: #343a40;
     }

     .icons-background{
        background-color: #343a40;
     }

     .icons-image{
         color: #fff;
     }

     .fields-rows{
       position: relative;
       top:-20px;
     }

     .fields-rows2{
       position: relative;
       top:-40px;
     }

     .fields-rows3{
       position: relative;
       top:-60px;
     }

     .fields-rows4{
       position: relative;
       top:-80px;
     }

     .fields-rows5{
       position: relative;
       top:-100px;
     }

     .fields-rows6{
       position: relative;
       top:-120px;
     }


       .btn-circle.btn-sm {
            width: 30px;
            height: 30px;
            padding: 6px 0px;
            border-radius: 15px;
            font-size: 8px;
            text-align: center;
    }
    .content-header {
    position: relative;
    top: 10px;
    padding: 1.2rem;
    }

    </style>
    <BR>
    <!-- BAR STATUS -->
    @if($resp['sessionstatustime']=="")    <!--GRAPH PIER CLICK if empty not click graph-->
      <div style="margin-left: 3%;margin-right: 3%" class="progress mb-1 d-none div-pb">
        <div class="progress-bar bg-danger pgb" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
          <span class="sr-only"></span>
        </div>
      </div>
      <div id="msg-pb" class="d-none" style="margin-left: 3%">Carregando Total de Registros do SLA...</div>
    @endif
     <!--END BAR-->

      <!-- /.card-header -->
      <div class="card-body div-list" style="position:relative;margin-top: -0.2%;"> <!-- style="position:relative;margin-top: -0.2%;overflow-y: auto;height: 800px;"-->

        <div class="col-12 col-sm-6 col-lg-12" class="card-col" >
            <div class="card card-primary card-tabs">
              @if($resp['sessionidteambox'] != "" || $resp['sessionstatustime'] != "") <!--show hide tabs if graph pier clicked-->
                <div class="card-header menu-tab p-0 d-none">
              @else
                <div class="card-header p-0 menu-tab">
              @endif

                    <ul class="nav nav-tabs" id="tab-company" role="tablist">
                      <li class="nav-item">

                        <a class="nav-link active" id="tab-company" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="false">

                        Abrir Chamado</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="link-tab-ticket" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="true">Listar Chamados</a>
                      </li>
                      <li class="nav-item" disabled>
                        <a class="nav-link" id="tab-response-ticket"   href="#custom-tabs-response" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Resposta Chamado</a>
                      </li>
                    </ul>
                  </div>

              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                @if($resp['sessionidteambox'] != "" || $resp['sessionstatustime'] != "") <!--show hide tabs if graph pier clicked-->
                  <div class="tab-pane fade " id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                @else
                  <div class="tab-pane fade active show" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                @endif

                    <!--start content-->
                        <div class="card"  >
                          <div class="card-body div-list" style="position:relative;margin-top: -0.1%;overflow-y: auto;height: 520px;">

                            <!--tab 1-->

                            <!--line 1 -->
                                <div class="row ">
                                   <div class="col-sm-6">
                                     <div class="form-group">
                                      <label class="label-text responsive">Telefone</label>

                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-phone icons-image" ></i></span>
                                        <input type="text" class="form-control " value="{{$resp['resp']['custom'][0]['phone']}}"  id="phonecontact-add"  maxlength="14" name="phonecontact_add" placeholder="* Telefone ">
                                        <i class="fas fa-edit edit-phone" data-value="{{$resp['resp']['custom'][0]['iduser']}}" style="cursor: pointer;margin-top: 10px;margin-left:5px" ></i>
                                      </div>
                                    </div>
                                  </div>

                                   <div class="col-sm-6 ">
                                    <div class="form-group">
                                      <label class=" label-text">Email</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background"><i class="fas fa-mail-bulk phone icons-image" ></i></span>
                                        <input type="text" class="form-control" id="email-add" value="{{$resp['resp']['custom'][0]['email']}}"  name="email" placeholder="* Email">
                                        <i class="fas fa-edit edit-mail" data-value="{{$resp['resp']['custom'][0]['iduser']}}"  style="cursor: pointer;margin-top: 10px;margin-left:5px" ></i>
                                      </div>
                                    </div>
                                   </div>

                                </div>

                                <?php #dd($resp) ?>
                                <!--line 2 -->
                                <div class="row fields-rows">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Empresas</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-city phone icons-image"></i></span>
                                        <select class="form-control select-company " style="width:100%;display:block;box-sizing:border-box">
                                          <option value="" >Selecione a Empresa...</option>
                                         @foreach ($resp["company"]["custom"]["query"] as $companys)
                                            <option value="{{$companys->id}}" >{{$companys->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Estabelecimentos</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-archive phone icons-image"></i></span>
                                          <select class="form-control select-branchoffice " style="width:100%;display:block;box-sizing:border-box">
                                            <option value="" >Selecione o Estabelecimento...</option>
                                             @foreach ($resp["branchoffice"]["custom"]["query"] as $branchoffice)
                                               <option value="{{$branchoffice->id}}" >{{$branchoffice->name}}</option>
                                             @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>


                                </div>


                             <!--line 3 -->
                                <div class="row fields-rows2">

                                   <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Time</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-question phone icons-image"></i></span>
                                        <select class="form-control select-team " title="Tipo da Categoria"  style="width: 100%;">
                                          <option value="" >Selecione o Time...</option>
                                         @foreach ($resp["userteam"]["custom"]["query"] as $team)
                                            <option selected value="{{$team->id}}" placeholder="Time">{{$team->name}}</option>
                                         @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>


                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Categoria</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-tasks phone icons-image"></i></span>
                                        <select class="form-control select-category " title="Categoria"  style="width: 100%;">
                                          <option value="" >Selecione a Categoria...</option>
                                         @foreach ($resp["category"]["custom"]["query"] as $categorys)
                                            <option value="{{$categorys->id}}" data-value="{{$categorys->status}}" placeholder="Categoria(s)">{{$categorys->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>


                             <!--line 4 -->
                                <div class="row fields-rows3">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Urgência</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-ambulance phone icons-image"></i></span>
                                        <select class="form-control select-severity " title="Urgência"  style="width: 100%;">
                                          <option value="" >Selecione a Urgência...</option>
                                         @foreach ($resp["severity"]["custom"]["query"] as $severitys)
                                            <option  value="{{$severitys->id}}" placeholder="Severidade">{{$severitys->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Prioridade</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-sitemap phone icons-image"></i></span>
                                            <select class="form-control select-priority " title="Prioridade">
                                             <option value="" >Escolha a Urgência...</option>
                                            </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                             <!--line 5 -->
                                <div class="row fields-rows4">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Tipo de Atendimento</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-question phone icons-image"></i></span>
                                        <select class="form-control select-typecategory " title="Tipo da Categoria"  style="width: 100%;">
                                          <option value="" >Selecione o Tipo de Atendimento...</option>
                                         @foreach ($resp["typecategory"]["custom"]["query"] as $typecategorys)
                                            <option selected value="{{$typecategorys->id}}" placeholder="Tipo da Categoria">{{$typecategorys->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="form-group">
                                      <label class="label-text">Departamento</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fa fa-user-tie phone icons-image"></i></span>
                                        <select class="form-control select-departament " title="Departamento"  style="width: 100%;">
                                          <option value="" >Selecione o Departamento...</option>
                                         @foreach ($resp["departament"]["custom"]["query"] as $departaments)
                                            <option   value="{{$departaments->id}}" placeholder="Departamento">{{$departaments->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                             <!--end line -->

                             <!--line 6 -->
                                <div class="row fields-rows5">
                                   <div class="col-sm-6">

                                     <div class="form-group">
                                      <label class="label-text">Breve Descrição</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                                        <input type="text" class="form-control "  id="description-add"   name="description" placeholder="* Breve Descrição ">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">

                                     <div class="form-group">
                                      <label class="label-text">Upload(1 arquivo por vez)</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-upload icons-image" ></i></span>

                                        <div class="input-group">
                                             <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputfile" name="inputfile" enctype="multipart/form-data">
                                                <label class="custom-file-label" for="inputfile">Escolha o Arquivo</label>
                                             </div>
                                              <!--<div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                              </div>-->
                                        </div>

                                        </div>

                                    </div>
                                  </div>

                                </div>
                             <!--end line -->

                            <!--line 7 -->
                                <div class="row fields-rows6">
                                   <div class="col-sm-12">

                                     <div class="form-group">
                                      <label class="label-text">Descrição Detalhada</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                                        <textarea class="form-control " cols="5" rows="5"  id="message-add"   name="message" placeholder="* Descrição Detalhada "></textarea>
                                      </div>
                                    </div>
                                  </div>

                                </div>

                          </div>
                       </div>

                        <div class="pull-right" style="text-align: right;">
                            <button type="button" class="btn btn-success btn-circle btn-sm bt-add"><i class="fas fa-check icons-image"></i></button>
                        </div>

                    <!--end-->
                  </div>
                  <?php #dd($resp) ?>


                    <hr>
                 @if($resp['sessionidteambox'] != "" || $resp['sessionstatustime'] != "") <!--show hide tabs if graph pier clicked-->
                     <div class="tab-pane fade div-bo active show"  id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                 @else
                     <div class="tab-pane fade div-bo "  id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                 @endif
                    <div class="custom-control custom-checkbox ">
                      <input class="custom-control-input chk-status" type="checkbox" id="chk-filter" disabled >
                      <label for="chk-filter" class="custom-control-label">Exibir Concluídos , Finalizados , Cancelados e Reprovados</label>
                    </div>
                    <br>
                  @include('ticket.ticketlist') <!--page list 'ticketlist.blade.php' ticket-->


                  </div>


                  @include('ticket.ticketresponse') <!--page reponse 'ticketresponse.blade.php' ticket-->

                        <br>


                   </div>
                    <!-- /.timeline -->


                  </div>

                </div>
              </div>

              <!-- /.card -->
            </div>
          </div>

      </div>
      <!-- /.card-body -->
    </div>
          <!-- /.card -->
  </div>

      @include('ticket.ticketmodals') <!--page reponse 'ticketmodals.blade.php' ticket-->

   </div>
 </div>

 <?php

        #clear session graph
        session(['sessionidteambox' => "" ]);
        session(['sessionstatustime' => "" ]);

 ?>

<script src="<?php echo URL::to('assets/system/js/responseticket/responseticket.js') ?>"></script>
<script src="<?php echo URL::to('assets/system/js/ticket/ticket.js') ?>"></script>
<script src="<?php echo URL::to('assets/system/js/global.js') ?>"></script><!--validate fields-->

