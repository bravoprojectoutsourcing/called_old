  <!--modal edit -->
  <div class="modal fade" id="modal-edit">
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Editar</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="hidden" id="id-modal-edit">
          <div class="modal-body">
            <!--line 1 -->
              <div class="row ">
                 <div class="col-sm-6">                                     
                   <div class="form-group">
                    <label class="label-text responsive">Telefone</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fas fa-phone icons-image" ></i></span>
                      <input type="text" class="form-control "  id="phonecontact-edit"  maxlength="14" name="phonecontact_edit" placeholder="* Telefone ">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6 ">
                  <div class="form-group">
                    <label class=" label-text">Email</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background"><i class="fas fa-mail-bulk phone icons-image" ></i></span>
                      <input type="text" class="form-control" id="email-edit" value="{{$resp['resp']['custom'][0]['email']}}"  name="email" placeholder="* Email">
                    </div>                        
                  </div>   
                 </div>                  
               
              </div>

              <?php #dd($resp) ?>
           <!--line 2 -->   
              <div class="row fields-rows">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Empresas</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-city phone icons-image"></i></span>
                      <select class="form-control select-company-edit " style="width:100%;display:block;box-sizing:border-box">
                        <option value="" >Selecione a Empresa...</option>
                       @foreach ($resp["company"]["custom"]["query"] as $companys)
                          <option value="{{$companys->id}}" >{{$companys->name}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Estabelecimentos</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-archive phone icons-image"></i></span>
                          <select class="form-control select-branchoffice-edit " title="Estabelecimentos">
                          
                          </select>   
                    </div>                                
                  </div>
                </div>


              </div>


           <!--line 3 -->   
              <div class="row fields-rows2">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Departamento</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-user-tie phone icons-image"></i></span>
                      <select class="form-control select-departament-edit " title="Departamento"  style="width: 100%;">
                        @foreach ($resp["departament"]["custom"]["query"] as $departaments)
                          @if($departaments->name!="")
                            <option  selected value="{{$departaments->id}}" placeholder="Departamento" >{{$departaments->name}}</option>
                          @endif
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Categoria</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-tasks phone icons-image"></i></span>
                      <select class="form-control select-category-edit " title="Categoria"  style="width: 100%;">
                        <option value="" >Selecione a Categoria...</option>
                       @foreach ($resp["category"]["custom"]["query"] as $categorys)
                          <option value="{{$categorys->id}}" data-value="{{$categorys->status}}" placeholder="Departamento">{{$categorys->name}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>
              </div>
                

           <!--line 4 -->   
              <div class="row fields-rows3">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Urgência</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-ambulance phone icons-image"></i></span>
                      <select class="form-control select-severity-edit " title="Urgência"  style="width: 100%;">
                        <option value="" >Selecione a Urgência...</option>
                       @foreach ($resp["severity"]["custom"]["query"] as $severitys)
                          <option  value="{{$severitys->id}}" placeholder="Severidade">{{$severitys->name}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Prioridade</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-sitemap phone icons-image"></i></span>
                          <select class="form-control select-priority-edit " title="Prioridade">
                           
                          </select>   
                    </div>                                
                  </div>
                </div>
              </div>

           <!--line 5 -->   
              <div class="row fields-rows4">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="label-text">Tipo de Atendimento</label>
                    <div class="input-group-prepend">
                      <span class="input-group-text icons-background" ><i class="fa fa-question phone icons-image"></i></span>
                      <select class="form-control select-typecategory-edit "  title="Tipo da Categoria"  style="width: 100%;">
                        <option value="" >Selecione o Tipo de Atendimento...</option>
                       @foreach ($resp["typecategory"]["custom"]["query"] as $typecategorys)
                          <option selected value="{{$typecategorys->id}}" placeholder="Tipo da Categoria">{{$typecategorys->name}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>
                 <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label-text">Time</label>
                          <div class="input-group-prepend">
                            <span class="input-group-text icons-background" ><i class="fa fa-question phone icons-image"></i></span>
                            <select class="form-control select-team-edit " disabled="true" title="Tipo da Categoria"  style="width: 100%;">
                              <option value="" >Selecione o Time...</option>
                             @foreach ($resp["userteam"]["custom"]["query"] as $team)
                                <option selected value="{{$team->id}}" placeholder="Time">{{$team->name}}</option>
                              @endforeach
                            </select>   
                          </div>                                
                        </div>
                      </div>

                    </div>
                 <!--end line -->
               
                 <!--line 6 -->   
                    <div class="row fields-rows5">
                       <div class="col-sm-6">
                         
                         <div class="form-group">
                          <label class="label-text">Breve Descrição</label>
                          <div class="input-group-prepend">
                            <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                            <input type="text" class="form-control "  id="description-edit"   name="description" placeholder="* Breve Descrição ">
                          </div>
                        </div>
                      </div>

                    </div>
                 <!--end line -->

                    <!--line 7 -->
                        <div class="row fields-rows6">
                           <div class="col-sm-12">
                             
                             <div class="form-group">
                              <label class="label-text">Descrição Detalhada</label>
                              <div class="input-group-prepend">
                                <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                                <textarea class="form-control " cols="5" rows="5"  id="message-edit"   name="message" placeholder="* Descrição Detalhada "></textarea>
                              </div>
                            </div>
                          </div>

                        </div>  

                </div>   

          
                 <div class="modal-footer justify-content-between fields-rows6">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-danger" id="btn-edit" >Salvar</button>
                 </div>

            </div>
          </div>
     </div>