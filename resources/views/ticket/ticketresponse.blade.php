                  <!--response ticket-->
                  <div class="tab-pane fade div-bo " id="custom-tabs-response" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                        
                    <div class="row">
                      <label id=""></label>
                    </div>

                      <!-- Main content -->
                      <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                          <div class="col-12">
                            <h4>
                              <i class="fas fa-globe"></i> Ticket
                              <small class="float-right" id="date-response"> </small>
                              <!--task 337-->
                              @if($resp['sessionidteambox'] != "" || $resp['sessionstatustime'] != "")
                                &nbsp;&nbsp;
                                <button id="bt-before-page">Voltar para a Listagem</button>
                              @endif
                            
                            </h4>



                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info" >
                          <div class="col-sm-4 invoice-col" style="width: 800px">
                            
                            <address id="response-address" >
                              
                            </address>
                          </div>
                          <!-- /.col -->
                         
                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                       </div>   
                            
                            <!--TASK 212 André - 12/08-->
                            <div class="invoice p-3 mb-3 d-none bt-status ">
                                <div class="row ">
                                   <div class="col-sm-12">
                                    
                                    <div class="row  ">
                                       <div class="col-sm-2">
                                          <button type="button" class="btn btn-success  btn-sm bt-aprove-ticket" data-status = "1" >Aprovar<i class="fas fa-check icons-image"></i></button>
                                       </div>
                                       <div >
                                          <button type="button" class="btn btn-warning  btn-sm bt-reprove-ticket" data-status = "0" >Reprovar<i class="fas fa-check icons-image"></i></button>
                                       </div>

                                    </div>

                                    <br>
                                      <div class=" row icon-obs">
                                            <div class="input-group-prepend col-sm-12">
                                            <span class="input-group-text icons-background " ><i class="fas fa-edit icons-image" ></i></span>
                                            <textarea class="form-control " cols="4" rows="5"  id="category-obs-add"   name="message" placeholder="Observação "></textarea>
                                          </div>
                                      </div>

                                  </div>  
                                 </div> 
                             </div>
                             <!--end task 212-->

                                <!--line 7 -->
                                <div class="row">
                                   <div class="col-sm-4">
                                     
                                     <div class="form-group">
                                      <label class="label-text">Status</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                                        <select class="form-control select-status" style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true">
                                          <option  selected></option>
                                          <option value="Aguardando Solicitante">Aguardando Mais Informações do Solicitante</option> 
                                          <option value="Suspenso">Suspender</option>
                                          <option value="Cancelado">Cancelar</option>
                                          <option value="Concluido">Concluír</option>
                                          <option value="Finalizado">Finalizar</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>

                                </div>         
                                <div class="row">
                                     <div class="form-group col-6">
                                      <label class="label-text">Upload(1 arquivo por vez)</label>
                                      <div class="input-group-prepend">
                                        <span class="input-group-text icons-background" ><i class="fas fa-upload icons-image" ></i></span>
                                        
                                        <div class="input-group">
                                             <div class="custom-file">
                                                <!--<input type="file" class="custom-file-input" style="border:1" id="inputfile-response" name="inputfileresponse" enctype="multipart/form-data">-->
                                                <input type="file" class=""  id="inputfile-response" name="inputfileresponse" enctype="multipart/form-data">
                                                <!--<label  class="custom-file-label-response" for="inputfile" >Escolha o Arquivo</label>-->
                                             </div>
                                              <!--<div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                              </div>-->
                                        </div>

                                        </div>

                                    </div>
                                 </div>   

                                  <br>
                                    <div class=" row ">
                                        <div class="input-group-prepend col-sm-12">
                                          <span class="input-group-text icons-background" ><i class="fas fa-edit icons-image" ></i></span>
                                          <textarea class="form-control " cols="4" rows="5"  id="message-response-add"   name="message" placeholder="Responder "></textarea>
                                        </div>                                       
                                    </div>  

                                    <br>
                                    <hr>
                                 
                               
                                  <div class="pull-right" style="text-align: right;">
                                      <button type="button" class="btn btn-success btn-circle btn-sm bt-add-response" id="bt-add-response2" ><i class="fas fa-check icons-image"></i></button>
                                  </div>
                              <!-- Timelime example  -->

                                    <div class="row" id="div-response" style="width: 100%;overflow-y: auto;max-height: 100%;height: 600px ;margin-left: 0.5%" >

                                      <!-- /.col -->
                                    </div>                               

                        </div>  