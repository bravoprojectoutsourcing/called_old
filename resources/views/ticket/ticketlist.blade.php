<!--NOT finalizados and concluidos-->
<table   class="table table-responsive-sm table-bordered table-striped table-sm list-table "   >
  <thead>
  <tr >
    <th style="width: 5%"  >#</th>
    <th  title="dia em que foi criado" >Empresa </th>
    <th title="Email de quem abriu o chamado" >Breve Descrição</th>
    <th  >Cat.</th>
    <th title="Usuário que respondeu pela última vez" >Ult. Resp.</th>
    <th  >Time</th>
    <th  >Status</th>
    <th title="Tolerância do SLA para essa empresa e categoria">Tempo SLA</th>
    <!--<th  >Hora(s) Gasta(s)</th>--><!--hourdiff-->
    <!--<th  >SLA de Atendimento Hora(s)</th>--><!--timemax -->
    <th  style="font-size: 12px;width: 15%; "  title="Tempo cronometrado em Hora Útil" >Tolerância </th> <!--RESTTIME-->
    <th  style="text-align: center;">SLA</th><!--STATUSTIME-->

<?php #dd($resp["lists"]) ?>

   <th style="text-align: center" >Ações</th>
  </tr>
  </thead>
  <tbody id="tl1">
@if($resp['sessionstatustime']=="")    <!--GRAPH PIER CLICK if empty not click graph-->
  @foreach ($resp["lists"]["custom"]["query"] as $ticket)
    @if(isset($ticket["STATUSTIME"]))
       @if($ticket["STATUSTIME"] != 'NULL')
        @if($ticket["status"] != 'Finalizado' and $ticket["status"] != 'Concluido' and $ticket["status"] != 'Cancelado' and $ticket["status"] != 'Reprovado'  )
         <tr style="position: relative;max-height: -5px;cursor: pointer;">

          <!--COLLUMN ID-->
          <th title="Criado Em: {{date('d m, Y G:i',strtotime($ticket['created'])) }} /n Email: {{$ticket['email']}} ">{{$ticket["id"]}}</th>

          <!--COLLUMN EMPRESA-->
          <th style="font-size: 11px;font-weight: bold; ">{{$ticket["companyname"] }}</th>

          <!--COLLUMN BREVE DESCRIÇÃO-->
          <th style="font-size: 11px;font-weight: bold;">{{$ticket["description"]}}</th>

          <!--COLLUMN NOME CATEGORIA-->
          @if($ticket["namecategorynormal"] != NULL)
            <th style="font-size: 11px;font-weight: bold;">{{$ticket["namecategorynormal"]}}</th>
          @else
            <th style="font-size: 11px;font-weight: bold;">{{$ticket["namecategorynormal"]}}</th>
          @endif

          <!--COLLUMN ULT. RESP-->
           <th style="font-size: 11px;font-weight: bold;" title="Data da última resposta: {{date('d-m-Y G:i',strtotime($ticket['createdresponse']))}}"><pre style="font-size:12px;">{{str_replace(',', "\n", $ticket["userresponse"])}}</pre></th>

           <!--COLLUMN TIME-->
          <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["nameteam"]}}</td>



          <!--add colors status COLLUMN STATUS-->
          @if($ticket["status"] == 'Finalizado')
           <td style="font-size: 11px;font-weight: bold;color: green ">{{$ticket["status"]}}</td>
          @elseif($ticket["status"] == 'Aguardando Atendimento'  )
            <td style="font-size: 11px;font-weight: bold;color: blue ">{{$ticket["status"]}}</td>
          @elseif($ticket["status"] == 'Em Andamento')
            <td style="font-size: 11px;font-weight: bold;color: #FF4500 ">{{$ticket["status"]}}</td>
          @elseif($ticket["status"] == 'Suspenso' || $ticket["status"] == 'Pausado' || $ticket["status"] == 'Aguardando Aprovação' || $ticket["status"] == 'Aguardando Solicitante'  || $ticket["status"] == 'Reprovado'    )
             <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["status"]}}</td>
          @endif

          <!--COLLUMN TOLERÂNCIA SLA-->
          @if($ticket["priorityid"] != "" || $ticket["priorityid"] != null)
            <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["nameburdenurgency"]}}</td>
          @elseif($ticket["priorityid"] == "" || $ticket["priorityid"] == NULL)
              <td style="font-size: 11px;font-weight: bold;color: red;text-align: center ">X</td>
          @endif

          <!--COLLUMN TEMPO RESTANTE-->

              @if(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"]==0 and $ticket["STATUSTIME"]!='')
                <th style="font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center" title="Dentro do prazo , mas não relaxe!"><p style="font-size: 9.5px;font-weight: bold">Ainda no Prazo! </p></th>
              @elseif(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"]==1 and $ticket["STATUSTIME"]!='')
                <th style="font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center" title="Ixi ja era estourou o prazo!"><p style="font-size: 9.5px;font-weight: bold">SLA Estourado! </p></th>
              @elseif(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"]==2 and $ticket["status"] == 'Aguardando Aprovação' or $ticket["status"] == 'Aguardando Solicitante' or $ticket["status"] == 'Suspenso' or $ticket["status"] == 'Pausado' and $ticket["STATUSTIME"]!='')
                <th style="font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center" title="Ixi ja era estourou o prazo!"><p style="font-size: 9.5px;font-weight: bold">Pausado </p></th>
              @elseif(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"] == 3 )
                <th style="font-size: 11px;font-weight: bold;color: red ;cursor:pointer;text-align: center" title="Reprovado!">X</th>
              @elseif( $ticket["STATUSTIME"] == "" and $ticket["priorityid"] == "" and $ticket["priorityid"] == null)
                 <th style="font-size: 11px;font-weight: bold;color: red ;cursor:pointer;text-align: center" title="Reprovado!">X</th>
              @elseif(isset($ticket["DIFERENCETIME"]) and $ticket["STATUSTIME"] == "" and $ticket["priorityid"] != "" or $ticket["priorityid"] != null)
                 <th style="font-size: 11px;font-weight: bold;color: red ;cursor:pointer;text-align: center" title="Reprovado!">X</th>
              @endif


          <!-- SLA  OFF METHOD OR ON ticketService.php comment line -->
          @if(session('sessionsla')=="")
            <td style="font-size: 11px;font-weight: bold;color: red;text-align: center ">Aguarde Carregando SLA...</td>

          @elseif($ticket["priorityid"] == "" || $ticket["priorityid"] == null )
            <td style="font-size: 11px;font-weight: bold;color: red;text-align: center ">Módulo SLA não cadastrado/não existente!</td>
          @endif



          @if(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"] == 0 and $ticket["status"] != "Aguardando Solicitante" and $ticket["status"] != "Aguardando Aprovação" and $ticket["status"] != "Cancelado" and $ticket["status"] != "Pausado" and $ticket["status"] != "Suspenso")
             <td style="font-size: 11px;font-weight: bold;color: blue ;text-align: center;">
                <svg class="bi bi-emoji-sunglasses" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                  <path fill-rule="evenodd" d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z"/>
                  <path d="M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z"/>
                </svg>
             </td>
          @endif

          @if(isset($ticket["DIFERENCETIME"]))
              @if($ticket["DIFERENCETIME"] == 1 and  $ticket["status"] != "Aguardando Solicitante" and $ticket["status"] != "Aguardando Aprovação" and $ticket["status"] != "Cancelado" and $ticket["status"] != "Pausado" and $ticket["status"] != "Suspenso" )
                 <td style="font-size: 11px;font-weight: bold;color: red ;text-align: center;">
                  <svg class="bi bi-emoji-dizzy" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path fill-rule="evenodd" d="M9.146 5.146a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708.708l-.647.646.647.646a.5.5 0 0 1-.708.708l-.646-.647-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708zm-5 0a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 1 1 .708.708l-.647.646.647.646a.5.5 0 1 1-.708.708L5.5 7.207l-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708z"/>
                    <path d="M10 11a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>
                  </svg>

                 </td>
              @endif
          @endif

            @if(session('sessionsla')=="")
            @elseif(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"] == 2  and $ticket["status"] == "Aguardando Solicitante" or $ticket["status"] == "Aguardando Aprovação" or $ticket["status"] == "Cancelado" or $ticket["status"] == "Pausado" or $ticket["status"] == "Suspenso" )
             <td style="font-size: 11px;font-weight: bold;color: orange ;text-align: center;">
                <svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-emoji-frown" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                  <path fill-rule="evenodd" d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"/>
                  <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z"/>
                </svg>

             </td>
          @endif


          @if(isset($ticket["DIFERENCETIME"]) and $ticket["status"] == 'Finalizado' and $ticket["STATUSTIME"]!= '')
             <td style="font-size: 11px;font-weight: bold;color: green ;text-align: center;">
                <svg class="bi bi-emoji-laughing" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                  <path fill-rule="evenodd" d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z"/>
                  <path d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z"/>
                </svg>
             </td>
          @endif



          @if(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"]==2 and $ticket["STATUSTIME"]!= '')
             <td style="font-size: 11px;font-weight: bold;color: green ;text-align: center;">
                <svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-alarm" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z"/>
                </svg>
                <!--<i class=" livicon iconrigth" data-n="alarm" data-s="40"
                 data-c="green" data-hc="0" data-onparent="true"></i>-->
             </td>
          @endif


          @if(isset($ticket["DIFERENCETIME"]) and $ticket["DIFERENCETIME"]==3 and $ticket["STATUSTIME"]!= ''  )
             <td style="font-size: 11px;font-weight: bold;color: #FF8C00 ;text-align: center;">
                <svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-emoji-frown" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                  <path fill-rule="evenodd" d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"/>
                  <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z"/>
                </svg>
                <!--<i class=" livicon iconrigth" data-n="ban" data-s="40"
                 data-c="red" data-hc="0" data-onparent="true"></i>-->
             </td>
          @endif


          <td style="text-align: center" colspan="2">
            <div class="btn-group">

               <div  class="col-sm-4">
                  @if((session('resp')["custom"][0]["nameprofile"] == "REQUESTER" || session('resp')["custom"][0]["nameprofile"] == "CLERK" ) AND $ticket["status"] == "Aguardando Aprovação" )
                    <button type="button" class="btn btn-default btn-sm  " disabled ><i class="fas fa-edit"></i></button>
                  @elseif((session('resp')["custom"][0]["nameprofile"] != "REQUESTER" and session('resp')["custom"][0]["nameprofile"] != "CLERK")  and $ticket["status"] != "Aguardando Aprovação")
                    <button type="button" class="btn btn-default btn-sm  edit-ticket"  data-toggle="modal" data-target="#modal-edit" data-value="{{ $ticket['id'] }}" data-category=""><i class="fas fa-edit"></i></button>
                  @elseif((session('resp')["custom"][0]["nameprofile"] != "REQUESTER" and session('resp')["custom"][0]["nameprofile"] != "CLERK")  and $ticket["status"] == "Aguardando Aprovação")
                    <button type="button" class="btn btn-default btn-sm  edit-ticket"  data-toggle="modal" data-target="#modal-edit" data-value="{{ $ticket['id'] }}" data-category=""><i class="fas fa-edit"></i></button>
                  @elseif((session('resp')["custom"][0]["nameprofile"] == "REQUESTER" OR session('resp')["custom"][0]["nameprofile"] == "CLERK") and  $ticket["status"] != "Aguardando Aprovação" )
                    <button type="button" class="btn btn-default btn-sm  edit-ticket"  data-toggle="modal" data-target="#modal-edit" data-value="{{ $ticket['id'] }}" data-category=""><i class="fas fa-edit"></i></button>
                  @endif

               </div>
              <div  class="col-sm-4">
                @if((session('resp')["custom"][0]["nameprofile"] == "REQUESTER" || session('resp')["custom"][0]["nameprofile"] == "CLERK" ) AND $ticket["status"] == "Aguardando Aprovação" )
                  <button type="button" class="btn btn-danger btn-sm " disabled ><i class="fas fa-question-circle"></i></button>
               @elseif((session('resp')["custom"][0]["nameprofile"] != "REQUESTER" and session('resp')["custom"][0]["nameprofile"] != "CLERK")  and $ticket["status"] != "Aguardando Aprovação")
                  <button type="button" class="btn btn-success btn-sm bt-response" data-value="{{ $ticket['id'] }}" data-status = "{{ $ticket['status'] }}" data-team="{{  session('resp')['custom'][0]['teamid']}}" data-category="{{ $ticket['idcategory'] }}" ><i class="fas fa-question-circle"></i></button>
               @elseif((session('resp')["custom"][0]["nameprofile"] != "REQUESTER" and session('resp')["custom"][0]["nameprofile"] != "CLERK")  and $ticket["status"] == "Aguardando Aprovação")
                  <button type="button" class="btn btn-success btn-sm bt-response" data-value="{{ $ticket['id'] }}" data-status = "{{ $ticket['status'] }}"
                  data-team="{{  session('resp')['custom'][0]['teamid'] }}"   data-category="{{ $ticket['idcategory'] }}" ><i class="fas fa-question-circle"></i></button>
               @elseif((session('resp')["custom"][0]["nameprofile"] == "REQUESTER" OR session('resp')["custom"][0]["nameprofile"] == "CLERK") and  $ticket["status"] != "Aguardando Aprovação" )
                  <button type="button" class="btn btn-success btn-sm bt-response" data-value="{{ $ticket['id'] }}" data-status = "{{ $ticket['status'] }}" data-team="{{ session('resp')['custom'][0]['teamid'] }}" data-category="{{ $ticket['idcategory'] }}" ><i class="fas fa-question-circle"></i></button>

               @endif

              </div>
              <div  class="col-sm-1">
                 <!-- <button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{ $ticket['id'] }}" ><i class="fas fa-trash"></i></button>-->
              </div>

            </div>
           </td>
        </tr>
      @endif
     @endif
   @endif

  @endforeach
 @endif<!--sessionstatustime-->
  </tbody>
</table>

<!--end NOT finalizados and concluidos-->

<!--finalizados and concluidos-->

<table   class="table table-responsive-sm table-bordered table-striped table-sm list-table2 d-none      "   >
  <thead>
  <tr >
    <th style="width: 5%;"  >#</th>
    <th  title="dia em que foi criado" >Empresa </th>
    <th title="Email de quem abriu o chamado" >Breve Descrição</th>
    <th  >Cat.</th>
    <th title="Usuário que respondeu pela última vez" >Ult. Resp.</th>
    <th  >Time</th>
    <th  >Status</th>
    <th title="Tolerância do SLA para essa empresa e categoria">Tempo SLA</th>
    <th  style="font-size: 12px;width: 15%; "  title="Tempo cronometrado em Hora Útil" >Tolerância </th>
    <th  style="text-align: center;">SLA</th>


   <th style="text-align: center" >Ações</th>
  </tr>
  </thead>
  <tbody id="tl2">

  @foreach ($resp["lists"]["custom"]["query"] as $ticket)

    @if($ticket["status"] == 'Finalizado' || $ticket["status"] == 'Concluido' || $ticket["status"] == 'Cancelado' || $ticket["status"] == 'Reprovado')
     <tr style="position: relative;max-height: -5px;cursor: pointer;">
      <th title="Criado Em: {{date('d m, Y G:i',strtotime($ticket['created'])) }} /n Email: {{$ticket['email']}} ">{{$ticket["id"]}}</th>
      <th style="font-size: 11px;font-weight: bold; ">{{$ticket["companyname"] }}</th>
      <th style="font-size: 11px;font-weight: bold;">{{$ticket["description"]}}</th>
          <!--COLLUMN NOME CATEGORIA-->
          @if($ticket["namecategorynormal"] != NULL)
            <th style="font-size: 11px;font-weight: bold;">{{$ticket["namecategorynormal"]}}</th>
          @else
            <th style="font-size: 11px;font-weight: bold;">{{$ticket["namecategorynormal"]}}</th>
          @endif
       <th style="font-size: 11px;font-weight: bold;" title="Data da última resposta: {{date('d-m-Y G:i',strtotime($ticket['createdresponse']))}}"><pre style="font-size:12px;">{{str_replace(',', "\n", $ticket["userresponse"])}}</pre></th>
      <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["nameteam"]}}</td>


      <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["status"]}}</td>


    <!--COLLUMN TOLERÂNCIA SLA-->
    @if($ticket["priorityid"] != "" || $ticket["priorityid"] != null)
      <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["nameburdenurgency"]}}</td>
    @elseif($ticket["priorityid"] == "" || $ticket["priorityid"] == NULL)
        <td style="font-size: 11px;font-weight: bold;color: red;text-align: center ">X</td>

    @endif



  @if($ticket["status"] == 'Finalizado' || $ticket["status"] == 'Concluido' || $ticket["status"] == 'Cancelado' || $ticket["status"] == 'Reprovado')


      @isset($ticket["DIFERENCETIME"])
               @if($ticket["DIFERENCETIME"]==0 and $ticket["priorityid"] != "" || $ticket["priorityid"] != NULL)
                <th style="font-size: 11px;font-weight: bold;color: green ;cursor:pointer;text-align: center" title="Parabéns!">
                  Concluído no Prazo!
                </th>
                @elseif($ticket["DIFERENCETIME"]==5 and $ticket["priorityid"] != "" || $ticket["priorityid"] != NULL)
                <th style="font-size: 11px;font-weight: bold;color: green ;cursor:pointer;text-align: center" title="Parabéns!">
                  Sem data de resposta do ticket e data de última resposta!
                </th>
               @elseif($ticket["DIFERENCETIME"]==1 and $ticket["priorityid"] != "" || $ticket["priorityid"] != NULL)
                <th style="font-size: 11px;font-weight: bold;color: #FF4500 ;cursor:pointer;text-align: center" title="Parabéns mais ou menos!">  Concluído porém fora do Prazo!</th>
               @elseif($ticket["slacalculate"]=="" ||  $ticket["slacalculate"] == null || $ticket["DIFERENCETIME"] == 3)
                <th style="font-size: 11px;font-weight: bold;color: #FF4500 ;cursor:pointer;text-align: center" title="Parabéns mais ou menos!">  Reprovado!</th>
               @elseif($ticket["priorityid"] == "" || $ticket["priorityid"] == NULL )
                 <th style="font-size: 11px;font-weight: bold;color: #FF4500 ;cursor:pointer;text-align: center" title="Parabéns mais ou menos!">
                  X
                </th>
                @endif

      @else


           @if($ticket["slacalculate"]==0 and $ticket["priorityid"] != "" || $ticket["priorityid"] != NULL)
                <th style="font-size: 11px;font-weight: bold;color: green ;cursor:pointer;text-align: center" title="Parabéns!">
                  Concluído no Prazo!
                </th>
           @elseif($ticket["slacalculate"]==1 and $ticket["priorityid"] != "" || $ticket["priorityid"] != NULL)
                <th style="font-size: 11px;font-weight: bold;color: #FF4500 ;cursor:pointer;text-align: center" title="Parabéns mais ou menos!">  Concluído porém fora do Prazo!</th>

           @elseif($ticket["priorityid"] == "" || $ticket["priorityid"] == NULL )
                 <th style="font-size: 11px;font-weight: bold;color: #FF4500 ;cursor:pointer;text-align: center" title="Parabéns mais ou menos!">
                  X
                </th>
           @endif


      @endisset





  @elseif($ticket["status"] == 'Aguardando Atendimento' )
    <td style="font-size: 11px;font-weight: bold;color: blue ">{{$ticket["status"]}}</td>
  @elseif($ticket["status"] == 'Pausado'  )
    <td style="font-size: 11px;font-weight: bold;color: red ">{{$ticket["status"]}}</td>
  @elseif($ticket["status"] == 'Em Andamento' )
    <td style="font-size: 11px;font-weight: bold;color: #FF4500 ">{{$ticket["status"]}}</td>
  @elseif(isset($ticket["DIFERENCETIME"]) and  isset($ticket["STATUSTIME"]) and $ticket["DIFERENCETIME"]==2 and $ticket["status"] == 'Aguardando Aprovação'|| $ticket["status"] == 'Aguardando Solicitante' || $ticket["status"] == 'Suspenso' || $ticket["status"] == 'Pausado' || $ticket["status"] == 'Cancelado')
    <th style="font-size: 11px;font-weight: bold;color: #FF4500;max-height: 5px ;cursor:pointer;text-align: center" title="ticket pausado.">X</th>

  @endif





  @if($ticket["priorityid"] == "" || $ticket["priorityid"] == NULL)
    <td style="font-size: 11px;font-weight: bold;color: red;text-align: center ">Módulo SLA não cadastrado/não existente!</td>

  @else

   @isset($ticket["DIFERENCETIME"])
      @if($ticket["DIFERENCETIME"] == 3 and $ticket["status"] == 'Reprovado')
         <td style="font-size: 11px;font-weight: bold;color: orange ;text-align: center;">
          <svg class="bi bi-emoji-frown"  width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path fill-rule="evenodd" d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"/>
            <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z"/>
          </svg>
         </td>
      @endif
   @else
      @if($ticket["status"] == 'Reprovado')
         <td style="font-size: 11px;font-weight: bold;color: orange ;text-align: center;">
          <svg class="bi bi-emoji-frown"  width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path fill-rule="evenodd" d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"/>
            <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z"/>
          </svg>
         </td>
      @endif
   @endisset

      @if($ticket["status"] == 'Concluido')
         <td style="font-size: 11px;font-weight: bold;color: blue ;text-align: center;">
            <svg class="bi bi-emoji-laughing" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path fill-rule="evenodd" d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z"/>
              <path d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z"/>
            </svg>
          </svg>
         </td>
      @endif

      @if($ticket["status"] == 'Cancelado' )
         <td style="font-size: 11px;font-weight: bold;color: blue ;text-align: center;">
            <svg class="bi bi-emoji-frown"  width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
          <path fill-rule="evenodd" d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"/>
          <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z"/>
         </svg>
        </td>
      @endif


      @if($ticket["status"] == 'Finalizado'  )
         <td style="font-size: 11px;font-weight: bold;color: green ;text-align: center;">
            <svg class="bi bi-emoji-laughing" width="32" height="32" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path fill-rule="evenodd" d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z"/>
              <path d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z"/>
            </svg>
         </td>
      @endif

  @endif





      <td style="text-align: center" colspan="2">
        <div class="btn-group">
           <div  class="col-sm-4">
              <button type="button" class="btn btn-default btn-sm  edit-ticket"  data-toggle="modal" data-target="#modal-edit" data-value="{{ $ticket['id'] }}"><i class="fas fa-edit"></i></button>
           </div>
          <div  class="col-sm-4">
             <button type="button" class="btn btn-success btn-sm bt-response" data-value="{{ $ticket['id'] }}" ><i class="fas fa-question-circle"></i></button>
          </div>
          <div  class="col-sm-1">
              <!--<button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{ $ticket['id'] }}" ><i class="fas fa-trash"></i></button>-->
          </div>

        </div>
      </td>
     </tr>
    @endif
  @endforeach

  </tbody>
</table>