
    <!--modal add -->
    <div class="modal fade" id="modal-add"   >
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
    <div class="modal-content" >
      <div class="modal-header" >
        <h4 class="modal-title" id="title-modal-add">Adicionar </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >

          <!--line 1 -->
              <div class="row">

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-file-invoice"></i></span>
                      <input type="text" class="form-control " onkeypress="$(this).mask('00.000.000/0000-00')" id="cpfcnpj-add"  maxlength="14" name="cpfcnpj" placeholder="* CNPJ">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-building"></i></span>
                      <input type="text" class="form-control" id="name-add"  name="name" placeholder="* Empresa">
                    </div>                        
                  </div>   
                 </div>                  
              </div>

              
           <!--line 2 -->   
              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                      <input type="text" class="form-control" id="address-add"  name="address" placeholder="Endereço">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mail-bulk"></i></span>
                      <input type="text" class="form-control" id="cep-add"  name="cep"  onkeypress="$(this).mask('00.000-000')" placeholder="CEP">
                    </div>                        
                  </div>
                </div>


              </div>


           <!--line 3 -->   
              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-globe-africa"></i></span>
                      <select class="form-control select-state " style="width: 100%;">
                        <option data-value="null" value="null" >Selecione o Estado...</option>
                       @foreach ($state as $states)
                          <option data-value="{{$states->idstate}}" value="{{$states->idstate}}">{{$states->alias}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-city"></i></span>
                          <select class="form-control select-city " >
                           
                          </select>   
                    </div>                                
                  </div>
                  </div>
              </div>
                

           <!--line 4 -->   
              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      <input type="text" class="form-control" id="phone-add"  name="phone" placeholder="Telefone">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-at"></i></span>
                      <input type="text" class="form-control" id="email-add"  name="email" placeholder="Email">
                    </div>                        
                  </div>
                </div>


              </div>

           <!--line 5 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-pin"></i></span>
                      <input type="text" class="form-control" id="neighborhood-add"  name="neighborhood" placeholder="Bairro">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-location-arrow"></i></span>
                      <input type="text" class="form-control" id="number-add"  name="number" placeholder="Número">
                    </div>                        
                  </div>
                </div>
              </div>



           <!--line 6 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-plus"></i></span>
                      <input type="text" class="form-control" id="compl-add"  name="compl" placeholder="Complemento">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-fantasy-flight-games"></i></span>
                      <input type="text" class="form-control" id="fantasy-name-add"  name="fantasy-name-add" placeholder="Nome Fantasia">
                    </div>                        
                  </div>
                </div>
              </div>

           </div>
             <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" id="btn-add" >Cadastrar</button>
             </div>

        </div>

       </div>

        </div>
      </div>
    </div>
    </div>


    <!--modal edit -->
    <div class="modal fade" id="modal-edit">
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" id="id-modal-edit">
      <div class="modal-body">
                      <!--line 1 -->
              <div class="row">

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-file-invoice"></i></span>
                      <input type="text" class="form-control" id="cpfcnpj-edit"  onkeypress="$(this).mask('00.000.000/0000-00')" name="cpfcnpj" maxlength="14"  placeholder="* CNPJ">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-building"></i></span>
                      <input type="text" class="form-control" id="name-edit"  name="name" placeholder="* Empresa">
                    </div>                        
                  </div>   
                 </div>                  




                
              </div>

              
           <!--line 2 -->   
              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                      <input type="text" class="form-control" id="address-edit"  name="address" placeholder="Endereço">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mail-bulk"></i></span>
                      <input type="text" class="form-control" id="cep-edit"  onkeypress="$(this).mask('00.000-000')" name="cep" placeholder="CEP">
                    </div>                        
                  </div>
                </div>


              </div>


           <!--line 3 -->   
              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-globe-africa"></i></span>
                      <select class="form-control select-state-edit " style="width: 100%;">
                        <option data-value="null" >Selecione o Estado...</option>
                       @foreach ($state as $states)
                          <option data-value="{{$states->idstate}}">{{$states->alias}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-city"></i></span>
                          <select class="form-control select-city-edit" >
                            
                          </select>   
                    </div>                                
                  </div>
                  </div>
              </div>
                

           <!--line 4 -->   
              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      <input type="text" class="form-control" id="phone-edit"  name="phone" placeholder="Telefone">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-at"></i></span>
                      <input type="text" class="form-control" id="email-edit"  name="email" placeholder="Email">
                    </div>                        
                  </div>
                </div>


              </div>

           <!--line 5 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-pin"></i></span>
                      <input type="text" class="form-control" id="neighborhood-edit"  name="neighborhood" placeholder="Bairro">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-location-arrow"></i></span>
                      <input type="text" class="form-control" id="number-edit"  name="number" placeholder="Número">
                    </div>                        
                  </div>
                </div>
              </div>

           <!--line 6 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-plus"></i></span>
                      <input type="text" class="form-control" id="compl-edit"  name="compl" placeholder="Complemento">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-fantasy-flight-games"></i></span>
                      <input type="text" class="form-control" id="fantasy-name-edit"  name="fantasy-name-edit" placeholder="Nome Fantasia">
                    </div>                        
                  </div>
                </div>
              </div>

           </div>
             <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" id="btn-edit" >Salvar</button>
             </div>

        </div>
      </div>
    </div>

    <!--modal edit branch office -->
    <div class="modal fade" id="modal-edit-branchoffice">
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editar Estabelecimento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" id="id-modal-edit-bo">
      <div class="modal-body">
                      <!--line 1 -->
              <div class="row">

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-file-invoice"></i></span>
                      <input type="text" class="form-control" id="cpfcnpj-edit-bo"  onkeypress="$(this).mask('00.000.000/0000-00')" name="cpfcnpj" maxlength="14"  placeholder="CNPJ">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-building"></i></span>
                      <input type="text" class="form-control" id="name-edit-bo"  name="name" placeholder="Empresa">
                    </div>                        
                  </div>   
                 </div>       
              </div>

              
           <!--line 2 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                      <input type="text" class="form-control" id="address-edit-bo"  name="address" placeholder="Endereço">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mail-bulk"></i></span>
                      <input type="text" class="form-control" id="cep-edit-bo"  onkeypress="$(this).mask('00.000-000')" name="cep" placeholder="CEP">
                    </div>                        
                  </div>
                </div>


              </div>


           <!--line 3 -->   
              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-globe-africa"></i></span>
                      <select class="form-control select-state-edit-bo" style="width: 100%;">
                        <option data-value="null" >Selecione o Estado...</option>
                       @foreach ($state as $states)
                          <option data-value="{{$states->idstate}}">{{$states->alias}}</option>
                        @endforeach
                      </select>   
                    </div>                                
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" ><i class="fa fa-city"></i></span>
                          <select class="form-control select-city-edit-bo" >                                
                          </select>   
                    </div>                                
                  </div>
                  </div>
              </div>
                

           <!--line 4 -->   
              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      <input type="text" class="form-control" id="phone-edit-bo"  name="phone" placeholder="Telefone">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-at"></i></span>
                      <input type="text" class="form-control" id="email-edit-bo"  name="email" placeholder="Email">
                    </div>                        
                  </div>
                </div>


              </div>

           <!--line 5 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-map-pin"></i></span>
                      <input type="text" class="form-control" id="neighborhood-edit-bo"  name="neighborhood" placeholder="Bairro">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-location-arrow"></i></span>
                      <input type="text" class="form-control" id="number-edit-bo"  name="number" placeholder="Número">
                    </div>                        
                  </div>
                </div>
              </div>



           <!--line 6 -->   
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-plus"></i></span>
                      <input type="text" class="form-control" id="compl-edit-bo"  name="compl" placeholder="Complemento">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-fantasy-flight-games"></i></span>
                      <input type="text" class="form-control" id="fantasy-name-edit-bo"  name="fantasy-name" placeholder="Nome Fantasia">
                    </div>                        
                  </div>
                </div>
              </div>

           </div>
             <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" id="btn-edit-bo" >Salvar</button>
             </div>

        </div>
      </div>
    </div>
 </div> 