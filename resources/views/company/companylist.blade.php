<table   class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" width="100%"  >
  <thead>
  <tr >
    <th style="width: 10%;" class="d-none" >#</th>
    <th  >Nome</th>
    <th  >Endereço</th>
    <th style="width: 10%;text-align: center" >Ações</th>
  </tr>
  </thead>
  <tbody>
  @foreach ($resp["list"]["custom"]["query"] as $company)
 
     <tr style="position: relative;max-height: -5px">
      <td class="d-none">{{$company["id"]}}</td>
      <td>{{$company["name"]}}</td>
      <td>{{$company["address"]}}</td>
      <td style="text-align: center" colspan="2">
        <div class="btn-group">
           <div  class="col-sm-4">
              <button type="button" class="btn btn-default btn-sm  btn-list-branchoffice"  data-value="{{ $company['id'] }}"><i class="fas fa-building"></i></button>
           </div>
           <div  class="col-sm-4">
              <button type="button" class="btn btn-default btn-sm  edit-company"  data-toggle="modal" data-target="#modal-edit" data-value="{{ $company['id'] }}"><i class="fas fa-edit"></i></button>
           </div>
          <div  class="col-sm-1">
              <button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{ $company['id'] }}" ><i class="fas fa-trash"></i></button>
          </div>
        </div>
      </td>               
  @endforeach                
  </tbody>  
</table>