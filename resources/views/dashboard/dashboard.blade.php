  <!-- Content Wrapper. Contains page content -->


    <section class="content-header">
      <div class="container-fluid">


    </section>

    <style type="text/css">

      .form-inline{
          display:block;
      }

       .dataTables_filter{
        position: relative ;
      }

      .dataTables_length{
        position: relative;
      }

      .select2-selection__rendered {
          line-height: 16px !important;
      }


    </style>


<br><br>
    <div class="card"  style="margin-left: 17.5%">

      <div class="card-body div-list" >


        <div class="row"  >
          <div class="col-sm-6">
            <h2>Visão Geral</h2>
          </div>
        </div>
		<br>

<div class="menu-wrap" style="margin-left: -4%;height: 500px;top:-0.5%">
				<nav class="menu">
					<div class="icon-list" >
						<!--<a href="#"><i class="fa fa-fw fa-star-o"></i><span>Favorites</span></a>
						<a href="#"><i class="fa fa-fw fa-bell-o"></i><span>Alerts</span></a>
						<a href="#"><i class="fa fa-fw fa-envelope-o"></i><span>Messages</span></a>
						<a href="#"><i class="fa fa-fw fa-comment-o"></i><span>Comments</span></a>
						<a href="#"><i class="fa fa-fw fa-bar-chart-o"></i><span>Analytics</span></a>
						<a href="#"><i class="fa fa-fw fa-newspaper-o"></i><span>Reading List</span></a>-->
					</div>
				</nav>
				<button class="close-button" id="close-button">Close Menu</button>
				<div class="morph-shape" id="morph-shape" data-morph-open="M-7.312,0H15c0,0,66,113.339,66,399.5C81,664.006,15,800,15,800H-7.312V0z;M-7.312,0H100c0,0,0,113.839,0,400c0,264.506,0,400,0,400H-7.312V0z">
					<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 1000" preserveAspectRatio="none">
						<path d="M-7.312,0H0c0,0,0,113.839,0,400c0,264.506,0,400,0,400h-7.312V0z"/>
					</svg>
				</div>
</div>


      	<div class="row" >

      		 <div class="col-12 col-sm-6 col-md-3">
      		 	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
					<div class=" info-box small-box bg-success bt-status" data-value="Finalizado"  style="cursor: pointer;" id="open-button" ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
	            	<div class=" info-box small-box bg-success "   ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @endif

	              <div class="inner">
	                <h3 id="idboxclosed">0</h3>

	                <p>Finalizados(s)</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>

	           </div>
	         </div>
	          <div class="col-12 col-sm-6 col-md-3">
	          	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
				<div class=" info-box small-box bg-warning bt-status" data-value="Em Andamento"  style="cursor: pointer;" id="open-button2"  > <!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
				@else
					<div class=" info-box small-box bg-warning "  > <!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->

				@endif
	              <div class="inner">
	                <h3 id="idboxopen1">0</h3>

	                <p>Em Andamento</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>
	           </div>

	          </div>

	          <div class="col-12 col-sm-6 col-md-3">
	          	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
					<div class=" info-box small-box bg-primary bt-status" data-value="Aguardando Atendimento"  style="cursor: pointer;" id="open-button3"  ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
					<div class=" info-box small-box bg-primary "  >
	            @endif

	              <div class="inner">
	                <h3 id="idboxopen2">0</h3>

	                <p> Aguardando Atendimento</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>
	           </div>
	          </div>

	          <div class="col-12 col-sm-6 col-md-3">
	          	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
					<div class=" info-box small-box bg-danger bt-status" data-value="Cancelado"  style="cursor: pointer;" id="open-button4"  ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
	            	<div class=" info-box small-box bg-danger "  >
	            @endif

	              <div class="inner">
	                <h3 id="idboxcancel">0</h3>
	                <p>Cancelado(s)</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>
	           </div>
	          </div>

      		 <div class="col-12 col-sm-6 col-md-3">
      		 	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
					<div class=" info-box small-box bt-status" data-value="Aguardando Solicitante"  style="background-color: #d6d8d9;cursor: pointer;"  id="open-button5" ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	           	@else
	           		<div class=" info-box small-box "   style="background-color: #d6d8d9"   >
	           	@endif

	              <div class="inner">
	                <h3 id="idboxwait">0</h3>

	                <p>Aguardando Solicitante</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>

	           </div>
	         </div>
      		 <div class="col-12 col-sm-6 col-md-3">
      		 	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))

					<div class=" info-box small-box bt-status" data-value="Concluido"  style="background-color: #d1ecf1;cursor: pointer;"   id="open-button6" ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
	            	<div class=" info-box small-box bt-status"   style="background-color: #d1ecf1"   >
	            @endif

	              <div class="inner">
	                <h3 id="idboxconcluded">0</h3>

	                <p>Concluído(s)</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>

	           </div>
	         </div>

	         <!--aprovados task-324 -->
      		 <div class="col-12 col-sm-6 col-md-3">
      		 	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))

					<div class=" info-box small-box bt-status" data-value="Aguardando Aprovacao"  style="background-color: #00BFFF;cursor: pointer;"   id="open-button7" ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
	            	<div class=" info-box small-box bt-status"   style="background-color: #00BFFF"   >
	            @endif

	              <div class="inner">
	                <h3 id="idboxonapproval">0</h3>

	                <p>Aguardando Aprovação</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>

	           </div>
	         </div>


	         <!--disapproved task-324 -->
      		 <div class="col-12 col-sm-6 col-md-3">
      		 	@if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))

					<div class=" info-box small-box bt-status" data-value="Reprovado"  style="background-color: #FF6347;cursor: pointer;"  id="open-button8" ><!--ATTENTION!!! id='open-button' file  public/assets/dist/js/menuteam/main4.js-->
	            @else
	            	<div class=" info-box small-box bt-status"   style="background-color: #FF6347"   >
	            @endif

	              <div class="inner">
	                <h3 id="idboxdisapproved">0</h3>

	                <p>Reprovado(s)</p>
	              </div>
	              <div class="icon" >
	                <i class="ion ion-pie-graph"></i>
	              </div>

	           </div>
	         </div>




        </div>


		<!--BAR GRAPH-->
		<div class="row">
			<div class="card card-dark" style="position: relative;width: 70%;">
				<div class="card-header">
					<h3 class="card-title" style="color: white">
						<i class="fas fa-chart-pie mr-1"></i>
						Chamados Concluído(S)/Em Andamento/Aguardando Atendimento /Cancelado(s) Mês
					</h3>
				</div>
				<div class="card-body" >
					<div class="tab-content ">
						<div class="chart" style="margin-top: -15%">
							<canvas id="salesChart"  ></canvas>
						</div>
						<div class="chart tab-pane active" id="revenue-barchart">
							<canvas id="revenue-barchart-canvas"></canvas>
						</div>
						<div class="chart tab-pane" id="sales-chart" >
							<canvas id="sales-barchart-canvas" ></canvas>
						</div>
					</div>
				</div>
			</div>

            <!-- /.card -->

          @if((session('resp')["custom"][0]["nameprofile"] != "REQUESTER"))
            <!-- DONUT CHART -->
            <div class="card card-dark" style="position: relative;width: 30%;left: 1%;height: 300px">
              <div class="card-header">
                <h3 class="card-title" style="color: white">SLA </h3>
              </div>
              <div class="card-body">
                <div class="spinner-border text-dark iconchange" style="position: relative;left: 50%;margin-top: 30% "><p style="color: red;font-weight: bold; ">B</p></div>
                <canvas id="donutChart" style="position: relative;left: 2%;min-height: 200px; height: 200px; max-height: 200px; max-width: 90%;cursor: pointer;"></canvas>


              </div>
            </div>
          @endif
        </div>
       </div>
		<!--END BAR GRAPH-->


			<!--<div class="card " style="position: relative;width: 70%;">
				  <div class="card-header">
				    <h3 class="card-title">
				      <i class="fas fa-chart-pie mr-1"></i>
				      Chamados Concluído(S)/Em Andamento/Aguardando Atendimento /Cancelado(s) Mês
				    </h3>
					<div class="col-md-12" style="margin-top:  30%">
                    <div class="chart" style="margin-top: -40%;height: 110%">
                      <canvas id="salesChart"  ></canvas>
                    </div>
                  </div>



					  <div class="card-body">
					    <div class="tab-content p-1">

					      <div class="chart tab-pane active" id="revenue-chart"
					           style="position: relative; max-height: 10%;">
					          <canvas id="revenue-chart-canvas"></canvas>
					       </div>
					      <div class="chart tab-pane" id="sales-chart" >
					        <canvas id="sales-chart-canvas" ></canvas>
					      </div>
					    </div>
					  </div>

				</div>

          	</div>-->

				<!--
				<div class="card">
				              <div class="card-header border-transparent">
				                <h3 class="card-title">Últimos Chamados </h3>

				                <div class="card-tools">
				                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
				                    <i class="fas fa-minus"></i>
				                  </button>
				                  <button type="button" class="btn btn-tool" data-card-widget="remove">
				                    <i class="fas fa-times"></i>
				                  </button>
				                </div>
				              </div>

				              <div class="card-body p-0">
				                <div class="table-responsive">
				                  <table class="table m-0">
				                    <thead>
				                    <tr>
				                      <th>Chamado</th>
				                      <th>suporte</th>
				                      <th>Status</th>
				                      <th>Equipe</th>
				                    </tr>
				                    </thead>
				                    <tbody>
				                    <tr>
				                      <td><a href="pages/examples/invoice.html">OR9842</a></td>
				                      <td>Mastersaf</td>
				                      <td><span class="badge badge-success">Atendido</span></td>
				                      <td>
				                        <div class="sparkbar" data-color="#00a65a" data-height="20">AMS</div>
				                      </td>
				                    </tr>
				                    <tr>
				                      <td><a href="pages/examples/invoice.html">OR1848</a></td>
				                      <td>Sistema de Suporte</td>
				                      <td><span class="badge badge-warning">Pendente</span></td>
				                      <td>
				                        <div class="sparkbar" data-color="#f39c12" data-height="20">Fábrica de Software</div>
				                      </td>
				                    </tr>
				                    <tr>
				                      <td><a href="pages/examples/invoice.html">OR7429</a></td>
				                      <td>Instalar VPN André</td>
				                      <td><span class="badge badge-danger">Resolvido</span></td>
				                      <td>
				                        <div class="sparkbar" data-color="#f56954" data-height="20">Helpitech</div>
				                      </td>
				                    </tr>

				                    </tbody>
				                  </table>
				                </div>

				              </div>

				              <div class="card-footer clearfix">
				                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
				                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
				              </div>

				            </div>
			-->
      </div>
   </div>
<iframe id="song3" class="video d-none" width="300" height="300" src="{{ URL::to('assets/sound/slow-spring-board.mp3') }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="autoplay"></iframe>
<!--menuteam dashboard-->
<script src="{{ URL::to('assets/dist/js/menuteam/snap.svg-min.js') }}"></script>
<script src="{{ URL::to('assets/dist/js/menuteam/classie.js') }}"></script>
<script src="{{ URL::to('assets/dist/js/menuteam/main4.js') }}"></script>