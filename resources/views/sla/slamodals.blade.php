<!--modal add -->
<div class="modal fade" id="modal-add"   >
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
        <div class="modal-content" >
            <div class="modal-header" >
                <h4 class="modal-title" id="title-modal-add">Adicionar </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="row fields-rows">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-text">Empresas</label>
                      <div class="input-group-prepend">
                        <span class="input-group-text icons-background" ><i class="fa fa-city phone icons-image"></i></span>
                        <select class="form-control select-company " style="width:100%;display:block;box-sizing:border-box">
                          <option value="" >Selecione a Empresa...</option>
                         @foreach ($resp["company"]["custom"]["query"] as $companys)
                            <option value="{{$companys->id}}" >{{$companys->name}}</option>
                          @endforeach
                        </select>   
                      </div>                                
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-text">Categoria</label>
                      <div class="input-group-prepend">
                        <span class="input-group-text icons-background" ><i class="fa fa-archive phone icons-image"></i></span>
                         <select class="form-control select-category " style="width:100%;display:block;box-sizing:border-box">
                          <option value="" >Selecione a Categoria...</option>
                         @foreach ($resp["category"]["custom"]["query"] as $category)
                            <option value="{{$category->id}}" >{{$category->name}}</option>
                          @endforeach
                        </select>   
                      </div>                                
                    </div>
                  </div>
                </div>
            </div>

         <!--line 3 -->   
                <div class="row fields-rows" style="margin-left: 0.3%">
                  <div class="col-sm-10">
                    <div class="form-group">
                      <label class="label-text">SLA</label>
                      <div class="input-group-prepend">
                        <span class="input-group-text icons-background" ><i class="fa fa-city phone icons-image"></i></span>
                    <select class="form-control select-sla " title="Tipo da Categoria"  style="width:100%;display:block;box-sizing:border-box">
                      <option value="" >Selecione o SLA...</option>
                     @foreach ($resp["priority"]["custom"]["query"] as $priority)

                        <option selected value="{{$priority->idpriority}}" placeholder="Time">{{$priority->nameburden}}--{{$priority->nameseverity}}--{{$priority->nametype}}--{{$priority->tolerancy}}--idprioridade --{{$priority->idpriority}}</option> 
                      @endforeach
                    </select>   
                  </div>                                
                </div>
              </div>
            </div>

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" id="btn-add" >Cadastrar</button>
            </div>
        </div>
    </div>
</div>

<!--modal edit -->
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" id="id-modal-edit">
            <div class="modal-body">
                <!--line 1 -->
                <?php #dd($resp) ?>
                <!--line 2 -->   
                <div class="row fields-rows">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-text">Empresas</label>
                      <div class="input-group-prepend">
                        <span class="input-group-text icons-background" ><i class="fa fa-city phone icons-image"></i></span>
                        <select class="form-control select-company " style="width:100%;display:block;box-sizing:border-box">
                          <option value="" >Selecione a Empresa...</option>
                         @foreach ($resp["company"]["custom"]["query"] as $companys)
                            <option value="{{$companys->id}}" >{{$companys->name}}</option>
                          @endforeach
                        </select>   
                      </div>                                
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-text">Categoria</label>
                      <div class="input-group-prepend">
                        <span class="input-group-text icons-background" ><i class="fa fa-archive phone icons-image"></i></span>

                      </div>                                
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" id="btn-edit" >Salvar</button>
            </div>
        </div>
    </div>    
</div>
