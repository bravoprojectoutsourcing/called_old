<table   class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" width="100%"  >
    <thead>
        <tr >
            <th style="width: 5%;"  >#</th>
            <th style="width: 5%;">Peso</th>
            <th  >Nome do Peso</th>
            <th  >Tipo</th>
            <th  >Tolerância</th>
            <th  >Empresa</th>
            <th  >Categoria</th>
            <th  >Severidade</th>
            <th style="width: 5%;text-align: center" >Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($resp["list"]["custom"]["query"] as $sla)
            <tr style="position: relative;max-height: -5px">
                <td id="{{$sla['idsla']}}" >{{$sla["idsla"]}}</td>
                <td data-priority="{{$sla['idpriority']}}" ><span >{{$sla["burdenname"]}}</span></td>
                <td ><span >{{$sla["descriptionburden"]}}</span></td>
                <td ><span >{{$sla["typename"]}}</span></td>
                <td ><span >{{$sla["burdenurgencyname"]}}</span></td>
                <td data-company ="{{$sla['idcompany']}}"><span >{{$sla["companyname"]}}</span></td>
                <td data-company ="{{$sla['idcategory']}}"><span >{{$sla["categoryname"]}}</span></td>
                <td data-severity="{{$sla['idseverity']}}"><span >{{$sla["severityname"]}}</span></td>
                <td style="text-align: center" colspan="2">
                    <div class="btn-group">
                        <!--<div  class="col-sm-4 mr-3">
                            <button type="button" class="btn btn-default btn-sm  edit-category"  data-toggle="modal" data-target="#modal-edit" data-value="{{$sla['idsla']}}"><i class="fas fa-edit"></i></button>
                        </div>-->
                        <div  class="col-sm-1">
                            <button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{$sla['idsla']}}" ><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach               
    </tbody>  
</table> 