<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bravo - Suporte</title>
        <link rel="shortcut icon" href="{{ URL::to('faviconbravo.ico') }}" type="image/x-icon">
        <style>
            @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');
            * {
                margin: 0;
                padding: 0;
                font-family: 'Poppins', sans-serif;
            }
            html,
            body {
                display: grid;
                height: 100%;
                place-items: center;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background: -webkit-gradient(linear, left top, left bottom, from(#394F76), to(#000)) fixed;
            }
            p {
                margin-left: 100px;
                padding-bottom: 20px;
                text-align: justify;
            }
            .main {
                top: 0px;
                width: 600px;
                max-width: 600px;
                height: 100%;
                padding: 0 20px;
                background-color: #394F76 !important;
                background-image: url({{ asset('img/sidebar-bg.png') }});
                background-repeat: repeat-y;
                text-align: center;
                color: #f2f2f2;
                box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.2);
            }
            .body-header {
                padding: 20px;
                margin-left: -20px;
                margin-right: -20px;
                font-size: 20px;
                font-weight: 400;
                line-height: 20px;
                box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.2);
            }
            .body-content {
                padding: 25px 25px 0px 25px;
                margin-left: -20px;
                margin-right: -20px;
                font-size: 16px;
                font-weight: 300;
                line-height: 24px;
                overflow-y: hidden;
            }
            .name{
                color: #f12020;
            }
            .btnActivate {
                margin-top: 10px;
                margin-left: 100px;
                position: relative;
                padding: 0 20px;
                height: 45px;
                border: none;
                outline: none;
                border-radius: 5px;
                background: #27AE60;
                color: #f2f2f2;
                border: 1px solid #186839;
                font-size: 18px;
                font-weight: 500;
                letter-spacing: 1px;
                cursor: pointer;
                float: left;
            }
            .btnActivate:hover {
                background: #186839;
                color: #f2f2f2;
                border: 1px solid #27AE60;
                transition: .5s;
            }
            .subcopy {
                box-sizing: border-box;
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
                position: relative;
                line-height: 1.5em;
                margin-top: 0;
                text-align: left;
                font-size: 12px;
                word-wrap: break-word;
            }
            .subcopy hr {
                margin-left: 100px;
                margin-bottom: 10px;
                height: 1px;
                background-color: #999999;
                border: none;
                ;
            }
            .subcopy p {
                color: #999999;
            }
            .subcopy p a {
                text-decoration: none;
                color: #cccccc;
                cursor: pointer;
            }
            .subcopy p a:hover {
                text-decoration: none;
                color: #f2f2f2;
                cursor: pointer;
            }
            .body-footer {
                display: flex;
                padding: 10px 25px;
                margin-left: -20px;
                margin-right: -20px;
                font-size: 16px;
                font-weight: 300;
                line-height: 24px;
                text-align: left;;
                box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.2);
            }
            .body-footer .center .box {
                padding: 10px 20px;
                float: left;
            }
            .box {
                margin-left: 100px;
            }
            .box h2 {
                font-size: 1rem;
                font-weight: 400;
                text-transform: uppercase;
            }

            .box .content {
                margin: 20px 0 0 0;
                position: relative;
            }
            .box .content:before {
                position: absolute;
                content: '';
                top: -10px;
                height: 2px;
                width: 100%;
                background: #1a1a1a;
            }
            .box .content:after {
                position: absolute;
                content: '';
                height: 2px;
                width: 15%;
                background: #f12020;
                top: -10px;
            }
            .box .content .fas {
                height: 24px;
                width: 24px;
                color: #f2f2f2;
                background: #f12020;
                text-align: center;
                font-size: 20px;
                line-height: 24px;
                border: 1px solid #f2f2f2;
                border-radius: 2px;
                transition: 0.3s;
                cursor: pointer;
            }
            .box .content .fas:hover {
                color: #f12020;
                background: #f2f2f2;
                border: 1px solid #f12020;
                border-radius: 2px;
            }
            .box .content .text {
                font-size: .75rem;
                font-weight: 400;
                padding: 10px;
                margin: -5px 0 0 0;
            }
            .box .content .place,
            .box .content .phone,
            .box .content .email,
            .box .content .social {
                margin: 5px 0 0 0;
            }
            .box .content .place,
            .box .content .phone,
            .box .content .email {
                padding: 0 2px;
            }
            .box .content .social a {
                padding: 0 2px;
            }
            .box .content .social a span {
                height: 26px;
                width: 26px;
                color: #f12020;
                background: #f2f2f2;
                text-align: center;
                font-size: 27px;
                line-height: 26px;
                border-radius: 2px;
            }
            .box .content .social a span:hover {
                color: #f2f2f2;
                background: #f12020;
                border-radius: 2px;
            }
            .icon {
                width: 26px;
                height: 26px;
                position: relative;
                align-items: center;
                vertical-align: middle;
                background-color: #f2f2f2 !important;
            }
            .bottom {
                position: relative;
                width: 600px;
                bottom: 0px;
                margin: 0 auto;
                padding: 5px;
                align-items: center;
                text-align: center;
                font-size: 0.875rem;
                background: transparent;
            }
            .bottom span {
                color: #f2f2f2;
            }
            .bottom a {
                color: #f12020;
                text-decoration: none;
            }
            .bottom a:hover {
                text-decoration: underline;
            }
            @media only screen and (max-width: 600px) {
            .inner-body {
            width: 100% !important;
            }

            .footer {
            width: 100% !important;
            }
            }

            @media only screen and (max-width: 500px) {
            .button {
            width: 100% !important;
            }
            }
        </style>
        <script src="https://kit.fontawesome.com/8c5886131d.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="main">
            <div class="body-header">
                 <img src="<?php echo URL::to('assets/dist/img/logo2.png' ) ?>" alt="Clique em mostrar conteudo para exibir" class="brand-image "
           >
            </div>
            <div class="body-content">
                @if($resp["status"]=="Aprovado")
                    <p><span class="name">Equipe</span>, Ticket  Aprovado pelo Gestor!.
                @else
                    <p><span class="name">Equipe</span>, Ticket  Reprovado pelo Gestor!. Motivo: {{$resp["obs"]}}
                @endif
                </p>
                <p>Para visualizar a mensagem basta se logar na plataforma e clicar em suporte!</p>
                <br /><br /><br />
                <p>Atenciosamente,<br />Equipe Bravo</p>
                <br /><br /><br />
            </div>

            <div class="body-footer">
                <div class="box">
                    <h2>Bravo | Project & Outsourcing</h2>
                    <div class="content">
                        <div class="place">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNS8yMS8yMLyvnkAAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAAC7klEQVRIib2WS0hUURjHf+feae4MKokmaWWKhLQq2oTgJsgQRWoRBJHRw4IsKJDcBKGGKA4JLcI2LoSyAaUosrCHIEhR0CYXIS6yxwwpaT7GaXIefi0cc2a8d6ZZ2Nkc7nf+5/87fPd85xzV1NkrF48dwLk5i41ogXkfXX3DqKlvXnF6XiLHmzcEpNzNBHYcQi0sLIjs2bMhkL+w0VFsKVUC2A3Qd8PpI6jiLDDssBSEz4NIz2uIhCAYBqUsbZKDIjpkl0KTC1VRBFqioBJ1YRFedSMtbpjzg75OBJhMXYMAeYdRfb2oqmJrpZYFVY2ovhuQl7EyLy1Q9j7oaYGdmyAiUbUOhhMcjpX0aQqQldTtPIzquQLZoXRAQahvRZWqGIgN8MPHtzAyAh+/gtjWHCJLUHoCVb8jHVAhqmIXBCWGPQf3byG1B5HqaqS2Ae69h2CMRVBBxdV0QKcgdyk+NHYXuXwbZrdBQQHMvkMarsKYP0a0DLmF6YBywJEQGn8G+Tlgj37bM2DrGDL+PV7nyMRsk1uA/BBOkBeVwY85WP3XoV8wXQpFOfG6yLKpowXoEUzr8aG9F6CuEnQPeL2gF8Op66i9W2JEOsxMmDpaFOwbZHgKdTIPfkdXaGxHuVxQXgZfQlC0H2rKgcBa7TiAoTuIiaP1WZdfAw86UZuXYDUbNic4Y9b22w+h6KDmgPlB5OhZmMyMh4yOJinYicfQNgi2mF0RDoDPB4uLK/0qBA1sk0hbO0xkmtpZg5wKnrQiA1PgSJBJQnIMHQZa4MkncJrbWYPQAA+01SFf9CRnnQO8D5G2l1hSkoMAzYDJD3CuG8KGiUCH8BhyxgWTqZedvNmdMN6FdLwHe4LcPgcd52F8Zv1Y2iAAww/9l5Chaf6WvdJhqAPp94KR+v78NxAahH5CYzt4oiHPc6TxKYRSQyDVDRvblAb+F8jNElRNCTJwDfwRTA82s+n/63GiBeZ9KHfzxkHczQTmfaj/9YD8A2jnA0gpPiGuAAAAAElFTkSuQmCC"
                                alt="Rua Samuel Morse, 74 - Cidade Monções, São Paulo/SP, 04576-060" class="icon">
                            <span class="text">Rua Samuel Morse, 74 - Cidade Monções, São Paulo/SP, 04576-060</span>
                        </div>
                        <div class="phone">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNS8yMS8yMLyvnkAAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAADIElEQVRIib3WX2gWVBjH8c95/+9lcxJdGEosE6MRpiVCMCXEXZTZTSTrLpAoDCKhiwr/jAyivxcxIgNpkDG0brKiJEhFqIvSdEaiICY5dU6W27v5bnv3vqeL18xXnaS969yd85zzfB+e8zvPecKmdz+Na1c/rKG5yXSM4lDBBzv2CP1/9MWGU9+JT3VOCyj0dCrOaReGh4djXLBgWiCXYb29UjUriRSpBJkcD65gxXLBYfHjN/l1Jvlbh9WCHnlFWLOMllmkI4kM5VbhzGHxx2/IN98yKFEz61jF/FmkIhHlSRrvYEmbMHuciVgn0NgElSudVRjLcv9DPLGQ8xcJ9QANDF27o1xk5nwWr+K2AqV6gE4Xro04RuRpu0tYNk6hHqBdRygnas35BgrH+ORrDjTTWA/Qsc85U7q0GkiWObKH558T39gmjt5Oph6gzCFx/3myoWpKR47uE3t2k51LbrKqxv8MKmXYsZ1SI8pMNtDeIby6nMFT12y/dVAsc3C7uHOQpiSlCfLzWLNRWN7CiXMkrzgSElVVDo9SjjeUfi0oIAyzdZM4micbKY5z51Lee1tYMovjZwlJEknGBijNZvHdXBzg9OCUj/qqXARU+G0n678g20SixMgE966ku0t4dC59JxkZINUqrNssdH0obP1IeOFxGs7Rf47xWuD1q/dkmdjChveFZxdSGCJkaczSd4At68XNB4Rnuuh68h8l/nmSQwf55QdxWzeD1Socenun+iYipRLNi9jwltDRysgQMUU+z8gJdv/OPfcxr5mx8Wo2UllymWoav3pHfHnHZdAUMgqkU1zYz2sviVt+orGZdIXRApk5PLaUlny1Pv6tgslxRgqYQebnG93RVaZMhsIhXl8rvtgtlpqYkas6LI4xeb2LD8SzfH/y34IumVNpnOWzTax+WvzyONlGcunryzkERvrEvcmbAV2KMJWq/lFH9rJupdjeQfc+BstI1gJDgv6jjFVqvdx0zxAr1fjSaSol5rTyQJuwaC65FLm0uGsj316gUo3gBqqr7wi9vRLFoYLQ0zl9kJ5OxaGC8H81kH8BKRs4Pva2t5UAAAAASUVORK5CYII="
                                alt="+55 (11) 2424-3148" class="icon">
                            <span class="text">+55 (11) 2424-3148</span>
                        </div>
                        <div class="email">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNS8yMS8yMLyvnkAAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAAB1klEQVRIie3WMUgbYRTA8f+rBclgu8RJcRaE1MWA4CCpIhQ6dAmJi1sHQTAgKEE0kxiwdCh1cjBIc/agNB2qQ0upgggBEZwylJZSK4hZkhsOB3kO0eoRvZxJzqlvOh5834973/vency/eqfj0UECj9vwI+ySxbL5HTn+81cDh1/QeMoXSIwUducwUi6XVUMhX5B/2MEBD3wVrsW9QQ+rMm++NWfniYgL1D+B9AQh2CBSzEM/6O5Vylm60ReNIwDBMIw+c6ScUKC1CcrlXu0u0NpnsJqAWAVYy7hAWwtofAYKDWg/NtB4Av3lTDuhlR1kqRediqJmAc7uIlhgzqCT20jaRObGXCCA7hhivEb2Emhy3VsprQJMR9G9XsRYhJ7quXnzhW3rhrSJDPyslHK/eDuyn6mUauAtko7BLbO5+sJeafA8iTz5ik5HYGQFiYfhsjHPipBdQt8/QpZz0OXesbVHUNcQsrqJHM6jUxk4OoWjPExG0N9DyIdkTaTGG12L1g6YzSH5dfRlHxCGxCbytMPTcu9QRYPwGPIpVnlu8b7yjtBFtNQ3PZxnZJ/WtcmNYZ+4QNmP4NLJnqOYh+yGI/X/U14/ZJcsxEj5BoiRwi5ZyH39QJ4DW8eXzpDFGykAAAAASUVORK5CYII="
                                alt="contato@bravocorp.com.br" class="icon">
                            <span class="text">contato@bravocorp.com.br</span>
                        </div>
                        <div class="social">
                            <a href="https://www.linkedin.com/company/bravo-project-outsourcing/" title="Bravo | Project & Outsourcing"><img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNS8yMS8yMLyvnkAAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAABmklEQVRIie2WOUgDQRSGv4mCEIgHCAELQW20STqbIAnYaOcBIY2d2IiNlSh4FYKFlQbEgxQiCYJgL4qaQrAQtdHKQllBTGGysBAhjsUm5Np4ZHdT+Vf/vpk3Hzxm3j6xsLYvx0P9NLmc2KGkqrETO0E8K0+y8XAONuOgWYloANKI6CLJ9gFEKpWS0uOxklAmcXeHw1ZCgeqNw50wPQHuN4hswEPaJtDqHmLQpXt/D3JkHF7MgYxL53XlvbMXAuYglUGXSt5rV3BmHmRcuqUg8j4ELR9wHjNdtsogVDjY/t0JzlboaNW98gDvfwEtnyKGssncIj1jQB8chxHubPhoGvkxgRjthrqC3PMV5FSs7Mjq31FgCREsgQD4Z2HGayGoOXszVRUyxUsiMGwhCBU570P6fEjfJPK1YKmty0JQIg5Hqu61OFwmvt1ePej5vvhbUYz3mQaV6od2WLPu/Q+qWsYt6GQLqeR+Fbnb9AiRdWQu/HJTnHMdRYYvSnLyqvHMYM+khT4J6XJkMp+I3UV7OG63Pm6pGqJWA+QXP9OAWogv+awAAAAASUVORK5CYII="
                                    alt="Siga a gente no LinkedIn" class="icon"></a><span class="text">Siga a gente no LinkedIn</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <span class="credit"><a href="https://www.bravocorp.com.br">Bravo | Project & Outsourcing</a> | </span>
            <span> &copy; {{ date("Y") }} - Todos os direitos reservados</span>
        </div>
    </body>
</html>