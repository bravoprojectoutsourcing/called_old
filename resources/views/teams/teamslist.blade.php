<table   class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" style="width: 100%"  >
    <thead>
        <tr >
            <th class="d-none" >#</th>
            <th>Usuario</th>
            <th style="width: 10%;text-align: center">Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($resp["user"]["custom"]["query"] as $user)
            <tr style="position: relative;max-height: -5px">
                <td class="d-none"> {{ $user["id"] }}</td>
                <td >{{ $user["name"] }}</td>
                <td style="text-align: center" colspan="2">
                    <button type="button" class="btn btn-success btn-sm addUserTeam" data-toggle="modal" data-target="#modal-user" data-value="{{$user['id']}}"><i class="fas fa-plus"></i></button>
                </td>
            </tr>
        @endforeach            
    </tbody>  
</table>