 <!-- CSS file of Page -->
<link rel="stylesheet" href="{{ URL::to('css/reports/reports.css') }}"> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="header">
			<div class="col-sm-3 text-left bc-left">
				<h2>Consultas</h2>
			</div>
			<div class="col-sm-6 text-center">
				<ol class="breadcrumb bc-center">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Consultas</li>
				</ol>
			</div>
			<div class="col-sm-3 text-right bc-right">
				<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" id="bt-open-modal-add" data-target="#modal-add">Adicionar</button>
			</div>
		</div>
	</section>
	<br>
	<section class="content">
    {{-- <div class="card"> --}}
        <!-- /.card-header -->
        <div class="div-list">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="card card-secondary card-tabs">
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="tab-company" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-company" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Times</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-branchoffice" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Time dos Usuários</a>
							</li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            <div class="tab-pane fade active show" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                <!--start content-->
                                <div class="card"  >
                                    <div class="card-body div-list">
                                        <table   class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" width="100%"  >
                                            <thead>
                                                <tr >
                                                    <th style="width: 10%;" class="d-none" >#</th>
                                                    <th  >Time</th>
                                                    <th style="width: 10%;text-align: center" >Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($resp["list"]["custom"]["query"] as $team)
                                                    <tr style="position: relative;max-height: -5px">
                                                        <td class="d-none">{{$team["id"]}}</td>
                                                        <td id="change{{$team["id"]}}"><span id="time{{$team["id"]}}">{{$team["name"]}}</span></td>
                                                        <td style="text-align: center" colspan="2">
                                                            <div class="btn-group">
                                                                <div  class="col-sm-4 mr-3">
                                                                    <button type="button" class="btn btn-secondary btn-sm  edit-team"  data-toggle="modal" data-target="#modal-edit" data-value="{{$team["id"]}}"><i class="fas fa-edit"></i></button>
                                                                </div>
                                                                <div  class="col-sm-1">
                                                                    <button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{$team["id"]}}" ><i class="fas fa-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach               
                                            </tbody>  
                                        </table>                                
                                    </div>
                                </div>
                            	<!--end-->
                            </div>
                            <div class="tab-pane fade div-bo" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                <div class="card"  >
                                    <div class="card-body div-list">
                                        <table class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" style="width: 100%"  >
                                            <thead>
                                                <tr >
                                                    <th class="d-none" >#</th>
                                                    <th>Usuario</th>
                                                    <th style="width: 10%;text-align: center">Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
												@foreach ($resp["list"]["custom"]["query"] as $team)
												<tr style="position: relative;max-height: -5px">
													<td class="d-none">{{$team["id"]}}</td>
													<td id="change{{$team["id"]}}"><span id="time{{$team["id"]}}">{{$team["name"]}}</span></td>
													<td style="text-align: center" colspan="2">
														<div class="btn-group">
															<div class="col-sm-4 mr-3">
																<button type="button" class="btn btn-secondary btn-sm  edit-team" data-toggle="modal"
																	data-target="#modal-edit" data-value="{{$team["id"]}}"><i class="fas fa-edit"></i></button>
															</div>
															<div class="col-sm-1">
																<button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{$team["id"]}}"><i
																		class="fas fa-trash"></i></button>
															</div>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
                                        </table>                             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
        <!-- /.card-header -->
	{{-- </div> --}}
	</section>
</div>

<script src="{{ URL::to('assets/system/js/global.js') }}>"></script><!--validate fields-->
<script src="{{ URL::to('assets/system/js/reports/reports.js') }}>"></script>