<!-- CSS file of Page -->
<link rel="stylesheet" href="{{ URL::to('css/reports/reports.css') }}">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="header">
			<div class="col-sm-3 text-left bc-left">
				<h2>Consultas</h2>
			</div>
			<div class="col-sm-6 text-center">
				<ol class="breadcrumb bc-center">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Consultas</li>
				</ol>
			</div>
			<div class="col-sm-3 text-right bc-right">
				{{-- <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" id="bt-open-modal-add" data-target="#modal-add">Adicionar</button> --}}
			</div>
		</div>
	</section>
	<br>
	<section class="content">
		<div class="div-list">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="card card-secondary card-tabs">
					<div class="card-header p-0 pt-1">
						<ul class="nav nav-tabs" id="tab-query" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="tab-query" data-toggle="pill" href="#custom-tabs-one-query" role="tab" aria-controls="custom-tabs-one-query" aria-selected="true">Relatórios</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="custom-tabs-one-tabContent">
							<div class="tab-pane fade active show" id="custom-tabs-one-query" role="tabpanel"
								aria-labelledby="custom-tabs-one-query-tab">
								<!--start content-->
								<div class="card">
									@isset($filter)
									<div class="form-group responseMessage m-3">
										@if ($filter['status'] == true)
										<div class="alert alert-success mb-0" role="alert">
											<i class="fas fa-check"></i> {{ $filter['message'] }}
										</div>
										@elseif ($filter['status'] == false)
										<div class="alert alert-warning mb-0" role="alert">
											<i class="fas fa-exclamation-circle"></i> {{ $filter['message'] }}
										</div>
										@endif
									</div>
									@endisset
									<form class="card-body div-list" method="post"
										action="{{ route('reports.filters.save') }}">
										<h1>Escolha alguns filtros e um método para gerar um relatório</h1>
										<div class="form-group">
											<label class="control-label" for="filtros">
												<span id="filtros">Filtros :: Faça sua seleção</span>
											</label>
											<!--start phone and email content-->
											<div class="row">
												<div class="col-sm-6 col-md-6">
													<label class="control-label" for="phone">
														<span class="text-secondary">Telefone</span>
													</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="fas fa-phone"></i>
														</div>
														<input class="form-control" id="phone" name="phone" type="text"
															maxlength="15" />
													</div>
												</div>
												<div class="col-sm-6 col-md-6">
													<label class="control-label" for="email">
														<span class="text-secondary">E-mail</span>
													</label>
													<div class="input-group">
														<div class="input-group-addon"><i class="fas fa-envelope""></i></div>
														<input class=" form-control" id="email" name="email" type="email" maxlength="192" />
														</div>
													</div>
												</div>
												<!--end phone and email content-->
												<!--start companies content-->
												<div class="row">
													<div class="col-sm-12 col-md-12 col-lg-12">
														<label class="control-label" for="selectCompany">
															<span class="text-secondary">Empresas</span>
														</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="fas fa-building"></i></div>
															<select multiple class="form-control" id="selectCompany" name="selectCompany[]" placeholder="Selecione uma ou mais Empresas">
																<option value="" disabled>-- Selecione uma ou mais Empresas --</option>
																@foreach ($resp["companies"]["custom"]["query"] as $companys)
																<option value="{{ $companys->id }}">{{ $companys->name }}</option>
																@endforeach
															</select>
														</div>
													</div>
													<input type="hidden" name="arrSelectCompany" class="arrSelectCompany" value="">
												</div>
												<!--end companies content-->
												<!--start branchoffices content-->
												<div class="row">
													<div class="col-sm-12 col-md-12 col-lg-12">
														<label class="control-label" for="selectBranchOffice">
															<span class="text-secondary">Estabelecimentos</span>
														</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="fas fa-map-marker-alt""></i></div>
																<select multiple class=" form-control" id="selectBranchOffice" name="selectBranchOffice[]">
																	<option value="" disabled>-- Selecione um ou mais Estabelecimentos --</option>
																</select>
															</div>
														</div>
														<input type="hidden" name="arrSelectBranchOffice" class="arrSelectBranchOffice" value="">
													</div>
												</div>
												<!--end branchoffices content-->
												<!--start departments and categories content-->
												<div class="row">
													<div class="col-sm-6 col-md-6 col-lg-6">
														<label class="control-label" for="selectDepartament">
															<span class="text-secondary">Departamentos</span>
														</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="fas fa-store-alt"></i></div>
															<select multiple class="form-control" id="selectDepartament" name="selectDepartament[]">
																<option value="" disabled>-- Selecione um ou mais Departamentos --</option>
																@foreach ($resp["departament"]["custom"]["query"] as $departaments)
																<option value="{{$departaments->id}}">{{$departaments->name}}</option>
																@endforeach
															</select>
														</div>
														<input type="hidden" name="arrSelectDepartament" class="arrSelectDepartament" value="">
													</div>
													<div class="col-sm-6 col-md-6 col-lg-6">
														<label class="control-label" for="selectCategory">
															<span class="text-secondary">Categorias</span>
														</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="fas fa-list""></i></div>
																<select multiple class=" form-control" id="selectCategory" name="selectCategory[]">
																	<option value="" disabled>-- Selecione uma ou mais Categorias --</option>
																	@foreach ($resp["category"]["custom"]["query"] as $categories)
																	<option value="{{ $categories->id }}">{{ $categories->name }}</option>
																	@endforeach
																</select>
															</div>
															<input type="hidden" name="arrSelectCategory" class="arrSelectCategory" value="">
														</div>
													</div>
													<!--end department and category content-->
													<!--start severity and priority content-->
													<div class="row">
														<div class="col-sm-6 col-md-6 col-lg-6">
															<label class="control-label" for="selectSeverity">
																<span class="text-secondary">Urgências</span>
															</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="fas fa-shipping-fast"></i></div>
																<select multiple class="form-control" id="selectSeverity" name="selectSeverity[]">
																	<option value="" disabled>-- Selecione um ou mais tipos de Urgência --</option>
																	@foreach ($resp["severity"]["custom"]["query"] as $severities)
																	<option value="{{$severities->id}}">{{$severities->name}}</option>
																	@endforeach
																</select>
															</div>
															<input type="hidden" name="arrSelectSeverity" class="arrSelectSeverity" value="">
														</div>
														<div class="col-sm-6 col-md-6 col-lg-6">
															<label class="control-label" for="selectPriority">
																<span class="text-secondary">Prioridades</span>
															</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="fas fa-bolt""></i></div>
																	<select	multiple class=" form-control" id="selectPriority" name="selectPriority[]">
																		<option value="" disabled>-- Selecione uma ou mais Prioridades --</option>
																	</select>
																</div>
																<input type="hidden" name="arrSelectPriority" class="arrSelectPriority" value="">
															</div>
														</div>
														<!--end severity and priority content-->
														<!--start type of categories and teams content-->
														<div class="row">
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="selectTypeCategory">
																	<span class="text-secondary">Tipo de Atendimento</span>
																</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="fas fa-concierge-bell"></i></div>
																	<select multiple class="form-control" id="selectTypeCategory" name="selectTypeCategory[]">
																		<option value="" disabled>-- Selecione um ou mais Tipos de Atendimento --</option>
																		@foreach ($resp["typeCategory"]["custom"]["query"] as $typeCategories)
																		<option value="{{$typeCategories->id}}">{{$typeCategories->name}}</option>
																		@endforeach
																	</select>
																</div>
																<input type="hidden" name="arrSelectTypeCategory" class="arrSelectTypeCategory" value="">
															</div>
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="selectTeam">
																	<span class="text-secondary">Times</span>
																</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="fab fa-teamspeak"></i></div>
																	<select multiple class="form-control" id="selectTeam" name="selectTeam[]">
																		<option value="" disabled>-- Selecione um ou mais Times --</option>
																		@foreach ($resp["team"]["custom"]["query"] as $teams)
																		<option value="{{$teams->id}}">{{$teams->name}}</option>
																		@endforeach
																	</select>
																</div>
																<input type="hidden" name="arrSelectTeam" class="arrSelectTeam" value="">
															</div>
														</div>
														<!--end type of categories and teams content-->
														<!--start answers and status content-->
														<div class="row">
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="selectResponse">
																	<span class="text-secondary">Possui respostas?</span>
																</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="far fa-comment"></i></div>
																	<select class="form-control" id="selectResponse" name="selectResponse">
																		<option value="" disabled>-- Selecione uma Opção --</option>
																		<option value="S">SIM</option>
																		<option value="N">NÃO</option>
																		<option selected value="T">TODOS</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="selectStatus">
																	<span class="text-secondary">Status</span>
																</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="fas fa-traffic-light"></i></div>
																	<select multiple class="form-control" id="selectStatus" name="selectStatus[]">
																		<option value="" disabled>-- Selecione um ou mais Status --</option>
																		@foreach ($resp["status"]["custom"]["query"] as $status)
																		<option value="{{$status->id}}">{{$status->name}}</option>
																		@endforeach
																	</select>
																</div>
																<input type="hidden" name="arrSelectStatus" class="arrSelectStatus" value="">
															</div>
														</div>
														<!--end answers and status content-->
														<!--start keywords content-->
														<div class="row">
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="keywords">
																	<span class="text-secondary">Palavras Chave</span>
																</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="fas fa-equals"></i></div>
																	<textarea class="form-control" id="keywords" name="keywords" rows="1" maxlength="140" /></textarea>
																</div>
															</div>
															<div class="col-sm-6 col-md-6 col-lg-6">
																<label class="control-label" for="keywords">
																	<span class="text-secondary">&nbsp;</span>
																</label>
																<div class="input-group">
																	<div class="alert alert-primary" role="alert">
																		<i class="fas fa-exclamation-circle"></i>
																		Palavras-chave <b>devem</b> estar separadas por <mark><b>&nbsp;,&nbsp;</b></mark> <i>(vírgula)</i>
																	</div>
																</div>
															</div>
														</div>
														<!--end keywords content-->
													</div>	
													<div class="form-group">
														<label class="control-label" for="metodo">
															<span id="metodo">Método :: Selecione para gerar e
																salvar o seu relatório</span>
														</label>
														<div class="row">
															<div class="col-sm-12 col-md-12 col-lg-12">
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="methodType" id="inlineRadio1" value="csv">
																	<i class="fas fa-file-csv"></i>
																	<label class="form-check-label" for="inlineRadio1">CSV</label>
																</div>
																{{-- <div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="methodType" id="inlineRadio2" value="pdf">
																	<i class="far fa-file-pdf"></i>
																	<label class="form-check-label" for="inlineRadio2">PDF</label>
																</div> --}}
																<div class="form-check form-check-inline">
																	<input class="form-check-input" type="radio" name="methodType" id="inlineRadio3" value="xls" checked>
																	<i class="far fa-file-excel"></i>
																	<label class="form-check-label" for="inlineRadio3">Excel</label>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<button class="btn btn-outline-dark submit" type="submit">Enviar</button>
											</div>
										</div>
									</form>
								</div>
								<!--end content-->
							</div>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
	</section>
</div>
@isset ($filter)
@php
$filterStatus = $filter['status'];
$filterMessage = $filter['message'];
@endphp
<script>
	$(function () {
			var	filterMessage = '{{$filterMessage}}';
			
			$('.responseMessage').slideDown();
		
			setTimeout(() => {
				$('.responseMessage').slideUp('slow');
			}, 15000);			
		});
</script>
@endisset
<script src="{{ URL::to('assets/system/js/global.js') }}"></script>
<!--validate fields-->
<script src="{{ URL::to('assets/system/js/reports/reports.js') }}"></script>