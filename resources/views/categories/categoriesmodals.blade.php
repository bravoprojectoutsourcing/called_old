<!--modal add -->
<div class="modal fade" id="modal-add"   >
<div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
    <div class="modal-content" >
        <div class="modal-header" >
            <h4 class="modal-title" id="title-modal-add">Adicionar </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" >
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-users"></i></span>
                            <input type="text" class="form-control" id="name-add"  name="name-add" placeholder="Nome da Categoria">
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-check"></i></span>

                                    <select class="form-control" id="select-status-category">
                                         <option  selected value="" >Necessita de Aprovação?</option>
                                         <option  value="1" >Sim</option>
                                         <option  value="0" >Não</option>
                                    </select>
                        </div>
                    </div>
                </div>                
            </div>



        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-danger" id="btn-add" >Cadastrar</button>
        </div>
    </div>
</div>
</div>

<!--modal edit -->
<div class="modal fade" id="modal-edit">
<div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Editar</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <input type="hidden" id="id-modal-edit">
        <div class="modal-body">
            <!--line 1 -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-list"></i></span>
                            <input type="text" class="form-control" id="nameCategory-edit" name="nameCategory-edit">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-check"></i></span>

                                    <select class="form-control" id="select-status-category-edit">
                                         <option  selected value="" >Necessita de Aprovação?</option>
                                         <option  value="1" >Sim</option>
                                         <option  value="0" >Não</option>
                                    </select>
                        </div>
                    </div>
                </div>                
            </div>

        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-danger" id="btn-edit" >Salvar</button>
        </div>
    </div>
</div>    
</div>

<!--modal add -->
<div class="modal fade" id="modal-user"   >
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
        <div class="modal-content" >
            <div class="modal-header" >
                <h4 class="modal-title" id="title-modal-add">Escolha a(s) Categoria(s) do Usuário(s) </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="form-group">
                                <div class="row d-flex align-items-end">
                                <div class="col-11">
                                    <label for="notHave" >Escolha a(s) Categoria(s)</label>
                                    <input type="hidden" id="idusercategory" value="">
                                    <select class="form-control" id="select-usercategory-modal">
                                         @foreach ($resp["list"]["custom"]["query"] as $categorymodal)

                                             <option  value="{{$categorymodal['id']}}" > {{$categorymodal["name"]}}</option>

                                         @endforeach
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-success addcategoryuser"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                            </div>
                        </form>
                        <table   class="table table-responsive-sm table-bordered table-striped table-sm " style="width: 100%"  >
                            <thead>
                                <tr >
                                    <th class="d-none" >#</th>
                                    <th>Categoria(s)</th>
                                    <th style="width: 10%;text-align: center">Ações</th>
                                </tr>
                            </thead>
                            <tbody id="tablecategorymodal">
                                          
                            </tbody>  
                        </table>
                    </div>                
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!--modal add task-283 -->
<div class="modal fade" id="modal-category"   >
    <div class="modal-dialog" style="max-width: 900px;margin-left: 25%">
        <div class="modal-content" >
            <div class="modal-header" >
                <h4 class="modal-title" id="title-modal-add">Escolha a(s) Categoria(s) do(s) Times(s) </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="form-group">
                                <div class="row d-flex align-items-end">
                                <div class="col-11">
                                    <label for="notHave" >Escolha a(s) Categoria(s)</label>
                                    <input type="hidden" id="idteamcategory" value="">
                                    <select class="form-control" id="select-teamcategory-modal">
                                         @foreach ($resp["list"]["custom"]["query"] as $categorymodal)

                                             <option  value="{{$categorymodal['id']}}" > {{$categorymodal["name"]}}</option>

                                         @endforeach
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-success addcategoryteam"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                            </div>
                        </form>
                        <table   class="table table-responsive-sm table-bordered table-striped table-sm " style="width: 100%"  >
                            <thead>
                                <tr >
                                    <th class="d-none" >#</th>
                                    <th>Categoria(s)</th>
                                    <th style="width: 10%;text-align: center">Ações</th>
                                </tr>
                            </thead>
                            <tbody id="tableteamcategorymodal">
                                          
                            </tbody>  
                        </table>
                    </div>                
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>