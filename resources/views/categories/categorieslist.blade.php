<table   class="table table-responsive-sm table-bordered table-striped table-sm list-datatable" width="100%"  >
    <thead>
        <tr >
            <th style="width: 10%;" class="d-none" >#</th>
            <th  >Categoria</th>
            <th>Necessita Aprovação?</th>
            <th style="width: 10%;text-align: center" >Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($resp["list"]["custom"]["query"] as $category)
            <tr style="position: relative;max-height: -5px">
                <td class="d-none">{{$category["id"]}}</td>
                <td id="change{{$category['id']}}"><span id="categoria{{$category['id']}}">{{$category["name"]}}</span></td>
                                                                         <!-- task 212 -->
                 @if ($category["status"] == 1)   
                  <td style="color: green"><span >Sim</span></td>
                 @elseif ($category["status"] == 0)
                  <td style="color: red"><span >Não</span></td>                                                          
                 @endif 

                <td style="text-align: center" colspan="2">
                    <div class="btn-group">
                        <div  class="col-sm-4 mr-3">
                            <button type="button" class="btn btn-default btn-sm  edit-category"  data-toggle="modal" data-target="#modal-edit" data-value="{{$category['id']}}"><i class="fas fa-edit"></i></button>
                        </div>
                        <div  class="col-sm-1">
                            <button type="button" class="btn btn-danger btn-sm bt-del" data-value="{{$category['id']}}" ><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach               
    </tbody>  
</table>                                
