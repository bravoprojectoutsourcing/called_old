<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (!function_exists('generateXLSFile')) {
	/**
	 * @param array $columnNames
	 * @param array $data
	 * @param string $fileName
	 * @return \Symfony\Component\HttpFoundation\StreamedResponse
	 */
	function generateXLSFile($columnNames, $data, $fileName)
	{
		$dataCount = 2;
		// call PhpOffice\PhpSpreadsheet library
		$spreadsheet = new Spreadsheet();
		
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Relatório de Chamados');
		
		$sheet->setCellValue('A1', 'Ticket #');
		$sheet->setCellValue('B1', 'Criado em');
		$sheet->setCellValue('C1', 'Usuário Solicitante');
		$sheet->setCellValue('D1', 'Telefone do Usuário');
		$sheet->setCellValue('E1', 'E-mail do Usuário');
		$sheet->setCellValue('F1', 'Empresa');
		$sheet->setCellValue('G1', 'Estabelecimento');
		$sheet->setCellValue('H1', 'Departamento');
		$sheet->setCellValue('I1', 'Categoria');
		$sheet->setCellValue('J1', 'Urgência');
		$sheet->setCellValue('K1', 'Prioridade');
		$sheet->setCellValue('L1', 'Tipo de Atendimento');
		$sheet->setCellValue('M1', 'Time');
		$sheet->setCellValue('N1', 'Mensagem');
		$sheet->setCellValue('O1', 'Descrição');
		$sheet->setCellValue('P1', 'Última resposta em');
		$sheet->setCellValue('Q1', 'Status');
		
		foreach ($data as $columnData) {
			$sheet->setCellValue('A'.$dataCount, $columnData['ticket_id']);
			$sheet->setCellValue('B'.$dataCount, $columnData['created_at']);
			$sheet->setCellValue('C'.$dataCount, $columnData['user_name']);
			$sheet->setCellValue('D'.$dataCount, $columnData['user_phone']);
			$sheet->setCellValue('E'.$dataCount, $columnData['user_email']);
			$sheet->setCellValue('F'.$dataCount, $columnData['company_name']);
			$sheet->setCellValue('G'.$dataCount, $columnData['branchoffice_name']);
			$sheet->setCellValue('H'.$dataCount, $columnData['departament_name']);
			$sheet->setCellValue('I'.$dataCount, $columnData['category_name']);
			$sheet->setCellValue('J'.$dataCount, $columnData['severity_name']);
			$sheet->setCellValue('K'.$dataCount, $columnData['priority_name']);
			$sheet->setCellValue('L'.$dataCount, $columnData['typecategory_name']);
			$sheet->setCellValue('M'.$dataCount, $columnData['team_name']);
			$sheet->setCellValue('N'.$dataCount, $columnData['ticket_short_message']);
			$sheet->setCellValue('O'.$dataCount, $columnData['ticket_description']);
			$sheet->setCellValue('P'.$dataCount, $columnData['ticket_created_response_at']);
			$sheet->setCellValue('Q'.$dataCount, $columnData['ticket_status']);
			$dataCount++;
		}

		$writer = new Xlsx($spreadsheet);
		header('Content-Description: File Transfer');
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.dateNowTZ('YmdHis').'-'.$fileName.'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Expires: 0');
		header('Last-Modified: ' . date('D, d M Y H:i:s') . ' -0300');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');
		$writer->setOffice2003Compatibility(true);
		$writer->save('php://output');
		exit;
	}
}

if (!function_exists('generateCSVFile')) {
	/**
	 * @param array $columnNames
	 * @param array $data
	 * @param string $fileName
	 * @return \Symfony\Component\HttpFoundation\StreamedResponse
	 */
	function generateCSVFile($columnNames, $data, $fileName)
	{
		//Use semicolon as field separator
		$newTab  = "\t";
		$newLine  = "\n";

		$fputcsv  =  count($columnNames) ? '"'. implode('"'.$newTab.'"', $columnNames).'"'.$newLine : '';

		// Loop over the * to export
		if (! empty($data)) {
			foreach ($data as $item) {
				$fputcsv .= '"'. implode('"'.$newTab.'"', $item).'"'.$newLine;
			}
		}

		//Convert CSV to UTF-16
		$encodedCSV = mb_convert_encoding($fputcsv, 'UTF-16LE', 'UTF-8');

		// Output CSV-specific headers
		header('Set-Cookie: fileDownload=true; path=/'); //This cookie is needed in order to trigger the success window.
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".dateNowTZ('YmdHis')."-".$fileName.".csv\";" );
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: '. strlen($encodedCSV));
		echo chr(255) . chr(254) . $encodedCSV; //php array convert to csv/excel
		exit;
	}
}

if (!function_exists('dateNowTZ')) {
	/**
	 * @param string $dateFormat
	 * @param string $timeZone
	 * @return date $formatedDate
	 */
	function dateNowTZ($dateFormat = 'YmdHis', $timeZone = 'America/Belem')
	{
		$tz = $timeZone;
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp);
		
		return $dt->format($dateFormat);
	}
}
