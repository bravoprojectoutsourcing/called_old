<?php 


namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\UtilModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class UtilService  {
    
 
    protected $statedao;
    protected $getmessage;

    #change injects
    function __construct(UtilModel $utilModel, ServiceResponseHelper $message) {
        $this->utilmodel   = $utilModel;
        $this->getMessage = $message;
     
    } 

public function listState(){

	$resp=$this->utilmodel->listState();
	return $resp;

}

public function listCity($id){

	$resp=$this->utilmodel->listCity($id);
	return $resp;
}

public function listCityState($state){

    $resp=$this->utilmodel->listCityState($state);
    return $resp;
}

public function cityName($name){


    $resp=$this->utilmodel->cityName($name);
    return $resp;
}







}    