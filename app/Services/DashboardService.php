<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\DashboardModel;
use App\Helpers\ServiceResponseHelper;
use App\Models\CompanyModel;

#input rules
class DashboardService  {
    
 
    protected $dashboardmodel;
    protected $getmessage;
    protected $companymodel;

    #change injects
    function __construct(DashboardModel $dashboardModel, ServiceResponseHelper $message , CompanyModel $companymodel) {
        $this->dashboardmodel = $dashboardModel;
        $this->getMessage     = $message;
        $this->companymodel   = $companymodel;
     
    } 


    public function menuBoxTeam($data){
            

             $profile = session('resp')["custom"][0]["nameprofile"];

             $x = 0;

             foreach (session('resp')["custom"] as  $value) {
                
                if($x == 0)       
                    $teams = $value["teamid"];
                else
                    $teams = $teams . "," . $value["teamid"];

             }


            $data = array(

                'nameprofile' => $profile,
                'teams'       => $teams,
                'status'      => $data

            );


            $resp = $this->dashboardmodel->menuBoxTeam($data);

            return $resp;

    }


    public function ticket($data,$status){

        $resp="";

        #closed      = finalizado
        #cancel      = cancelado
        #wait        = aguardando atendimento
        #open1       = aguardando atendimento
        #open2       = aguardando solicitante
        #conclued    = concluido
        #onaprovall  = aguardando aprovacao
        #disaproved  = reprovado
        
      if($status=="closed")
                $resp = $this->ticketBoxCalculate($data,$status);
           else if($status=="open1" || $status=="open2" )
                $resp = $this->ticketBoxCalculate($data,$status);
           else if($status=="cancel")
                $resp = $this->ticketBoxCalculate($data,$status);
           else if($status=="wait")
                $resp = $this->ticketBoxCalculate($data,$status);    
           else if($status=="concluded")
                $resp = $this->ticketBoxCalculate($data,$status);
           else if($status=="onapproval")
                $resp = $this->ticketBoxCalculate($data,$status);
           else if($status=="disapproved")
                $resp = $this->ticketBoxCalculate($data,$status);


           if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
                $resp= $this->getMessage->getServiceResponse(TRUE,"Tickets(s) Finalizado(s) Carregada(s) com Sucesso!","",$resp);#case success
            else if(count($resp["count"]) == 0)
                $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Ticket(s) não encontrado(s)","",$resp["query"]);#case error
            else if ($resp["exception"] != null)
                $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp);#case empty
                

            return $resp;     
        
    }


public function ticketBoxCalculate($data,$status){


         if($data["custom"][0]["nameprofile"]=="ADMIN"){ 

                #show gadget general blade
                $resp=$this->dashboardmodel->ticketAdmin($status);



          }else if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK") {  

                    #show gadget general blade GESTOR/ATENDENTE
                    $resp=$this->dashboardmodel->ticketManagerClark($status);


          }else if($data["custom"][0]["nameprofile"]=="REQUESTER"){    
        
                    #show gadget general blade
                    $resp=$this->dashboardmodel->ticketRequester($status);
          }else{

                              $usercompany = $this->companymodel->usercompany(); #get company(s) user
                              $usercompany = json_decode(json_encode($usercompany),true); #convert stdclass for array              
                              
                              $i=0;

                              foreach ($usercompany["query"] as $value) {
                              
                                if($i == 0)
                                  $companys = $value["id"];
                                else
                                  $companys = $companys . "," . $value["id"];
                              
                                $i++;  

                              }              

                              $data = array(
                                "status"   => $status,
                                "companys" => $companys
                              );

                            #show gadget general blade
                    $resp=$this->dashboardmodel->ticketManagerCustomer($data);

                    #echo "<pre>" ,print_r($resp);exit;

                }  

                return $resp;

}


    public function graph($data){

               if($data["custom"][0]["nameprofile"]=="ADMIN"){ 
                
                    #show gadget general blade
                    $resp=$this->dashboardmodel->graph();



               }else if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK") {  

                               #list tickets team     

                    #show gadget general blade GESTOR/ATENDENTE
                    $resp=$this->dashboardmodel->graphManagerClark($data);


                }else if($data["custom"][0]["nameprofile"]=="ADMIN"){

                    #show gadget general blade
                    $resp=$this->dashboardmodel->graphRequester();
	
               }else if($data["custom"][0]["nameprofile"]=="REQUESTER"){
	
	               #show gadget general blade
	               $resp=$this->dashboardmodel->graphRequester();
               }else{
        

                             $usercompany = $this->companymodel->usercompany(); #get company(s) user
                              $usercompany = json_decode(json_encode($usercompany),true); #convert stdclass for array              
                              
                              $i=0;

                              foreach ($usercompany["query"] as $value) {
                              
                                if($i == 0)
                                  $companys = $value["id"];
                                else
                                  $companys = $companys . "," . $value["id"];
                              
                                $i++;  

                              }              


                    #show gadget general blade
                    $resp=$this->dashboardmodel->graphManagerCustomer($companys);
                }  

                #echo "<pre>" , print_r($resp);exit;

           if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
                $resp= $this->getMessage->getServiceResponse(TRUE,"Tickets(s) Aberto(s),Em Andamento , Cancelado(s) e Finalizado(s) Carregado(s) com Sucesso!","",$resp);#case success
            else if(count($resp["count"]) == 0)
                $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Ticket(s) não encontrado(s)","",$resp["query"]);#case error
            else if ($resp["exception"] != null)
                $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp["exception"][0]);#case empty
                

            return $resp;    

    }



}    