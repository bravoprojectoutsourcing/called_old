<?php
namespace App\Services;

# Autor: André Camargo
# Date : 1º semestre 2020

# injects
use App\Models\TicketResponseModel;
use App\Models\TicketModel;
use App\Models\CompanyModel; # created André 21/07/2020 - ticket 227 item 6
use App\Models\UploadFilesModel;
use App\Helpers\ServiceResponseHelper;
use App\Helpers\ServiceDateHelper;
use App\Services\TeamsService;
use App\Mail\BravoMail;
use Mail;
use DateTime;
use DateTimeZone;

# input rules
class TicketResponseService {

	protected $ticketresponsemodel;

	protected $companymodel;

	# created André 21/07/2020 - ticket 227 item 6
	protected $uploadfilesmodel;

	protected $getmessage;

	protected $getdatemessage;

	# created André 15/06/2020
	protected $ticketmodel;

	protected $teamsservice;

	# change injects
	function __construct(CompanyModel $companymodel, TeamsService $teamsservice, TicketModel $ticketmodel, TicketResponseModel $ticketResponseModel, UploadFilesModel $uploadFilesModel, ServiceResponseHelper $message, ServiceDateHelper $getdatemessage) {
		$this->ticketresponsemodel = $ticketResponseModel;
		$this->uploadfilesmodel = $uploadFilesModel;
		$this->getMessage = $message;
		$this->getdatemessage = $getdatemessage; # created André 15/06/2020
		$this->ticketmodel = $ticketmodel;
		$this->teamsService = $teamsservice;
		$this->companymodel = $companymodel; # created André 21/07/2020 - ticket 227 item 6
	}

	public function lists($id) {
		$resp = $this->ticketresponsemodel->lists($id);

		# get files ticket
		$resp["filepath"] = $this->uploadfilesmodel->view($id);

		if ($resp["success"] and count($resp["count"]) > 0 and $resp["exception"] == null)
			$resp = $this->getMessage->getServiceResponse(TRUE, "Chamado(s) Listado(s) com Sucesso!", "", $resp); # case success
		else if (count($resp["count"]) == 0)
			$resp = $this->getMessage->getServiceResponse(FALSE, "Oppsssss! não existe(m) chamado(s)", "", $resp["exception"]); # case error
		else if ($resp["exception"] != null)
			$resp = $this->getMessage->getServiceResponse(FALSE, "Opsss! erro ao listar Chamado(s)", "", $resp); # case empty
		return $resp;
	}

	public function add($data) {
		$emails = $data["email"];
		$teamid = $data["idteam"];

		unset($data["idteam"]); # destroy
		unset($data["email"]); # destroy

		$data["iduser"] = session('resp')["custom"][0]["iduser"];

		# André created 15/06/2020 - helpers date time zone
		$created = $this->getdatemessage->getDateTimeZone('Y-m-d  H:i:s');

		$data["created"] = $created;

		$resp = $this->ticketresponsemodel->add($data);

		if ($resp["success"] and $resp["exception"] == null) {

			# get open user ticket
			$ticket = $this->ticketmodel->liststicket($data["idticket"]);

			# if user login diferent send team
			if (session('resp')["custom"][0]["iduser"] != $ticket["query"][0]["iduser"]) {} else {

				# send email notification

				# get list emails team
				$emailsteam = $this->teamsService->emailsTeam($ticket["query"][0]["idcategory"]);
				$x = 0;
				$emails = "";

				# echo "<pre>" ,print_r($emailsteam);exit;

				# mount emails team
				foreach ($emailsteam["custom"]["query"] as $value) {

					if ($x == 0)
						$emails = "$value";
					else
						$emails = $emails . "," . "$value";

					$x ++;
				}

				# clear symbols
				$emails = str_replace("{", "", $emails);
				$emails = str_replace("}", "", $emails);
				$emails = str_replace('"', '', $emails);
				$emails = str_replace("email:", "", $emails);

				$emails = str_replace(' ', '', $emails);
				$emails = (string) $emails;
				$emails = explode(',', $emails);
			}

			# get company name - created André 21/07/2020 - ticket 227 item 6
			$company = $this->companymodel->viewcompany($ticket["query"][0]["idcompany"]);

			$resp = json_decode(json_encode($resp), true); # convert stdclass for array
			$company = json_decode(json_encode($company), true); # convert stdclass for array

			# condition user
			$data["email"] = $emails;
			$data["view"] = "email.emailresponse"; # view mail
			$data["subject"] = "Número do chamado: " . $data["idticket"] . " Empresa: " . $company["query"][0]["name"];

			Mail::to($data["email"])->send(new BravoMail($data));

			# end send mail

			# task 254 update user response ticket - André 14/08/2020
			$dataitem = array(
				"id" => $data["idticket"],
				"createdresponse" => $this->getdatemessage->getDateTimeZone('Y-m-d  H:i:s'),
				"user" => session('resp')["custom"][0]["name"],
				'email' => session('resp')["custom"][0]["email"]
			);

			$respupdateticket = $this->ticketmodel->updateDataUserTicket($dataitem);

			if ($resp["success"] and $resp["exception"] == null)
				$resp = $this->getMessage->getServiceResponse(TRUE, "Resposta Cadastrada com Sucesso!", "", $resp); # case success
			else
				$resp = $this->getMessage->getServiceResponse(FALSE, "Oppsssss! ocorreu um erro ao cadastrar o ticket", "", $resp["exception"]); # case error

			# end task 254
		} else if ($resp["exception"] != null) {
			$resp = $this->getMessage->getServiceResponse(FALSE, "Oppsssss! ocorreu um erro ao cadastrar o ticket", "", $resp["exception"]); # case error
		}

		return $resp;
	}

	public function edit($data) {
		$resp = $this->ticketresponsemodel->edit($data);

		if ($resp["success"] and $resp["exception"] == null)
			$resp = $this->getMessage->getServiceResponse(TRUE, "Chamado editado com Sucesso!", "", $resp); # case success
		else if ($resp["exception"] != null)
			$resp = $this->getMessage->getServiceResponse(FALSE, "Oppsssss! ocorreu um erro", "", $resp["exception"]); # case error

		return $resp;
	}

	public function del($id) {
		$resp = $this->ticketresponsemodel->del($id);

		if ($resp["success"] and $resp["exception"] == null)
			$resp = $this->getMessage->getServiceResponse(TRUE, "Chamado Deletado com Sucesso!", "", $resp); # case success
		else if ($resp["exception"] != null)
			$resp = $this->getMessage->getServiceResponse(FALSE, "Oppsssss! ocorreu um erro", "", $resp["exception"]); # case error

		return $resp;
	}

	public function view($id) {

		# show Ticket general blade
		$resp = $this->ticketresponsemodel->view($id);

		$resp = json_decode(json_encode($resp), true); # convert stdclass for array

		# André created 15/06/2020 - helpers date time zone
		$created = $this->getdatemessage->getDateTimeZone($resp["query"][0]["created"]);
		$resp["query"][0]["created"] = $created;

		if (count($resp) > 0) {

			# exits Ticket show blade
			return $this->getMessage->getServiceResponse(TRUE, "", "", $resp);
		} else {

			# not exist Ticket
			return $this->getMessage->getServiceResponse(FALSE, "", "", $resp);
		}
	}
}