<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\TeamCategoryModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class TeamCategoryService  {
    
 
    protected $teamcategorymodel;
    protected $getmessage;

    #change injects
    function __construct(TeamCategoryModel $teamcategorymodel, ServiceResponseHelper $message) {
        $this->teamcategorymodel = $teamcategorymodel;
        $this->getMessage = $message;
     
    } 

    public function add($data){

    	$resp=$this->teamcategorymodel->add($data);   

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Cadastrada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
 

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->teamcategorymodel->edit($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->teamcategorymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    



    public function lists(){

        #show branchoffice general blade
        $resp=$this->teamcategorymodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Carregado com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Categoria de Time não encontrada(s)","",$resp);#case error
        else if ($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }


     public function listsTeamCategory($idteam){
        
        #show branchoffice general blade
        $resp=$this->teamcategorymodel->liststeamcategory($idteam);

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Carregada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
        else  if($resp["count"] == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! categoria de Time não encontrada!","",$resp);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

    public function view($id){

        #show company general blade
        $resp=$this->teamcategorymodel->view($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

     public function find($data){
        
        #show branchoffice general blade
        $resp=$this->teamcategorymodel->find($data);

          

       if($resp["success"] AND $resp["count"] > 0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Time Encontrada com Sucesso!","",$resp);#case success
        else if($resp["count"] == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! categoria de Time não encontrada!","",$resp);#case empty
        else  if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }    

}    