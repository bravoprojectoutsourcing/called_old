<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\CompanyModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class CompanyService  {
    
 
    protected $companymodel;
    protected $getmessage;

    #change injects
    function __construct(CompanyModel $companyModel, ServiceResponseHelper $message) {
        $this->companymodel = $companyModel;
        $this->getMessage = $message;
     
    } 

    public function lists(){

        $resp=$this->companymodel->lists();


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa(s) Listada(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existe empresa na lista.","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Empresa(s)!","",$resp["exception"]);#case empty
         

        return $resp; 


    }

    public function add($data){


        #find company cnpj
        $resp =$this->companymodel->findCompany($data["cpfcnpj"]); 


        #$count = count($resp["query"]); 


        #echo "<pre>" ,print_r($count);exit;

        if(isset($resp["query"]) && count($resp["query"]) > 0 ) {

            $resp= $this->getMessage->getServiceResponse(TRUE,"A Empresa já está cadastrada","1",$resp);#case success
            return $resp;
        }

    	$resp=$this->companymodel->add($data);     




        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Cadastrada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->companymodel->edit($data);
  

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa editada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->companymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }    


    public function usercompanybo(){  #user company branch office(estabelecimentos)

        $resp=$this->companymodel->usercompanybo();
        
        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa(s) listada com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existe empresas na lista.","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Empresa(s)","",$resp["exception"]);#case empty
            

        return $resp;    
    }

    public function usercompany(){  #user company branch office(estabelecimentos)

        $resp=$this->companymodel->usercompany();
        
        #echo "<pre>" ,print_r($resp);exit;

        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa(s) listada com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existem empresa do usuario","",$resp["exception"]);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Empresa(s)","",$resp);#case empty
            

        return $resp;    
    }


    public function viewcompany($id){

        #show company general blade
        $resp=$this->companymodel->viewcompany($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

}    