<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\CategoryModel;
use App\Models\TicketModel;

use App\Helpers\ServiceResponseHelper;


#input rules
class CategoryService  {
    
 
    protected $categorymodel;
    protected $ticketmodel;
    protected $getmessage;

    #change injects
    function __construct(CategoryModel $categoryModel,TicketModel $ticketmodel, ServiceResponseHelper $message) {
        $this->categorymodel = $categoryModel;
        $this->ticketmodel   = $ticketmodel;
        $this->getMessage    = $message;
     
    } 

    public function add($data){


    	$resp=$this->categorymodel->add($data);   


        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Cadastrada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
 

        return $resp; 	

    }

    public function edit($data){


    	$resp=$this->categorymodel->edit($data);


        if($data["status"] == 1){ #status aproved

           $dataitem = array(
            'idcategory'=> $data["id"],
            'status'    => "Aguardando Atendimento",
           );
        

        }else if($data["status"] == 2){ #status reproved

           $dataitem = array(
            'idcategory'=> $data["id"],
            'status'    => "Reprovado",
           );

        }else if($data["status"] == 0){ #status reproved

           $dataitem = array(
            'idcategory'=> $data["id"],
            'status'    => "Aguardando Aprovação",
           );
        }   

        #update ticket category status
        $respdata = $this->ticketmodel->updatestatuscategory($dataitem);



        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->categorymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    



    public function lists(){

        #show branchoffice general blade
        $resp=$this->categorymodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria(s) Carregada(s) com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Categoria(s) não encontrada(s)","",$resp);#case error
        else if ($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }



    public function listscategoryteam(){

        #show branchoffice general blade
        $resp=$this->categorymodel->listscategoryteam();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria(s) time Carregada(s) com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Categoria(s) time não encontrada(s)","",$resp);#case error
        else if ($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Categoria(s) time!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }

     public function listscategoryuser($iduser){

        #show branchoffice general blade
        $resp=$this->categorymodel->listscategoryuser($iduser);

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria(s) Usuário(s) Carregada(s) com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
        else  if($resp["count"] == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! categoria(s) usuário(s) não encontrada(s)!","",$resp);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

    public function view($id){

        #show company general blade
        $resp=$this->categorymodel->view($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   
        
    }

    public function updateStatusCategory($data){

        $resp=$this->categorymodel->updateStatusCategory($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Status Categoria Alterada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }



}    