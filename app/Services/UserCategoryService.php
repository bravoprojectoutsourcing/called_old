<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\UserCategoryModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class UserCategoryService  {
    
 
    protected $usercategorymodel;
    protected $getmessage;

    #change injects
    function __construct(UserCategoryModel $usercategorymodel, ServiceResponseHelper $message) {
        $this->usercategorymodel = $usercategorymodel;
        $this->getMessage = $message;
     
    } 

    public function add($data){

    	$resp=$this->usercategorymodel->add($data);   

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Usuário Cadastrada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
 

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->usercategorymodel->edit($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Usuário Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->usercategorymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Usuário Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    



    public function lists(){

        #show branchoffice general blade
        $resp=$this->usercategorymodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Usuário Carregado com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Categoria de Usuário não encontrada(s)","",$resp);#case error
        else if ($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }


     public function listsUserCategory($iduser){
        
        #show branchoffice general blade
        $resp=$this->usercategorymodel->listsusercategory($iduser);

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Usuário Carregada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
        else  if($resp["count"] == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! categoria de usuário não encontrada!","",$resp);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

    public function view($id){

        #show company general blade
        $resp=$this->usercategorymodel->view($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

}    