<?php 


# Autor: André Camargo
# Date : 1º semestre 2020

namespace App\Services;

#injects
use App\Models\PriorityModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class PriorityService  {
    
 
    protected $prioritymodel;
    protected $getmessage;

    #change injects
    function __construct(PriorityModel $priorityModel, ServiceResponseHelper $message) {
        $this->prioritymodel = $priorityModel;
        $this->getMessage    = $message;     
    } 

    public function add($data){




        #find branchoffice cnpj
        $resp =$this->prioritymodel->add($data); 


        if($resp["success"] AND  $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Prioridade Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
                 

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->prioritymodel->edit($data);

     
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Prioridade Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->prioritymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Prioridade Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }    

	public function listPriorities($arrIdSeverities)
	{
		$resp = $this->prioritymodel->listPriorities($arrIdSeverities);
		
		if ($resp["success"] && $resp["exception"] == null) {
			#case success
			$resp = $this->getMessage->getServiceResponse(true, "Prioridade(s) listada(s) com Sucesso!", "", $resp);
		} elseif ($resp["exception"] != null) {
			#case error
			$resp = $this->getMessage->getServiceResponse(false, "Ops, ocorreu um erro!", "", $resp["exception"]);
		} else {
			#case empty
			$resp = $this->getMessage->getServiceResponse(false, "Ops, erro ao listar a(s) Prioridade(s)!", "", $resp);
		}
		return $resp;
	}

    public function listPrioritySeverity($id){

        #show branchoffice general blade
        $resp=$this->prioritymodel->listPrioritySeverity($id);

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Prioridade Carregada com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! prioridade(s) não encontrada.","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Prioridade!","",$resp["exception"]);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }


    public function listPriorityBurden(){

        #show branchoffice general blade
        $resp=$this->prioritymodel->listPriorityBurden();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Peso Carregado com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! peso(s) não encontrada.","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao carregar peso!","",$resp["exception"]);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

}    