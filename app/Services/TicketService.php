<?php 
namespace App\Services;

set_time_limit(300);

# Autor: Andr� Camargo
# Date : 1� semestre 2020

#injects
use App\Models\TicketModel;
use App\Models\TicketResponseModel; #task 337 André - 08/10/2020
use App\Models\CompanyModel; #created André 21/07/2020 - ticket 227 item 6
use App\Models\CategoryModel; #created André 01/09/2020 - ticket 304
use App\Models\UploadFilesModel;
use App\Helpers\ServiceResponseHelper;
use App\Helpers\ServiceDateHelper;#created Andre 15/06/2020
use App\Services\TeamsService;
use App\Mail\BravoMail;
use Mail; 
use DateTime;
use DateInterval;
use DatePeriod;


#input rules
class TicketService  {
    
 
    protected $ticketmodel;
    protected $ticketresponsemodel;#task 337 André - 08/10/2020
    protected $companymodel; #created André 21/07/2020 - ticket 227 item 6
    protected $categorymodel; #created Andre 01/09/2020 - ticket 304 item 6
    protected $uploadfilesmodel;
    protected $getmessage;
    protected $getdatemessage;#created Andre 15/06/2020 
    protected $teamsservice;

    #change injects
    function __construct(CategoryModel $categorymodel , CompanyModel $companymodel , TeamsService $teamsservice, TicketModel $ticketmodel, UploadFilesModel $uploadfilesmodel, ServiceResponseHelper $message, ServiceDateHelper $getdatemessage, TicketResponseModel $ticketresponsemodel ) {
        
        $this->ticketmodel         = $ticketmodel;
        $this->ticketresponsemodel = $ticketresponsemodel; #task 337 André - 08/10/2020
        $this->uploadfilesmodel    = $uploadfilesmodel;
        $this->getMessage          = $message;
        $this->getdatemessage      = $getdatemessage; #created Andr� 15/06/2020
        $this->teamsService        = $teamsservice;
        $this->companymodel        = $companymodel;#created André 21/07/2020 - ticket 227 item 6
        $this->categorymodel       = $categorymodel;#created Andre 01/09/2020 - ticket 304
        
     
    }  

    public function lists($data){


        session(['sessionsla' => "" ]);

       if($data["custom"][0]["nameprofile"]=="ADMIN"){ 

         

         $data = $this->conditionSQLGraph($data);

         $resp=$this->ticketmodel->listsAdmin($data);#list all tickets


         #echo "<pre>",print_r($resp);exit;

         #send SLA ajax
         session(['respsla' => $resp ]);



        #Calculate time SLA AND GET LAST DATE STATUS
        

         #$resp = $this->getSLA($resp);   

        #echo "<pre>",print_r($resp);exit;

       }else if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK") {  

            $data = $this->conditionSQLGraph($data);

            #echo "<pre>" ,print_r($data);exit;

            $resp = $this->ticketmodel->listsManager($data);
           
            #send SLA ajax
            session(['respsla' => $resp ]);



            #echo "<pre>",print_r($resp);exit;
            #Calculate time SLA AND GET LAST DATE STATUS
            #$resp = $this->getSLA($resp);            
            
           #echo "<pre>",print_r($resp);exit;
        }else if($data["custom"][0]["nameprofile"]=="MCUSTOMER" ) {  #TASK 357 - André 

              $usercompany = $this->companymodel->usercompany(); #get company(s) user
              $usercompany = json_decode(json_encode($usercompany),true); #convert stdclass for array              
              
              $i=0;

              foreach ($usercompany["query"] as $value) {
              
                if($i == 0)
                  $companys = $value["id"];
                else
                  $companys = $companys . "," . $value["id"];
              
                $i++;  

              }              


             if(session('sessionidteambox') != "" ){
                $where  = " AND tc.idteam IN(".session('sessionidteambox').") AND ticket.status='".str_replace("%"," ",session('sessionstatusbox')) ."' " ;
                $where2 =  "  left JOIN team_category tc ON ticket.idcategory = tc.idcategory 
                 left join `team` on tc.idteam = team.id";
              }else{
                $where ="  WHERE  `user`.`id` = " . $data["custom"][0]["iduser"] ." AND `profile`.`id` = " . $data["custom"][0]["idprofile"] . " AND `profile`.`id` <> 'NULL' AND ticket.id IN($companys) OR ticket.idcompany IN($companys)";
                $where2 = " left join `team` on `team`.`id` = `ticket`.`idteam`";
              }



            #list tickets user
            $resp=$this->ticketmodel->listsOthers($data,$where,$where2);


            #send SLA ajax
            session(['respsla' => $resp ]);

            #Calculate time SLA AND GET LAST DATE STATUS
            #$resp = $this->getSLA($resp);           

        }else{    


             if(session('sessionidteambox') != "" ){
                $where  = " AND tc.idteam IN(".session('sessionidteambox').") AND ticket.status='".str_replace("%"," ",session('sessionstatusbox')) ."' " ;
                $where2 =  "  left JOIN team_category tc ON ticket.idcategory = tc.idcategory 
                 left join `team` on tc.idteam = team.id";
              }else{
                $where ="  WHERE  `user`.`id` = " . $data["custom"][0]["iduser"] ." AND `profile`.`id` = " . $data["custom"][0]["idprofile"] . " AND `profile`.`id` <> 'NULL'";
                $where2 = " left join `team` on `team`.`id` = `ticket`.`idteam`";
              }



            #list tickets user
            $resp=$this->ticketmodel->listsOthers($data,$where,$where2);


            #send SLA ajax
            session(['respsla' => $resp ]);

            #Calculate time SLA AND GET LAST DATE STATUS
            #$resp = $this->getSLA($resp); 


        }      
            
        #echo "<pre>" ,print_r($resp);exit;


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"TIcket(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Tiket(s)  nao encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Ticket(s) .","",$resp["exception"]);#case empty
         
        #echo "<pre>" ,print_r($resp);exit;

         return $resp;

    }


    public function getSLA($resp) {


            $i = 0;

            session(['sessionsla' => "1" ]);


            #get last date status
            foreach ($resp["query"] as $value) {

              #verify if sla this calculate - finalizados,cancelados e concluidos -TASK-351 
              if($value["slacalculate"] == "" or $value["slacalculate"] == NULL and $value["status"] == "Finalizado" || $value["status"] == "Concluido" || $value["status"] == "Cancelado"){


                    if($value["priorityid"] != ""){

                        $datecreated = new DateTime($value["created"]);#date created ticket


                        if($value["status"] == "Aguardando Atendimento" || $value["status"] == "Em Andamento" ){

                            #get difference  created ticket up until end hour work
                            $d1 = $value["created"];

                            #get difference  date status ticket up until start hour work
                            $d2 = date('Y-m-d H:m:i');

                            $h = $this->getIntervalPeriod($d1,$d2,$value);



                            if($h <= $value["TIMEMAX"]  ){
                              $resp["query"][$i]["DIFERENCETIME"] = 0;#SLA TIME EXTRA
                              $resp["query"][$i]["STATUSTIME"] = "OK";
                            }else{  
                               $resp["query"][$i]["DIFERENCETIME"] = 1; #SLA END
                               $resp["query"][$i]["STATUSTIME"] = "CRITICAL";
                                #echo "<pre>" ,print_r($resp["query"][$i]["DIFERENCETIME"]);   
                            }

                        }


                                #if status date not null get last date response            
                                if($value["status"] == "Finalizado" || $value["status"] == "Concluido" || $value["status"] == "Cancelado"  ){
                                    
                                                     
                                 
                                  #send id ticket get last date status responseticket
                                  $resplastdate = $this->ticketresponsemodel->getlastdate($value["id"]);
                                  

                                   if($resplastdate["count"] > 0){
                                      #get date 
                                      if(isset($resplastdate["query"][0]["created"])){

                                          $resp["query"][$i]["createdstatus"] = $resplastdate["query"][0]["created"];

                                          $datecreated = $value["created"]; #date created ticket
                                          $datestatus  = $resplastdate["query"][0]["created"]; #date created status ticket

                                          #get work day
                                          $respdays = $this->WorkingDays($datecreated,$datestatus);


                                          $datecreated = new DateTime($value["created"]);
                                          $datestatus  = new DateTime($resplastdate["query"][0]["created"]);


                                          #-----------------------------------------------------if only 1 day calcule hours SLA 
                                            if($respdays["working_days"] <= 1){

                                                  #get difference  created ticket up until end hour work
                                                  $d1 = $value["created"];

                                                  #get difference  date status ticket up until start hour work
                                                  $d2 = $resplastdate["query"][0]["created"];


                                                  $h = $this->getIntervalPeriod($d1,$d2,$value);


                       
                                                  #compare if datecreated and datestatus different   
                                                  if($h <= $value["TIMEMAX"] ){

                                                     #calculate if hour before SLA timemax TABLE BD
                                                        $resp["query"][$i]["DIFERENCETIME"] = 0; #concluido no prazo
                                                        $resp["query"][$i]["STATUSTIME"]    = "OK";#SUM status cancelados,finalizados and concluidos CRITICAL DONUT
                                                  
                                                  }else{
                                                     #calculate if hour before SLA timemax TABLE BD
                                                        $resp["query"][$i]["DIFERENCETIME"] = 1; #concluido fora do prazo
                                                        $resp["query"][$i]["STATUSTIME"]    = "CRITICAL";#SUM status cancelados,finalizados and concluidos CRITICAL DONUT                                              
                                                  }
                            
                                                
                                             }else{ #------------------------------------------------------------- calcule SLA MORE DAYS

                                                              

                                                  #get difference  created ticket up until end hour work
                                                  $d1 = $value["created"];

                                                  #get difference  date status ticket up until start hour work
                                                  $d2 = $resplastdate["query"][0]["created"];


                                                  $h = $this->getIntervalPeriod($d1,$d2,$value);



                                                  if($h <= $value["TIMEMAX"]){
                                                    $resp["query"][$i]["DIFERENCETIME"] = 0; #concluido no prazo
                                                    $resp["query"][$i]["STATUSTIME"] = "OK";#SUM status cancelados,finalizados and concluidos IN CRITICAL DONUT
                                                  }else{
                                                    $resp["query"][$i]["DIFERENCETIME"] = 1; #concluido fora do prazo
                                                    $resp["query"][$i]["STATUSTIME"] = "CRITICAL";#SUM status cancelados,finalizados and concluidos IN CRITICAL DONUT
                                                  }



                                             }  

                                                #task-351 mark ticket calculate------------------------------------------------------
                                                  $datavalue = array(
                                                    'id'           => $value["id"], #id ticket
                                                    'slacalculate' => $resp["query"][$i]["DIFERENCETIME"],
                                                    'slastatustime'=> $resp["query"][$i]["STATUSTIME"]
                                                  );

                                                  $respmark = $this->ticketmodel->markTicketCalculate($datavalue);
                                                #-----------------------------------------------------------------------------------


                                      }else{
                                          #add key if not status
                                          $resp["query"][$i]["createdstatus"] = "000-00-00 00:00:00" ; 
                                      }
                                    
                                    }else{ #case not date "finalizado" concluido fora do prazo 

                                        if($value["createdresponse"] != NULL OR $value["createdresponse"] != "" ){

                                                  #get difference  created ticket up until end hour work
                                                  $d1 = $value["created"];

                                                  #get difference  date status ticket up until start hour work
                                                  $d2 = $value["createdresponse"];


                                                  $h = $this->getIntervalPeriod($d1,$d2,$value);



                                                  if($h <= $value["TIMEMAX"]){
                                                    $resp["query"][$i]["DIFERENCETIME"] = 0; #concluido no prazo
                                                    $resp["query"][$i]["STATUSTIME"] = "OK";#SUM status cancelados,finalizados and concluidos IN CRITICAL DONUT
                                                  }else{
                                                    $resp["query"][$i]["DIFERENCETIME"] = 1; #concluido fora do prazo
                                                    $resp["query"][$i]["STATUSTIME"] = "CRITICAL";#SUM status cancelados,finalizados and concluidos IN CRITICAL DONUT
                                                  }


                                                  #task-351 mark ticket calculate------------------------------------------------------
                                                    $datavalue = array(
                                                      'id'           => $value["id"], #id ticket
                                                      'slacalculate' => $resp["query"][$i]["DIFERENCETIME"],
                                                      'slastatustime'=> $resp["query"][$i]["STATUSTIME"]
                                                    );

                                                    $respmark = $this->ticketmodel->markTicketCalculate($datavalue);
                                                  #-----------------------------------------------------------------------------------

                                             }else{


                                                $resp["query"][$i]["DIFERENCETIME"] = 5; #SLA END
                                                $resp["query"][$i]["STATUSTIME"] = "CRITICAL";                                              
                                             }  

                                         

                                   }

                                          

                                      
                                

                                }else{



                                  #CREATE EMPTY CASE NOT EXISTS
                                  $resp["query"][$i]["createdstatus"] = NULL;

                               }


                                  if($value["status"] == "Aguardando Solicitante" || $value["status"] == "Suspenso" || $value["status"] == "Pausado" || $value["status"] == "Aguardando Aprovação")
                                   $resp["query"][$i]["DIFERENCETIME"] = 2;#SLA TIME EXTRA

                                  if($value["status"] == "Reprovado")
                                   $resp["query"][$i]["DIFERENCETIME"] = 3;#SLA TIME EXTRA
                                 
                         }else{
                              
                              unset($resp["query"][$i]["DIFERENCETIME"]);#SLA OFF
                              $resp["query"][$i]["STATUSTIME"] = "";

                              
                         }        
                  }   

                  $i++;
            
            }


            #case select graph DONUT -------------------------------------------------------------
            
                $i=0;

                if(session('sessionstatustime') != ""){
                 
                  foreach ($resp["query"] as $value) {
                        #echo "<pre>" ,print_r($resp);exit;
                        if(session('sessionstatustime') == "OK") {
                          
                           if($value["STATUSTIME"] != session('sessionstatustime') )
                             unset($resp["query"][$i]);#remove key case donut click graph

                        }


                        if(session('sessionstatustime') == "CRITICAL" ) {
                          
                           if($value["STATUSTIME"] != session('sessionstatustime') )
                             unset($resp["query"][$i]);#remove key case donut click graph

                           if($value["STATUSTIME"] == session('sessionstatustime') && $value["status"] == "Reprovado" || $value["status"] == "Aguardando Aprovação" || $value["status"] == "Suspenso")
                             unset($resp["query"][$i]);#remove key case donut click graph


                        }
                       $i++;  
                  }      
                  
                               

                }  

            #END DONUT-----------------------------------------------------------------------    
                

            return $resp;

    }



public function getIntervalPeriod($d1,$d2,$value){

          #get interval dates 
          $datestart = $d1;
          $dateend   = $d2;


          $datetime1 = new DateTime($datestart);
          $datetime2 = new DateTime($dateend);

          $intervalminutes = new DateInterval('PT1M');
          $period          = new DatePeriod($datetime1, $intervalminutes, $datetime2);
          $minutes         = 0;
          $x               = 0;

           #$period = json_decode(json_encode($period),true); #convert stdclass for array


         foreach ($period as $data) {
                        
                      #echo $data->format('H:i:s')."***".$value["id"]."<br>";
                        #verify weekend
                        $dayweek = date('w', strtotime($data->format('Y-m-d H:i:s')));

                        if($dayweek > 0 and $dayweek < 6  ){

                            $hour = date("H")-3; #hour now


                            #echo $hour;exit;
                             #echo $data->format('H:i:s')."***".$value["id"]."***".$hour."<br>";
	
	                        if (in_array($data->format('H'), [7,8,9,10,11,13,14,15,16,17,18]))
                           $minutes++;
                               
                            

                            

                        }  


          }

            $h = $minutes /60;


            return $h;

}

    public function WorkingDays($datecreated,$datestatus){



              #----------------------------------------------------------------calcule util days 
              $begin = strtotime($datecreated);
              $end   = strtotime($datestatus);
              
              if ($begin > $end) {
              echo "startdate is in the future!
              ";

                  return 0;
              
              } else {
                  $no_days  = 0;
                  $weekends = 0;
                  while ($begin <= $end) {
                      $no_days++; // no of days in the given interval
                      $what_day = date("N", $begin);
                      if ($what_day > 5) { // 6 and 7 are weekend days
                          $weekends++;
                      };
                      $begin += 86400; // +1 day
                  };
                  $working_days = $no_days - $weekends;

                  #echo $working_days." --" .$value["id"]."<br>";
              }

              $workingdays = $working_days * 8;
              #-------------------------------------------------------------------end utildays

              $data = array(
                'week'         => $weekends,
                'working_days' => $working_days
              );

           return $data;             

    }

    public function conditionSQLGraph($data){


                          #task 250 - André - 05/08/2020
                          $teamid = "";
                          $i      = 0 ;

                          foreach ($data["custom"] as  $value){

                            if($i == 0)
                              $teamid = $value["teamid"];
                            else
                              $teamid = $teamid . "," . $value["teamid"];
                          
                            $i++;
                          } 
                          #end task 250  


                            #get value box dashboard session passed AJAX
                          if(session('sessionidteambox') != "" ){

                            if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK") {  


                                  if(session('sessionidteambox') != "" ){
                                    $where  = " WHERE tc.idteam IN(".session('sessionidteambox').") AND ticket.status='".str_replace("%"," ",session('sessionstatusbox')) ."' group by `ticket`.`id` order by `ticket`.`id` desc " ;
                                    $where2 =  " left JOIN team_category tc ON ticket.idcategory = tc.idcategory 
                                     left join `team` on tc.idteam = team.id";
                                  }else{
                                    $where ="WHERE `team_category`.`idteam` In(".$teamid.") OR ticket.iduser = " . $data["custom"][0]["iduser"] . " group by `ticket`.`id` order by `ticket`.`id` desc";
                                    $where2 = " left join `team` on `team`.`id` = `ticket`.`idteam`";
                                  }


      


                            }else{
                               
                                $where  = " WHERE tc.idteam IN(".session('sessionidteambox').") AND ticket.status='".str_replace("%"," ",session('sessionstatusbox')) ."' group by `ticket`.`id` order by `ticket`.`id` desc" ;
                                $where2 =  " left JOIN team_category tc ON ticket.idcategory = tc.idcategory 
                                  left join `team` on tc.idteam = team.id";

                                  
                            }

                            goto end;
                           

                          #task 324 - get critical,ok or attention sla view graph donut sla     
                          }elseif(session('sessionstatustime') != ""){
                            
                            if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK" AND session('sessionstatustime')=="CRITICAL") {  

                                     $where  = " WHERE `burden_urgency`.`name`is NOT NULL 
                                            AND `team_category`.`idteam` In(".$teamid.") OR ticket.iduser = " . $data["custom"][0]["iduser"] ."
                                            AND `ticket`.`status` IN ('Aguardando Atendimento','Em Andamento','Finalizado','Concluido','Cancelado') group by `ticket`.`id` order by `ticket`.`id` desc";
                                                                       

                            }else if($data["custom"][0]["nameprofile"]=="ADMIN"  AND session('sessionstatustime')=="CRITICAL") {                      


                                     $where  = " WHERE `burden_urgency`.`name`is NOT NULL 
                                            AND `ticket`.`status` IN ('Aguardando Atendimento','Em Andamento','Finalizado','Concluido','Cancelado')
                                            group by `ticket`.`id` order by `ticket`.`id` desc";

                            }



                            if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK" AND session('sessionstatustime')=="ATTENTION") {  

                                     $where  = " WHERE `burden_urgency`.`name`is NOT NULL 
                                            AND `team_category`.`idteam` In(".$teamid.") OR ticket.iduser = " . $data["custom"][0]["iduser"] ."
                                            group by `ticket`.`id`
                                            -- HAVING STATUSTIME='ATTENTION'
                                            -- AND `ticket`.`status` <> 'Finalizado' 
                                            AND `ticket`.`status` <> 'Aguardando Aprovação'
                                            -- AND `ticket`.`status` <> 'Cancelado'
                                            AND `ticket`.`status` <> 'Concluido'
                                            AND `ticket`.`status` <> 'Reprovado'                                             
                                            order by `ticket`.`id` desc";                              

                            }if($data["custom"][0]["nameprofile"]=="ADMIN"  AND session('sessionstatustime')=="ATTENTION") {    
                               

                                       $where  = " WHERE `burden_urgency`.`name`is NOT NULL 
                                           -- AND `burden_urgency`.`timehour` - (TOTAL_WEEKDAYS(`ticket`.`created`, now())*8)  = 0
                                            group by `ticket`.`id`
                                            -- HAVING STATUSTIME='ATTENTION'
                                            -- AND `ticket`.`status` <> 'Finalizado' 
                                            AND `ticket`.`status` <> 'Aguardando Aprovação'
                                            -- AND `ticket`.`status` <> 'Cancelado'
                                            AND `ticket`.`status` <> 'Concluido'
                                            AND `ticket`.`status` <> 'Reprovado'                                             
                                            order by `ticket`.`id` desc";


                                }


                            if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK" AND session('sessionstatustime')=="OK") {  

                                     $where  = " where `burden_urgency`.`name`is NOT NULL 
                                            AND `team_category`.`idteam` In(".$teamid.") OR ticket.iduser = " . $data["custom"][0]["iduser"] ."
                                             AND `ticket`.`status` IN ('Aguardando Atendimento','Em Andamento','Finalizado','Concluido','Cancelado')
                                             group by `ticket`.`id` order by `ticket`.`id` desc";
                                            
                                                             

                            }if($data["custom"][0]["nameprofile"]=="ADMIN"  AND session('sessionstatustime')=="OK") {   

                                    $where  = " WHERE `burden_urgency`.`name`is NOT NULL 
                                    AND `ticket`.`status` IN ('Aguardando Atendimento','Em Andamento','Finalizado','Concluido','Cancelado') 
                                                       group by `ticket`.`id` order by `ticket`.`id` desc";

                                    }

                                            
                              $where2 = " left join `team` on `team`.`id` = `ticket`.`idteam`";


                          }else{ #CONDITION NORMAL CLICK MENU

                            if($data["custom"][0]["nameprofile"]=="MANAGER" || $data["custom"][0]["nameprofile"]=="CLERK") 
                               $where =" WHERE `team_category`.`idteam` In(".$teamid.") OR ticket.iduser = " . $data["custom"][0]["iduser"] ." group by `ticket`.`id` order by `ticket`.`id` desc";
                            else if($data["custom"][0]["nameprofile"]=="ADMIN")
                               $where =" group by `ticket`.`id` order by `ticket`.`id` desc ";



                               $where2 = " left join `team` on `team`.`id` = `ticket`.`idteam` ";



                          }

end:
                          $data=array(
                            'where'     => $where,
                            'where2'    => $where2
                          );


                          return $data;

    }

    public function add($data){

        

        $data["iduser"]  = session('resp')["custom"][0]["iduser"];

        #Andr� created 15/06/2020 - helpers date time zone
        $created = $this->getdatemessage->getDateTimeZone("Y-m-d H:i:s"); 

        $data["created"] = $created;


        #echo "<pre>" ,print_r($data);exit;

        $resp=$this->ticketmodel->add($data);     

        if($resp["success"] AND $resp["exception"] == null){
            
            $resp= $this->getMessage->getServiceResponse(TRUE,"Chamado Cadastrado com Sucesso!","",$resp);#case success
            
            #send email notification

                #get  list emails team
                $emailsteam = $this->teamsService->emailsTeam($data["idcategory"]);

                #echo "<pre>" ,print_r($emailsteam);exit;

                $x = 0;

                $emails = "";
                
                #mount emails team
                foreach ($emailsteam["custom"]["query"] as $value) {

                    if($x==0)
                     $emails = "$value";
                    else 
                     $emails = $emails.","."$value";       

                    $x++;
                }

                #clear symbols
                $emails = str_replace("{", "", $emails);
                $emails = str_replace("}", "", $emails);
                $emails = str_replace('"', '', $emails);
                $emails = str_replace("email:", "", $emails);


                $emails = str_replace(' ','',$emails);
                $emails = (string) $emails;
                $emails = explode(',',$emails);

                $data["email"] = $emails;
                $data["view"]  = "email.email" ;#view mail
                
                #echo "<pre>" ,print_r($resp);exit;

                #get company name - created André 21/07/2020 - ticket 227 item 6
                $company = $this->companymodel->viewcompany($data["idcompany"]);

                 $resp = json_decode(json_encode($resp),true); #convert stdclass for array
                 $company = json_decode(json_encode($company),true); #convert stdclass for array

                $data["subject"] = "Número do chamado: ".$resp["custom"]["lastid"] . " Empresa: " . $company["query"][0]["name"] ; 

                #send email 
                Mail::to($data["email"])->send(new BravoMail($data)); 

            #end send mail

        }else if($resp["exception"] != null){
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
        }
            

        return $resp;   

    }

    public function updateStatus($data){




        #created André task 304 - send email
        #key $data get 4(four) values this
        if($data["status"] == "Aprovado"){

                $mail = $data["email"];#solicitante ticket

                #get  list emails team
                $emailsteam = $this->teamsService->emailsTeam($data["idcategory"]);


                $x = 0;

                $emails = "";
                
                #mount emails team
                foreach ($emailsteam["custom"]["query"] as $value) {

                    if($x==0)
                     $emails = $value["email"].", $mail";
                    else 
                     $emails = $emails.",".$value["email"];       

                    $x++;
                }

                #clear symbols
                $emails = str_replace("{", "", $emails);
                $emails = str_replace("}", "", $emails);
                $emails = str_replace('"', '', $emails);
                $emails = str_replace("email:", "", $emails);


                $emails = str_replace(' ','',$emails);
                $emails = (string) $emails;
                $emails = explode(',',$emails);
                
                


                $data["email"] = $emails ;
                $data["view"]  = "email.emailstatus" ;#view mail

                $data["subject"] = " Ticket Aprovado pelo Gestor! , Número do chamado: ".$data["id"] ; 

                #echo "<pre>" ,print_r($data);exit;

                #send email 
                Mail::to($data["email"])->send(new BravoMail($data)); 



                $data = array(
                    'status' => "Aguardando Atendimento",
                    'id'     => $data["id"]
                );



        }elseif($data["status"] == "Reprovado"){

                $mail = $data["email"];#solicitante ticket

                #get  list emails team
                $emailsteam = $this->teamsService->emailsTeam($data["idcategory"]);


                $x = 0;

                $emails = "";
                
                #mount emails team
                foreach ($emailsteam["custom"]["query"] as $value) {

                    if($x==0)
                     $emails = $value["email"].", $mail";
                    else 
                     $emails = $emails.",".$value["email"];       

                    $x++;
                }

                #clear symbols
                $emails = str_replace("{", "", $emails);
                $emails = str_replace("}", "", $emails);
                $emails = str_replace('"', '', $emails);
                $emails = str_replace("email:", "", $emails);


                $emails = str_replace(' ','',$emails);
                $emails = (string) $emails;
                $emails = explode(',',$emails);
                
                


                $data["email"] = $emails ;
                $data["view"]  = "email.emailstatus" ;#view mail

                $data["subject"] = " Ticket Reprovado pelo Gestor! , Número do chamado: ".$data["id"] ; 

                #echo "<pre>" ,print_r($data);exit;

                #send email 
                Mail::to($data["email"])->send(new BravoMail($data)); 



                $data = array(
                    'status' => "Reprovado",
                    'id'     => $data["id"]
                );


        }    

        #case diferent aproved/reproved $data get 2 values
            
        #Andr� created 15/06/2020 - helpers date time zone
        $created = $this->getdatemessage->getDateTimeZone('Y-m-d  H:i:s'); 


        $data["createdstatus"] = $created;
        $resp=$this->ticketmodel->updateStatus($data);
  

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Ticket Status com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

        return $resp;    
    }

    public function updateStatusCategory($data){



        #Andr� created 15/06/2020 - helpers date time zone
        $created = $this->getdatemessage->getDateTimeZone('Y-m-d  H:i:s'); 


        $data["createdresponse"] = $created;
        $resp=$this->ticketmodel->updateStatusCategory($data);
  

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Ticket Status com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

        return $resp;    
    }

    public function edit($data){

        $resp=$this->ticketmodel->edit($data);
  

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Ticket editado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }



    public function del($id){

        $resp=$this->ticketmodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Chamado Deletado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }    



    public function view($id){

        #show Ticket general blade
        $resp = $this->ticketmodel->view($id);

        $resp = json_decode(json_encode($resp),true); #convert stdclass for array

        #echo "<pre>" ,print_r($resp);exit;

        #Andr� created 15/06/2020 - helpers date time zone
       $created = $this->getdatemessage->getDateTimeZone($resp["query"][0]["created"]); 
       $resp["query"][0]["created"] = $created;


       #echo "<pre>" ,print_r($resp);exit;  

        if(count($resp)>0){

            #exits Ticket show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist Ticket 
            return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

    public function liststicket($id){

        $resp=$this->ticketmodel->liststicket($id);
  
        #get files ticket
        $resp["filepath"] = $this->uploadfilesmodel->view($id);


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Lista(s) TIcket(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Lista(s) Ticket(s) n�o encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Lista(s) Ticket(s) de Usu�rio(s)!","",$resp["exception"]);#case empty
                     

        return $resp;    
    }
    

    public function listStatus()
    {
        $resp = $this->ticketmodel->listStatus();
        
        if ($resp["success"] && $resp["exception"] == null) {
            #case success
            $resp = $this->getMessage->getServiceResponse(true, "Status listado(s) com Sucesso!", "", $resp);
        } elseif ($resp["exception"] != null) {
            #case error
            $resp = $this->getMessage->getServiceResponse(false, "Ops, ocorreu um erro!", "", $resp["exception"]);
        } else {
            #case empty
            $resp = $this->getMessage->getServiceResponse(false, "Ops, erro ao listar o(s) Status!", "", $resp);
        }
        return $resp;
    }


    #cron schedule - task 263 - created André - 19/08/2020
    public function cronStatusTicket(){


        $resp = $this->ticketmodel->cronStatusTicket();

        

        if ($resp["success"] && $resp["exception"] == null) {
            #case success
            
            $datastatus[] = array();
            $resp         = $this->getMessage->getServiceResponse(true, "Status update com Sucesso!", "", $resp);    
            $resp         = json_decode(json_encode($resp),true); #convert stdclass for array



            foreach ($resp["custom"]["query"] as $value) {


                    $datecreated = new DateTime($value["datestatus"]);
                    $now         = new DateTime($this->getdatemessage->getDateTimeZone('Y-m-d  H:i:s')); 


   
                    $interval    = date_diff($datecreated, $now);

                    if($interval->d > 5){

                        $data = array(
                            'status' => "Finalizado",
                            'id'     => $value["id"]
                        );


                        $respupdate = $this->updateStatus($data);

                         if($respupdate["custom"]["success"] == 1){

                            $datastatus[] = array(

                                'idticketfinalizado' => $value["id"],
                                'sucessoupdate'      => 'sucesso' 

                            );


                         }else{


                            $datastatus[] = array(

                                'idticketfinalizado' => $value["id"],
                                'sucessoupdate'      => 'erro' 

                            );

                         }
 
                    }


            }


            $resp = $this->getMessage->getServiceResponse(true, "Status update com Sucesso!", "", $datastatus);

        } elseif ($resp["exception"] != null) {
            #case error
            $resp = $this->getMessage->getServiceResponse(false, "Ops, ocorreu um erro!", "", $resp["exception"]);
        } else {
            #case empty
            $resp = $this->getMessage->getServiceResponse(false, "Ops, erro ao listar o(s) Status!", "", $resp);
        }
        return $resp;


    }

}    