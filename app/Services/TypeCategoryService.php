<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\TypeCategoryModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class TypeCategoryService  {
    
 
    protected $typecategorymodel;
    protected $getmessage;

    #change injects
    function __construct(TypeCategoryModel $typecategoryModel, ServiceResponseHelper $message) {
        $this->typecategorymodel = $typecategoryModel;
        $this->getMessage        = $message;
     
    } 

    public function add($data){


        #find type category exists
        $resp =$this->typecategorymodel->add($data["name"]); 


        if(isset($resp["query"]) && count($resp["query"]) > 0 ) {

            $resp= $this->getMessage->getServiceResponse(TRUE,"O Tipo Categoria já está cadastrada","1",$resp);#case success
            return $resp;
        }
  

        
        $resp =$this->typecategorymodel->add($data); 


        if($resp["success"]  AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Tipo Categoria Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

            

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->typecategorymodel->edit($data);

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Tipo Categoria Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->typecategorymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Tipo Categoria Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    



    public function lists(){

        #show category general blade
        $resp=$this->typecategorymodel->lists();

        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Tipo(s)  Categoria(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Tipo(s) Categorias(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Tipo(s) Categoria(s).","",$resp["exception"]);#case empty
         

        return $resp;     

        
    }

}    