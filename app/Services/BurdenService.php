<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\burdenmodel;
use App\Helpers\ServiceResponseHelper;


#input rules
class BurdenService  {
    
 
    protected $burdenmodel;
    protected $getmessage;

    #change injects
    function __construct(BurdenModel $burdenmodel, ServiceResponseHelper $message) {
        $this->burdenmodel = $burdenmodel;
        $this->getMessage = $message;
     
    } 

    public function add($data){


        #find branchoffice cnpj
        $resp =$this->burdenmodel->add($data); 


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Estabelecimento Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

            

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->burdenmodel->edit($data);

 
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->burdenmodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    
    


    public function lists(){

        #show branchoffice general blade
        $resp=$this->burdenmodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Categoria Carregada com Sucesso!","",$resp);#case success
        else else if($resp["count"] == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existe nenhuma categoria.","",$resp["query"]);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Categoria!","",$resp);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

}    