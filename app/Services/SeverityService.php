<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\SeverityModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class SeverityService  {
    
 
    protected $severitymodel;
    protected $getmessage;

    #change injects
    function __construct(SeverityModel $severityModel, ServiceResponseHelper $message) {
        $this->severitymodel = $severityModel;
        $this->getMessage = $message;
     
    } 

    public function add($data){


        #find branch office - estabelecimentos cnpj
        $resp =$this->severitymodel->findbranchoffice($data["cpfcnpj"]); 


        if(isset($resp["query"]) && count($resp["query"]) > 0 ) {

            $resp= $this->getMessage->getServiceResponse(TRUE,"A Severidade já está cadastrada","1",$resp);#case success
            return $resp;
        }
  

        #find branchoffice cnpj
        $resp =$this->severitymodel->add($data); 


        if($resp["success"] AND  $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Severidade Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
           

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->severitymodel->edit($data);

  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Severidade Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

        return $resp;    
    }


    public function del($id){

        $resp=$this->severitymodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Severidade Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }    



    public function lists(){

        #show branchoffice general blade
        $resp=$this->severitymodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Severidade Carregada com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Severidade não encontrada","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Severidade!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }

}    