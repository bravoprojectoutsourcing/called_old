<?php 
namespace App\Services;
session_start();
#injects
use App\Models\UploadFilesModel;
use App\Helpers\ServiceResponseHelper;
use URL;


#Autor: Andre Camargo
# Date : 1� semestre 2020 

#input rules
class UploadFilesService  {
    
 
    protected $uploadfilesmodel;
    protected $getmessage;

    #change injects
    function __construct(UploadFilesModel $uploadFilesModel, ServiceResponseHelper $message) {
        $this->uploadfilesmodel = $uploadFilesModel;
        $this->getMessage = $message;
     
    } 

    public function uploadfile($data){

	#echo "<pre>",print_r($data);

         $path = public_path() . '/upload/';

         if ($data['resume']['name']) {
            if (!$data['resume']['error']) {
                $name = md5(rand(100, 200));
                $ext = explode('.', $data['resume']['name']);


                 if($data["idticket"]["name"] != "" )
                    $filename = $data["idticket"]["name"] . $name . '.' . $ext[1];#file not repeat responseticket
                 else                      
                    $filename =  $name . $data["complfile"]["name"] . '.' . $ext[1];#file not repeat ticket  

                
                $destination = public_path() . '/upload/' . $filename;
                $location = $data["resume"]["tmp_name"];
                
                if ( ! is_dir($path)) 
                   mkdir($path , 0777, $recursive = true);

                    
                move_uploaded_file($location, $destination);


                 if($data["idticket"]["name"] != "" ){
                 
                    $data = array(
                        'idticket' => $data["idticket"]["name"],
                        'path'     => URL::to('/upload/'.$filename),
                    );

                 }else{
                 
                    $data = array(
                        'idticket' => $data["idticketoriginal"]["name"],
                        'path'     => URL::to('/upload/'.$filename),
                    );

                
                 }                      
                 
                 echo "<pre>",print_r($data);

                 #aDd path file table upload
                 $this->add($data);

                $data["originalfile"] = $filename;

                $resp= $this->getMessage->getServiceResponse(TRUE,"Upload Eftuado com Sucesso!","",$data);#case success

                #echo "<pre>",print_r($filename);exit;
                

            } else {

                $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$data['resume']['error']);#case error

            }
          }

           

        return $resp; 	

    }



    public function add($data){

        $data["created"] = date("Y-m-d H:m:s");
       

        $resp=$this->uploadfilesmodel->add($data);

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Caminho Upload Salvo com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    

    }


}    