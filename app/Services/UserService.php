<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\UserModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class UserService  {
    
 
    protected $usermodel;
    protected $getmessage;

    #change injects
    function __construct(UserModel $userModel, ServiceResponseHelper $message) {
        $this->usermodel = $userModel;
        $this->getMessage = $message;
     
    }

    public function lists(){

        $resp=$this->usermodel->lists();


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Usuário(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Usuário(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Usuário(s)!","",$resp["exception"]);#case empty
                  
        #dd($resp);    
        return $resp; 
    }


    public function editview($data){

        $resp=$this->usermodel->editview($data);

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Email e Telefone editado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
         
        return $resp; 
    }

}