<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\BranchOfficeModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class BranchOfficeService  {
    
 
    protected $branchofficemodel;
    protected $getmessage;

    #change injects
    function __construct(BranchOfficeModel $branchofficeModel, ServiceResponseHelper $message) {
        $this->branchofficemodel = $branchofficeModel;
        $this->getMessage = $message;
     
	} 
	
	public function listBranchOffices($arrIdCompany)
	{
		$resp = $this->branchofficemodel->listBranchOffices($arrIdCompany);
		
		if ($resp["success"] && $resp["exception"] == null) {
			#case success
			$resp = $this->getMessage->getServiceResponse(true, "Estabelecimento(s) listado(s) com Sucesso!", "", $resp);
		} elseif ($resp["exception"] != null) {
			#case error
			$resp = $this->getMessage->getServiceResponse(false, "Ops, ocorreu um erro!", "", $resp["exception"]);
		} else {
			#case empty
			$resp = $this->getMessage->getServiceResponse(false, "Ops, erro ao listar Estabelecimento(s)!", "", $resp);
		}
		return $resp;
	}

    public function listBranchOfficeCompany($id){

        $resp=$this->branchofficemodel->listbranchofficecompany($id);


        #echo "<pre>" ,print_r(count($resp["query"]));exit;

        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Filia(is) Listada(s) com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existe filiais cadastradas","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Filia(is)!","",$resp["exception"]);#case empty
         


        return $resp; 


    }

    public function add($data){


        #find branch office - estabelecimentos cnpj
        $resp =$this->branchofficemodel->findbranchoffice($data["cpfcnpj"]); 


        if(isset($resp["query"]) && count($resp["query"]) > 0 ) {

            $resp= $this->getMessage->getServiceResponse(TRUE,"A Empresa já está cadastrada","1",$resp);#case success
            return $resp;
        }
  

        #find branchoffice cnpj
        $resp =$this->branchofficemodel->add($data); 


        if($resp["success"]  AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Estabelecimento Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error

            

        return $resp; 	

    }

    public function edit($data){

    	$resp=$this->branchofficemodel->edit($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->branchofficemodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Deletada com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }    



    public function view($id){

        #show branchoffice general blade
        $resp=$this->branchofficemodel->view($id);

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Carregada com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! não existe empresa a ser carregada.","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar Empresa!","",$resp["exception"]);#case empty
            
           # echo "<pre>" ,print_r($resp);exit;    

        return $resp;     

        
    }

}    