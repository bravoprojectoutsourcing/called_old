<?php


namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020


#injects
use App\Models\LoginModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class LoginService  {
    
 
    protected $loginModel;
    protected $getMessage;

    #change injects
    function __construct(LoginModel $loginmodel, ServiceResponseHelper $message) {
        $this->loginModel   = $loginmodel;
        $this->getMessage = $message;
     
    } 

    public function login($data){
        

        #layer dao
    	$resp=$this->loginModel->login($data);

       #success here
        if(count($resp)==0)        	
           return $this->getMessage->getServiceResponse(false,"Login ou senha inválido!",null,null);
        else
           return $this->getMessage->getServiceResponse(true,"Login efetuado com sucesso!",null,$resp);	
    	

    }


    public function modules($id){

            #layer dao
            $resp=$this->loginModel->modules($id);
            return $resp;

    } 

    #inner join login + team
    public function loginTeam(){

            #layer dao
            $resp=$this->loginModel->loginTeam();
            return $resp;

    } 

    #inner join login + usercategory
    public function LoginUserCategory(){

            #layer dao
            $resp=$this->loginModel->LoginUserCategory();
            return $resp;

    } 

    public function submenus($menus){

        $data = array();

        foreach ($menus as $value) {


           
            $resp = $this->loginModel->submenus($value->idmodule);
            
              foreach ($resp as $item) {

                    if($value->idmodule!= 1) #module visão geral not submenus
                        $data[] = $item;


                }  

             


        }
        
            return $data;

    }    

    public function submenu_category(){

    	#layer dao
    	$resp=$this->loginModel->submenu_category();
    	return $resp;

    }

    public function  listsubcategory($data){

       	#layer dao
    	$resp=$this->loginModel->listsubcategory($data);
    	return $resp;


    }

    public function dashboard(){

        #layer dao
        $resp=$this->loginDao->dashboard();
        return $resp;        

    }
    
    
}

