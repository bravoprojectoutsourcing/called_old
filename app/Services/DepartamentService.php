<?php 

namespace App\Services;



# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\DepartamentModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class DepartamentService  {
    
 
    protected $departamentmodel;
    protected $getmessage;

    #change injects
    function __construct(DepartamentModel $departamentModel, ServiceResponseHelper $message) {
        $this->departament = $departamentModel;
        $this->getMessage  = $message;
     
    } 

    public function lists(){

        #show branchoffice general blade
        $resp=$this->departament->lists();

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Departamentos Carregados com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Não existem departamentos na lista","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao carregar Departamento!","",$resp["exception"]);#case empty
            

        return $resp;     
        
    }

    public function add($data){ 

        #find Departament cnpj
        $resp =$this->departament->add($data); 


        if($resp["success"]  AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Departamento Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
          

        return $resp; 	

    }

    public function view($id){

        #show company general blade
        $resp=$this->departament->view($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

    public function edit($data){

    	$resp=$this->departament->edit($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa Salva com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->departament->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Departamento deletado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }  

    public function listuserdepartament(){


        $iduser  = session('resp')["custom"][0]["iduser"];
        
        #show Departament general blade
        $resp=$this->departament->listuserdepartament($iduser);
        
       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Empresa(s) Carregada com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! departamento(s) não existente(s)","",$resp);#case error
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Departamento!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }

}    