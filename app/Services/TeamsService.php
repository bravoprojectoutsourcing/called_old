<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\TeamsModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class TeamsService  {
    
 
    protected $teamsmodel;
    protected $getmessage;

    #change injects
    function __construct(TeamsModel $teamsModel, ServiceResponseHelper $message) {
        $this->teamsmodel = $teamsModel;
        $this->getMessage = $message;
     
    }


    public function listsUserTeams($iduser){

        $resp=$this->teamsmodel->listsUserTeams($iduser);

        if($resp["success"] AND count($resp["count"])>0 AND  $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time(s) de Usuário(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Time(s) de Usuário(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Time(s) de Usuário(s).","",$resp["exception"]);#case empty
         
        #dd($resp);    
        return $resp; 
    }

    public function lists(){

        $resp=$this->teamsmodel->lists();

        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time(s)  Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Time(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Time(s).","",$resp["exception"]);#case empty
                  
        #dd($resp);    
        return $resp; 
    }

    public function edit($data){

    	$resp=$this->teamsmodel->edit($data);
  

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time editado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error


        return $resp;    
    }

    public function del($id){

        $resp=$this->teamsmodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time Deletado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }
    
    public function add($data){

    	$resp=$this->teamsmodel->add($data);   

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp; 	

    }

    public function delUserTeam($data){

        $resp=$this->teamsmodel->delUserTeam($data); ;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time Deletado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            
            

        return $resp;    
    }

    public function addUserTeam($data){

    	$resp=$this->teamsmodel->addUserTeam($data);   

        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time Cadastrado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp; 	

    }

    public function emailsTeam($id){

        $resp=$this->teamsmodel->emailsTeam($id);   

        #echo "<pre>" ,print_r($resp);exit;

        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Email(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Email(s) de Usuário(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Email(s) de Usuário(s)!","",$resp["exception"]);#case empty
                     

        return $resp; 

    }

    public function getOutTeams($id){

        #show company general blade
        $resp=$this->teamsmodel->getOutTeams($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

    public function getTeams($id){

        #show company general blade
        $resp=$this->teamsmodel->getTeams($id);

        if(count($resp)>0)
            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
        else
            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            
       
    }



    #get team category
    public function teamCategory($idteam){

        $resp=$this->teamsmodel->teamCategory($idteam);   


        if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"Time(s) Categoria(s) Listado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["count"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! Time(s) Categoria(s) não encontrado(s)","",$resp);#case error
        elseif($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar Time(s) Categoria(s)!","",$resp["exception"]);#case empty
                     

        return $resp; 

    }


}