<?php 

namespace App\Services;


# Autor: André Camargo
# Date : 1º semestre 2020

#injects
use App\Models\SLAModel;
use App\Helpers\ServiceResponseHelper;


#input rules
class SLAService  {
    
 
    protected $slamodel;
    protected $getmessage;

    #change injects
    function __construct(SLAModel $slaModel, ServiceResponseHelper $message) {
        $this->slamodel   = $slaModel;
        $this->getMessage = $message;
     
    } 

    public function add($data){

        $resp = $this->slamodel->find($data);

       if(count($resp["query"])>0){
               
            $resp= $this->getMessage->getServiceResponse(TRUE,"Esse SLA já está cadastrado!","",$resp);#case success

       }else if(count($resp["query"]) == 0){

            #-------------------------------------------------------------------------------------------ADD case SLA null
            $resp = $this->slamodel->add($data);   

            if($resp["success"] AND $resp["exception"] == null)
                $resp= $this->getMessage->getServiceResponse(TRUE,"SLA Cadastrado com Sucesso!","",$resp);#case success
            else if($resp["exception"] != null)
                $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
     

            return $resp;   


       }else if ($resp["exception"] != null){
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao listar SLA!","",$resp["exception"]);#case empty
       }

        return $resp;
        #echo "<pre>" ,print_r($resp);exit;

    }

    public function edit($data){

    	$resp=$this->slamodel->edit($data);

        #echo "<pre>" ,print_r($data);exit;
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"SLA Salvo com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }


    public function del($id){

        $resp=$this->slamodel->del($id);
  
        if($resp["success"] AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"SLA Deletado com Sucesso!","",$resp);#case success
        else if($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! ocorreu um erro","",$resp["exception"]);#case error
            

        return $resp;    
    }    



    public function lists(){

        #show branchoffice general blade
        $resp=$this->slamodel->lists();

       

       if($resp["success"] AND count($resp["count"])>0 AND $resp["exception"] == null)
            $resp= $this->getMessage->getServiceResponse(TRUE,"SLA(s) Carregado(s) com Sucesso!","",$resp);#case success
        else if(count($resp["query"]) == 0)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Oppsssss! SLA(s) não encontrada(s)","",$resp);#case error
        else if ($resp["exception"] != null)
            $resp= $this->getMessage->getServiceResponse(FALSE,"Opsss! erro ao deletar SLA!","",$resp["exception"]);#case empty
            

        return $resp;     

        
    }


  
    public function view($id){

        #show company general blade
        $resp=$this->slamodel->view($id);

       #echo "<pre>" ,print_r($resp["query"][0]->id);exit;        

        if(count($resp)>0){

            #exits company show blade
            return $this->getMessage->getServiceResponse(TRUE,"","",$resp);
            

          }else{

            #not exist company 
			return $this->getMessage->getServiceResponse(FALSE,"","",$resp);
            

        }   

        
    }

}    