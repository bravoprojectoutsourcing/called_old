<?php

namespace App\Services;

#injects
use App\Models\ReportModel;
use App\Helpers\ServiceResponseHelper;

#input rules
class ReportService
{
	protected $reportModel;
	protected $getMessage;
	#change injects
	function __construct(ReportModel $report, ServiceResponseHelper $message)
	{
		$this->reportModel = $report;
		$this->getMessage = $message;
	}
	
	public function listFilterResults($arrData)
	{
		$resp = $this->reportModel->listFilterResults($arrData);
		
		if ($resp["success"] && $resp["exception"] == null) {
			#case success
			$resp = $this->getMessage->getServiceResponse(true, "Arquivo gerado com Sucesso!", "", $resp);
		} elseif ($resp["exception"] != null) {
			#case error
			$resp = $this->getMessage->getServiceResponse(false, "Ops, ocorreu um erro!", "", $resp["exception"]);
		} else {
			#case empty
			$resp = $this->getMessage->getServiceResponse(false, "Ops, erro ao gerar o arquivo!", "", $resp);
		} 
		return $resp;
	}
}
