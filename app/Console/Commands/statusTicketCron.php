<?php

namespace App\Console\Commands;



use Illuminate\Console\Command;
use App\Services\TicketService;


class statusTicketCron extends Command
{

     protected $ticketservice ;

     #signature cron 
     protected $signature = 'statusticket:cron';


    public function __construct(TicketService $ticketService)
    {
        #change service construct
        $this->ticketservice  = $ticketService;
        parent::__construct();
    }


    public function handle()
    {
        $this->ticketservice->cronStatusTicket();     
    }
}
