<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BravoMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $itens;
   

    public function __construct($data)
    {
         $this->itens = $data;    		
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


            return $this->from($this->itens["email"], 'Suporte Bravo')
                ->subject($this->itens["subject"])
                #->message('teste')
                ->view($this->itens["view"])->with('resp', $this->itens); 



        //return $this->view('email.email');
    }
}
