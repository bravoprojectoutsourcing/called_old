<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class MediumRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    


        if(session('resp')["custom"][0]["idprofile"]!=1 && session('resp')["custom"][0]["idprofile"]!=3 && session('resp')["custom"][0]["idprofile"]!=6 ){

            echo "Rota não permitida!";
            exit;
            //return Redirect::to('singin');

        }




            return $next($request);
    }
}