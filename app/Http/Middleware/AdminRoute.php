<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class AdminRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next){



        if(session('resp')["custom"][0]["idprofile"]!=1  ){
           #echo "j";exit;
            return Redirect::to('/');

        }


            return $next($request);
    }
}
