<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\UserService;
use Illuminate\Http\Request;
use View;
use Session;

class UserController extends Controller {



    protected $userService;

    #dependency injection
    function __construct(UserService $user) {

    	$this->userservice = $user;


    } 

 
	public function editview(){

        $data = array(
        	"id"    => $_REQUEST['id'],
        	"phone" => $_REQUEST['phone'],
        	"email" => $_REQUEST['email'],
        );

        $resp = $this->userservice->editview($data);

        echo json_encode($resp); 

    }
    
 
}
