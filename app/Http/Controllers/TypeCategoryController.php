<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\TypeCategoryService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class TypeCategoryController extends Controller{
    
    protected $typecategoryservice;
    protected $utilservice;

    #dependency injection
    public function __construct(TypeCategoryService $typecategoryservice , UtilService $utilservice){
        
        #change service construct
        $this->typecategoryservice = $typecategoryService;
        $this->utilservice         = $utilservice;
    
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->typecategoryservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->typecategoryservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->typecategoryservice->lists();

        response $resp;

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->typecategoryservice->del($id); 

        
        echo json_encode($data); 
    }



}    