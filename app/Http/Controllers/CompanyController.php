<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\CompanyService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class CompanyController extends Controller{
    
    protected $companyservice;
    protected $utilservice;

    #dependency injection
    public function __construct(CompanyService $companyService , UtilService $utilservice){
        
        #change service construct
        $this->companyservice  = $companyService;
        $this->utilservice     = $utilservice;
    
    }


    public function index(){

 
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->companyservice->lists(),
            'page'     => 'company.company',
            'state'    =>  $this->utilservice->listState()#load states
        );

        #echo "<pre>" ,print_r($resp);exit;

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('state',$resp['state'])
        ->with('page', $resp['page']);

    	
    }



    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];

        #echo "<pre>" ,print_r($data);exit;

        $resp = $this->companyservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){

        $data = $_REQUEST['data'];
        $resp = $this->companyservice->edit($data);

        echo json_encode($resp); 

    }



    #show company general
    public function viewcompany(){



        $id = $_REQUEST['id'];
        $resp = $this->companyservice->viewcompany($id);

        echo json_encode($resp); 

    }


    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->companyservice->del($id); 

        
        echo json_encode($data); 
    }


    public function listCity(){


        $id = $_REQUEST['id'];


        $resp=$this->utilservice->listCity($id);
       # echo "<pre>" ,print_r($resp);exit;
        echo json_encode($resp);    


    }

    public function cityName(){


        $name = $_REQUEST['name'];

        $resp=$this->utilservice->cityName($name);

        echo json_encode($resp);    


    }

    public function listCityState(){


        $state = $_REQUEST['state'];

        $resp=$this->utilservice->listCityState($state);

        echo json_encode($resp);    


    }

}    