<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\DashboardService;
use App\Services\TicketService;
use Illuminate\Http\Request;
use View;
use Session;


class DashboardController extends Controller{
    
    protected $dashboardservice;
    protected $ticketservice;
    

    #dependency injection
    public function __construct(DashboardService $dashboardService,TicketService $ticketservice  ){
        #change service construct
        $this->dashboardservice  = $dashboardService;
        $this->ticketservice     = $ticketservice;
    }


    public function index(){


        #clear session graph
        session(['sessionidteambox' => "" ]);
        session(['sessionstatustime' => "" ]);

        $resp=array(
            'resp'         =>  session('resp'),
            'menus'        =>  session('menus'),
            'submenus'     =>  session('submenus'),
            'page'         => 'dashboard.dashboard', 
        );

        #echo "<pre>" ,print_r($resp);exit;

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('page', $resp['page']);

    	
    }


    public function indexAjaxGraph(){


        $resp = $this->ticketservice->lists(session('resp'));

        #send list sla grap and list
        $resp = $this->ticketservice->getSLA(session('respsla'));

        echo json_encode($resp);    
    }

    #task 306
    public function menuBoxTeam(){

          $data = $_REQUEST['data'];  

          $resp = $this->dashboardservice->menuBoxTeam($data);    

          #echo "<pre>" ,print_r($resp);
          #exit;     

          echo json_encode($resp);
    }


   public function ticket(){ 
   
        #BAR GRAPH

        #closed      = finalizado
        #cancel      = cancelado
        #wait        = aguardando atendimento
        #open1       = aguardando atendimento
        #open2       = aguardando solicitante
        #conclued    = concluido
        #onaprovall  = aguardando aprovacao
        #disaproved  = reprovado


        #tickets closed
        $respclosed      = $this->dashboardservice->ticket(session('resp'),'closed');     
        $respopen        = $this->dashboardservice->ticket(session('resp'),'open1'); 
        $respopen2       = $this->dashboardservice->ticket(session('resp'),'open2');  
        $respcancel      = $this->dashboardservice->ticket(session('resp'),'cancel');   
        $respwait        = $this->dashboardservice->ticket(session('resp'),'wait');
        $respconcluded   = $this->dashboardservice->ticket(session('resp'),'concluded');
        $responapproval  = $this->dashboardservice->ticket(session('resp'),'onapproval');#task-324
        $respdisapproved = $this->dashboardservice->ticket(session('resp'),'disapproved');#task-324

        # echo "<pre>" ,print_r($respopen);exit;

        if($respclosed["custom"]["count"] > 0){
            $boxclosed["custom"]["count"] = $respclosed["custom"]["query"][0]["total"];
            $boxclosed["success"] = $respclosed["success"];
        }else{
            $boxclosed["success"] = $respclosed["success"];
            $boxclosed = 0;
        }


        if($respopen["custom"]["count"] > 0){
            $boxopen["custom"]["count"] = $respopen["custom"]["count"];
            $boxopen["success"] = $respopen["success"];
        }else{
            $boxopen["success"] = $respopen["success"];
            $boxopen["custom"]["count"] = 0;
        }

        if($respopen2["custom"]["count"] > 0){
            $boxopen2["custom"]["count"] = $respopen2["custom"]["count"];
            $boxopen2["success"] = $respopen2["success"];
        }else{
            $boxopen2["success"] = $respopen2["success"];
            $boxopen2["custom"]["count"] = 0;
        }


        if($respcancel["custom"]["count"] > 0){

            $boxcancel["custom"]["count"]  = $respcancel["custom"]["count"];
            $boxcancel["success"] = $respcancel["success"];
        }else{
            $boxcancel["success"] = $respcancel["custom"]["success"];
            $boxcancel["custom"]["count"] = 0;
        }

        if($respwait["custom"]["count"] > 0){
            $boxwait["custom"]["count"] = $respwait["custom"]["count"];
            $boxwait["success"] = $respwait["success"];
        }else{
            $boxwait["success"] = $respwait["success"];
            $boxwait["custom"]["count"] = 0;
        }

        if($respconcluded["custom"]["count"] > 0){
            $boxconcluded["custom"]["count"] = $respconcluded["custom"]["count"];
            $boxconcluded["success"] = $respconcluded["success"];
        }else{
            $boxconcluded["custom"]["count"] = 0;
            $boxconcluded["success"] = $respconcluded["success"];
        }


        if($responapproval["custom"]["count"] > 0){
            $boxonapproval["custom"]["count"] = $responapproval["custom"]["count"];
            $boxonapproval["success"] = $responapproval["success"];
        }else{
            $boxonapproval["custom"]["count"] = 0;
            $boxonapproval["success"] = $responapproval["success"];
        }


        if($responapproval["custom"]["count"] > 0){
            $boxdisapproved["custom"]["count"] = $respdisapproved["custom"]["count"];
            $boxdisapproved["success"] = $respdisapproved["success"];
        }else{
            $boxdisapproved["custom"]["count"] = 0;
            $boxdisapproved["success"] = $respdisapproved["success"];
        }

        $data = array(
            'boxopen1'      => $boxopen,
            'boxopen2'      => $boxopen2,
            'boxclosed'     => $boxclosed,
            'boxcancel'     => $boxcancel,
            'boxwait'       => $boxwait,
            'boxconcluded'  => $boxconcluded,
            #task-324
            'boxonapproval' => $boxonapproval,
            'boxdisapproved'=> $boxdisapproved,
            
        );

     
           #echo "<pre>" ,print_r($data);exit;

      echo json_encode($data); 
   }

  public function graph(){

     $resp = $this->dashboardservice->graph(session('resp'));         

     #echo "<pre>" ,print_r($resp);exit;

      echo json_encode($resp);

  }

}    