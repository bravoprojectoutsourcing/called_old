<?php

#inject model 
namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\TicketService;
use App\Services\CompanyService;
use App\Services\DepartamentService;
use App\Services\CategoryService;
use App\Services\TypeCategoryService;
use App\Services\TeamsService;
use App\Services\SeverityService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;

class TicketController extends Controller {

    protected $ticketservice;
    protected $utilservice;
    protected $companyservice;
    protected $departamentservice;
    protected $severityservice;
    protected $categoryservice;
    protected $typecategoryservice;
    protected $teamsservice;


    #dependency injection
    public function __construct(TicketService $ticketService , UtilService $utilservice , CompanyService $companyservice, DepartamentService $departamentservice , CategoryService $categoryservice, TypeCategoryService $typecategoryservice,SeverityService $severityservice, TeamsService $teamsservice){
        
        #change service construct
        $this->ticketservice       = $ticketService;
        $this->utilservice         = $utilservice;
        $this->companyservice      = $companyservice;
        $this->departamentservice  = $departamentservice;
        $this->categoryservice     = $categoryservice;
        $this->typecategoryservice = $typecategoryservice;
        $this->severityservice     = $severityservice;
        $this->teamsservice        = $teamsservice;

    }

    public function index(){



        $resp=array(
            'resp'              =>  session('resp'),
            'menus'             =>  session('menus'),
            'submenus'          =>  session('submenus'),
            'sessionidteambox'  =>  session('sessionidteambox'), #GET BOX EVENT CLICK
            'sessionstatustime' =>  session('sessionstatustime'),#GET GRAPHPIER EVENT CLICK
            'lists'             =>  $this->ticketservice->lists(session('resp')),
            'page'              => 'ticket.ticket',
            'branchoffice'      =>  $this->companyservice->usercompanybo(), #branchoffice(estabelecimentos por usuarios e empresas) 
            'userteam'          =>  $this->teamsservice->listsUserTeams(session('resp')["custom"][0]["iduser"]),
            'company'           =>  $this->companyservice->usercompany(), 
            'departament'       =>  $this->departamentservice->listuserdepartament(), #departament user
            'category'          =>  $this->categoryservice->listscategoryuser(session('resp')["custom"][0]["iduser"]), #list category user
            'typecategory'      =>  $this->typecategoryservice->lists(),#list type category
            'severity'          =>  $this->severityservice->lists(), #list severity
            
        );
       

        if($resp["departament"]["success"]== null || $resp["departament"]["success"]== false AND $resp["category"]["success"]== null || $resp["category"]["success"]== null  ){

            return View::make('templates.default')
            ->with('resp', $resp)
            ->with('page', 'error.error');

        }else{

            return View::make('templates.default')
            ->with('resp', $resp)
            ->with('page', $resp['page']);


        }

    	
    }



    public function addSessionTicket(){

        $data   = $_REQUEST['data'];
        $status = $_REQUEST['status'];

        session(['sessionidteambox' => $data]);
        session(['sessionstatusbox' => "$status"]);

        echo json_encode(session('sessionidteambox')); 
    }


    public function addSessionTicketGraphPier(){


        $statustime = $_REQUEST['statustime'];

        session(['sessionstatustime' => $statustime]);
        
        echo json_encode(session('sessionstatustime')); 
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->ticketservice->add($data);
        echo json_encode($resp); 

    }



    public function updateStatus(){

        $data = $_REQUEST['data'];
        $resp = null;

        #echo "<pre>" ,print_r($data);exit;


        if(session('resp')["custom"][0]["nameprofile"] != "REQUESTER" AND $data["status"] == "Concluido" || $data["status"] == "Finalizado" || $data["status"] == "Cancelado" || $data["status"] == "Suspenso" || $data["status"] == "Aguardando Solicitante" || $data["status"] == "Em Andamento" || $data["status"] == "Aprovado" || $data["status"] == "Reprovado") #created André - 21/07/2020 ticket 227 item 2
            $resp = $this->ticketservice->updateStatus($data);
        elseif(session('resp')["custom"][0]["nameprofile"] == "REQUESTER" AND $data["status"] == "Aguardando Atendimento")
            $resp = null;   

        echo json_encode($resp); 

    }

    public function edit(){

        $data = $_REQUEST['data'];
        $resp = $this->ticketservice->edit($data);

        echo json_encode($resp); 

    }

    #show ticket general
    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->ticketservice->view($id);

        echo json_encode($resp); 

    }

    #show ticket response first view 
    public function liststicket(){
        
        $id = $_REQUEST['id'];
        $resp = $this->ticketservice->liststicket($id);

        echo json_encode($resp); 

    }


    public function del(){

        $id = $_REQUEST['id'];
       
        $data=$this->ticketservice->del($id); 

        
        echo json_encode($data); 
    }




	public function responseCalled(){

		$resp = array(
		'page'  => 'called.responsecalled',
		#'resp'  => session('resp'),
		#'list'  => $this->customerservice->lists(), #list customer
		#'count' => count($this->customerservice->lists()), #total customers
		#'npage' => $this->customerservice->pages(5), #number pages
		#'custom'  => null,
		#'state' => $this->utilservice->listState()#load states
		
		) ;

		#echo "<pre>" ,print_r($resp);exit;

		return View::make('templates.default')
		->with('page', $resp['page']);
    		
	}

    public function cronStatusTicket(){

        $resp = $this->ticketservice->cronStatusTicket();

        echo "<pre>" ,print_r($resp);
        exit;

    }

    public function getSLA(){

        #to charge list SLA
           if(session('sessionidteambox') != "" || session('sessionstatustime') != "" ) #graph box not clicked
               $this->ticketservice->lists(session('resp'));

        
        #send list sla grap and list
        $resp = $this->ticketservice->getSLA(session('respsla'));

        echo json_encode($resp); 

    }

}
