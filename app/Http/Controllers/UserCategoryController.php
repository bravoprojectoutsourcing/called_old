<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020


use App\Services\UserCategoryService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class UserCategoryController extends Controller{
    
    protected $usercategoryservice;
    protected $utilservice;
    protected $userservice;

    #dependency injection
    public function __construct(UserCategoryService $usercategoryservice , UtilService $utilservice, UserService $userService){
        
        #change service construct
        $this->usercategoryservice = $usercategoryservice;
        $this->utilservice         = $utilservice;
        $this->userservice         = $userService;
    
    }

    public function index(){

        $id = $_REQUEST['id'];


        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->usercategoryservice->listsUserCategory($id),
            'user'     =>  $this->userservice->lists(),
            'page'     => 'categories.categories'
        );
        
        #echo "<pre>" ,print_r($resp);exit;
        

        echo json_encode($resp); 

    	
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->usercategoryservice->add($data);


        echo json_encode($resp); 

    }

    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->usercategoryservice->view($id);

        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->usercategoryservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->usercategoryservice->lists();
        
        echo json_encode($resp);

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->usercategoryservice->del($id); 

        
        echo json_encode($data); 
    }



}    