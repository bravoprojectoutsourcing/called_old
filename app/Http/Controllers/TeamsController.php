<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use Illuminate\Http\Request;
use View;
use Session;
use App\Services\TeamsService;
use App\Services\UserService;

class TeamsController extends Controller
{
    protected $teamsservice;
    protected $userservice;

    #dependency injection
    public function __construct(TeamsService $teamsService, UserService $userService ){
        
        #change service construct
        $this->teamsservice  = $teamsService;
        $this->userservice  = $userService;
    
    }

    public function index(){
        

 
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->teamsservice->lists(),
            'user'     =>  $this->userservice->lists(),
            'page'     => 'teams.teams'
        );

        //echo "<pre>" ,print_r($resp);exit;

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('page', $resp['page']);

    	
    }
    public function edit(){
        $data = $_REQUEST['data'];
        $resp = $this->teamsservice->edit($data);

        echo json_encode($resp); 
    }
    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->teamsservice->del($id);         
        echo json_encode($data); 
    }

    public function userteam(){
        $id = $_REQUEST['id'];
        $data['isntHave'] = $this->teamsservice->getOutTeams($id)['custom']['query'];
        $data['have'] = $this->teamsservice->getTeams($id)['custom']['query'];
        echo json_encode($data); 

    }

    public function adduserteam(){
        $data = $_REQUEST['data'];
        $resp = $this->teamsservice->addUserTeam($data);
        echo json_encode($resp); 

    }

    public function deluserteam(){
        $data = $_REQUEST['data'];
        $resp = $this->teamsservice->delUserTeam($data);
        echo json_encode($resp); 

    }

    public function teamCategory(){
        $id = $_REQUEST['id'];
        $resp = $this->teamsservice->teamCategory($id);
        echo json_encode($resp); 

    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->teamsservice->add($data);


        echo json_encode($resp); 

    }


}
