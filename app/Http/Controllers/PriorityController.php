<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\PriorityService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class PriorityController extends Controller{
    
    protected $priorityservice;
    protected $utilservice;

    #dependency injection
    public function __construct(PriorityService $priorityService , UtilService $utilservice){
        
        #change service construct
        $this->priorityservice = $priorityService;
        $this->utilservice     = $utilservice;
    
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->priorityservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->priorityservice->edit($data);

        echo json_encode($resp); 

    }



    public function listPrioritySeverity(){

        $id = $_REQUEST['id'];
        $resp = $this->priorityservice->listPrioritySeverity($id);


         echo json_encode($resp); 

    }


    public function lists(){

        $resp = $this->priorityservice->lists();

        return $resp;

    }

    public function listpriority(){

        $id = $_REQUEST['id'];
        $resp = $this->priorityservice->listpriority($id);

        return $resp;

    }

    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->priorityservice->del($id); 

        
        echo json_encode($data); 
    }



}    