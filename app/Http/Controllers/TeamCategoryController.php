<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020


use App\Services\TeamCategoryService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class TeamCategoryController extends Controller{
    
    protected $teamcategoryservice;
    protected $utilservice;
    protected $userservice;

    #dependency injection
    public function __construct(TeamCategoryService $teamcategoryservice , UtilService $utilservice, UserService $userService){
        
        #change service construct
        $this->teamcategoryservice = $teamcategoryservice;
        $this->utilservice         = $utilservice;
        $this->userservice         = $userService;
    
    }

    public function index(){

        $id = $_REQUEST['id'];

        #echo $id;exit;
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->teamcategoryservice->liststeamcategory($id),
            'user'     =>  $this->userservice->lists(),
            'page'     => 'categories.categories'
        );
        
        #echo "<pre>" ,print_r($resp);exit;
        

        echo json_encode($resp); 

    	
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->teamcategoryservice->add($data);


        echo json_encode($resp); 

    }

    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->teamcategoryservice->view($id);

        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->teamcategoryservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->teamcategoryservice->lists();
        
        echo json_encode($resp);

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->teamcategoryservice->del($id); 

        
        echo json_encode($data); 
    }


    public function find(){


        $data = $_REQUEST['data'];

        
        $data = $this->teamcategoryservice->find($data); 

        
        echo json_encode($data); 


    }



}    