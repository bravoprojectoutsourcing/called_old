<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\SeverityService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class SeverityController extends Controller{
    
    protected $severityservice;
    protected $utilservice;

    #dependency injection
    public function __construct(SeverityService $severityService , UtilService $utilservice){
        
        #change service construct
        $this->severityservice = $severityService;
        $this->utilservice     = $utilservice;
    
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->severityservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->severityservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->severityservice->lists();

        response $resp;

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->severityservice->del($id); 

        
        echo json_encode($data); 
    }



}    