<?php 

#portuguese: cadastro de prioridades (P1,P2...)
#relaciona-se com a(s) tabela(s): 
namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\BurdenService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class BurdenController extends Controller{
    
    protected $burdenservice;
    protected $utilservice;

    #dependency injection
    public function __construct(BurdenService $burdenservice , UtilService $utilservice){
        
        #change service construct
        $this->burdenservice = $burdenservice;
        $this->utilservice   = $utilservice;
    
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->burdenservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->burdenservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->burdenservice->lists();

        response $resp;

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->burdenservice->del($id); 

        
        echo json_encode($data); 
    }



}    