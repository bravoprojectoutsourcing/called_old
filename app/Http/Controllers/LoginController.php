<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\LoginService;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use View;
use Session;


class LoginController extends Controller {



    protected $loginService;

    #dependency injection
    function __construct(LoginService $login) {

    	$this->loginService = $login;


    } 

    #suport case error session open
    public function index(Request $request){

    	#echo \Config::get('app.env');exit;
    	return new RedirectResponse(\Config::get('app.env'));
    	
    }

	public function login(Request $request){
		#exit;
			#get inputs
		$email = $request->input('email');
		$pass  = $request->input('pass');


     	#login only POST			
		if ($request->isMethod('post')) {		
		   

			if($email=="" || $pass==""){

				echo "faça o login novamente!";
				exit;
			}
				#array user
				$data = array(
				  'email'=> $email,
				  'pass' => $pass 
				);



				#call service menus and submenus
				$resp     	   = $this->loginService->login($data);
				$resp     	   = json_decode(json_encode($resp),true); #convert stdclass for array
				$menus    	   = $this->loginService->modules($resp["custom"][0]["idprofile"]);
				$team     	   = $this->loginService->loginTeam(); #get team session id
				$usercategory  = $this->loginService->LoginUserCategory(); #created André 16/06/2020
				$submenus      = $this->loginService->submenus($menus);


				#echo "<pre>" ,print_r($usercategory);exit();				
				
				#save session
				session(['resp'     => $resp]);
				session(['menus'    => $menus]);
				session(['team'     => $team]);
				session(['category' => $usercategory]);
				session(['submenus' => $submenus]);
			

				$resp=array(
					'resp'     => $resp,
					'menus'    => $menus,
					'team'     => $team,
					'submenus' => $submenus,
					'category' => $usercategory,
					'page'     => 'dashboard.dashboard'
				);

				#echo "<pre>" ,print_r($resp);exit;
				return View::make('templates.default')
				->with('resp', $resp)
				->with('page', $resp['page']);

		   	


		}else{
			echo "Opssss É preciso efetuar o login!";
			exit;
		}

    		
	}


}
