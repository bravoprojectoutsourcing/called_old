<?php
 
#inject model
namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\TicketResponseService;
use App\Services\TeamsService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;
use DateTime;
USE DateTimeZone;

class TicketResponseController extends Controller {

    protected $ticketresponseservice;
    protected $utilservice;
    protected $teamsservice;


    #dependency injection
    public function __construct(TicketResponseService $ticketResponseService , UtilService $utilservice , TeamsService $teamsservice){
        
        #change service construct
        $this->ticketresponseservice = $ticketResponseService;
        $this->utilservice           = $utilservice;
        $this->teamsservice          = $teamsservice;

    }

    public function index(){

        $id = $_REQUEST['id'];

       
        $resp=array(
            'lists'        =>  $this->ticketresponseservice->lists($id),
        );

#echo "<pre>" ,print_r($resp["lists"]["custom"]);exit; 
        $i = 0;

        foreach ($resp["lists"]["custom"]["query"] as  $value) {
                   
          $dt_obj  = new DateTime($value["created"] ." UTC");
          $dt_obj->setTimezone(new DateTimeZone("America/Belem"));
          $created = date_format($dt_obj, 'Y-m-d H:i:s');

           #echo "<pre>",print_r($value["created"] . "---". $created);exit; 
                $resp["lists"]["custom"]["query"][$i]["created"] = $value["created"];

                $i++;
        }
        

        echo json_encode($resp["lists"]); 

    	
    }


    public function add(){

        #echo "ok";exit;



        $data = $_REQUEST['data'];
        $resp = $this->ticketresponseservice->add($data);
        echo json_encode($resp); 

    }



    public function edit(){

        $data = $_REQUEST['data'];
        $resp = $this->ticketresponseservice->edit($data);

        echo json_encode($resp); 

    }

    #show ticket general
    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->ticketresponseservice->view($id);


        echo json_encode($resp); 

    }

    public function del(){

        $id = $_REQUEST['id'];
       
        $data=$this->ticketresponseservice->del($id); 

        
        echo json_encode($data); 
    }





}
