<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ReportService;
use App\Services\CompanyService;
use App\Services\BranchOfficeService;
use App\Services\DepartamentService;
use App\Services\CategoryService;
use App\Services\SeverityService; // same as "Urgency" in pt-BR locale
use App\Services\TicketService;
use App\Services\TypeCategoryService;
use App\Services\TeamsService;
use App\Services\PriorityService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class ReportController extends Controller
{
	protected $reportService;
	protected $companyService;
	protected $branchofficeService;
	protected $departamentService;
	protected $categoryService;
	protected $severityService;
	protected $ticketService;
	protected $typeCategoryService;
	protected $teamService;
	protected $priorityService;
	
	public function __construct(ReportService $reportsService, CompanyService $companiesService, BranchOfficeService $branchofficesService, DepartamentService $departamentsService, CategoryService $categoriesService, SeverityService $severitiesService, TicketService $ticketsService, TypeCategoryService $typeCategoryServices, TeamsService $teamsService, PriorityService $prioritiesService)
	{
		$this->reportService  		= $reportsService;
		$this->companyService  		= $companiesService;
		$this->branchofficeService	= $branchofficesService;
		$this->departamentService  	= $departamentsService;
		$this->categoryService 		= $categoriesService;
		$this->severityService 		= $severitiesService;
		$this->ticketService		= $ticketsService;
		$this->typeCategoryService	= $typeCategoryServices;
		$this->teamService  		= $teamsService;
		$this->priorityService		= $prioritiesService;
	}
	
	public function filters()
	{
			$filter = array(
				'status' => '',
				'message' => ''
			);
			if (session('resp')["custom"][0]["idprofile"] == 1) {
				$resp = array(
					'resp'			=>  session('resp'),
					'menus'			=>  session('menus'),
					'submenus'		=>  session('submenus'),
					'companies'     =>  $this->companyService->lists(),
					'departament'	=>  $this->departamentService->lists(),
					'category'      =>  $this->categoryService->lists(),
					'severity'      =>  $this->severityService->lists(),
					'typeCategory'	=>	$this->typeCategoryService->lists(),
					'team'			=> 	$this->teamService->lists(),
					'status'		=>	$this->ticketService->listStatus(),
					'page'			=> 	'reports.filters'
				);
			} else {
				$resp = array(
					'resp'			=>  session('resp'),
					'menus'			=>  session('menus'),
					'submenus'		=>  session('submenus'),
					'companies'     =>  $this->companyService->usercompany(),
					'departament'	=>  $this->departamentService->listuserdepartament(),
					'category'      =>  $this->categoryService->listscategoryuser(session('resp')["custom"][0]["iduser"]),
					'severity'      =>  $this->severityService->lists(),
					'typeCategory'	=>	$this->typeCategoryService->lists(),
					'team'			=> 	$this->teamService->listsUserTeams(session('resp')["custom"][0]["iduser"]),
					'status'		=>	$this->ticketService->listStatus(),
					'page'			=> 	'reports.filters'
				);
			}

			return View::make('templates.default')
				->with('resp', $resp)
				->with('page', $resp['page']);
				// ->with('filter', $filter);
	}
	
	public function saveFilters(Request $request)
	{
		$arrForm = $request->all();
		$methodType = $arrForm['methodType'];
		$resp = $this->reportService->listFilterResults($arrForm);
		
		$columnNames =  [
			"ticket_id",
			"created_at",
			"user_name",
			"user_phone",
			"user_email",
			"company_name",
			"branchoffice_name",
			"departament_name",
			"category_name",
			"severity_name",
			"priority_name",
			"type_category_name",
			"team_name",
			"ticket_short_message",
			"ticket_description",
			"ticket_created_response_at",
			"ticket_status"
		];
		
		if (count($resp["custom"]["query"]) > 0) {
			foreach ($resp["custom"]["query"] as $tickets) {
				$rows[] = [
					"ticket_id" => $tickets->ticket_id,
					"created_at" => $tickets->created_at,
					"user_name" => $tickets->user_name,
					"user_phone" => $tickets->user_phone,
					"user_email" => $tickets->user_email,
					"company_name" => $tickets->company_name,
					"branchoffice_name" => $tickets->branchoffice_name,
					"departament_name" => $tickets->departament_name,
					"category_name" => $tickets->category_name,
					"severity_name" => $tickets->severity_name,
					"priority_name" => $tickets->priority_name,
					"typecategory_name" => $tickets->typecategory_name,
					"team_name" => $tickets->team_name,
					"ticket_short_message" => $tickets->ticket_short_message,
					"ticket_description" => $tickets->ticket_description,
					"ticket_created_response_at" => $tickets->ticket_created_response_at,
					"ticket_status" => $tickets->ticket_status,
				];
			}
			
			if ($methodType == "csv") {
				// Este return faz o stream do arquivo no formato CSV
				return generateCSVFile($columnNames, $rows, 'called_tickets');
			// } elseif ($methodType == "pdf") {
			// 	// Este return faz o stream do arquivo no formato PDF
			// 	return generateCSVFile($columnNames, $rows, 'called_tickets');
			} elseif ($methodType == "xls") {
				// Este return faz o stream do arquivo no formato Excel (2003/365)
				return generateXLSFile($columnNames, $rows, 'called_tickets');
			} else {
				echo json_encode($resp);
			}
		} else {
			if (session('resp')["custom"][0]["idprofile"] == 6) {
				$resp = array(
					'resp'			=>  session('resp'),
					'menus'			=>  session('menus'),
					'submenus'		=>  session('submenus'),
					'companies'     =>  $this->companyService->usercompany(),
					'departament'	=>  $this->departamentService->listuserdepartament(),
					'category'      =>  $this->categoryService->listscategoryuser(session('resp')["custom"][0]["iduser"]),
					'severity'      =>  $this->severityService->lists(),
					'typeCategory'	=>	$this->typeCategoryService->lists(),
					'team'			=> 	$this->teamService->listsUserTeams(session('resp')["custom"][0]["iduser"]),
					'status'		=>	$this->ticketService->listStatus(),
					'page'			=> 	'reports.filters'
				);
				
				$filter = array(
					'status' => false,
					'message' => 'Não encontramos nenhum resultado com os dados do filtro informado ou seu perfil de Gestor não possui todos os parâmetros necessários para uma consulta. Tente novamente usando outras combinações de filtros ou contate o Administrador do Sistema.'
				);
			} else {
				$resp = array(
					'resp'			=>  session('resp'),
					'menus'			=>  session('menus'),
					'submenus'		=>  session('submenus'),
					'companies'     =>  $this->companyService->lists(),
					'departament'	=>  $this->departamentService->lists(),
					'category'      =>  $this->categoryService->lists(),
					'severity'      =>  $this->severityService->lists(),
					'typeCategory'	=>	$this->typeCategoryService->lists(),
					'team'			=> 	$this->teamService->lists(),
					'status'		=>	$this->ticketService->listStatus(),
					'page'			=> 	'reports.filters'
				);
				
				$filter = array(
					'status' => false,
					'message' => 'Não encontramos nenhum resultado com os dados do filtro informado. Tente novamente usando outras combinações de filtros.'
				);
			}
			
			

			return View::make('templates.default')
				->with('resp', $resp)
				->with('page', $resp['page'])
				->with('filter', $filter);
		}
	}
	
	public function listBranchOffices(Request $request)
	{
		$arrIdCompany = $request->id;
		$resp = $this->branchofficeService->listBranchOffices($arrIdCompany);

		echo json_encode($resp);
	}
	
	public function listPriorities(Request $request)
	{
		$arrIdSeverities = $request->id;
		$resp = $this->priorityService->listPriorities($arrIdSeverities);

		echo json_encode($resp);
	}
}
