<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020


use App\Services\CategoryService;
use App\Services\TicketService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use App\Services\UserService;
use App\Services\TeamsService; #task 283
use View;
use Session;


class CategoryController extends Controller{
    
    protected $categoryservice;
    protected $ticketservice;
    protected $userservice;

    #dependency injection
    public function __construct(CategoryService $categoryService ,TicketService $ticketservice , UserService $userservice, TeamsService $teamservice){
        
        #change service construct
        $this->categoryservice = $categoryService;
        $this->ticketservice   = $ticketservice; #task 212
        $this->userservice     = $userservice;
        $this->teamservice     = $teamservice; # task 283
    
    }

    public function index(){
        

 
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->categoryservice->lists(),
            'user'     =>  $this->userservice->lists(),
            'team'     =>  $this->teamservice->lists(),
            'page'     => 'categories.categories'
        );
        

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('page', $resp['page']);

    	
    }

    public function add(){



        $data = $_REQUEST['data'];

        $resp = $this->categoryservice->add($data);
        #echo "<pre>" ,print_r($resp);exit;

        echo json_encode($resp); 

    }

    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->categoryservice->view($id);

        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->categoryservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->categoryservice->lists();

        //response $resp;

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->categoryservice->del($id); 

        
        echo json_encode($data); 
    }


    public function updateStatusCategory(){ #task 212

        $idcategory = $_REQUEST['id'];
        $status     = $_REQUEST['status'];
        $obs        = $_REQUEST['obs'];


        
        if($status == 1){ #status aproved

           $data = array(
            'idcategory'=> $idcategory,
            'status'    => "Aguardando Atendimento",
           );
        

        }else if($status == 2){ #status reproved

           $data = array(
            'idcategory'=> $idcategory,
            'status'    => "Reprovado",
           );

        }


        #update ticket category status
        $resp = $this->ticketservice->updatestatuscategory($data);

        $data = array(
            'id'     => $idcategory,
            'status' => $status,
            'obs'    => $obs
        );
        
        #update category
        $data = $this->categoryservice->updatestatuscategory($data); 

        
        echo json_encode($data); 

    }






}    