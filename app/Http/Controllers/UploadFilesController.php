<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\UploadFilesService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class UploadFilesController extends Controller{
    
    protected $uploadfilesservice;
    protected $utilservice;

    #dependency injection
    public function __construct(UploadFilesService $uploadFilesService , UtilService $utilservice){
        
        #change service construct
        $this->uploadfilesservice = $uploadFilesService;
        $this->utilservice        = $utilservice;
    
    }



    public function uploadfile(){
 
        $resp = $this->uploadfilesservice->uploadfile($_FILES);

        echo json_encode($resp); 

    }


    public function add(){



        $id = $_REQUEST['data'];
        $resp = $this->uploadfilesservice->add($id);

        echo json_encode($resp); 

    }

   



}    