<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020


use App\Services\SLAService;
use App\Services\CompanyService;
use App\Services\CategoryService;
use App\Services\PriorityService;
use App\Services\BurdenService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class SLAController extends Controller{
    
    protected $categoryservice;
    protected $slaservice;
    protected $companyservice;
    protected $utilservice;

    #dependency injection
    public function __construct(PriorityService $priorityservice, CategoryService $categoryservice ,SLAService $slaservice ,CompanyService $companyservice ,UtilService $utilservice){
        
        #change service construct
        $this->slaservice      = $slaservice;
        $this->categoryservice = $categoryservice;
        $this->companyservice  = $companyservice;
        $this->utilservice     = $utilservice;
        $this->priorityservice = $priorityservice;
    
    }

    public function index(){
        

 
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->slaservice->lists(),
            'company'  =>  $this->companyservice->lists(),  
            'category' =>  $this->categoryservice->listscategoryteam(),
            'priority' =>  $this->priorityservice->listpriorityburden(),
            'page'     => 'sla.sla'
        );
        

        #echo "<pre>" ,print_r($resp["category"]);exit;

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('page', $resp['page']);

    	
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->slaservice->add($data);


        echo json_encode($resp); 

    }

    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->slaservice->view($id);

        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->slaservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->slaservice->lists();

        //response $resp;

    }



    public function del(){

        $id = $_REQUEST['id'];

        
        $data=$this->slaservice->del($id); 

        
        echo json_encode($data); 
    }



}    