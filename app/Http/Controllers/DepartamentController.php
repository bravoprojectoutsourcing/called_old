<?php
 
namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\DepartamentService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use App\Services\UserService;
use View;
use Session;


class DepartamentController extends Controller{
    
    protected $departamentservice;
    protected $utilservice;
    protected $userservice;

    #dependency injection
    public function __construct(DepartamentService $departamentService, UtilService $utilservice, UserService $userservice){
        
        #change service construct
        $this->departamentservice  = $departamentService;
        $this->utilservice         = $utilservice;
        $this->userservice         = $userservice;
    
    }

    public function index(){
        

 
        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->departamentservice->lists(),
            'user'     =>  $this->userservice->lists(),
            'page'     => 'departaments.departaments'
        );
        

        return View::make('templates.default')
        ->with('resp', $resp)
        ->with('page', $resp['page']);

    	
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->departamentservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->departamentservice->edit($data);

        echo json_encode($resp); 

    }



    public function view(){

        $id = $_REQUEST['id'];

        $resp = $this->departamentservice->view($id);

        echo json_encode($resp); 

    }


    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->departamentservice->del($id); 

        
        echo json_encode($data); 
    }



}    