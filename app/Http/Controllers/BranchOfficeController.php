<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020

use App\Services\BranchOfficeService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class BranchOfficeController extends Controller{
    
    protected $branchofficeservice;
    protected $utilservice;

    #dependency injection
    public function __construct(BranchOfficeService $branchofficeService , UtilService $utilservice){
        
        #change service construct
        $this->branchofficeservice  = $branchofficeService;
        $this->utilservice     = $utilservice;
    
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->branchofficeservice->add($data);


        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->branchofficeservice->edit($data);

        echo json_encode($resp); 

    }



    public function view(){

        $id = $_REQUEST['id'];

        $resp = $this->branchofficeservice->view($id);

        echo json_encode($resp); 

    }

    #show branchoffice general
    public function listBranchOfficeCompany(){

        $id = $_REQUEST['id'];


        $resp = $this->branchofficeservice->listbranchofficecompany($id);

        echo json_encode($resp); 

    }


    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->branchofficeservice->del($id); 

        
        echo json_encode($data); 
    }


    public function listCity(){


        $id = $_REQUEST['id'];


        $resp=$this->utilservice->listCity($id);
       # echo "<pre>" ,print_r($resp);exit;
        echo json_encode($resp);    


    }

    public function cityName(){


        $name = $_REQUEST['name'];

        $resp=$this->utilservice->cityName($name);

        echo json_encode($resp);    


    }

}    