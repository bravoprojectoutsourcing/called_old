<?php

namespace App\Http\Controllers;


# Autor: André Camargo
# Date : 1º semestre 2020


use App\Services\UserDepartamentService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\UtilService;
use View;
use Session;


class UserDepartamentController extends Controller{
    
    protected $userdepartamentservice;
    protected $utilservice;
    protected $userservice;

    #dependency injection
    public function __construct(UserDepartamentService $userdepartamentservice , UtilService $utilservice, UserService $userService){
        
        #change service construct
        $this->userdepartamentservice = $userdepartamentservice;
        $this->utilservice            = $utilservice;
        $this->userservice            = $userService;
    
    }

    public function index(){

        $id = $_REQUEST['id'];


        $resp=array(
            'resp'     =>  session('resp'),
            'menus'    =>  session('menus'),
            'submenus' =>  session('submenus'),
            'list'     =>  $this->userdepartamentservice->listsUserdepartament($id),
            'user'     =>  $this->userservice->lists(),
            'page'     => 'departaments.departaments'
        );
        
        #echo "<pre>" ,print_r($resp);exit;
        

        echo json_encode($resp); 

    	
    }

    public function add(){

        #echo "ok";exit;

        $data = $_REQUEST['data'];
        $resp = $this->userdepartamentservice->add($data);


        echo json_encode($resp); 

    }

    public function view(){



        $id = $_REQUEST['id'];
        $resp = $this->userdepartamentservice->view($id);

        echo json_encode($resp); 

    }


	public function edit(){


        $data = $_REQUEST['data'];
        $resp = $this->userdepartamentservice->edit($data);

        echo json_encode($resp); 

    }



    public function lists(){

        $resp = $this->userdepartamentservice->lists();
        
        echo json_encode($resp);

    }



    public function del(){

        $id = $_REQUEST['id'];
        
        $data=$this->userdepartamentservice->del($id); 

        
        echo json_encode($data); 
    }



}    