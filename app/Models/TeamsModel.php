<?php 

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class TeamsModel extends Eloquent{
     
     /**
     * @var bool 
     */
    public $timestamps = false;

    #name table
    protected $table='team';
    

    #list team(s)
    public function listsUserTeams($iduser){

        try{

          $query= TeamsModel::select('team.id AS id','team.name AS name')
          ->join('user_team','user_team.idteam',"=","team.id")
          ->where('user_team.iduser' , '=' , $iduser)
          ->orderBy('team.name','asc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => NULL,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }


    #email team(s)
    public function emailsTeam($id){

        try{

          $query= TeamsModel::select('user.email')
          ->join('user_team','user_team.idteam',"=","team.id")
          ->join('user','user.id',"=","user_team.iduser")
          ->join('team_category','team_category.idteam',"=","team.id")
          ->where('team_category.idcategory' , '=' , $id)
          ->orderBy('team.name','asc')
          ->get();
          #->toSql();
          
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => NULL,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }

    public function getOutTeams($id){


      try{
        $query= DB::table('user_team')->select('idteam')->where('iduser','=', $id)->get();
        $times = array();
        foreach($query as $q){
          $times[] = $q->idteam;
        }
          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('team')->select('*')->whereNotIn('id',$times)->get();

         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => null,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

    public function getTeams($id){


      try{
        $query= DB::table('user_team')->select('team.id', 'team.name')
                ->join('team', 'user_team.idteam', '=', 'team.id')
                ->join('user', 'user_team.iduser', '=', 'user.id')
                ->where('user.id', '=', $id)->get();

         #echo "<pre>" ,print_r($query);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => null,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

    #list team(s)
    public function lists(){

        try{

          $query= TeamsModel::select('*')
          ->orderBy('team.name','asc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => NULL,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }

    public function edit($data){
        try{
 
            $query= TeamsModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]); 
 
            $query = array(
                 'exception' => null,
                 'query'     => $query,
                 'success'   => TRUE
            );
 
             return $query;
        }catch(\Illuminate\Database\QueryException $exception){
            $query = array(
                'exception' => $exception->errorInfo,
                'query'     => "", 
                'success'   => FALSE
            );
            return $query;
        }
    }

    public function del($id){

        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('team')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }

    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;

           $query = TeamsModel::insert($data);
           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
            );
            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
          
            $query = array(
                'exception' => $exception->errorInfo,
                'query'     => "", 
                'success'   => FALSE
            );

            return $query;

         } 


    }

    public function delUserTeam($data){

      try{
          #select with join ELOQUENT profiles menus ...
          $query= DB::table('user_team')->where('iduser', '=', $data['user'])->where('idteam', '=', $data['team'])
          ->delete();

          #alter idcompany table user
          $query=array(
          'exception' =>null,
          'query'=> $query,
          'success' => TRUE
          );

          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;

      }


  }

    public function addUserTeam($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = DB::table('user_team')->insert(
              ['iduser' => $data['user'], 'idteam' => $data['team']]
          );
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


  }



  public function teamCategory($idteam){


      try{
       
          $query= TeamsModel::select('category.id','category.name')
          ->join('team_category', 'team_category.idteam', '=', 'team.id')
          ->join('category', 'category.id', '=', 'team_category.idcategory')
          ->where('team_category.idteam', '=', $idteam)
          ->orderBy('team.name','asc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

         #echo "<pre>" ,print_r($query);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => null,
            'success'   => FALSE
          );
          return $query;
      }


      
    }


}