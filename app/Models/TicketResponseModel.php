<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class TicketResponseModel extends Eloquent{

     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='ticket_response';


    public function add($data){


        try{

            #echo "<pre>" ,print_r(session('resp')["custom"]);exit;

           $query = TicketResponseModel::insert($data);


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE,
                'lastid'    => DB::getPdo()->lastInsertId()
           );



            return $query;

         }catch(\Illuminate\Database\QueryException $exception){

                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "",
                   'success'   => FALSE
                 );

            return $query;

         }


    }


    public function edit($data){

      #echo "<pre>" ,print_r($data);
      #exit;

       try{

        $query= TicketResponseModel::where('id', '=', $data["id"])
            ->update(['phone' => $data["phone"],'email' => $data["email"],
                     'idbranchoffice' => $data["idbranchoffice"],'idcompany' => $data["idcompany"],'iddepartament' => $data["iddepartament"],
                     'idcategory' => $data["idcategory"],'idseverity' => $data["idseverity"],
                     'idpriority' => $data["idpriority"],'idtypecategory' => $data["idtypecategory"],
                     'description' => $data["description"],'message' => $data["message"]

              ]);

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "",
                   'success'   => FALSE
                 );

            return $query;
      }
    }


    #list ADMIN
    public function lists($id){
      #echo "<pre>" ,print_r($data["custom"][0]["iduser"]);exit;
        try{



          $query= TicketResponseModel::select('ticket.userresponse', 'ticket_response.id','ticket_response.iduser','ticket_response.idticket','ticket_response.message','ticket_response.created','user.name as name')
          ->join('user','user.id','=','ticket_response.iduser')
          ->leftjoin('ticket','ticket.id','=','ticket_response.idticket')
          #->leftJoin('upload' , 'upload.idticket' ,'=' ,'ticket.id')
          ->where('ticket_response.idticket','=', $id)
          ->orderBy('ticket_response.created','desc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){

            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => "",
              'success'   => FALSE
            );

            return $query;

        }

    }


    #show company user
    public function view($id){


        try{

          $query = DB::table('ticket')->select('*')->where('ticket.id','=', $id)->get();

          #echo "<pre>" ,print_r($query);exit;
            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('ticket')->select('ticket.id AS id' ,'ticket.iduser','ticket.created AS created' ,'ticket.phone','ticket.email','departament.id AS iddepartament','departament.name AS namedepartament','category.id AS idcategory','company.id AS idcompany' ,'branch_office.id AS idbranchoffice','branch_office.name AS namebranchoffice','severity.id AS idseverity','priority.id AS idpriority','type.name AS nametype','type.id AS idtype','type_category.id AS idtypecategory','ticket.description','ticket.message','team.name','team.id AS idteam')
          ->join('departament','ticket.iddepartament','=','departament.id')
          ->join('category','ticket.idcategory','=','category.id')
          ->join('company','ticket.idcompany','=','company.id')
          ->join('branch_office','ticket.idbranchoffice','=','branch_office.id') #estabelecimentos
          ->join('severity','ticket.idseverity','=','severity.id')
          ->join('priority','severity.id','=','priority.idseverity')
          ->join('type','priority.idtype','=','type.id') #type Moderado...
          ->join('type_category','ticket.idtypecategory','=','type_category.id')
          ->join('team','ticket.idteam','=','team.id')
          ->where('ticket.id','=', $id)
          ->where('priority.id','=', $query[0]->idpriority)
          ->get();
          #->toSql();

            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => null,
              'success'   => FALSE
            );
            return $query;
        }



    }


    #alter idcompany user
    private function alterusercompany($id){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> UserModel::where('iduser', '=', session('iduser'))
        ->update(['idcompany' => $id]),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $errorInfo = array('exception'=> $exception->errorInfo);
            return $errorInfo;

      }

    }


    #alter idcompany user
    public function findCompany($cnpj){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> TicketResponseModel::where('cpfcnpj', '=', $cnpj)
        ->get(),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $errorInfo = array('exception'=> $exception->errorInfo);
            return $errorInfo;

      }

    }


    public function del($id){

        #echo $id;exit;

        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('ticket_response')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
    }

    public function getLastDate($id){

        try{

          $query= TicketResponseModel::select('ticket_response.id','ticket_response.idticket','ticket_response.created')
          ->where('ticket_response.idticket','=', $id)
          ->orderBy('ticket_response.created','desc')
          ->get();
          #->toSql();
          #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){

            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => "",
              'success'   => FALSE
            );

            return $query;

        }


    }

}