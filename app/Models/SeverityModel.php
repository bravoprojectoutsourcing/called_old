<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class SeverityModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='severity';    


    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;

           $query = SeverityModel::insert($data);           


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

 

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
          
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            
            return $query;

         } 


    }


    public function edit($data){

       try{

        $query= SeverityModel::where('id', '=', $data["id"])
            ->update(['name' => $data["name"],'address' => $data["address"],
                     'cpfcnpj' => $data["cpfcnpj"],'email' => $data["email"],'cep' => $data["cep"],
                     'neighborhood' => $data["neighborhood"],'number' => $data["number"],
                     'compl' => $data["compl"],'phone' => $data["phone"],
                     'fantasy_name' => $data["fantasy_name"],'state' => $data["state"],
                     'city' => $data["city"]

              ]); 

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            


            return $query;
      }
    }

   
    #list company(s)

    public function lists(){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('severity')->select('*')->get();


           #echo "<pre>" ,print_r($query[0]);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );
         #   echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }


    public function del($id){


        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('severity')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }




}    