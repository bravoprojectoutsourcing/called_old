<?php
namespace App\Models;

# Autor: Andr� Camargo
# Date : 1� semestre 2020
use Eloquent;
use DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class TicketModel extends Eloquent {

	/**
	 *
	 * @var bool
	 */
	public $timestamps = false;

	# name table
	protected $table = 'ticket';

	public function add($data) {
		try {

			# save name column ticket show view left join no show
			$resp = DB::table('category')->select('*')
				->where('id', '=', $data["idcategory"])
				->get();

			# convert stdclass in array Andre
			$resp = json_decode(json_encode($resp), true);

			$data["namecategorynormal"] = $resp[0]["name"];

			# echo "<pre>" ,print_r($data);exit;

			$query = TicketModel::insert($data);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE,
				'lastid' => DB::getPdo()->lastInsertId()
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	public function edit($data) {

		# echo "<pre>" ,print_r($data);
		# exit;
		try {

			$query = TicketModel::where('id', '=', $data["id"])->update([
				'phone'          => $data["phone"],
				'email'          => $data["email"],
				'idbranchoffice' => $data["idbranchoffice"],
				'idcompany'      => $data["idcompany"],
				'iddepartament'  => $data["iddepartament"],
				'idcategory'     => $data["idcategory"],
				'idseverity'     => $data["idseverity"],
				'idpriority'     => $data["idpriority"],
				'idtypecategory' => $data["idtypecategory"],
				'description'    => $data["description"],
				'message'        => $data["message"]
			]);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	public function updateDataUserTicket($data) {

		# echo "<pre>" ,print_r($data);
		# exit;
		try {

			$query = TicketModel::where('id', '=', $data["id"])->update([
//				'userresponse' => $data["user"],
				'userresponse' => $data["email"],
				'createdresponse' => $data["createdresponse"]
			]);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	public function updateStatus($data) {

		# echo "<pre>" ,print_r($data);
		# exit;
		try {

			$query = TicketModel::where('id', '=', $data["id"])->update([
				'status' => $data["status"],
				'createdstatus' => $data["createdstatus"]
			]);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	public function updateStatusCategory($data) {

		# task 212
		try {

			$query = TicketModel::where('idcategory', '=', $data["idcategory"])->update([
				'status' => $data["status"]
			]);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	# list ADMIN
	public function listsAdmin($data) {
		$where = $data["where"];
		$where2 = $data["where2"];

		try {

			$sql = "select
                          `cn`.`name` AS companyname ,
                           ticket.slacalculate, -- task-351
                           ticket.slastatustime, -- task-351
                           sla.id as priorityid,
                           ticket.description,
                          `burden`.`name` as `nameburden`,
                          `burden`.`description` as `descriptionburden`,
                          `type`.`name` as `nametype`,
                          `burden_urgency`.`timehour` as TIMEMAX,
                          `burden_urgency`.`name` as `nameburdenurgency`,
                          `burden_urgency`.`tolerancehour` ,
                          `urgency`.`name`,
                          `priority`.`idseverity`,
                          `priority`.`idtype`,
                          `priority`.`idburden`,
                          `ticket`.`userresponse`,
                          `ticket`.`createdresponse`,
                           -- TIMESTAMPDIFF(day,`ticket`.`created`,now()) as totaldays,
                           -- TOTAL_WEEKDAYS(`ticket`.`created`, now()) utildays, TOTAL_WEEKDAYS(`ticket`.`created`, now())*8 utilhour ,
                           -- (TIMEDIFF(now(),`ticket`.`created` )) as HOURDIFF,
                          `ticket`.`id` as `id`, `ticket`.`iduser`,
                          `ticket`.`status`, `ticket`.`created` as `created`,
                          `ticket`.`phone`, `ticket`.`email`,
                          `departament`.`name` as `namedepartament`,
                          `ticket`.`namecategorynormal` ,
                          `ticket`.`idcategory`,
                          `profile`.`profile` as `nameprofile`,
                          `profile`.`nameprofile` as `profilename`,
                          `profile`.`id` as `idprofile`,
                          `team`.`id` as `idteam`,
                          `team`.`name` as `nameteam`,
                          `sla`.`id` as `idsla`, `sla`.`idcompany`,
                          `sla`.`idpriority`, `sla`.`time`,
                          '' AS STATUSTIME


                          from `team_category`
                          left join `ticket` on `ticket`.`idcategory` = `team_category`.`idcategory`
                          -- inner join user_team ut on ut.idteam = ticket.idteam
                          inner join `departament` on `ticket`.`iddepartament` = `departament`.`id`
                          inner join `category` on `ticket`.`idcategory` = `category`.`id`
                          left join `user` on `user`.`id` = `ticket`.`iduser`
                          left join `profile` on `profile`.`id` = `user`.`idprofile`" . $where2 . " left join `sla` sla on `sla`.`idcompany` = `ticket`.`idcompany`
                          AND sla.idpriority = ticket.idpriority  AND sla.idcategory =ticket.idcategory
                          -- left join `sla` slap on `slap`.`idpriority` = `ticket`.`idpriority`
                          -- left join `sla` slac on `slac`.`idcategory` = `ticket`.`idcategory`
                          left join `priority` on  `ticket`.`idpriority` = `priority`.`id`
                          -- left join `company` ON `company`.`id` = `sla`.`idcompany`
                          left join `company` cn ON `cn`.`id` = `ticket`.`idcompany`
                          left join `burden` on `burden`.`id` = `priority`.`idburden`
                          left join `burden_urgency` on `burden_urgency`.`id` = `priority`.`idburdenurgency`
                          left join `type` on `type`.`id` = `priority`.`idtype`
                          left join `urgency` on `urgency`.`id` = `burden_urgency`.`idurgency` " . $where;

			# echo $sql;exit;

			$query = DB::select($sql);

			# convert stdclass in array Andre
			$query = json_decode(json_encode($query), true);

			$query = array(
				'exception' => null,
				'success' => TRUE,
				'query' => $query,
				'count' => count($query) # number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	# list manager(GESTOR)
	public function listsManager($data) {
		# echo "j";exit;
		try {

			# #######################------------------------MTO CUIDADO AO MEXER AQUI FAÇA UMA CÓPIA --------------------------------------------------##################

			# STATUS - FINISH = TICKET FINISH
			# STATUS - OK = TICKET TIME OK
			# STATUS - ATTENTION = TICKET TIME REST WARNING!!!
			# STATUS - CRITICAL = TICKET HELLPPPPPPP!!!!

			$where = $data["where"];
			$where2 = $data["where2"];

			$sql = "select
                          `cn`.`name` AS companyname ,
                           ticket.slacalculate, -- task-351
                           ticket.slastatustime, -- task-351
                           sla.id as priorityid,
                           ticket.description,
                          `burden`.`name` as `nameburden`,
                          `burden`.`description` as `descriptionburden`,
                          `type`.`name` as `nametype`,
                          `burden_urgency`.`timehour` as TIMEMAX,
                          `burden_urgency`.`name` as `nameburdenurgency`,
                          `burden_urgency`.`tolerancehour` ,
                          `urgency`.`name`,
                          `priority`.`idseverity`,
                          `priority`.`idtype`,
                          `priority`.`idburden`,
                          `ticket`.`userresponse`,
                          `ticket`.`createdresponse`,
                           -- TIMESTAMPDIFF(day,`ticket`.`created`,now()) as totaldays,
                           -- TOTAL_WEEKDAYS(`ticket`.`created`, now()) utildays, TOTAL_WEEKDAYS(`ticket`.`created`, now())*8 utilhour ,
                           -- (TIMEDIFF(now(),`ticket`.`created` )) as HOURDIFF,
                          `ticket`.`id` as `id`, `ticket`.`iduser`,
                          `ticket`.`status`, `ticket`.`created` as `created`,
                          `ticket`.`phone`, `ticket`.`email`,
                          `departament`.`name` as `namedepartament`,
                          `ticket`.`namecategorynormal` ,
                          `ticket`.`idcategory`,
                          `profile`.`profile` as `nameprofile`,
                          `profile`.`nameprofile` as `profilename`,
                          `profile`.`id` as `idprofile`,
                          `team`.`id` as `idteam`,
                          `team`.`name` as `nameteam`,
                          `sla`.`id` as `idsla`, `sla`.`idcompany`,
                          `sla`.`idpriority`, `sla`.`time`,
                          '' AS STATUSTIME

                          from `team_category`
                          left join `ticket` on `ticket`.`idcategory` = `team_category`.`idcategory`
                          -- inner join user_team ut on ut.idteam = ticket.idteam
                          inner join `departament` on `ticket`.`iddepartament` = `departament`.`id`
                          inner join `category` on `ticket`.`idcategory` = `category`.`id`
                          left join `user` on `user`.`id` = `ticket`.`iduser`
                          left join `profile` on `profile`.`id` = `user`.`idprofile`" . $where2 . " left join `sla` sla on `sla`.`idcompany` = `ticket`.`idcompany`
                          AND sla.idpriority = ticket.idpriority  AND sla.idcategory =ticket.idcategory
                          -- left join `sla` slap on `slap`.`idpriority` = `ticket`.`idpriority`
                          -- left join `sla` slac on `slac`.`idcategory` = `ticket`.`idcategory`
                          left join `priority` on  `ticket`.`idpriority` = `priority`.`id`
                          -- left join `company` ON `company`.`id` = `sla`.`idcompany`
                          left join `company` cn ON `cn`.`id` = `ticket`.`idcompany`
                          left join `burden` on `burden`.`id` = `priority`.`idburden`
                          left join `burden_urgency` on `burden_urgency`.`id` = `priority`.`idburdenurgency`
                          left join `type` on `type`.`id` = `priority`.`idtype`
                          left join `urgency` on `urgency`.`id` = `burden_urgency`.`idurgency` " . $where;

			# echo $sql; exit;

			$query = DB::select($sql);

			# clear session graph
			session([
				'sessionidteambox' => ""
			]);

			# convert stdclass in array Andre
			$query = json_decode(json_encode($query), true);

			# echo "<pre>" ,print_r($query);exit;

			$query = array(
				'exception' => null,
				'success' => TRUE,
				'query' => $query,
				'count' => count($query) # number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);
			# echo "<pre>" ,print_r($query);exit;
			return $query;
		}
	}

	# list Others
	public function listsOthers($data, $where, $where2) {
		# echo "<pre>" ,print_r($data["custom"][0]["iduser"]);exit;
		try {

			$sql = "select
                          `cn`.`name` AS companyname ,
                           ticket.slacalculate, -- task-351
                           ticket.slastatustime, -- task-351
                           sla.id as priorityid,
                           ticket.description,
                          `burden`.`name` as `nameburden`,
                          `burden`.`description` as `descriptionburden`,
                          `type`.`name` as `nametype`,
                          `burden_urgency`.`timehour` as TIMEMAX,
                          `burden_urgency`.`name` as `nameburdenurgency`,
                          `burden_urgency`.`tolerancehour` ,
                          `urgency`.`name`,
                          `priority`.`idseverity`,
                          `priority`.`idtype`,
                          `priority`.`idburden`,
                          `ticket`.`userresponse`,
                          `ticket`.`createdresponse`,
                           -- TIMESTAMPDIFF(day,`ticket`.`created`,now()) as totaldays,
                           -- TOTAL_WEEKDAYS(`ticket`.`created`, now()) utildays, TOTAL_WEEKDAYS(`ticket`.`created`, now())*8 utilhour
                           -- ,(TIMEDIFF(now(),`ticket`.`created` )) as HOURDIFF,
                          `ticket`.`id` as `id`, `ticket`.`iduser`,
                          `ticket`.`status`, `ticket`.`created` as `created`,
                          `ticket`.`phone`, `ticket`.`email`,
                          `departament`.`name` as `namedepartament`,
                          `ticket`.`namecategorynormal` ,
                          `ticket`.`idcategory`,
                          `profile`.`profile` as `nameprofile`,
                          `profile`.`nameprofile` as `profilename`,
                          `profile`.`id` as `idprofile`,
                          `team`.`id` as `idteam`,
                          `team`.`name` as `nameteam`,
                          `sla`.`id` as `idsla`, `sla`.`idcompany`,
                          `sla`.`idpriority`,
                          `sla`.`time`,
                          '' AS STATUSTIME



                          from `team_category`
                          left join `ticket` on `ticket`.`idcategory` = `team_category`.`idcategory`
                          -- inner join user_team ut on ut.idteam = ticket.idteam
                          inner join `departament` on `ticket`.`iddepartament` = `departament`.`id`
                          inner join `category` on `ticket`.`idcategory` = `category`.`id`
                          left join `user` on `user`.`id` = `ticket`.`iduser`
                          left join `profile` on `profile`.`id` = `user`.`idprofile`" . $where2 . "
                          left join `sla` sla on `sla`.`idcompany` = `ticket`.`idcompany`
                          AND sla.idpriority = ticket.idpriority  AND sla.idcategory =ticket.idcategory
                          -- left join `sla` slap on `slap`.`idpriority` = `ticket`.`idpriority`
                          -- left join `sla` slac on `slac`.`idcategory` = `ticket`.`idcategory`
                          left join `priority` on  `ticket`.`idpriority` = `priority`.`id`
                          -- left join `company` ON `company`.`id` = `sla`.`idcompany`
                          left join `company` cn ON `cn`.`id` = `ticket`.`idcompany`
                          left join `burden` on `burden`.`id` = `priority`.`idburden`
                          left join `burden_urgency` on `burden_urgency`.`id` = `priority`.`idburdenurgency`
                          left join `type` on `type`.`id` = `priority`.`idtype`
                          left join `urgency` on `urgency`.`id` = `burden_urgency`.`idurgency` " . $where . " group by `ticket`.`id` order by `ticket`.`id` desc;";

			# echo $sql;exit;

			$query = DB::select($sql);

			# clear session graph
			session([
				'sessionidteambox' => ""
			]);

			# convert stdclass in array Andre
			$query = json_decode(json_encode($query), true);

			# echo "<pre>" ,print_r($query);exit;

			$query = array(
				'exception' => null,
				'success' => TRUE,
				'query' => $query,
				'count' => count($query) # number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			# echo "<pre>" ,print_r($query);exit;

			return $query;
		}
	}

	public function listStatus() {
		try {
			$query = DB::select("SELECT DISTINCT(`status`) AS id, `status` AS name, createdstatus AS datastatus FROM ticket WHERE `status` is not null order by `status` asc");

			$query = array(
				'exception' => null,
				'success' => true,
				'query' => $query,
				'count' => count($query) # number reg
			);

			# echo "<pre>" ,print_r($query);exit;
			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => null,
				'success' => false
			);
			return $query;
		}
	}

	public function cronStatusTicket() {
		try {
			$query = DB::select("SELECT id, `status` AS name, createdstatus AS datestatus FROM ticket WHERE status ='Concluido' ");

			$query = array(
				'exception' => null,
				'success' => true,
				'query' => $query,
				'count' => count($query) # number reg
			);

			# echo "<pre>" ,print_r($query);exit;
			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => null,
				'success' => false
			);
			return $query;
		}
	}

	public function liststicket($id) {
		try {

			$query = TicketModel::select('ticket.userresponse', 'ticket.id', 'ticket.iduser', 'ticket.message', 'ticket.created', 'user.name as name', 'ticket.idcompany', 'ticket.idcategory')->join('user', 'user.id', '=', 'ticket.iduser')
				->where('ticket.id', '=', $id)
				->orderBy('ticket.created', 'desc')
				->get();
			# ->toSql();
			# echo "<pre>",print_r($query);exit;

			$query = array(
				'exception' => null,
				'success' => TRUE,
				'query' => $query,
				'count' => $query->count() # number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$query = array(
				'exception' => $exception->errorInfo,
				'query' => "",
				'success' => FALSE
			);

			return $query;
		}
	}

	# show company user
	public function view($id) {
		try {

			$sql = "SELECT ticket.id AS id ,ticket.iduser,ticket.created AS created ,ticket.phone,ticket.email,departament.id AS iddepartament,departament.name AS namedepartament,category.id AS idcategory,company.id AS idcompany,company.name AS companyname ,branch_office.id AS idbranchoffice,branch_office.fantasy_name AS namebranchoffice,severity.id AS idseverity,priority.id AS idpriority,type.name AS nametype,type.id AS idtype,type_category.id AS idtypecategory,ticket.description,ticket.message,ticket.status,ticket.createdresponse,team.name,team.id AS idteam
            from `ticket`
            left join `branch_office` on `ticket`.`idbranchoffice` = `branch_office`.`id`
            left join `departament` on `ticket`.`iddepartament` = `departament`.`id`
            left join `category` on `ticket`.`idcategory` = `category`.`id`
            left join `company` on `ticket`.`idcompany` = `company`.`id`
            left join `user` on `user`.`id` = `ticket`.`iduser`
            left join `profile` on `profile`.`id` = `user`.`idprofile`
            left join `team` on `team`.`id` = `ticket`.`idteam`
            left join `sla` s on `s`.`idpriority` = `ticket`.`idpriority`
            left join `sla` sla on `sla`.`idcategory` = `ticket`.`idcategory`
            left join `priority` on `ticket`.`idpriority` = `priority`.`id`
            left join `severity` on `ticket`.`idseverity` = `severity`.`id`
            left join `burden` on `burden`.`id` = `priority`.`idburden`
            left join `burden_urgency` on `burden_urgency`.`id` = `priority`.`idburdenurgency`
            left join `type` on `type`.`id` = `priority`.`idtype`
            left join `type_category` on `idtypecategory` = `type_category`.`id`
            left join `urgency` on `urgency`.`id` = `burden_urgency`.`idurgency`
            WHERE  `ticket`.`id` = $id
            group by `ticket`.`id` order by `ticket`.`id` asc";

			$query = DB::select($sql);

			# convert stdclass in array Andre
			$query = json_decode(json_encode($query), true);

			# echo "<pre>" ,print_r($query);exit;
			$query = array(
				'exception' => null,
				'success' => TRUE,
				'query' => $query,
				'count' => count($query) # number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => null,
				'success' => FALSE
			);
			# echo "<pre>" ,print_r($query);exit;
			return $query;
		}
	}

	# alter idcompany user
	private function alterusercompany($id) {
		try {

			# alter idcompany table user
			$query = array(
				'exception' => null,
				'query' => UserModel::where('iduser', '=', session('iduser'))->update([
					'idcompany' => $id
				]),
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$errorInfo = array(
				'exception' => $exception->errorInfo
			);
			return $errorInfo;
		}
	}

	# alter idcompany user
	public function findCompany($cnpj) {
		try {

			# alter idcompany table user
			$query = array(
				'exception' => null,
				'query' => TicketModel::where('cpfcnpj', '=', $cnpj)->get(),
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {

			$errorInfo = array(
				'exception' => $exception->errorInfo
			);
			return $errorInfo;
		}
	}

	public function del($id) {

		# echo $id;exit;
		try {
			# select with join ELOQUENT profiles menus ...
			$query = DB::table('ticket')->where('id', '=', $id)->delete();

			# alter idcompany table user
			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => $query,
				'success' => FALSE
			);
			return $query;
		}
	}

	# alter idcompany user
	public function markTicketCalculate($data) {
		try {

			$query = TicketModel::where('id', '=', $data["id"])->update([
				'slacalculate' => $data["slacalculate"],
				'slastatustime' => $data["slastatustime"]
			]);

			$query = array(
				'exception' => null,
				'query' => $query,
				'success' => TRUE
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query = array(
				'exception' => $exception->errorInfo,
				'query' => $query,
				'success' => FALSE
			);
			return $query;
		}
	}
}