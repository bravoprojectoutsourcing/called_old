<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class SLAModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='sla';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = SLAModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }


    public function edit($data){
      try{

          $query= SLAModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('sla')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

    public function find($data){

      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('sla')->select('*')
         ->where('idcategory','=', $data["idcategory"])
         ->where('idcompany','=', $data["idcompany"])
         ->where('idpriority','=', $data["idpriority"])
         ->get();

       #echo "<pre>" ,print_r($query);exit;

         
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
        


          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );

         #echo "<pre>" ,print_r($query);exit;

          return $query;
      }


      
    }

   
    #list company(s)

    public function lists(){

      try{

        #TASK 245 Andre 27/07/2020
        if(session('resp')['custom'][0]['nameprofile'] == "MANAGER")
          $WHERE =  "WHERE tc.idteam =" . session('resp')['custom'][0]['teamid'];
        else
          $WHERE =  "";


            $sql = "select s.id as idsla, s.idcompany,p.idseverity,p.idtype,p.idburden,s.idpriority,s.time,
                    b.name as burdenname,b.description as descriptionburden,
                    t.name as typename ,bu.name as burdenurgencyname , 
                    u.name AS urgencyname,c.name AS companyname ,
                    sv.name severityname,
                    ct.name as categoryname , ct.id as idcategory
                    from sla s 
                    inner join category ct ON ct.id = s.idcategory
                    inner join company c ON c.id = s.idcompany
                    inner join priority p ON s.idpriority = p.id
                    inner join severity sv ON sv.id = p.idseverity
                    inner join burden b ON b.id = p.idburden
                    inner join burden_urgency bu ON bu.id = p.idburdenurgency
                    inner join type t ON t.id = p.idtype
                    inner join urgency u ON u.id = bu.idurgency
                    inner join team_category tc ON tc.idcategory = s.idcategory
                    $WHERE
                    group by idsla;";

     #echo "<pre>",print_r($sql);exit;     
        $query = DB::select($sql);


        #convert stdclass in array  Andre 
        $query  =  json_decode(json_encode($query), true);       
  

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => count($query), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

    
  public function del($id){

    try{
        #select with join ELOQUENT profiles menus ...
        $query= DB::table('sla')->where('id', '=', $id)
        ->delete();

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> $query,
        'success' => TRUE
        );

        return $query;

    }catch(\Illuminate\Database\QueryException $exception){
        $query=array(
          'exception' =>$exception->errorInfo,
          'query'     => $query,
          'success'   => FALSE
        );
        return $query;

    }


  }




}    