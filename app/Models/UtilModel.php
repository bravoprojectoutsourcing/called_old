<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;

class UtilModel extends Eloquent{

   /**
     * @var bool
     */


	public  function listState(){

		
		$query=DB::table('state')->select('*')->get(); // Query Builder
		return $query;
	}

	public function listCity($id){

		$query=DB::table('city')->where('idstate','=',$id)->get();
		return $query;
	}


	public function listCityState($state){



		$query=DB::table('state')->where('alias','=',$state)->get();
		$resp = json_decode(json_encode($query),true); #convert stdclass for array
		


		$idstate = $resp[0]["idstate"];

		return $idstate;
	}

	public function city($id){

		$query=DB::table('city')->where('idcity','=',$id)->get();
		return $query;	
	}


	public function cityName($name){

		#echo "<pre>" ,print_r($name);exit;

		$query=DB::table('city')->where('name','=',$name)->get();
		return $query;	
	}

}