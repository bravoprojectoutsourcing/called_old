<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class BranchOfficeModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='branch_office';    


    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;
           $data["created"] = date('Y-m-d H:m:s');
           $query = BranchOfficeModel::insert($data);           


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

 

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
          
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            
            return $query;

         } 


    }


    public function edit($data){

       try{

        $query= BranchOfficeModel::where('id', '=', $data["id"])
            ->update(['name' => $data["name"],'address' => $data["address"],
                     'cpfcnpj' => $data["cpfcnpj"],'email' => $data["email"],'cep' => $data["cep"],
                     'neighborhood' => $data["neighborhood"],'number' => $data["number"],
                     'compl' => $data["compl"],'phone' => $data["phone"],
                     'fantasy_name' => $data["fantasy_name"],'state' => $data["state"],
                     'city' => $data["city"]

              ]); 

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            


            return $query;
      }
    }

	public function listBranchOffices($arrIdCompany)
	{
		$arrId = array_map('trim', explode(',', $arrIdCompany));
		try {
			$query = BranchOfficeModel::select(DB::raw("branch_office.id, CONCAT(IFNULL(branch_office.fantasy_name, 'SEM ESTABELECIMENTO'), ' (', UPPER(company.name), ') ') AS fantasy_name"))
				->join('company', 'branch_office.idcompany', '=', 'company.id')
				->whereIn('branch_office.idcompany', $arrId)
				->get();
				
			$query = array(
				'exception' => null,
				'success'   => true,
				'query'     => $query,
				'count'     => $query->count(), #number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query=array(
			  'exception' =>$exception->errorInfo,
			  'query'     => null,
			  'success'   => false
			);
			return $query;
		}
	}
   
    #list company(s)
    public function listBranchOfficeCompany($id){
      #echo $id;exit;

        try{

          $query= BranchOfficeModel::select('*')
          ->where('idcompany','=',$id)
          ->get();
          #->toSql();
          #echo print_r($query);
          #exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }

    #show company user 
    public function view($id){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('branch_office')->select('*')->where('id','=', $id)->get();


           #echo "<pre>" ,print_r($query[0]);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );
         #   echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }


    #alter idcompany user
    private function alterusercompany($id){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> UserModel::where('iduser', '=', session('iduser'))
        ->update(['idcompany' => $id]),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $errorInfo = array('exception'=> $exception->errorInfo);
            return $errorInfo;

      }

    }


   #alter idcompany user
    public function findBranchOffice($cnpj){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> BranchOfficeModel::where('cpfcnpj', '=', $cnpj)
        ->get(),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => "",
              'success'   => FALSE
            );

            return $query;

      }

    }


    public function del($id){


        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('branch_office')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }




}    