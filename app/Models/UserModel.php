<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class UserModel extends Eloquent{
     
     /**
     * @var bool 
     */
    public $timestamps = false;

    #name table
    protected $table='user';

    #list team(s)
    public function lists(){

        try{

          $query= UserModel::select('*')
          ->orderBy('user.name','asc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => NULL,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }


public function editview($data){
  
      try{

          $query= UserModel::where('id', '=', $data["id"])->update(['email' => $data["email"],'phone' => $data["phone"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

}