<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class UploadFilesModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='upload';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = UploadFilesModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }




    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('upload')->select('*')->where('idticket','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

     

}    