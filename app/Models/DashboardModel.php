<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;
use statement;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class DashboardModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;
   
    #task 306
    public function menuboxteam($data){


      try{

       if($data['nameprofile']=="ADMIN")
              $sql = "SELECT count(*) AS total,te.name,te.id AS idteam FROM ticket t 
            left JOIN team_category tc ON t.idcategory = tc.idcategory  
            left join `team` te on tc.idteam = te.id
            WHERE t.status = '" . $data['status'] . "' AND te.name is NOT NULL  group by te.name;";  
        
       



       if($data['nameprofile']=="MANAGER" || $data['nameprofile']=="CLERK"){ 

            $sql = "SELECT count(*) AS total,te.name,te.id AS idteam FROM ticket t
             left JOIN team_category tc ON t.idcategory = tc.idcategory  
             left join `team` te on tc.idteam = te.id
             WHERE tc.idteam =  " . $data['teams'] .  " AND t.status = '" . $data['status'] . "'  group by te.name";  
        
         }   


       if($data['nameprofile']=="REQUESTER")
            $sql = "SELECT count(*) AS total,te.name,te.id AS idteam FROM ticket t
             left JOIN team_category tc ON t.idcategory = tc.idcategory  
             left join `team` te on tc.idteam = te.id
             WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = '" . $data['status'] . "'  group by te.name";  
        
               # echo "<pre>" ,print_r($sql);exit;

        $query = DB::select($sql);

        $query = json_decode(json_encode($query),true); #convert stdclass for array


        #$query["total"]["0"][$count];

        if(isset($query[0]["total"])){

          $query=array(
                    'exception' => null,
                    'success'   => TRUE,
                    'query'     => $query,
                    'count'     => $query[0]["total"], #number reg
                    
          );

        }else{


            $query=array(
                      'exception' => null,
                      'success'   => TRUE,
                      'query'     => $query,
                      'count'     => 0, #number reg
                      
                    );



        }

        
         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        



    }
   
    #list ADMIN

    public function ticketAdmin($status){

      try{


       if($status=="closed")
          $sql = "SELECT  count(*) AS total FROM ticket t WHERE  t.status = 'Finalizado' ";  
       else if($status=="open1")
          $sql = "SELECT  count(*) AS total FROM ticket t WHERE  t.status = 'Em Andamento'";  
       else if($status=="open2")
          $sql = "SELECT  count(*) AS total FROM ticket t WHERE  t.status = 'Aguardando Atendimento'";  
       else if($status=="cancel")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status  = 'Cancelado'";  
       else if($status=="wait")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status  = 'Aguardando Solicitante'";  
       else if($status=="concluded")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status  = 'Concluido'";  
       else if($status=="disapproved")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status  = 'Reprovado'";
       else if($status=="onapproval")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status  = 'Aguardando Aprovação'";           

 
        $query = DB::select($sql);

        $query = json_decode(json_encode($query),true); #convert stdclass for array
       #echo "<pre>" ,print_r($query);

        #$query["total"]["0"][$count];


        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query[0]["total"], #number reg
          
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }


    #SOLICITANTE
    public function ticketRequester($status){



      try{
       
      if($status=="closed")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Finalizado'";  
       else if($status=="open1")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Em Andamento'";  
       else if($status=="open2")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Aguardando Atendimento'";  
       else if($status=="cancel")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Cancelado'";  
       else if($status=="wait")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Aguardando Solicitante'";  
       else if($status=="concluded")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Concluido'"; 
       #task-324    
       else if($status=="disapproved")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Reprovado'";  
       else if($status=="onapproval")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.iduser =  " . session('resp')["custom"][0]["iduser"] .  " AND t.status = 'Aguardando Aprovação'";          


        $query = DB::select($sql);

        $query = json_decode(json_encode($query),true); #convert stdclass for array
       #echo "<pre>" ,print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query[0]["total"], #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }


    #SOLICITANTE
    public function ticketManagerCustomer($data){

      $status   = $data["status"];
      $companys = $data["companys"];

      try{
       
      if($status=="closed")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Finalizado' AND t.idcompany IN($companys)";  
       else if($status=="open1")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Em Andamento' AND t.idcompany IN($companys)";  
       else if($status=="open2")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Aguardando Atendimento' AND t.idcompany IN($companys)";  
       else if($status=="cancel")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Cancelado' AND t.idcompany IN($companys)";  
       else if($status=="wait")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Aguardando Solicitante' AND t.idcompany IN($companys)";  
       else if($status=="concluded")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Concluido' AND t.idcompany IN($companys)"; 
       #task-324    
       else if($status=="disapproved")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Reprovado' AND t.idcompany IN($companys)";  
       else if($status=="onapproval")
          $sql = "SELECT count(*) AS total FROM ticket t WHERE  t.status = 'Aguardando Aprovação' AND t.idcompany IN($companys)";          

  
        $query = DB::select($sql);

        $query = json_decode(json_encode($query),true); #convert stdclass for array
       #echo "<pre>" ,print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query[0]["total"], #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }


    #GESTOR/ATENDENTE
    public function ticketManagerClark($status){

      try{


              $where="";
  
           
               if($status=="closed"){

                 $where ="'Finalizado'";

               }else if($status=="open1"){

                 $where = "'Em Andamento'";

               }else if($status=="open2"){

                 $where = "'Aguardando Atendimento'";

               }else if($status=="cancel"){

                 $where = "'Cancelado'";         

               }else if($status=="wait"){

                 $where = "'Aguardando Solicitante'";         

               }else if($status=="concluded"){

                 $where = "'Concluido'";         

               }else if($status=="disapproved"){

                 $where = "'Reprovado'";         
               }else if($status=="onapproval"){

                 $where = "'Aguardando Aprovação'";         
               }  


                        #echo "$where";exit;


                if($status=="closed")
                  $WHERE = "group by status";
                else
                  $WHERE = "group by m,status,monthnumber,ticket.id";
                

                $sql = "SELECT  month(ticket.created) AS monthnumber  ,
                     monthname(ticket.created) AS  m ,
                     count(*)  total,status , `profile`.`id`
                     FROM `ticket` 
                     inner join `team_category` on `team_category`.`idcategory` = `ticket`.`idcategory` 
                     left join `user` on `user`.`id` = `ticket`.`iduser` 
                     left join `profile` on `profile`.`id` = `user`.`idprofile`
                     #inner join user_team ut on ut.idteam = ticket.idteam 
                     left join `team` on `team`.`id` = `ticket`.`idteam` 
                     WHERE ticket.status in($where) AND `team_category`.`idteam` In(".session('resp')["custom"][0]["teamid"].")
                     $WHERE
                     order by m desc";



                    $query = DB::select($sql);

                    #echo "<pre>" ,print_r($sql);exit;

                    $query = json_decode(json_encode($query),true); #convert stdclass for array

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => count($query), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE,
            'count'     => NULL
          );
          
          return $query;

      }        

  }


    public function graph(){

      try{

        $sql   = "SELECT month(created) AS monthnumber  , monthname(created) AS  m ,count(*)  total,status ,iduser 
           FROM called.ticket
           group by m,status,m  order by m desc";
        
          # echo $sql;exit;

        $query = DB::select($sql);

       #echo "<pre>" ,print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => count($query), #number reg
        );



         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

      

  }

    public function graphRequester(){
     #solicitante
      try{

        $sql   = "SELECT month(created) AS monthnumber  , monthname(created) AS  m ,count(*)  total,status ,iduser 
           FROM called.ticket
           WHERE iduser = " . session('resp')["custom"][0]["iduser"] . 
           " group by status,m order by m desc";
           #" group by m,status,monthnumber,ticket.id  order by m desc";
          
          # echo $sql;exit;

        $query = DB::select($sql);

       #echo "<pre>" ,print_r($sql);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => count($query), #number reg
        );



         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  

     

  }


    public function graphManagerCustomer($companys){
     #solicitante
      try{

        $sql   = "SELECT month(created) AS monthnumber  , monthname(created) AS  m ,count(*)  total,status ,iduser 
           FROM called.ticket
           WHERE  idcompany IN($companys) group by status,m order by m desc";
           #" group by m,status,monthnumber,ticket.id  order by m desc";
          
          # echo $sql;exit;

        $query = DB::select($sql);

       #echo "<pre>" ,print_r($sql);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => count($query), #number reg
        );



         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  

     

  }

      public function graphManagerClark($data){

          try{



                $sql = "SELECT  month(ticket.created) AS monthnumber  ,
                     monthname(ticket.created) AS  m ,
                     count(*)  total,status
                     FROM `ticket` 
                     inner join `team_category` on `team_category`.`idcategory` = `ticket`.`idcategory` 
                     #inner join user_team ut on ut.idteam = ticket.idteam
                     left join `team` on `team`.`id` = `ticket`.`idteam` 
                     WHERE `team_category`.`idteam` In(".$data["custom"][0]["teamid"].")
                     group by status,m order by m desc";


                    $query = DB::select($sql);

                    #echo "<pre>" ,print_r($sql);exit;
          
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => count($query), #number reg
            );



             return $query;

           }catch(\Illuminate\Database\QueryException $exception){
              
              $query=array(
                'exception' =>$exception->errorInfo,
                'query'     => NULL,
                'success'   => FALSE
              );
              
              return $query;

          }        

      }


}    