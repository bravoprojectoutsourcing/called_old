<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PriorityModel extends Eloquent{
     #mmmm
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='priority';    


    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;

           $query = priorityModel::insert($data);           


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

 

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
          
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            
            return $query;

         } 


    }


    public function edit($data){

       try{

        $query= priorityModel::where('id', '=', $data["id"])
            ->update(['name' => $data["name"],'address' => $data["address"],
                     'cpfcnpj' => $data["cpfcnpj"],'email' => $data["email"],'cep' => $data["cep"],
                     'neighborhood' => $data["neighborhood"],'number' => $data["number"],
                     'compl' => $data["compl"],'phone' => $data["phone"],
                     'fantasy_name' => $data["fantasy_name"],'state' => $data["state"],
                     'city' => $data["city"]

              ]); 

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            


            return $query;
      }
    }

   
    #list company(s)

    public function lists(){
      

        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('priority')->select('*')->get();


           #echo "<pre>" ,print_r($query[0]);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );
         #   echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

	public function listPriorities($arrIdSeverities)
	{
		$arrId = array_map('trim', explode(',', $arrIdSeverities));
		try {
			$query = DB::table('priority')->select(DB::raw("priority.id AS idpriority, CONCAT(severity.name, ' :: ', type.name, ' [', burden.name, ']') AS nametype"))
				->join('burden', 'priority.idburden', '=', 'burden.id')
				->join('severity', 'severity.id', '=', 'priority.idseverity')
				->join('type', 'priority.idtype', '=', 'type.id')
				->whereIn('severity.id', $arrId)
				->orderBy('severity.id', 'type.id')
				->get();
				
			$query = array(
				'exception' => null,
				'success'   => true,
				'query'     => $query,
				'count'     => $query->count(), #number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query=array(
			  'exception' =>$exception->errorInfo,
			  'query'     => null,
			  'success'   => false
			);
			return $query;
		}
	}

    public function listPrioritySeverity($id){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('priority')->select('priority.id AS idpriority' , 'severity.id AS idseverity' , 'burden.name AS nameburden' , 'severity.name AS nameseverity' , 'type.name AS nametype', 'type.id AS idtype')
           ->join('burden','priority.idburden','=','burden.id')
           ->join('severity','severity.id','=','priority.idseverity')
           ->join('type','priority.idtype','=','type.id')
           ->where('severity.id','=',$id)
           ->groupBy('type.id')
           ->get();



           #echo "<pre>" ,print_r($query);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );
            #echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

    public function listPriorityBurden(){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('priority')->select('priority.id AS idpriority' , 'severity.id AS idseverity' , 'burden.name AS nameburden' , 'severity.name AS nameseverity' , 'type.name AS nametype', 'type.id AS idtype','burden_urgency.name as tolerancy' )
           ->join('burden','priority.idburden','=','burden.id')
           ->join('burden_urgency','burden_urgency.id','=','priority.idburdenurgency')
           ->join('severity','severity.id','=','priority.idseverity')
           ->join('type','priority.idtype','=','type.id')
           ->orderby('burden.id')
           ->get();
           #->toSql();
          #convert stdclass in array  Andre 
         # $query  =  json_decode(json_encode($query), true);

           #echo "<pre>" ,print_r($query);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => ($query), #number reg
            );
            #echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

    public function del($id){


        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('priority')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }




}    