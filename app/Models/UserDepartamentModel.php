<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class UserDepartamentModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='user_departament';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = UserDepartamentModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }


    public function edit($data){
      try{

          $query= UserDepartamentModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('UserDepartament')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }

     
    }

   
    #list company(s)

    public function lists(){

      try{

        $query= UserDepartamentModel::select('*')
        ->orderBy('UserDepartament.name','asc')
        ->get();
        #->toSql();
       #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

    #list company(s)

    public function listsUserDepartament($iduser){

      try{

        $query= UserDepartamentModel::select('user_departament.id','user_departament.iddepartament','user_departament.iduser','user.name as username','departament.name as namedepartament')
        ->join('departament','departament.id','=','user_departament.iddepartament')
        ->join('user','user.id','=','user_departament.iduser')
        ->where('user_departament.iduser' ,'=', $iduser )
        ->groupBy('departament.name')
        ->orderBy('departament.name','asc')
        ->get();
       # ->toSql();
      #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){

          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );

        #echo "<pre>",print_r($query);exit;
          
          return $query;

      }        

  }

  public function del($id){

    try{
        #select with join ELOQUENT profiles menus ...
        $query= DB::table('user_departament')->where('id', '=', $id)
        ->delete();

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> $query,
        'success' => TRUE
        );

        return $query;

    }catch(\Illuminate\Database\QueryException $exception){
        $query=array(
          'exception' =>$exception->errorInfo,
          'query'     => $query,
          'success'   => FALSE
        );
        return $query;

    }


  }




}    