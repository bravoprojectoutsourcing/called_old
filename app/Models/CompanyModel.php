<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class CompanyModel extends Eloquent{
     
     /**
     * @var bool 
     */
    public $timestamps = false;

    #name table
    protected $table='company';    


    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;

           $query = CompanyModel::insert($data);           


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

 

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
          
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );

            return $query;

         } 


    }


    public function edit($data){

       try{

        $query= CompanyModel::where('id', '=', $data["id"])
            ->update(['name' => $data["name"],'address' => $data["address"],
                     'cpfcnpj' => $data["cpfcnpj"],'email' => $data["email"],'cep' => $data["cep"],
                     'neighborhood' => $data["neighborhood"],'number' => $data["number"],
                     'compl' => $data["compl"],'phone' => $data["phone"],
                     'fantasy_name' => $data["fantasy_name"],'state' => $data["state"],
                     'city' => $data["city"]

              ]); 

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            
            return $query;
      }
    }

   
    #list company(s)
    public function lists(){

        try{

          $query= CompanyModel::select('*')
          ->orderBy('company.name','asc')
          ->get();
          #->toSql();
         #echo "<pre>",print_r($query);exit;

          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

           return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => NULL,
              'success'   => FALSE
            );
            
            return $query;

        }        

    }

    #show company user 
    public function viewcompany($id){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('company')->select('*')->where('id','=', $id)->get();


           #echo "<pre>" ,print_r($query[0]);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;
        }


        
    }



    #show company user 
    public function usercompanybo(){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('company')->select('*')
           ->join('user_company','user_company.idcompany','=','company.id')
           ->join('branch_office','branch_office.idcompany','=','user_company.id')
           ->where('user_company.iduser','=', session('resp')['custom'][0]['iduser'])
           ->get();
           #->toSql();

           #echo "<pre>" ,print_r($query);exit;

            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => null,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

    #show company user 
    public function usercompany(){



        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('company')->select('company.name' , 'company.id')
           ->join('user_company','user_company.idcompany','=','company.id')
           ->where('user_company.iduser','=', session('resp')['custom'][0]['iduser'])
           ->get();
           #->toSql();

           #echo "<pre>" ,print_r($query);exit;

            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => null,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

    #alter idcompany user
    private function alterusercompany($id){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> UserModel::where('iduser', '=', session('iduser'))
        ->update(['idcompany' => $id]),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $errorInfo = array('exception'=> $exception->errorInfo);
            return $errorInfo;

      }

    }


    #alter idcompany user
    public function findCompany($cnpj){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> CompanyModel::where('cpfcnpj', '=', $cnpj)
        ->get(),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $errorInfo = array('exception'=> $exception->errorInfo);
            return $errorInfo;

      }

    }


    public function del($id){


        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('company')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }

}    