<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class TeamCategoryModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='team_category';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = TeamCategoryModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }


    public function edit($data){
      try{

          $query= TeamCategoryModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('team_category')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }


      
    }


    public function find($data){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('team_category')->select('*')->where('idcategory','=', $data["idcategory"])->where('idteam', '=', $data['idteam'])->get();


         #echo "<pre>" ,print_r($query);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => "",
            'success'   => FALSE
          );
          return $query;
      }


      
    }
   
    #list company(s)

    public function lists(){

      try{

        $query= TeamCategoryModel::select('*')
        ->orderBy('teamcategory.name','asc')
        ->get();
        #->toSql();
       #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

    #list team(s)

    public function listsTeamCategory($idteam){

      try{

        $query= TeamCategoryModel::select('team_category.id','team_category.idcategory','team_category.idteam','team.name','category.name as namecategory')
        ->join('category','category.id','=','team_category.idcategory')
        ->join('team','team.id','=','team_category.idteam')
        ->where('team_category.idteam' ,'=', $idteam )
        ->groupBy('category.name')
        ->orderBy('category.name','asc')
        ->get();
       # ->toSql();
      #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){

          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );

        #echo "<pre>",print_r($query);exit;
          
          return $query;

      }        

  }

  public function del($id){

    try{
        #select with join ELOQUENT profiles menus ...
        $query= DB::table('team_category')->where('id', '=', $id)
        ->delete();

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> $query,
        'success' => TRUE
        );

        return $query;

    }catch(\Illuminate\Database\QueryException $exception){
        $query=array(
          'exception' =>$exception->errorInfo,
          'query'     => $query,
          'success'   => FALSE
        );
        return $query;

    }


  }




}    