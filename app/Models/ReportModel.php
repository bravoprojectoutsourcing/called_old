<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ReportModel extends Model
{
	/**
	 * Documented variable
	 *
	 * @var string $table
	 */
	protected $table = [
		'branch_office',
		'ticket'
	];
	
	public function listFilterResults($arrData)
	{
		$input = $arrData;
		$phone = preg_replace('/[^\d]/', '', $arrData['phone']);
		$email = $arrData['email'];
		$arrSelectCompany = $input['arrSelectCompany'];
		$arrSelectBranchOffice = $input['arrSelectBranchOffice'];
		$arrSelectDepartament = $input['arrSelectDepartament'];
		$arrSelectCategory = $input['arrSelectCategory'];
		$arrSelectSeverity = $input['arrSelectSeverity'];
		$arrSelectPriority = $input['arrSelectPriority'];
		$arrSelectTypeCategory = $input['arrSelectTypeCategory'];
		$arrSelectTeam = $input['arrSelectTeam'];
		$selectResponse = $input['selectResponse'];
		$arrSelectStatus = $input['arrSelectStatus'];
		$keywords = $input['keywords'];
		
		$querySQL =
			"SELECT
				ticket.id AS ticket_id,
				ticket.created AS created_at,
				ticket.iduser AS user_id,
				user.name AS user_name,
				ticket.phone AS user_phone,
				ticket.email AS user_email,
				ticket.idcompany AS company_id, 
				UPPER(company.name) AS company_name,
				ticket.idbranchoffice AS branchoffice_id, 
				UPPER(IFNULL(branch_office.fantasy_name, 'SEM ESTABELECIMENTO')) AS branchoffice_name,
				ticket.iddepartament AS departament_id, 
				UPPER(departament.name) AS departament_name,
				ticket.idcategory AS category_id, 
				UPPER(category.name) AS category_name,
				ticket.idseverity AS severity_id,
				severity.name AS severity_name,
				ticket.idpriority AS priority_id, 
				CONCAT(severity.name, ' :: ', type.name, ' [', burden.name, ']') AS priority_name,
				ticket.idtypecategory AS typecategory_id,
				type_category.name AS typecategory_name,
				ticket.idteam AS team_id,
				team.name AS team_name,
				ticket.message AS ticket_short_message,
				ticket.description AS ticket_description, 
				IFNULL(ticket.createdresponse, ' ') AS ticket_created_response_at,
				ticket.status AS ticket_status
			FROM `ticket`
			INNER JOIN `user` ON `ticket`.`iduser` = `user`.`id`
			INNER JOIN `company` ON `ticket`.`idcompany` = `company`.`id`
			INNER JOIN `branch_office` ON `ticket`.`idbranchoffice` = `branch_office`.`id`
			INNER JOIN `departament` ON `ticket`.`iddepartament` = `departament`.`id`
			INNER JOIN `category` ON `ticket`.`idcategory` = `category`.`id`
			INNER JOIN `severity` ON `ticket`.`idseverity` = `severity`.`id`
			INNER JOIN `priority` ON `ticket`.`idpriority` = `priority`.`id`
			INNER JOIN `burden` ON `priority`.`idburden` = `burden`.`id`
			INNER JOIN `type` ON `priority`.`idtype` = `type`.`id`
			INNER JOIN `type_category` ON `ticket`.`idtypecategory` = `type_category`.`id`
			INNER JOIN `team` ON `ticket`.`idteam` = `team`.`id`
			WHERE `ticket`.`id` > 0
		";
		
		// Caso o usuário logado tiver o perfil de Gestor (idprofile = 6) e o mesmo não possuir nenhuma categoria cadastrada para o time em que o perfil
		// dele está cadastrado, o resultado deste _where condition_ vai ser _vazio_ ou _nulo_, logo, vai voltar uma query sem valores e consequentemente
		// a mensagem de crítica definida no ReportController.php
		if (session('resp')["custom"][0]["idprofile"] == 6) {
			$querySQL .= " AND `ticket`.idcategory IN (SELECT team_category.idcategory 
														FROM team_category 
														WHERE team_category.idteam IN (SELECT user_team.idteam 
																						FROM user_team 
																						WHERE user_team.iduser = ".session('resp')['custom'][0]['iduser']."))";
		}
		
		if (isset($phone) && $phone != "") {
			$querySQL .= " AND ticket.phone LIKE '%$phone%' ";
		}
		if (isset($email) && $email != "") {
			$querySQL .= " AND ticket.email LIKE '%$email%' ";
		}
		if (isset($arrSelectCompany)) {
			$querySQL .= " AND ticket.idcompany IN ($arrSelectCompany) ";
		}
		if (isset($arrSelectBranchOffice)) {
			$querySQL .= " AND ticket.idbranchoffice IN ($arrSelectBranchOffice) ";
		}
		if (isset($arrSelectDepartament)) {
			$querySQL .= " AND ticket.iddepartament IN ($arrSelectDepartament) ";
		}
		if (isset($arrSelectCategory)) {
			$querySQL .= " AND ticket.idcategory IN ($arrSelectCategory) ";
		}
		if (isset($arrSelectSeverity)) {
			$querySQL .= " AND ticket.idseverity IN ($arrSelectSeverity) ";
		}
		if (isset($arrSelectPriority)) {
			$querySQL .= " AND ticket.idpriority IN ($arrSelectPriority) ";
		}
		if (isset($arrSelectTypeCategory)) {
			$querySQL .= " AND ticket.idtypecategory IN ($arrSelectTypeCategory) ";
		}
		if (isset($arrSelectTeam)) {
			$querySQL .= " AND ticket.idteam IN ($arrSelectTeam) ";
		}
		if (isset($selectResponse)) {
			if ($selectResponse == "S") {
				$querySQL .= " AND ticket.createdresponse IS NOT NULL";
			} elseif ($selectResponse == "N") {
				$querySQL .= " AND ticket.createdresponse IS NULL";
			}
		}
		if (isset($arrSelectStatus)) {
			$querySQL .= " AND ticket.status IN ($arrSelectStatus) ";
		}
		if (isset($keywords)) {
			$arrKeywords = array_map('trim', explode(',', $keywords));
			$strKeywords = str_replace(',', '|', implode(',', $arrKeywords));
			$querySQL .= " AND LOWER(ticket.description) REGEXP LOWER('$strKeywords') OR lower(ticket.message) REGEXP LOWER('$strKeywords')";
		}
		
		$querySQL .= "
			ORDER BY ticket.id DESC
		";
		
		// dd($phone, $email, $arrSelectCompany, $arrSelectBranchOffice, $arrSelectDepartament, $arrSelectCategory, $arrSelectSeverity, $arrSelectPriority, $arrSelectTypeCategory, $arrSelectTeam, $selectResponse, $arrSelectStatus, $keywords, $methodType, $querySQL, $input);
		
		try {
			$query = DB::select($querySQL);
				
			$query = array(
				'exception' => null,
				'success'   => true,
				'query'     => $query,
				'count'     => count($query), #number reg
			);

			return $query;
		} catch (\Illuminate\Database\QueryException $exception) {
			$query=array(
			  'exception' =>$exception->errorInfo,
			  'query'     => null,
			  'success'   => false
			);
			return $query;
		}
	}
}
