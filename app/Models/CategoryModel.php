<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class CategoryModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='category';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = CategoryModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE,
              'lastid'    => DB::getPdo()->lastInsertId()
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }


    public function edit($data){
      try{

          $query= CategoryModel::where('id', '=', $data["id"])->update(['name' => $data["name"],'status' => $data["status"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('category')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

   
    #list company(s)

    public function lists(){

      try{

        $query= CategoryModel::select('*')
        ->orderBy('category.name','asc')
        ->get();
        #->toSql();
       #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }


    #list category team
    public function listscategoryteam(){

      try{

        #TASK 245 Andre 27/07/2020
        if(session('resp')['custom'][0]['nameprofile'] == "MANAGER"){
          
          $query = CategoryModel::select('category.name','category.id','category.status')
          ->join('team_category','team_category.idcategory','=','category.id')
          ->where('team_category.idteam' ,'=', session('resp')['custom'][0]['teamid'] )
          ->orderBy('category.name','asc')
          ->get();

        }else{
          
          $query = CategoryModel::select('*')
          ->orderBy('category.name','asc')
          ->get();
        }


        #->toSql();
       #echo "<pre>",print_r(session('resp')['custom'][0]['profile'] );exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

    #list company(s)

    public function listscategoryuser($iduser){

      try{

        $query= CategoryModel::select('category.name','category.id','category.status')
        ->join('user_category','user_category.idcategory','=','category.id')
        ->where('user_category.iduser' ,'=', $iduser )
        ->orderBy('category.name','asc')
        ->get();
        #->toSql();
      #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

  public function del($id){

    try{
        #select with join ELOQUENT profiles menus ...
        $query= DB::table('category')->where('id', '=', $id)
        ->delete();

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> $query,
        'success' => TRUE
        );

        return $query;

    }catch(\Illuminate\Database\QueryException $exception){
        $query=array(
          'exception' =>$exception->errorInfo,
          'query'     => $query,
          'success'   => FALSE
        );
        return $query;

    }


  }

    public function updateStatusCategory($data){
      try{

          $query= CategoryModel::where('id', '=', $data["id"])->update(['status' => $data["status"],'obs' => $data["obs"],'iduser' => session('iduser')]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }


}    