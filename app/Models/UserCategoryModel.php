<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class UserCategoryModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='user_category';    


    public function add($data){

      try{

          #echo "<pre>" ,print_r($data);exit;

         $query = UserCategoryModel::insert($data);
         $query = array(
              'exception' => null,
              'query'     => $query,
              'success'   => TRUE
          );
          return $query;

      }catch(\Illuminate\Database\QueryException $exception){
        
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );

          return $query;

       } 


    }


    public function edit($data){
      try{

          $query= UserCategoryModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]); 

          $query = array(
               'exception' => null,
               'query'     => $query,
               'success'   => TRUE
          );

           return $query;
      }catch(\Illuminate\Database\QueryException $exception){
          $query = array(
              'exception' => $exception->errorInfo,
              'query'     => "", 
              'success'   => FALSE
          );
          return $query;
      }
    }

    public function view($id){


      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('UserCategory')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }


      
    }

   
    #list company(s)

    public function lists(){

      try{

        $query= UserCategoryModel::select('*')
        ->orderBy('UserCategory.name','asc')
        ->get();
        #->toSql();
       #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }

    #list company(s)

    public function listsUserCategory($iduser){

      try{

        $query= UserCategoryModel::select('user_category.id','user_category.idcategory','user_category.iduser','user.name as username','category.name as namecategory')
        ->join('category','category.id','=','user_category.idcategory')
        ->join('user','user.id','=','user_category.iduser')
        ->where('user_category.iduser' ,'=', $iduser )
        ->groupBy('category.name')
        ->orderBy('category.name','asc')
        ->get();
       # ->toSql();
      #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){

          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );

        #echo "<pre>",print_r($query);exit;
          
          return $query;

      }        

  }

  public function del($id){

    try{
        #select with join ELOQUENT profiles menus ...
        $query= DB::table('user_category')->where('id', '=', $id)
        ->delete();

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> $query,
        'success' => TRUE
        );

        return $query;

    }catch(\Illuminate\Database\QueryException $exception){
        $query=array(
          'exception' =>$exception->errorInfo,
          'query'     => $query,
          'success'   => FALSE
        );
        return $query;

    }


  }




}    