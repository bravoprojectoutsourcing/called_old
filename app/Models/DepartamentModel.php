<?php

namespace App\Models;

# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class DepartamentModel extends Eloquent{
     
     /**
     * @var bool
     */
    public $timestamps = false;

    #name table
    protected $table='departament';

    public function lists(){

      try{

        $query= DepartamentModel::select('*')
        ->orderBy('departament.name','asc')
        ->get();
        #->toSql();
       #echo "<pre>",print_r($query);exit;

        $query=array(
          'exception' => null,
          'success'   => TRUE,
          'query'     => $query,
          'count'     => $query->count(), #number reg
        );

         return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => NULL,
            'success'   => FALSE
          );
          
          return $query;

      }        

  }    


    public function add($data){

        try{

            #echo "<pre>" ,print_r($data);exit;

           $query = DepartamentModel::insert($data);           


           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

 

            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
          
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            
            return $query;

         } 


    }


    public function edit($data){

       try{

           $query= DepartamentModel::where('id', '=', $data["id"])->update(['name' => $data["name"]]);  

           $query = array(
                'exception' => null,
                'query'     => $query,
                'success'   => TRUE
           );

            return $query;

      }catch(\Illuminate\Database\QueryException $exception){
                $query = array(
                   'exception' => $exception->errorInfo,
                   'query'     => "", 
                   'success'   => FALSE
                 );
            


            return $query;
      }
    }   

    public function view($id){

      try{

          #select with join ELOQUENT if company exist show blade general ...
         $query= DB::table('departament')->select('*')->where('id','=', $id)->get();


         #echo "<pre>" ,print_r($query[0]);exit;
          $query=array(
            'exception' => null,
            'success'   => TRUE,
            'query'     => $query,
            'count'     => $query->count(), #number reg
          );

          return $query;

       }catch(\Illuminate\Database\QueryException $exception){
          $query=array(
            'exception' =>$exception->errorInfo,
            'query'     => $query,
            'success'   => FALSE
          );
          return $query;
      }
      
    }

   #alter idcompany user
    public function find($id){


      try{

        #alter idcompany table user
        $query=array(
        'exception' =>null,
        'query'=> BranchOfficeModel::where('id', '=', $id)
        ->get(),
        'success' => TRUE
        );


        return $query;

      }catch(\Illuminate\Database\QueryException $exception){

            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => "",
              'success'   => FALSE
            );

            return $query;

      }

    }


    public function del($id){


        try{
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('departament')->where('id', '=', $id)
            ->delete();

            #alter idcompany table user
            $query=array(
            'exception' =>null,
            'query'=> $query,
            'success' => TRUE
            );

            return $query;

        }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' =>$exception->errorInfo,
              'query'     => $query,
              'success'   => FALSE
            );
            return $query;

        }
 

    }


    public function listuserdepartament($iduser){


        try{

            #select with join ELOQUENT if company exist show blade general ...
           $query= DB::table('departament')->select('departament.id','departament.name','departament.created')
           ->join('user_departament','user_departament.iddepartament','=','departament.id')
           ->where('user_departament.iduser', '=', $iduser)
           ->get();
           #->toSql();

           #echo "<pre>" ,print_r($query);exit;
            $query=array(
              'exception' => null,
              'success'   => TRUE,
              'query'     => $query,
              'count'     => $query->count(), #number reg
            );
         #   echo "<pre>" ,print_r($query);exit;
            return $query;

         }catch(\Illuminate\Database\QueryException $exception){
            $query=array(
              'exception' => $exception->errorInfo,
              'query'     => "" ,
              'success'   => FALSE
            );
            return $query;
        }


        
    }

}    