<?php

namespace App\Models;


# Autor: André Camargo
# Date : 1º semestre 2020

use Eloquent;
use DB;
use Hash;


class LoginModel extends Eloquent{
	 
	 /**
	 * @var bool
	 */
	public $timestamps = false;


	#name table
	protected $table='user';



		public function login($data)
	{
		try {
			#base64  
			$password=base64_encode($data["pass"]);

			$user = DB::table('user')
				->where('email', '=', $data["email"])
				->get();
	
			#session iduser
			session(['iduser' =>  $user[0]->id ]);
			session(['nameuser' =>  $user[0]->name ]);
			$pass = $user[0]->pass;

			#decode base64
			$pass     = base64_decode($pass);
			$password = base64_decode($password);

			if ($password == $pass)
				{
				#echo "<pre>" ,print_r($user);exit;

				#select with join ELOQUENT profiles menus ...
                    $query= DB::table('user')->select(
                    	'user.id AS iduser' ,
                    	'user.name',
                    	'user.pass',
                    	'user.phone',
                    	'user.email',
                    	'profile.profile',
                    	'profile.nameprofile',
                    	'profile.icon',
                    	'user.created',
                    	'user.inactive',
                    	'profile.id as idprofile',
                    	'team.name AS teamname',
                    	'team.id AS teamid')
                      ->join('profile','profile.id','=','user.idprofile')
                    ->join('user_team','user_team.iduser','=','user.id') 
                    ->join('team','team.id','=','user_team.idteam') 
                    ->where('user.email','=',$data["email"])
                    ->get();
			} else {
				echo "Senha ou user invalido";
				exit;
			}
			return $query;
		}
		catch(\App\Exceptions\NotFoundException $e)
		{

			return $e->getMessage();
		}
	}

	public function modules($id){
	   
		try{
			
			#select with join ELOQUENT profiles menus ...
			$query= DB::table('module_profile')->select('*')
			->join('module','module.id','=','module_profile.idmodule')
			->where('module_profile.idprofile','=', $id)
			->groupBy('module.name')
			->orderBy('module.order_module')
			->get();
			

			return $query;

		}catch(\App\Exceptions\NotFoundException $e){
			return $e->getMessage();

		}        
	}


	public function loginTeam(){

		try {

			 #select with join ELOQUENT profiles menus ...
			 $query=LoginModel::join('user_team','user_team.iduser','=', 'user.id')
			 ->join('team','team.id','=','user_team.idteam')
			 ->where('user.id','=', session('iduser'))
			 ->get();
				
			return $query;  
	 
		}
		catch(\App\Exceptions\NotFoundException $e)
		{

			return $e->getMessage();
		}

	}


	public function loginUserCategory(){

		try {

			 #select with join ELOQUENT profiles menus ...
			 $query=LoginModel::join('user_category','user_category.iduser','=', 'user.id')
			 ->join('category','category.id','=','user_category.idcategory')
			 ->where('user.id','=', session('iduser'))
			 ->get();
				
			return $query;  
	 
		}
		catch(\App\Exceptions\NotFoundException $e)
		{

			return $e->getMessage();
		}

	}

        public function submenus($id){
       
        try{
            
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('submenu')->select('*')
            ->where('idmodule','=', $id)
            ->get();
            
            return $query;

        }catch(\App\Exceptions\NotFoundException $e){
            return $e->getMessage();

        }        
    }

    public function submenu_category(){

        try{
            
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('submenu_category')->select('*')->get();
            return $query;

        }catch(\App\Exceptions\NotFoundException $e){
            return $e->getMessage();

        }



    }

    public function listsubcategory($data){

        try{
            
            #select with join ELOQUENT profiles menus ...
            $query= DB::table('submenu_category')->select('*')->where('idsubmenu','=', $data)->get();
            return $query;

        }catch(\App\Exceptions\NotFoundException $e){
            return $e->getMessage();

        }        

    }

}

