<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


#Reports
Route::group(
	[
		'middleware' => 'AdminManager'
	],
	function () {
		Route::get('reports', 'ReportController@filters')->name('reports.filters');
		Route::post('reports/save', 'ReportController@saveFilters')->name('reports.filters.save');
		Route::post('reports/listBranchOffices', 'ReportController@listBranchOffices')->name('reports.listBranchOffices');
		Route::post('reports/listPriorities', 'ReportController@listPriorities')->name('reports.listPriorities');
	}
);

#Dashboard
Route::get('dashboard', 'DashboardController@index');#call service instancy 
Route::get('dashboard/ticket',['middleware' => 'BasicRoute', 'uses'  =>'DashboardController@ticket' ]); #gadgets
Route::get('dashboard/graph' ,['middleware' => 'BasicRoute', 'uses'  =>'DashboardController@graph' ]); #gadgets
Route::post('dashboard/menuboxteam' ,['middleware' => 'BasicRoute', 'uses'  =>'DashboardController@menuBoxTeam' ]); #gadgets
Route::post('dashboard/listsla' ,['middleware' => 'BasicRoute', 'uses'  =>'DashboardController@indexAjaxGraph' ]); #gadgets
Route::post('/listsla' ,['middleware' => 'BasicRoute', 'uses'  =>'DashboardController@indexAjaxGraph' ]); #gadgets



#User
Route::post('user/editview',['middleware' => 'BasicRoute', 'uses'  =>'UserController@editview' ]); 

#company
Route::get('company',['middleware' => 'MediumRoute', 'uses'  =>'CompanyController@index' ]);
Route::post('company/add' ,['middleware' => 'MediumRoute', 'uses'  =>'CompanyController@add' ]);
Route::post('company/edit',['middleware' => 'MediumRoute', 'uses'  =>'CompanyController@edit' ]); 
Route::post('company/del',['middleware' => 'MediumRoute', 'uses'  =>'CompanyController@del' ]); 
Route::get('company/view',['middleware' => 'MediumRoute', 'uses'  =>'CompanyController@viewcompany' ]);

#branchoffice
Route::post('branchoffice/add' ,['middleware' => 'AdminRoute', 'uses'  =>'BranchOfficeController@add' ]);
Route::post('branchoffice/edit',['middleware' => 'AdminRoute', 'uses'  =>'BranchOfficeController@edit' ]); 
Route::post('branchoffice/del',['middleware'  => 'AdminRoute', 'uses'  =>'BranchOfficeController@del' ]); 
Route::get('branchoffice/view',['middleware'  => 'AdminRoute', 'uses'  =>'BranchOfficeController@view' ]); 
Route::post('branchoffice/listbranchofficecompany','BranchOfficeController@listbranchofficecompany' );

#teams
Route::get('teams',['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@index' ]);
Route::post('teams/edit',['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@edit' ]);
Route::post('teams/del',['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@del' ]); 
Route::post('teams/add' ,['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@add' ]);
Route::get('teams/userteam' ,['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@userteam' ]);
Route::post('teams/adduserteam' ,['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@adduserteam' ]);
Route::post('teams/deluserteam' ,['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@deluserteam' ]);
Route::post('teams/teamCategory' ,['middleware' => 'MediumRoute', 'uses'  =>'TeamsController@teamCategory' ]);


#categories BY RENAN
Route::get('categories',['middleware' => 'AdminRoute', 'uses'  =>'CategoryController@index' ]);
Route::post('categories/edit',['middleware' => 'AdminRoute', 'uses'  =>'CategoryController@edit' ]);
Route::post('categories/del',['middleware' => 'AdminRoute', 'uses'  =>'CategoryController@del' ]);
Route::post('categories/add' ,['middleware' => 'AdminRoute', 'uses'  =>'CategoryController@add' ]); 
Route::get('categories/view',['middleware'  => 'AdminRoute', 'uses'  =>'CategoryController@view' ]); 
Route::get('categories/updatestatuscategory' ,['middleware' => 'MediumRoute', 'uses'  =>'CategoryController@UpdateStatusCategory' ]); #created André 03/08/2020 - task 212



#usercategory
Route::get('usercategory',['middleware' => 'AdminRoute', 'uses'  =>'UserCategoryController@index' ]);
Route::post('usercategory/edit',['middleware' => 'AdminRoute', 'uses'  =>'UserCategoryController@edit' ]);
Route::post('usercategory/del',['middleware' => 'AdminRoute', 'uses'  =>'UserCategoryController@del' ]);
Route::post('usercategory/add' ,['middleware' => 'AdminRoute', 'uses'  =>'UserCategoryController@add' ]); 
Route::get('usercategory/view',['middleware'  => 'AdminRoute', 'uses'  =>'UserCategoryController@view' ]); 


#teamcategory - task 283
Route::get('teamcategory',['middleware' => 'AdminRoute', 'uses'  =>'TeamCategoryController@index' ]);
Route::post('teamcategory/edit',['middleware' => 'AdminRoute', 'uses'  =>'TeamCategoryController@edit' ]);
Route::post('teamcategory/del',['middleware' => 'AdminRoute', 'uses'  =>'TeamCategoryController@del' ]);
Route::post('teamcategory/add' ,['middleware' => 'AdminRoute', 'uses'  =>'TeamCategoryController@add' ]); 
Route::get('teamcategory/view',['middleware'  => 'AdminRoute', 'uses'  =>'TeamCategoryController@view' ]); 
Route::post('teamcategory/find' ,['middleware' => 'BasicRoute', 'uses'  =>'TeamCategoryController@find' ]);

#userdepartament
Route::get('userdepartament',['middleware' => 'AdminRoute', 'uses'  =>'UserDepartamentController@index' ]);
Route::post('userdepartament/edit',['middleware' => 'AdminRoute', 'uses'  =>'UserDepartamentController@edit' ]);
Route::post('userdepartament/del',['middleware' => 'AdminRoute', 'uses'  =>'UserDepartamentController@del' ]);
Route::post('userdepartament/add' ,['middleware' => 'AdminRoute', 'uses'  =>'UserDepartamentController@add' ]); 
Route::get('userdepartament/view',['middleware'  => 'AdminRoute', 'uses'  =>'UserDepartamentController@view' ]); 

#SLA
Route::get('sla',['middleware' => 'MediumRoute', 'uses'  =>'SLAController@index' ]);
Route::post('sla/edit',['middleware' => 'AdminRoute', 'uses'  =>'SLAController@edit' ]);
Route::post('sla/del',['middleware' => 'MediumRoute', 'uses'  =>'SLAController@del' ]);
Route::post('sla/add' ,['middleware' => 'MediumRoute', 'uses'  =>'SLAController@add' ]); 
Route::get('sla/view',['middleware'  => 'AdminRoute', 'uses'  =>'SLAController@view' ]); 


#ticket
Route::get('ticket', ['middleware' => 'BasicRoute', 'uses'   =>'TicketController@index']);#call service instancy
Route::post('ticket/add' ,['middleware' => 'BasicRoute', 'uses'   =>'TicketController@add'] );
Route::post('ticket/edit',['middleware' => 'BasicRoute', 'uses'   =>'TicketController@edit'] ); 
Route::post('ticket/del',['middleware' => 'BasicRoute', 'uses'   =>'TicketController@del'] ); 
Route::post('ticket/updateStatus',['middleware' => 'BasicRoute', 'uses'   =>'TicketController@updateStatus'] ); 
Route::post( 'ticket/view',['middleware' => 'BasicRoute', 'uses'   =>'TicketController@view'] );
Route::get( 'ticket/liststicket',['middleware' => 'BasicRoute', 'uses'   =>'TicketController@liststicket'] );
Route::post('ticket/addSessionTicket' ,['middleware' => 'BasicRoute', 'uses'  =>'TicketController@addSessionTicket' ]); #gadgets
Route::post('ticket/addSessionTicketGraphPier' ,['middleware' => 'BasicRoute', 'uses'  =>'TicketController@addSessionTicketGraphPier' ]); #gadgets
Route::post('ticket/getsla', ['middleware' => 'BasicRoute', 'uses'   =>'TicketController@getSLA']);#call service instancy
#-------------somente utilizar pra testar o cron deixar comentado ----------------
#Route::get( 'ticket/cronstatusticket','TicketController@cronStatusTicket' );


#upload files
Route::post('uploadfiles/uploadfile','UploadFilesController@uploadfile' ); 
Route::post('uploadfiles/add','UploadFilesController@add' ); 

#ticket response
Route::get('ticketresponse', ['middleware' => 'BasicRoute', 'uses'   =>'TicketResponseController@index']);#call service instancy
Route::post('ticketresponse/add' ,['middleware' => 'BasicRoute', 'uses'   =>'TicketResponseController@add'] );
Route::post('ticketresponse/edit',['middleware' => 'BasicRoute', 'uses'   =>'TicketResponseController@edit'] ); 
Route::post('ticketresponse/del',['middleware' => 'BasicRoute', 'uses'   =>'TicketResponseController@del'] ); 
Route::get( 'ticketresponse/view',['middleware' => 'BasicRoute', 'uses'   =>'TicketResponseController@view'] );


Route::get('listcity','CompanyController@listCity');#call service instancy
Route::get('cityName','CompanyController@cityName');#call service instancy
Route::get('listcitystate','CompanyController@listCityState');#call service instancy



#priority
Route::get('priority',['middleware' => 'AdminRoute', 'uses'  =>'PriorityController@index' ]);
Route::post('priority/add' ,['middleware' => 'AdminRoute', 'uses'  =>'PriorityController@add' ]);
Route::post('priority/edit',['middleware' => 'AdminRoute', 'uses'  =>'PriorityController@edit' ]); 
Route::post('priority/del',['middleware' => 'AdminRoute', 'uses'   =>'PriorityController@del' ]);
Route::post('priority/listpriority',['middleware' => 'AdminRoute', 'uses'   =>'PriorityController@listpriority' ]); 
Route::get(' priority/view',['middleware' => 'AdminRoute', 'uses'   =>'PriorityController@view' ]);
Route::post(' priority/listpriorityseverity','PriorityController@listpriorityseverity' );

#departaments
Route::get('departaments',['middleware' => 'AdminRoute', 'uses'  =>'DepartamentController@index' ]);
Route::post('departaments/add' ,['middleware' => 'AdminRoute', 'uses'  =>'DepartamentController@add' ]);
Route::get('departaments/view',['middleware'  => 'AdminRoute', 'uses'  =>'DepartamentController@view' ]);  
Route::post('departaments/edit',['middleware' => 'AdminRoute', 'uses'  =>'DepartamentController@edit' ]);
Route::post('departaments/del',['middleware' => 'AdminRoute', 'uses'  =>'DepartamentController@del' ]);

#not loggin redirect task 251 - André 28/08/2020
Route::get('/','LoginController@index' ); 
Route::get('/local','LoginController@index' ); 
Route::post('/','LoginController@login' ); 
Route::post('/local','LoginController@index' ); 

